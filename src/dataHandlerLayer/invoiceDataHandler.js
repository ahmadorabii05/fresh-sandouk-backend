const {prepareErrorLog} = require("../helperKit/loggingKit");
const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("./common/LanguageHeaderHandler");
const {isAdminRole} = require("../services/userService");
const UserQueries = require("../dataBaseLayer/queries/user/userQueries")
const OrderQueries = require("../dataBaseLayer/queries/order/OrderQueries")
const {calculateTotalAmount} = require("../helperKit/helperKit");

const getAllInvoiceReportDataHandler = async (req, res, next) => {
    try {
        const userId = req.userFromToken._id
        const getUserById = await UserQueries.getUserInfoById(userId);
        if (isAdminRole(getUserById)) {
            const getAllOrdersForAdmin = await OrderQueries.getAllOrdersForAdminPaginated(req);
            req.responseModel = await calculateTotalAmount(getAllOrdersForAdmin);
        } else {
            const getAllOrdersByUser = await OrderQueries.getAllOrdersByUserPaginated(userId,req);
            req.responseModel = await calculateTotalAmount(getAllOrdersByUser);

        }
        next();

    } catch (error) {
        prepareErrorLog(error, getAllInvoiceReportDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

module.exports = {
    getAllInvoiceReportDataHandler
}
