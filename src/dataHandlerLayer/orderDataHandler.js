const OrderQueries = require("../dataBaseLayer/queries/order/OrderQueries");
const {isEmpty} = require("lodash");
const CartQueries = require("../dataBaseLayer/queries/cart/cartQueries")
const AddressQueries = require("../dataBaseLayer/queries/address/addressQueries")
const UserQueries = require("../dataBaseLayer/queries/user/userQueries")
const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("./common/LanguageHeaderHandler");
const {prepareErrorLog} = require("../helperKit/loggingKit");
const StockQueries = require("../dataBaseLayer/queries/stock/stockQueries")
const {
    checkIfThereIsAnyProductOutOfStock, returnAllOutOfStockProduct,
    updateStockToAddQuantitySold, generateKeyOrder, calculateTotalAmountInOrderById
} = require("../helperKit/helperKit");
const mongoose = require("mongoose");
const moment = require('moment');
const {getTimeSlotPresenter} = require("../presentationLayer/invoicePresenter");
const {getTimeSlots} = require("../helperKit/timeSlotsKit");

const createOrderDataHandler = async (req, res, next) => {
    try {
        const userId = req.userFromToken._id;
        const getUserCart = await CartQueries.getUserCartWithoutPopulate(userId);
        if (!isEmpty(getUserCart)) {
            // Check if there is any product out of stock
            const getAllOutOfStockProduct = await checkIfThereIsAnyProductOutOfStock(getUserCart?.productIds);
            if (getAllOutOfStockProduct.outOfStock === false) {

                const getAddress = await AddressQueries.getAddressByUser(userId);
                if (!isEmpty(getAddress)) {
                    const getIfThereIsDefaultAddress = await AddressQueries.getIfIsDefaultExist(userId);
                    if (!isEmpty(getIfThereIsDefaultAddress)) {
                        req.requestModel.addressId = getIfThereIsDefaultAddress._id;
                    } else {
                        return res
                            .status(ErrorStatus.YOU_SHOULD_CREATE_ADDRESS_FIRST.code)
                            .json(prepareErrorResponse(ErrorStatus.YOU_SHOULD_CREATE_ADDRESS_FIRST, null, extractLanguageFromRequest(req)));
                    }
                } else {
                    return res
                        .status(ErrorStatus.ADDRESS_DOES_NOT_EXIST.code)
                        .json(prepareErrorResponse(ErrorStatus.ADDRESS_DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
                }

                const getAllProductFromCart = getUserCart.productIds;
                await Promise.all(getAllProductFromCart.map(async (item) => {
                    const getStock = await StockQueries.getStockByProductId(item.productId);
                    const getQuantity = getStock?.quantity ?? 0;
                    if (item?.quantity > getQuantity?.quantity ?? 0) {
                        return res
                            .status(ErrorStatus.ITEM_QUANTITY_IS_LESS_THAN_QUANTITY_SELECTED.code)
                            .json(prepareErrorResponse(ErrorStatus.ITEM_QUANTITY_IS_LESS_THAN_QUANTITY_SELECTED, null, extractLanguageFromRequest(req)));
                    }
                }));

                req.requestModel.products = getUserCart.productIds;
                req.requestModel.userId = userId;
            } else {
                const outOfStockProducts = await returnAllOutOfStockProduct(getAllOutOfStockProduct.outOfStockProductIds);
                req.responseModel = {
                    outOfStock: getAllOutOfStockProduct.outOfStock,
                    outOfStockProducts
                };
                next();
                return; // Return here to prevent executing the remaining code in the try block
            }
            const getTotalPriceFromCart = await CartQueries.findUserCart(userId);
            req.requestModel.keyOrder = await generateKeyOrder();
            const createOrder = await OrderQueries.createOrder(req.requestModel);
            // Remove from stock and add to quantity sold
            await updateStockToAddQuantitySold(getUserCart.productIds);
            await CartQueries.removeAllProductAfterOrder(userId);
            await UserQueries.deductPriceFromWallet(userId, getTotalPriceFromCart[0].totalPrice)
            req.responseModel = await OrderQueries.getOrderById(createOrder._id);
            next();
        } else {
            return res
                .status(ErrorStatus.CART_NOT_FOUND.code)
                .json(prepareErrorResponse(ErrorStatus.CART_NOT_FOUND, null, extractLanguageFromRequest(req)));
        }
    } catch (error) {
        prepareErrorLog(error, createOrderDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const getOrderByIdDataHandler = async (req, res, next) => {
    try {
        const {id} = req.params;
        const getOrderById = await OrderQueries.getOrderById(id);
        if (isEmpty(getOrderById)) {
            return res
                .status(ErrorStatus.ORDER_DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.ORDER_DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }
        req.responseModel = await calculateTotalAmountInOrderById(getOrderById);
        next();
    } catch (error) {
        prepareErrorLog(error, getOrderByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const getOrdersBySearchPaginatedDataHandler = async (req, res, next) => {
    try {
        const list = await OrderQueries.getAllOrdersBySearch(req);
        const userId = req.userFromToken?._id;
        const getUser = await UserQueries.getUserInfoById(userId);
        if (!userId) {
            req.responseModel = list;
            next();
        } else {
            req.responseModel = list;
            req.responseModel.admin = getUser;
            next();
        }

    } catch (error) {
        prepareErrorLog(error, getOrdersBySearchPaginatedDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const editOrderQuantityFieldDataHandler = async (req, res, next) => {
    try {
        if (req.body.orderId && req.body.productId && req.body.newQuantity) {
            const orderId = req.body.orderId;
            const productId = req.body.productId;
            const newQuantity = req.body.newQuantity;
            let productIdObjectId = mongoose.Types.ObjectId(productId)

            // get stock for product
            const stock = await StockQueries.getStockByProductId(productId);
            // get order 
            const order = await OrderQueries.getOrderById(orderId);
            const product = order.products.find((prod) => { return prod.productId._id.equals(productIdObjectId) });
            const currentOrderQuantity = product.quantity;
            const newStockQuantity = (stock.quantity + currentOrderQuantity - newQuantity);
            const newStockQuantitySold = (stock.quantitySold - currentOrderQuantity + newQuantity);
            await StockQueries.updateProductQuantityByIdFixed(productId, newStockQuantity, newStockQuantitySold);
            await OrderQueries.updateOrderQuantity(orderId, productId, newQuantity);

            next();
        } else {
            // fix msg
            return res
                .status(ErrorStatus.SERVER_DOWN.code)
                .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
        }

    } catch (error) {
        prepareErrorLog(error, editOrderQuantityFieldDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }

}

const editOrderStatusDataHandler = async (req, res, next) => {
    try {
        if (req.body.orderId && req.body.newStatus) {
            const orderId = req.body.orderId;
            const newStatus = req.body.newStatus;

            await OrderQueries.updateStatusOrderFromAdmin(orderId, newStatus);

            next();
        } else {
            return res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(prepareErrorResponse(ErrorStatus.BAD_REQUEST, null, extractLanguageFromRequest(req)));
        }
    } catch (error) {
        prepareErrorLog(error, editOrderStatusDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const getTimeSlotsDataHandler = async (req, res, next) => {
    try {
        if (req.query.timeStamp && req.query.city) {
            const timeStamp = req.query.timeStamp;
            const city = req.query.city;
            const numberOfOptions = req.query.numberOfOptions;
            const result = getTimeSlots(city, timeStamp, numberOfOptions);
            req.responseModel = result;
            next();
        } else {
            return res
                .status(ErrorStatus.SERVER_DOWN.code)
                .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
        }

    } catch (error) {
        prepareErrorLog(error, getTimeSlotPresenter.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

module.exports = {
    createOrderDataHandler,
    getOrderByIdDataHandler,
    getOrdersBySearchPaginatedDataHandler,
    editOrderQuantityFieldDataHandler,
    getTimeSlotsDataHandler,
    editOrderStatusDataHandler,
}
