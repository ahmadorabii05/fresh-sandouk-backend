const { isEmpty } = require("lodash");
const RoleQueries = require("../dataBaseLayer/queries/role/roleQueries");
const { ErrorStatus } = require("../constants/error/errorStatus");
const { prepareErrorResponse } = require("../errorHandlerLayer/errorHandler");
const { prepareErrorLog } = require("../helperKit/loggingKit");
const {extractLanguageFromRequest} = require("./common/LanguageHeaderHandler");
const {isAdminRole} = require("../services/userService");

/**
 * CreateRoleDataHandler prepares the role data needed from the database
 */
const createRoleDataHandler = async (req, res, next) => {
    try {
        const requestModel = req.requestModel;
        const roleCheck = await RoleQueries.getRoleByName(requestModel.name);
            if (!isEmpty(roleCheck)) {
                return res
                    .status(ErrorStatus.ROLE_EXIST.code)
                    .json(prepareErrorResponse(ErrorStatus.ROLE_EXIST, null, extractLanguageFromRequest(req)));
            } else {
                const role = await RoleQueries.createRole(requestModel);
                await role.save();
                req.role = role;
                next();
            }

    } catch (error) {
        prepareErrorLog(error, createRoleDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

/**
 * GetRoleDataHandler prepares the role data needed from the database
 */
const getRoleDataHandler = async (req, res, next) => {
    try {
        const role = await RoleQueries.getRoleById(req.params.id);
        if (isEmpty(role)) {
            return res
                .status(ErrorStatus.NOT_FOUND.code)
                .json(prepareErrorResponse(ErrorStatus.NOT_FOUND, null, extractLanguageFromRequest(req)));
        } else {
            req.responseModel = role;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, getRoleDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

/**
 * GetAllRolesDataHandler prepares the role data needed from the database
 */
const getAllRolesDataHandler = async (req, res, next) => {
    try {
        const roles = await RoleQueries.getRoles();
        if (isEmpty(roles)) {
            return res
                .status(ErrorStatus.NOT_FOUND.code)
                .json(prepareErrorResponse(ErrorStatus.NOT_FOUND, null, extractLanguageFromRequest(req)));
        } else {
            req.responseModel = roles;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, getAllRolesDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

/**
 * UpdateRoleDataHandler prepares the role data needed from the database
 */
const updateRoleDataHandler = async (req, res, next) => {
    try {
        const requestModel = req.requestModel;
        const roleCheck = await RoleQueries.getRoleByName(requestModel.name);
        if (!isEmpty(roleCheck) && ((roleCheck._id).valueOf() !== req.params.id)) {
            return res
                .status(ErrorStatus.ROLE_DOESNT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.ROLE_DOESNT_EXIST, null, extractLanguageFromRequest(req)));
        } else {
            await RoleQueries.updateRoleById(req.params.id, requestModel);
            next();
        }
    } catch (error) {
        prepareErrorLog(error, updateRoleDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

/**
 * DeleteRoleByIdDataHandler deletes the role data from the database
 */
const deleteRoleByIdDataHandler = async (req, res, next) => {
    try {
        const result = await RoleQueries.deleteRoleById(req.params.id);
        if (result?.deletedCount === 0) {
            return res
                .status(ErrorStatus.ROLE_DOESNT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.ROLE_DOESNT_EXIST, null, extractLanguageFromRequest(req)));
        } else {
            next();
        }
    } catch (error) {
        prepareErrorLog(error, deleteRoleByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

module.exports = {
    createRoleDataHandler,
    getRoleDataHandler,
    updateRoleDataHandler,
    getAllRolesDataHandler,
    deleteRoleByIdDataHandler,
};
