const {prepareErrorLog} = require("../helperKit/loggingKit");
const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("./common/LanguageHeaderHandler");
const StockQueries = require("../dataBaseLayer/queries/stock/stockQueries")
const OrderQueries = require("../dataBaseLayer/queries/order/OrderQueries")
const UserQueries = require("../dataBaseLayer/queries/user/userQueries")
const {isEmpty} = require("lodash");

const createProductInStockDataHandler = async (req, res, next) => {
    try {
        const checkIfProductAlreadyExist = await StockQueries.checkIfProductIdExist(req.requestModel.productId);
        if (!isEmpty(checkIfProductAlreadyExist)) {
            return res
                .status(ErrorStatus.PRODUCT_IN_STOCK_ALREADY_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.PRODUCT_IN_STOCK_ALREADY_EXIST, null, extractLanguageFromRequest(req)));
        }

        req.responseModel = await StockQueries.createProductInStock(req.requestModel);
        next()
    } catch (error) {
        prepareErrorLog(error, createProductInStockDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}


const updateProductInStockDataHandler = async (req, res, next) => {
    try {
        const checkIfProductAlreadyExist = await StockQueries.getStockByProductId(req.params.id);
        if (isEmpty(checkIfProductAlreadyExist)) {
            return res
                .status(ErrorStatus.PRODUCT_IN_STOCK_DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.PRODUCT_IN_STOCK_DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }

        req.responseModel = await StockQueries.updateProductQuantityById(req.params.id,req.requestModel.quantity);
        next()
    } catch (error) {
        prepareErrorLog(error, updateProductInStockDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}
const getProductInStockByIdDataHandler = async (req, res, next) => {
    try {
        const checkIfProductInStockExist = await StockQueries.getStockById(req.params.id);
        if (isEmpty(checkIfProductInStockExist)) {
            return res
                .status(ErrorStatus.PRODUCT_IN_STOCK_DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.PRODUCT_IN_STOCK_DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }
        req.responseModel = checkIfProductInStockExist;
        next();
    } catch (error) {
        prepareErrorLog(error, getProductInStockByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const updateStockDataHandler = async (idFromStripe,price)=>{
    try {
   const getOrder = await OrderQueries.getOrderByCreateIdFromStripe(idFromStripe);
   const productsFromOrder = getOrder?.products;
    for( const productsFromOrderData of productsFromOrder){
        await StockQueries.updateProductFromStripe(productsFromOrderData.productId,productsFromOrderData.quantity);
    }
     await UserQueries.addPriceFromWallet(getOrder.userId,price/100);
    await OrderQueries.updateStatusOrder(idFromStripe);
    }catch (error) {
        prepareErrorLog(error, updateStockDataHandler.name);
    }
}


module.exports = {
    createProductInStockDataHandler,
    getProductInStockByIdDataHandler,
    updateProductInStockDataHandler,
    updateStockDataHandler
}