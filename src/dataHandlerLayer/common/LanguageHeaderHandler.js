const extractLanguageFromRequest = (req) => {
    return req.header("accept-language");
};

module.exports = {
    extractLanguageFromRequest,
};
