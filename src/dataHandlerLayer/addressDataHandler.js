const {prepareErrorLog} = require("../helperKit/loggingKit");
const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("./common/LanguageHeaderHandler");
const AddressQueries = require("../dataBaseLayer/queries/address/addressQueries")
const {isEmpty} = require("lodash");
const {isValidNumber} = require("libphonenumber-js");

const getAllAddressDataHandler = async (req, res, next) => {
    try {
        req.responseModel = await AddressQueries.getAllAddressPaginated(req,req.userFromToken._id);
        next();

    } catch (error) {
        prepareErrorLog(error, getAllAddressDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const createAddressDataHandler = async (req, res, next) => {
    try {
        const isValid = isValidNumber(req.requestModel.contactNumber);
        let savedAddress;
        if(isValid){
            req.requestModel.userId =req.userFromToken._id;
            if(req.requestModel.isDefaultAddress === true ){
                //update all addresses default to false
                await AddressQueries.updateAllAddressToDefaultAddressToFalse(req.userFromToken._id);
                //create new address
                savedAddress = await AddressQueries.createAddress(req.requestModel);
            }else{
                savedAddress = await AddressQueries.createAddress(req.requestModel);
                const defaultAddressExists = await AddressQueries.getIfIsDefaultExist(req.userFromToken._id);
                if (!defaultAddressExists) {
                    // If no default address exists, make the latest record isDefault true
                    const latestAddress = await AddressQueries.getLatestRecord(req.userFromToken._id);
                    if (latestAddress) {
                        latestAddress.isDefaultAddress = true;
                        await AddressQueries.updateAddressById(latestAddress._id,latestAddress);
                    }
                }
            }
            req.responseModel = savedAddress;
            next()
        }else{
            return res
                .status(ErrorStatus.SERVER_DOWN.code)
                .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, "phone Number is Invalid", extractLanguageFromRequest(req)));
        }


    } catch (error) {
        prepareErrorLog(error, createAddressDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const getAddressByIdDataHandler = async (req, res, next) => {
    try {
        const checkIfAddressExist = await AddressQueries.getAddressById(req.params.id);
        if (isEmpty(checkIfAddressExist)) {
            return res
                .status(ErrorStatus.ADDRESS_DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.ADDRESS_DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }
        req.responseModel = checkIfAddressExist;
        next();
    } catch (error) {
        prepareErrorLog(error, getAddressByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}


const updateAddressByIdDataHandler = async (req, res, next) => {
    try {
        let updatedAddress;
        const checkIfAddressExist = await AddressQueries.getAddressById(req.params.id);
        if (isEmpty(checkIfAddressExist)) {
            return res
                .status(ErrorStatus.ADDRESS_DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.ADDRESS_DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }
        if(req.requestModel.isDefaultAddress === true){
            await AddressQueries.updateAllAddressToDefaultAddressToFalse(req.userFromToken._id);
            updatedAddress = await AddressQueries.updateAddressById(req.params.id, req.requestModel);
        }else{
             updatedAddress = await AddressQueries.updateAddressById(req.params.id, req.requestModel);
            const defaultAddressExists = await AddressQueries.getIfIsDefaultExist(req.userFromToken._id);
            if (!defaultAddressExists) {
                // If no default address exists, make the latest record isDefault true
                const latestAddress = await AddressQueries.getLatestRecord(req.userFromToken._id);
                if (latestAddress) {
                    latestAddress.isDefaultAddress = true;
                    await AddressQueries.updateAddressById(latestAddress._id,latestAddress);
                }
            }
        }

        req.responseModel = updatedAddress;
        next();
    } catch (error) {
        prepareErrorLog(error, updateAddressByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const deleteAddressDataHandler = async (req, res, next) => {
    try {
        const addressIds = req.requestModel.addressIds;
        const deletePromises = addressIds.map(async (id) => await AddressQueries.deleteAddressById(id));
        const deleteResults = await Promise.all(deletePromises);

        // Check if any IDs did not exist
        const notFoundIds = [];
        deleteResults.forEach((result, index) => {
            if (result === null) {
                notFoundIds.push(addressIds[index]);
            }
        });

        if (notFoundIds.length > 0) {
            // Some IDs were not found
            return res
                .status(ErrorStatus.SERVER_DOWN.code)
                .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, "The following address IDs do not exist: " + notFoundIds.join(", "), extractLanguageFromRequest(req)));
        }
        const defaultAddressExists = await AddressQueries.getIfIsDefaultExist(req.userFromToken._id);
        if (!defaultAddressExists) {
            // If no default address exists, make the latest record isDefault true
            const latestAddress = await AddressQueries.getLatestRecord(req.userFromToken._id);
            if (latestAddress) {
                latestAddress.isDefaultAddress = true;
                await AddressQueries.updateAddressById(latestAddress._id,latestAddress);
            }
        }
        next();
    } catch (error) {
        prepareErrorLog(error, deleteAddressDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}
module.exports = {
    getAllAddressDataHandler,
    createAddressDataHandler,
    getAddressByIdDataHandler,
    updateAddressByIdDataHandler,
    deleteAddressDataHandler
}