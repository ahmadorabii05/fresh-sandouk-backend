const { prepareErrorLog } = require("../helperKit/loggingKit");
const { ErrorStatus } = require("../constants/error/errorStatus");
const { prepareErrorResponse } = require("../errorHandlerLayer/errorHandler");
const { extractLanguageFromRequest } = require("./common/LanguageHeaderHandler");
const BrandQueries = require("../dataBaseLayer/queries/brand/brandQueries")
const UserQueries = require("../dataBaseLayer/queries/user/userQueries")
const { isEmpty } = require("lodash");
const { getFileStorePathForFile } = require("../helperKit/helperKit");
const { uploadFile } = require("../helperKit/uploadFile");
const path = require("path");
const { deleteFileInAWS } = require("../helperKit/deleteFileFromAWSKit");

const getAllBrandsDataHandler = async (req, res, next) => {
    try {
        req.responseModel = await BrandQueries.getAllBrandPaginated(req);
        next();

    } catch (error) {
        prepareErrorLog(error, getAllBrandsDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const createBrandDataHandler = async (req, res, next) => {
    try {
        if (req.file) {
            if (getFileStorePathForFile(req.file.mimetype.split("/")[1])) {
                const brand = await BrandQueries.createBrand(req.requestModel);
                const createBrand = await brand.save();
                const uploadedFile = await uploadFile(res, req.file, createBrand._id, req.baseUrl);
                req.responseModel = await BrandQueries.updateBrandImageUrl(createBrand._id, uploadedFile);
                next()
            } else {
                return res
                    .status(ErrorStatus.ONLY_IMAGES.code)
                    .json(prepareErrorResponse(ErrorStatus.ONLY_IMAGES, null, extractLanguageFromRequest(req)));
            }
        } else {
            return res
                .status(ErrorStatus.FILE_MISSING.code)
                .json(prepareErrorResponse(ErrorStatus.FILE_MISSING, null, extractLanguageFromRequest(req)));
        }

    } catch (error) {
        prepareErrorLog(error, createBrandDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}


const createBulkBrandDataHandler = async (req, res, next) => {
    try
    {
        const brands = req.requestModel.brands;
        for (const brandsData of brands) {

            brandsData.source = brandsData.source.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()).join(' ');
            if (isEmpty(brandsData?.sourceAr)) {
                brandsData.sourceAr = "";
            }
            brandsData.imageUrl = "";
            await BrandQueries.createBrand(brandsData);
        }
        next()

    } catch (error) {
        prepareErrorLog(error, createBulkBrandDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}
const getBrandByIdDataHandler = async (req, res, next) => {
    try {
        const checkIfBrandExist = await BrandQueries.getBrandById(req.params.id);
        if (isEmpty(checkIfBrandExist)) {
            return res
                .status(ErrorStatus.BRAND_DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.BRAND_DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }
        req.responseModel = checkIfBrandExist;
        next();
    } catch (error) {
        prepareErrorLog(error, getBrandByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}


const updateBrandByIdDataHandler = async (req, res, next) => {
    try {
        const checkIfBrandExist = await BrandQueries.getBrandById(req.params.id);
        if (isEmpty(checkIfBrandExist)) {
            return res
                .status(ErrorStatus.BRAND_DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.BRAND_DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }
        if (req.requestModel?.sourceAr) {
            const checkIfSourceAlreadyExistInArabic = await BrandQueries.getBrandBySourceAr(req.requestModel?.sourceAr)
            if (!isEmpty(checkIfSourceAlreadyExistInArabic)) {
                if (req.params.id.valueOf() === checkIfSourceAlreadyExistInArabic._id.valueOf()) {
                    delete req.requestModel.sourceAr
                } else {
                    return res
                        .status(ErrorStatus.BRAND_ALREADY_EXIST_IN_ARABIC.code)
                        .json(prepareErrorResponse(ErrorStatus.BRAND_ALREADY_EXIST_IN_ARABIC, null, extractLanguageFromRequest(req)));
                }
            }
        }
        if (req.file) {
            if (getFileStorePathForFile(req.file.mimetype.split("/")[1])) {
                const imageUrl = path.basename(checkIfBrandExist.imageUrl);

                const timestampSegment = checkIfBrandExist.imageUrl.match(/\/(\d+)\//);
                if (timestampSegment && timestampSegment.length >= 2) {
                    const timestampString = timestampSegment[1];
                    await deleteFileInAWS(`kibson${req.baseUrl}`, timestampString + "/" + imageUrl);
                }
                req.requestModel.imageUrl = await uploadFile(res, req.file, req.params.id, `${req.baseUrl}`);
            } else {
                return res
                    .status(ErrorStatus.ONLY_IMAGES.code)
                    .json(prepareErrorResponse(ErrorStatus.ONLY_IMAGES, null, extractLanguageFromRequest(req)));
            }
        }

        req.responseModel = await BrandQueries.updateBrandById(req.params.id, req.requestModel)
        next();
    } catch
    (error) {
        prepareErrorLog(error, updateBrandByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const deleteBrandDataHandler = async (req, res, next) => {
    try {
        const brandIds = req.requestModel.brandIds;
        const deletePromises = brandIds.map(async (id) => await BrandQueries.deleteBrandById(id));
        const deleteResults = await Promise.all(deletePromises);

        // Check if any IDs did not exist
        const notFoundIds = [];
        deleteResults.forEach((result, index) => {
            if (result === null) {
                notFoundIds.push(brandIds[index]);
            }
        });

        if (notFoundIds.length > 0) {
            // Some IDs were not found
            return res
                .status(ErrorStatus.SERVER_DOWN.code)
                .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, "The following brand IDs do not exist: " + notFoundIds.join(", "), extractLanguageFromRequest(req)));
        }
        next();
    } catch (error) {
        prepareErrorLog(error, deleteBrandDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const deleteBrandByIdDataHandler = async (req, res, next) => {
    try {
        const brandId = req.params.id;
        const getBrandById = await BrandQueries.getBrandById(brandId);
        if (isEmpty(getBrandById)) {
            return res
                .status(ErrorStatus.BRAND_DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.BRAND_DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }

        if (getBrandById.imageUrl) {
            const imageUrl = path.basename(getBrandById.imageUrl);

            const timestampSegment = getBrandById.imageUrl.match(/\/(\d+)\//);
            if (timestampSegment && timestampSegment.length >= 2) {
                const timestampString = timestampSegment[1];
                await deleteFileInAWS(`kibson${req.baseUrl}`, timestampString + "/" + imageUrl);
            }
        }
        await BrandQueries.deleteBrandById(brandId);

        next();
    } catch (error) {
        prepareErrorLog(error, deleteBrandDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const getBrandBySearchPaginatedDataHandler = async (req, res, next) => {
    try {
        const brands = await BrandQueries.getAllBrandsBySearch(req);
        const userId = req.userFromToken?._id;
        const getUser = await UserQueries.getUserInfoById(userId);
        if (!userId) {
            req.responseModel = brands;
            next();
        } else {
            req.responseModel = brands;
            req.responseModel.admin = getUser;
            next();
        }

    } catch (error) {
        prepareErrorLog(error, getBrandBySearchPaginatedDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

module.exports = {
    getAllBrandsDataHandler,
    createBrandDataHandler,
    getBrandByIdDataHandler,
    updateBrandByIdDataHandler,
    deleteBrandDataHandler,
    deleteBrandByIdDataHandler,
    createBulkBrandDataHandler,
    getBrandBySearchPaginatedDataHandler
}