const { prepareErrorLog } = require("../helperKit/loggingKit");
const { ErrorStatus } = require("../constants/error/errorStatus");
const { prepareErrorResponse } = require("../errorHandlerLayer/errorHandler");
const { extractLanguageFromRequest } = require("./common/LanguageHeaderHandler");
const BannerQueries = require("../dataBaseLayer/queries/banner/bannerQueries")
const { isEmpty } = require("lodash");
const { getFileStorePathForFile } = require("../helperKit/helperKit");
const { uploadFile } = require("../helperKit/uploadFile");
const path = require("path");
const { deleteFileInAWS } = require("../helperKit/deleteFileFromAWSKit");

const getAllBannersDataHandler = async (req, res, next) => {
    try {
        req.responseModel = await BannerQueries.getAllBannerPaginated(req);
        next();

    } catch (error) {
        prepareErrorLog(error, getAllBannersDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const createBannerDataHandler = async (req, res, next) => {
    try {
        if (req.file) {
            if (getFileStorePathForFile(req.file.mimetype.split("/")[1])) {
                const banner = await BannerQueries.createBanner(req.requestModel);
                const createBanner = await banner.save();
                const uploadedFile = await uploadFile(res, req.file, createBanner._id, req.baseUrl);
                req.responseModel = await BannerQueries.updateBannerImageUrl(createBanner._id, uploadedFile);
                next()
            } else {
                return res
                    .status(ErrorStatus.ONLY_IMAGES.code)
                    .json(prepareErrorResponse(ErrorStatus.ONLY_IMAGES, null, extractLanguageFromRequest(req)));
            }
        } else {
            return res
                .status(ErrorStatus.FILE_MISSING.code)
                .json(prepareErrorResponse(ErrorStatus.FILE_MISSING, null, extractLanguageFromRequest(req)));
        }

    } catch (error) {
        prepareErrorLog(error, createBannerDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const getBannerByIdDataHandler = async (req, res, next) => {
    try {
        const checkIfBannerExist = await BannerQueries.getBannerById(req.params.id);
        if (isEmpty(checkIfBannerExist)) {
            return res
                .status(ErrorStatus.BANNER_DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.BANNER_DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }
        req.responseModel = checkIfBannerExist;
        next();
    } catch (error) {
        prepareErrorLog(error, getBannerByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}


const updateBannerByIdDataHandler = async (req, res, next) => {
    try {
        const checkIfBannerExist = await BannerQueries.getBannerById(req.params.id);
        if (isEmpty(checkIfBannerExist)) {
            return res
                .status(ErrorStatus.BANNER_DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.BANNER_DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }
        if (req.file) {
            if (getFileStorePathForFile(req.file.mimetype.split("/")[1])) {
                const imageUrl = path.basename(checkIfBannerExist.imageUrl);

                const timestampSegment = checkIfBannerExist.imageUrl.match(/\/(\d+)\//);
                if (timestampSegment && timestampSegment.length >= 2) {
                    const timestampString = timestampSegment[1];
                    await deleteFileInAWS(`kibson${req.baseUrl}`, timestampString + "/" + imageUrl);
                }
                req.requestModel.imageUrl = await uploadFile(res, req.file, req.params.id, `${req.baseUrl}`);
            } else {
                return res
                    .status(ErrorStatus.ONLY_IMAGES.code)
                    .json(prepareErrorResponse(ErrorStatus.ONLY_IMAGES, null, extractLanguageFromRequest(req)));
            }
        }

        req.responseModel = await BannerQueries.updateBannerById(req.params.id, req.requestModel)
        next();
    } catch
    (error) {
        prepareErrorLog(error, updateBannerByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const deleteBannerDataHandler = async (req, res, next) => {
    try {
        const bannerIds = req.requestModel.bannerIds;
        const deletePromises = bannerIds.map(async (id) => await BannerQueries.deleteBannerById(id));
        const deleteResults = await Promise.all(deletePromises);

        // Check if any IDs did not exist
        const notFoundIds = [];
        deleteResults.forEach((result, index) => {
            if (result === null) {
                notFoundIds.push(bannerIds[index]);
            }
        });

        if (notFoundIds.length > 0) {
            // Some IDs were not found
            return res
                .status(ErrorStatus.SERVER_DOWN.code)
                .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, "The following Banner IDs do not exist: " + notFoundIds.join(", "), extractLanguageFromRequest(req)));
        }
        next();
    } catch (error) {
        prepareErrorLog(error, deleteBannerDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const deleteBannerByIdDataHandler = async (req, res, next) => {
    try {
        const bannerId = req.params.id;
        const getBannerById = await BannerQueries.getBannerById(bannerId);
        if (isEmpty(getBannerById)) {
            return res
                .status(ErrorStatus.BANNER_DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.BANNER_DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }

        if (getBannerById.imageUrl) {
            const imageUrl = path.basename(getBannerById.imageUrl);

            const timestampSegment = getBannerById.imageUrl.match(/\/(\d+)\//);
            if (timestampSegment && timestampSegment.length >= 2) {
                const timestampString = timestampSegment[1];
                await deleteFileInAWS(`kibson${req.baseUrl}`, timestampString + "/" + imageUrl);
            }
        }
        await BannerQueries.deleteBannerById(bannerId);

        next();
    } catch (error) {
        prepareErrorLog(error, deleteBannerDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}
module.exports = {
    getAllBannersDataHandler,
    createBannerDataHandler,
    getBannerByIdDataHandler,
    updateBannerByIdDataHandler,
    deleteBannerDataHandler,
    deleteBannerByIdDataHandler
}