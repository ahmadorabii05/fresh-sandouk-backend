class UpdateAddressRequestModel {
    addressTitle = undefined;
    address = undefined;
    street = undefined;
    nearestLand = undefined;
    contactNumber = undefined;
    isDefaultAddress=undefined;
    longtitude = undefined;
    latitude =undefined;
    constructor(values) {
        this.addressTitle = values?.addressTitle
        this.address = values?.address
        this.street = values?.street
        this.nearestLand = values?.nearestLand
        this.contactNumber = values?.contactNumber
        this.isDefaultAddress=values.isDefaultAddress
        this.longtitude = values?.longtitude
        this.latitude = values?.latitude

    }
}

module.exports = {
    UpdateAddressRequestModel
}