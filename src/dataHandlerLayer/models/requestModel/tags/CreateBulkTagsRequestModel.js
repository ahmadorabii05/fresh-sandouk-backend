class CreateBulkTagsRequestModel {

    tags = undefined;

    constructor(values) {
        this.tags = values?.tags;
    }
}

module.exports ={
    CreateBulkTagsRequestModel
}