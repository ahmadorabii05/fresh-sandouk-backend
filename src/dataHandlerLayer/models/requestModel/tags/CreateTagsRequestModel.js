class CreateTagsRequestModel {
    name = undefined;
    nameAr=undefined;
    constructor(values) {
        this.name = values?.name.trim().toUpperCase();
        this.nameAr=values?.nameAr;
    }
}

module.exports = {
    CreateTagsRequestModel
}