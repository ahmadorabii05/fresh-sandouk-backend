class DeleteTagsRequestModel {
    tagIds = undefined;

    constructor(values) {
        this.tagIds = values?.tagIds;
    }
}

module.exports = {
    DeleteTagsRequestModel
}