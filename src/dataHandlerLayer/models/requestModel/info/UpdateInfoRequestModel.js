const {UpdateUserProfileGeneralRequestModel} = require("../general/UpdateUserProfileGeneralRequestModel");

class UpdateInfoRequestModel {
    general = undefined;
    constructor(values) {
        this.general = new UpdateUserProfileGeneralRequestModel(values?.general);
    }
}

module.exports = {
    UpdateInfoRequestModel,
};
