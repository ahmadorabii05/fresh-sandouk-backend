const { GeneralRequestModel } = require("../general/GeneralRequestModel");

class InfoRequestModel {
    general = undefined;
    constructor(values) {
        this.general = new GeneralRequestModel(values?.general);
    }
}

module.exports = {
    InfoRequestModel,
};
