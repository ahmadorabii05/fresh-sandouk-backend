class CreateBulkBrandRequestModel {

    brands = undefined;

    constructor(values) {
        this.brands = values.brands;
    }
}

module.exports = {
    CreateBulkBrandRequestModel
}