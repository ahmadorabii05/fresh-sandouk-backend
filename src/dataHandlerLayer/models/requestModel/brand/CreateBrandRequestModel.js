class CreateBrandRequestModel {
    source = undefined;
    sourceAr = undefined;

    constructor(values) {
        this.source = values?.source?.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()).join(' ');
        this.sourceAr = values?.sourceAr;
    }
}

module.exports = {
    CreateBrandRequestModel
}