class DeleteBrandRequestModel {
    brandIds = undefined;

    constructor(values) {
        this.brandIds = values.brandIds;
    }
}

module.exports = {
    DeleteBrandRequestModel
}