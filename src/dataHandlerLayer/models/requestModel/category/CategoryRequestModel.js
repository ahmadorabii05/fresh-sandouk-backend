class CategoryRequestModel {
    name = undefined;
    nameAr = undefined;
    color=undefined;
    subCategories = undefined;
    priority = undefined;
    isActive = undefined;
    constructor(values) {
        this.name = values?.name?.trim().toUpperCase();
        this.nameAr = values?.nameAr ? values?.nameAr?.trim() : values.nameAr;
        this.color=values?.color;
        this.subCategories = values?.subCategories || [];
        this.priority = values?.priority;
        this.isActive = values?.isActive;
    }
}

module.exports = {
    CategoryRequestModel,
};
