class DeleteCategoryByIdsRequestModel {
    categoryIds = undefined;

    constructor(values) {
        this.categoryIds = values?.categoryIds;
    }
}

module.exports = {
    DeleteCategoryByIdsRequestModel,
};
