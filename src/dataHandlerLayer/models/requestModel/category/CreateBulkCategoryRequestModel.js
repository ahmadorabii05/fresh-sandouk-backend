class CreateBulkCategoryRequestModel {
    category = undefined;

    constructor(values) {
        this.category = values?.category;
    }
}

module.exports = {
    CreateBulkCategoryRequestModel
}