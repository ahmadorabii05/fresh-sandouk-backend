class PermissionsRequestModel {
    permissions = undefined;

    constructor(values) {
        this.permissions = values?.permissions || [];
    }
}

module.exports = {
    PermissionsRequestModel,
};
