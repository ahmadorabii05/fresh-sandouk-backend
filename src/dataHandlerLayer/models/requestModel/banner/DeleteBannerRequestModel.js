class DeleteBannerRequestModel {
    bannerIds = undefined;

    constructor(values) {
        this.bannerIds = values.bannerIds;
    }
}

module.exports = {
    DeleteBannerRequestModel
}