class CreateBannerRequestModel {
    actionUrl = undefined;
    location = undefined;
    priority = undefined;

    constructor(values) {
        this.actionUrl = values?.actionUrl;
        this.location = values?.location;
        this.priority = values?.priority;
    }
}

module.exports = {
    CreateBannerRequestModel
}