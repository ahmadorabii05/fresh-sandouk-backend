
class CreateStockRequestModel{
    productId=undefined;
    quantity=undefined
    constructor(values) {
        this.productId=values?.productId;
        this.quantity=values?.quantity;
    }
}

module.exports = {
    CreateStockRequestModel
}