class LoginRequestModel {
  email = undefined;
  password = undefined;
  constructor(values) {
    this.email = values?.email?.trim();
    this.password = values?.password?.trim() ;
  }
}

module.exports = {
  LoginRequestModel,
};
