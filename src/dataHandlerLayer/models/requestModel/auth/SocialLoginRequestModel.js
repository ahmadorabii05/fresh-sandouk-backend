const { DefaultPassword } = require("../../../../constants/general/general");

class SocialLoginRequestModel {
  email = undefined;
  password = undefined;
  name = undefined;
  phoneNumber = undefined;
  socialLoginType = undefined;
  socialLoginToken = undefined;
  socialLoginId= undefined;
  constructor(values) {
    this.email = values.email;
    this.password = values.password || DefaultPassword;
    this.name = values.name;
    this.phoneNumber=values?.phoneNumber;
    this.socialLoginType = values.socialLoginType;
    this.socialLoginToken = values.socialLoginToken;
    this.socialLoginId=values.socialLoginId;
  }
}

module.exports = {
  SocialLoginRequestModel,
};
