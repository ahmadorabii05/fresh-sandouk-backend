class ResetPasswordRequestModel {
    sentToken = undefined;
    password = undefined;

    constructor(values) {
        this.sentToken = values.sentToken;
        this.password = values.password;
    }
}

module.exports = {
    ResetPasswordRequestModel,
};
