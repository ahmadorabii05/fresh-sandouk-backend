class AccountVerificationRequestModel {
  sentToken = undefined;
  constructor(values) {
    this.sentToken = values.sentToken || "";
  }
}

module.exports = {
  AccountVerificationRequestModel,
};
