class SignUpRequestModel {
  name = undefined;
  email = undefined;
  password = undefined;
  phoneNumber=undefined;
  otp=undefined;
  constructor(values) {
    this.email = values?.email?.trim();
    this.name=values?.name;
    this.password = values?.password?.trim();
    this.phoneNumber = values?.phoneNumber?.replace(/\s/g, '');
    this.otp=values?.otp?.replace(/\s/g, '') || "";
  }
}

module.exports = {
  SignUpRequestModel,
};
