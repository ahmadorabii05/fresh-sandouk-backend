class ChangePasswordRequestModel {
    email = undefined;
    password = undefined;

    constructor(values) {
        this.email = values.email || "";
        this.password = values.password;
    }
}

module.exports = {
    ChangePasswordRequestModel,
};
