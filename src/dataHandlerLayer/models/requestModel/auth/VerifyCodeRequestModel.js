class VerifyCodeRequestModel {
    verifyCode = undefined;
    email = undefined;
    constructor(values) {
        this.verifyCode = values.verifyCode;
        this.email = values.email;
    }
}
module.exports = {
    VerifyCodeRequestModel,
};