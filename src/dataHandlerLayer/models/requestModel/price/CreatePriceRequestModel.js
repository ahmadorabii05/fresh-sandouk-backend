class CreatePriceRequestModel {
    unit = undefined;
    unitAr=undefined;
    constructor(values) {
        this.unit = values?.unit;
        this.unitAr=values?.unitAr;
    }
}

module.exports = {
    CreatePriceRequestModel
}