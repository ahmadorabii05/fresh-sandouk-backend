class DeletePriceRequestModel {
    priceIds = undefined;

    constructor(values) {
        this.priceIds = values.priceIds;
    }
}

module.exports = {
    DeletePriceRequestModel
}