class CreateFavoriteRequestModel {
    productIds = undefined;

    constructor(values) {
        this.productIds = values?.productIds ;
    }
}

module.exports = {
    CreateFavoriteRequestModel,
};
