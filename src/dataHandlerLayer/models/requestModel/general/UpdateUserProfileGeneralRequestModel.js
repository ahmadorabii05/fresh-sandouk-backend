class UpdateUserProfileGeneralRequestModel {
    firstName = undefined;
    lastName= undefined;
    phoneNumber = undefined;
    alternativePhoneNumber = undefined;
    email = undefined;
    alternativeEmail=undefined;
    dateOfBirth=undefined;
    nationality=undefined;
    constructor(values) {
        this.email = values?.email;
        this.firstName = values?.firstName;
        this.lastName = values?.lastName;
        this.phoneNumber = values?.phoneNumber;
        this.alternativePhoneNumber = values?.alternativePhoneNumber;
        this.alternativeEmail = values?.alternativeEmail;
        this.dateOfBirth = values?.dateOfBirth;
        this.nationality = values?.nationality;

    }
}

module.exports = {
    UpdateUserProfileGeneralRequestModel,
};
