class GeneralRequestModel {
    email = undefined;
    password = undefined;
    name = undefined;
    accountVerification = undefined;
    role = undefined;
    phoneNumber=undefined;
    constructor(values) {
        this.email = values?.email;
        this.name = values?.name;
        this.password = values?.password;
        this.accountVerification = values?.accountVerification;
        this.phoneNumber = values?.phoneNumber;
        this.role = values?.role;
    }
}

module.exports = {
    GeneralRequestModel,
};
