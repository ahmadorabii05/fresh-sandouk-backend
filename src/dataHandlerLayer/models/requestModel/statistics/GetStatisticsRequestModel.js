class GetStatisticsRequestModel {
    dateFrom = undefined;
    dateTo = undefined;

    constructor(values) {
        this.dateFrom = values.dateFrom;
        this.dateTo = values.dateTo;
    }
}

module.exports = {
    GetStatisticsRequestModel
}