
class CreateListRequestModel {
    name = undefined;
    productIds = undefined;

    constructor(values) {
        this.name = values?.name;
        this.productIds = values?.productIds || [];

    }
}

module.exports = {
    CreateListRequestModel
}