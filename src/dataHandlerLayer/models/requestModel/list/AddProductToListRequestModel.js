class AddProductToListRequestModel {
    listId = undefined;
    productId = undefined;

    constructor(values) {
        this.listId = values?.listId;
        this.productId = values?.productId
    }


}

module.exports = {
    AddProductToListRequestModel
}