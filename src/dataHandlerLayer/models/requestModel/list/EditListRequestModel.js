class EditListRequestModel {

    name = undefined
    listId = undefined;

    constructor(values) {
        this.name = values?.name;
        this.listId = values?.listId;
    }
}

module.exports = {
    EditListRequestModel
}