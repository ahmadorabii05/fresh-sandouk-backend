const {UpdateInfoRequestModel} = require("../info/UpdateInfoRequestModel");

class UpdateUserProfileForMobileRequestModel {
    info = undefined;
    constructor(values) {
        this.info = new UpdateInfoRequestModel(values.info);
    }
}

module.exports = {
    UpdateUserProfileForMobileRequestModel,
};
