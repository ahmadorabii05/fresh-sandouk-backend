const { InfoRequestModel } = require("../info/InfoRequestModel");

class UserRequestModel {
    info = undefined;
    active = undefined;

    constructor(values) {
        this.info = new InfoRequestModel(values.info);
        this.active = values?.active;
    }
}

module.exports = {
    UserRequestModel,
};
