const { InfoRequestModel } = require("../info/InfoRequestModel");
const {GeneralRequestModel} = require("../general/GeneralRequestModel");
const {DefaultPassword} = require("../../../../constants/general/general");

class CreateUserRequestModel {
    info = undefined;
    active = undefined;
    constructor(values) {
        this.info = new InfoRequestModel(values.info);
        this.active = values?.active;
    }

    fromSocial = (values) => {
        const info = new InfoRequestModel();
        const general = new GeneralRequestModel();
        general.email = values.email;
        general.accountVerification = false;
        general.name = values.name || "";
        general.password = values.password ?? DefaultPassword;
        general.phoneNumber=values.phoneNumber || "";
        general.socialLoginType = values.socialLoginType;
        general.socialLoginToken = values.socialLoginToken;
        general.socialLoginId =values.socialLoginId || "";
        general.role = values.role;
        info.general = general;
        this.info = info;
        this.active = true;
    };



    fromSignUp = (values) => {
        const info = new InfoRequestModel();
        const general = new GeneralRequestModel();
        general.email = values.email;
        general.accountVerification = false;
        general.name = values.name || "";
        general.password = values.password ?? DefaultPassword;
        general.phoneNumber=values.phoneNumber;
        general.role = values.role;
        info.general = general;
        info.general = general;

        this.info = info;
        this.active = true;
    };
}


module.exports = {
    CreateUserRequestModel,
};
