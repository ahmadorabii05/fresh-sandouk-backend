class DeleteSubCategoryRequestModel {
    subCategoryIds = undefined;
    constructor(values) {
        this.subCategoryIds = values?.subCategoryIds;
    }
}

module.exports = {
    DeleteSubCategoryRequestModel,
};
