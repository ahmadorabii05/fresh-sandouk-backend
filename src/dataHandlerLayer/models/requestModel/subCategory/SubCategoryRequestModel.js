class SubCategoryRequestModel {
    name = undefined;
    nameAr = undefined;
    productIds = undefined;
    categoryId = undefined;
    isActive = undefined;

    constructor(values) {
        this.name = values?.name?.trim();
        this.nameAr = values?.nameAr?.trim();
        this.categoryId = values?.categoryId;
        this.productIds = values?.productIds || [];
        this.isActive = values?.isActive;
    }
}

module.exports = {
    SubCategoryRequestModel,
};
