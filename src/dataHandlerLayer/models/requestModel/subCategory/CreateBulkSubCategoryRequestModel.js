
class CreateBulkSubCategoryRequestModel{
    subCategories=undefined;

    constructor(values) {
        this.subCategories=values?.subCategories;
    }
}

module.exports={
    CreateBulkSubCategoryRequestModel
}