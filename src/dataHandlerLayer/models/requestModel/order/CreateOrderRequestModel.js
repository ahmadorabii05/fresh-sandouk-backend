class CreateOrderRequestModel {
    packingInstruction = undefined;
    deliveryInstruction = undefined;
    scheduledDate = undefined;
    scheduledTime = undefined;
    constructor(values) {
        this.deliveryInstruction = values?.deliveryInstruction;
        this.scheduledDate = values?.scheduledDate;
        this.scheduledTime = values?.scheduledTime;
        this.packingInstruction = values?.packingInstruction;
    }
}

module.exports = {
    CreateOrderRequestModel
}