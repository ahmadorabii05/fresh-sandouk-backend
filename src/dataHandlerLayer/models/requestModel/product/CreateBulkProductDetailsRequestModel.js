const {NutritionRequestModel} = require("../nutrition/NutritionRequestModel");

class CreateBulkProductDetailsRequestModel {
    name = undefined
    country = undefined;
    price = undefined;
    priceModelName = undefined;
    description = undefined;
    sku = undefined;
    discountValue = undefined;
    tagsName = undefined;
    generalDescription = undefined;
    ingredient = undefined;
    storage = undefined;
    nameAr = undefined;
    generalDescriptionAr = undefined;
    ingredientAr = undefined;
    storageAr = undefined;
    descriptionAr = undefined;
    brandName = undefined;
    nutrition = undefined;
    subCategoryName = undefined;
    quantity = undefined;

    constructor(values) {
        this.name = values?.name?.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()).join(' ');
        this.nameAr = values?.nameAr;
        this.subCategoryName = values?.subCategoryName;
        this.country = values?.country;
        this.price = values?.price;
        this.priceModelName = values?.priceModelName;
        this.description = values?.description;
        this.descriptionAr = values?.descriptionAr;
        this.discountValue = values?.discountValue || "";
        this.generalDescription = values?.generalDescription || "";
        this.generalDescriptionAr = values?.generalDescriptionAr || "";
        this.sku = values?.sku;
        this.ingredient = values?.ingredient || "";
        this.storage = values?.storage || "";
        this.ingredientAr = values?.ingredientAr || "";
        this.storageAr = values?.storageAr || "";
        this.tagsName = values?.tagsName || [];
        this.brandName = values?.brandName;
        this.nutrition = values?.nutrition?.length > 0 ? values?.nutrition?.map(item => {
            return new NutritionRequestModel(item)
        }) : []
        this.quantity = values.quantity;
    }
}

module.exports = {
    CreateBulkProductDetailsRequestModel
}