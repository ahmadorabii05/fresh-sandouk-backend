class DeleteProductByIdsRequestModel {
    productIds = undefined;

    constructor(values) {
        this.productIds = values?.productIds
    }
}

module.exports = {
    DeleteProductByIdsRequestModel
}