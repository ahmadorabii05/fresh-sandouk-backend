class UpdateProductRequestModel {


    name = undefined;

    nameAr = undefined

    country = undefined;
    price = undefined;
    priceModel = undefined;
    description = undefined;

    descriptionAr = undefined;

    sku = undefined;
    barcode = undefined;
    sort_order = 1;

    discountValue = undefined;
    generalDescription = undefined;
    ingredient = undefined;
    storage = undefined;

    generalDescriptionAr = undefined;
    ingredientAr = undefined;
    storageAr = undefined;

    subCategoryId = undefined;
    tags = undefined;
    brandId = undefined;
    isActive = undefined;
    nutrition = undefined;

    constructor(values) {
        this.name = values?.name;
        this.nameAr = values?.nameAr;
        this.sku = values?.sku;
        this.barcode = values?.barcode;
        this.sort_order = values?.sort_order;
        this.subCategoryId = values?.subCategoryId;
        this.country = values?.country;
        this.price = values?.price;
        this.priceModel = values?.priceModel;
        this.description = values?.description;

        this.descriptionAr = values?.descriptionAr;

        this.discountValue = values?.discountValue;
        this.generalDescription = values?.generalDescription;
        this.ingredient = values?.ingredient;
        this.storage = values?.storage;

        this.generalDescriptionAr = values?.generalDescriptionAr;
        this.ingredientAr = values?.ingredientAr;
        this.storageAr = values?.storageAr;
        this.brandId = values?.brandId;
        this.tags = values?.tags;
        this.quantity = values?.quantity;
        this.nutrition = values?.nutrition;
        this.isActive = values.isActive;
        this.isFeatured = values.isFeatured;
    }
}

module.exports = {
    UpdateProductRequestModel,
};
