const {NutritionRequestModel} = require("../nutrition/NutritionRequestModel");

class CreateProductRequestModel {
    name = undefined
    country = undefined;
    price = undefined;
    priceModel = undefined;
    description = undefined;
    sku = undefined;
    discountValue = undefined;
    tags = undefined;
    generalDescription = undefined;
    ingredient = undefined;
    storage = undefined;
    nameAr = undefined;
    generalDescriptionAr = undefined;
    ingredientAr = undefined;
    storageAr = undefined;
    descriptionAr = undefined;
    brandId = undefined;
    nutrition = undefined;
    subCategoryId = undefined;
    quantity = undefined;

    constructor(values) {
        this.name = values?.name?.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()).join(' ');
        this.nameAr = values?.nameAr;
        this.subCategoryId = values?.subCategoryId;
        this.country = values?.country;
        this.price = values?.price;
        this.priceModel = values?.priceModel;
        this.description = values?.description;
        this.descriptionAr = values?.descriptionAr;
        this.discountValue = values?.discountValue || 0;
        this.generalDescription = values?.generalDescription || "";
        this.generalDescriptionAr = values?.generalDescriptionAr || "";
        this.sku = values?.sku;
        this.ingredient = values?.ingredient || "";
        this.storage = values?.storage || "";
        this.ingredientAr = values?.ingredientAr || "";
        this.storageAr = values?.storageAr || "";
        this.tags = values?.tags || [];
        this.brandId = values?.brandId;
        this.nutrition = values?.nutrition?.length > 0 ? values?.nutrition?.map(item => {
            return new NutritionRequestModel(item)
        }) : []
        this.quantity = values.quantity;
    }
}

module.exports = {
    CreateProductRequestModel,
};
