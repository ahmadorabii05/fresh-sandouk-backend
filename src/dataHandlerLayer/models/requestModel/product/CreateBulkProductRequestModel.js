class CreateBulkProductRequestModel {
    products = undefined;

    constructor(values) {
        this.products = values.products
    }
}

module.exports = {
    CreateBulkProductRequestModel
}