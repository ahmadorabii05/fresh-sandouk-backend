const { RoleRequestModel } = require("./RoleRequestModel");
class GetRoleRequestModel extends RoleRequestModel {
    id = undefined;

    constructor(values) {
        super(values);
        this.id = values._id || -1;
    }
}

module.exports = {
    GetRoleRequestModel,
};
