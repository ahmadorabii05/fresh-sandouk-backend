const {
    PermissionsRequestModel,
} = require("../permissions/PermissionRequestModel");

class RoleRequestModel {
    name = undefined;
    permissions = undefined;

    constructor(values) {
        this.name = values?.name || "";
        this.permissions = new PermissionsRequestModel(values?.permissions);
    }
}

module.exports = {
    RoleRequestModel,
};
