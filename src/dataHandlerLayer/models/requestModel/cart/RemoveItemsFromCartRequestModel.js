class RemoveItemsFromCartRequestModel {

    productIds = undefined;

    constructor(values) {
        this.productIds = values?.productIds;
    }
}

module.exports = {
    RemoveItemsFromCartRequestModel
}