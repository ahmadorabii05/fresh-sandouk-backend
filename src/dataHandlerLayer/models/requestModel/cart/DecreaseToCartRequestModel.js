class DecreaseToCartRequestModel {

    productId = undefined;

    constructor(values) {
        this.productId = values?.productId;
    }
}

module.exports = {
    DecreaseToCartRequestModel
}