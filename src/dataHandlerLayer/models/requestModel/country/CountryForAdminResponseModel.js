
class CountryForAdminResponseModel {
    id = undefined;
    code = undefined;
    country = undefined;
    countryAr = undefined;
    constructor(values) {
        this.id = values?._id || values?.id;
        this.code = values?.code;
        this.country = values?.country_en;
        this.countryAr =  values?.country_ar || "";
    }
}

module.exports = {
    CountryForAdminResponseModel
}