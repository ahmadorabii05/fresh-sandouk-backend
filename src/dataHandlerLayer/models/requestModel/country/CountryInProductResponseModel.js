const {extractLanguageFromRequest} = require("../../../common/LanguageHeaderHandler");

class CountryInProductResponseModel {
    id = undefined;
    code = undefined;
    country = undefined;

    constructor(values, req) {
        this.id = values?._id || values?.id;
        this.code = values?.code;
        this.country = extractLanguageFromRequest(req) === "en" ? values?.country_en || values?.country : values?.country_ar || values?.country;
    }
}

module.exports = {
    CountryInProductResponseModel
}