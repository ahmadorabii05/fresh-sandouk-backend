const {extractLanguageFromRequest} = require("../../../common/LanguageHeaderHandler");

class CountryResponseModel {
    id = undefined;
    code = undefined;
    country = undefined;

    constructor(values, req) {
        this.id = values?._id;
        this.code = values?.code;
        this.country = extractLanguageFromRequest(req) === "en" ? values?.country_en : values?.country_ar;
    }
}

module.exports = {
    CountryResponseModel
}