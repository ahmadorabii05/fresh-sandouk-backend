class NutritionRequestModel {
    id=undefined;
    description = undefined
    per_100g_ml = undefined;
    per_serving = undefined;
    measure = undefined;
    nrv = undefined;

    descriptionAr = undefined
    per_100g_mlAr = undefined;
    per_servingAr = undefined;
    measureAr = undefined;
    nrvAr = undefined;

    constructor(values) {
        this.id=values?._id;
        this.per_100g_ml = values?.per_100g_ml || 0;
        this.per_serving = values?.per_serving || 0;
        this.measure = values?.measure || 0;
        this.nrv = values?.nrv || 0;
        this.description = values?.description || "";


        this.per_100g_mlAr = values?.per_100g_mlAr || 0;
        this.per_servingAr = values?.per_servingAr || 0;
        this.measureAr = values?.measureAr || 0;
        this.nrvAr = values?.nrvAr || 0;
        this.descriptionAr = values?.descriptionAr || "";
    }
}

module.exports = {
    NutritionRequestModel,
};
