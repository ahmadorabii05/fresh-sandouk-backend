class DeleteNutritionRequestModel {

    productId = undefined;
    nutritionId = undefined;

    constructor(values) {
        this.productId = values?.productId;
        this.nutritionId = values?.nutritionId;
    }
}

module.exports = {
    DeleteNutritionRequestModel
}