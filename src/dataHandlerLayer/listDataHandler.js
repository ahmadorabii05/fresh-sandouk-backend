const listQueries = require("../dataBaseLayer/queries/list/listQueries");
const CartModel = require("../dataBaseLayer/models/cart/CartModel")
const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("./common/LanguageHeaderHandler");
const {prepareErrorLog} = require("../helperKit/loggingKit");
const {setFavoriteProductFlagNotPaginated,
    setFlagIsFavoriteForLists
} = require("../helperKit/helperKit");
const {isEmpty} = require("lodash");
const { calculateFinalPriceAfterDiscount } = require("../helperKit/productKit");


const createListDataHandler = async (req, res, next) => {
    try {
        const name = req.requestModel.name;
        const userId = req.userFromToken._id;
        const checkIfNameExist = await listQueries.getListByNameAndUserId(name, userId);
        req.requestModel.userId = userId;
        if (checkIfNameExist) {
            return res
                .status(ErrorStatus.LIST_ALREADY_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.LIST_ALREADY_EXIST, null, extractLanguageFromRequest(req)));
        }
        req.responseModel = await listQueries.createList(req.requestModel);
        next();
    } catch (error) {
        prepareErrorLog(error, createListDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


const editListNameDataHandler = async (req, res, next) => {
    try {
        const name = req.requestModel.name;
        const userId = req.userFromToken._id;
        const checkIfNameExistInAnotherUser = await listQueries.getListByNameAndUserId(name, userId);
        if (checkIfNameExistInAnotherUser) {
            const listId = checkIfNameExistInAnotherUser._id;
            if (listId.toString() === req.requestModel.listId) {
                delete req.requestModel.name;
            } else {
                return res
                    .status(ErrorStatus.LIST_ALREADY_EXIST.code)
                    .json(prepareErrorResponse(ErrorStatus.LIST_ALREADY_EXIST, null, extractLanguageFromRequest(req)));
            }
        }
        req.responseModel = await listQueries.updateListName(req.requestModel.listId, name);
        next();
    } catch (error) {
        prepareErrorLog(error, editListNameDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const getListDataHandler = async (req, res, next) => {
    try {
        const userId = req.userFromToken._id;
        const response = await listQueries.getPaginatedListsForUser(userId, req);
        const cart = await CartModel.findOne({ userId });
        // Define an asynchronous function to handle the loop
        const processProducts = async () => {
            for (const list of response.docs) {
                for (const product of list.productIds) {
                    const cartItem = cart.productIds.find(item => item.productId.equals(product._id));
                    if (cartItem) {
                        product.quantityInCart = cartItem.quantity;
                    } else {
                        product.quantityInCart = 0;
                    }
                    if (product?.isDiscount) {
                        const discountValue = parseFloat(product.discountValue);
                        if (!isNaN(discountValue)) {
                            product.discountedPrice = calculateFinalPriceAfterDiscount(product.price, discountValue);
                        }
                    }
                }
            }
        };

        // Await the completion of the processProducts function
        await processProducts();
        req.responseModel = await setFavoriteProductFlagNotPaginated(userId, response, res);
        next();
    } catch (error) {
        prepareErrorLog(error, getListDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


const getListByIdDataHandler = async (req,res,next)=>{
    try {
        const userId = req.userFromToken._id;
        const {id} =req.params;
        const response = await listQueries.getListById(id);
        // Retrieve the cart information for the user
        const cart = await CartModel.findOne({userId});
        // Modify the response to include quantity and discount information
        const processProducts = async () => {

            for (const product of response.productIds) {
                const cartItem = cart.productIds.find(item => item.productId.equals(product._id));
                if (cartItem) {
                    product.quantityInCart = cartItem.quantity; // Add quantity field from CartModel
                } else {
                    product.quantityInCart = 0; // Set quantity to 0 if not found in cart
                }

                // Check if the product has a discount value
                if (product?.isDiscount) {
                    const discountValue = parseFloat(product.discountValue);
                    if (!isNaN(discountValue)) {
                        product.discountedPrice = calculateFinalPriceAfterDiscount(product.price, discountValue);
                    }
                }
            }
        }
        await processProducts();

        // Wait for setFavoriteProductFlagById to finish before assigning the response
        req.responseModel = await setFlagIsFavoriteForLists(userId, response, res,req);
        next();
    }catch (error) {
        prepareErrorLog(error, getListByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const addProductToListDataHandler = async (req, res, next) => {
    try {
        const {listId, productId} = req.requestModel;

        const listInfo = await listQueries.getListById(listId);
        if (isEmpty(listInfo)) {
            return res
                .status(ErrorStatus.LIST_DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.LIST_DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }
        const productIndex = listInfo.productIds.findIndex((id) =>
            (id._id).equals(productId)
        );
        if (productIndex !== -1) {
            // Product exists in the list, so remove it
            req.responseModel = await listQueries.removeProductFromList(
                listId,
                productId
            );
        } else {
            // Product doesn't exist in the list, so add it
            req.responseModel = await listQueries.addProductToList(listId, productId);
        }
        next();
    } catch (error) {
        prepareErrorLog(error, addProductToListDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}


const deleteListDataHandler = async (req,res,next)=>{
    try {
        const {listId} = req.requestModel;
        await listQueries.deleteList(listId);
        next();
    }catch (error) {
        prepareErrorLog(error, deleteListDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

module.exports = {
    createListDataHandler,
    editListNameDataHandler,
    getListDataHandler,
    addProductToListDataHandler,
    deleteListDataHandler,
    getListByIdDataHandler
}