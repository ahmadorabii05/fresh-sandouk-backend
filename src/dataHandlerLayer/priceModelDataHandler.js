const {prepareErrorLog} = require("../helperKit/loggingKit");
const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("./common/LanguageHeaderHandler");
const PriceQueries = require("../dataBaseLayer/queries/price/priceQueries");
const UserQueries = require("../dataBaseLayer/queries/user/userQueries");
const {isEmpty} = require("lodash");

const getAllPricesDataHandler = async (req, res, next) => {
    try {
        req.responseModel = await PriceQueries.getAllPricePaginated(req);
        next();

    } catch (error) {
        prepareErrorLog(error, getAllPricesDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const createPriceDataHandler = async (req, res, next) => {
    try {
        req.requestModel.unit = req.requestModel.unit.toLowerCase()
        const checkIfUnitAlreadyExist = await PriceQueries.getPriceByUnit(req.requestModel.unit);
        if (!isEmpty(checkIfUnitAlreadyExist)) {
            return res
                .status(ErrorStatus.PRICE_MODEL_ALREADY_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.PRICE_MODEL_ALREADY_EXIST, null, extractLanguageFromRequest(req)));
        }
        if(req.requestModel?.unitAr){
            const checkIfUnitArAlreadyExist = await PriceQueries.getPriceByUnitAr(req.requestModel.unitAr);
            if(!isEmpty(checkIfUnitArAlreadyExist)){
                return res
                    .status(ErrorStatus.PRICE_MODEL_IN_ARABIC_ALREADY_EXIST.code)
                    .json(prepareErrorResponse(ErrorStatus.PRICE_MODEL_IN_ARABIC_ALREADY_EXIST, null, extractLanguageFromRequest(req)));
            }
        }

        req.responseModel = await PriceQueries.createPrice(req.requestModel);
        next()
    } catch (error) {
        prepareErrorLog(error, createPriceDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}


const createBulkPriceDataHandler = async (req, res, next) => {
    try {

        const priceModel = req.requestModel.priceModel;

        for(const priceModelData of priceModel){
            priceModelData.unit = priceModelData.unit.trim().toLowerCase();
            const checkIfUnitAlreadyExist = await PriceQueries.getPriceByUnit(priceModelData.unit);
            if (!isEmpty(checkIfUnitAlreadyExist)) {
                return res
                    .status(ErrorStatus.PRICE_MODEL_ALREADY_EXIST.code)
                    .json(prepareErrorResponse(ErrorStatus.PRICE_MODEL_ALREADY_EXIST, priceModelData.unit + " already exist", extractLanguageFromRequest(req)));
            }
            if(priceModelData?.unitAr){
                const checkIfUnitArAlreadyExist = await PriceQueries.getPriceByUnitAr(priceModelData.unitAr);
                if(!isEmpty(checkIfUnitArAlreadyExist)){
                    return res
                        .status(ErrorStatus.PRICE_MODEL_IN_ARABIC_ALREADY_EXIST.code)
                        .json(prepareErrorResponse(ErrorStatus.PRICE_MODEL_IN_ARABIC_ALREADY_EXIST, priceModelData.unitAr + " already exist", extractLanguageFromRequest(req)));
                }
                priceModelData.unitAr = priceModelData.unitAr.trim();
            }else{
                priceModelData.unitAr = "";
            }
             await PriceQueries.createPrice(priceModelData);
        }


        next()
    } catch (error) {
        prepareErrorLog(error, createBulkPriceDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const getPriceByIdDataHandler = async (req, res, next) => {
    try {
        const checkIfPriceExist = await PriceQueries.getPriceById(req.params.id);
        if (isEmpty(checkIfPriceExist)) {
            return res
                .status(ErrorStatus.PRICE_MODEL_DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.PRICE_MODEL_DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }
        req.responseModel = checkIfPriceExist;
        next();
    } catch (error) {
        prepareErrorLog(error, getPriceByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}


const updatePriceByIdDataHandler = async (req, res, next) => {
    try {
        const checkIfPriceExist = await PriceQueries.getPriceById(req.params.id);
        if (isEmpty(checkIfPriceExist)) {
            return res
                .status(ErrorStatus.PRICE_MODEL_DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.PRICE_MODEL_DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }
        if(req.requestModel.unit){
            req.requestModel.unit = req.requestModel.unit.toLowerCase()
            const checkIfPriceNameExist = await PriceQueries.getPriceByUnit(req.requestModel.unit);
            if (!isEmpty(checkIfPriceNameExist)) {
                if (req.params.id.valueOf() === checkIfPriceNameExist._id.valueOf()) {
                    delete req.requestModel.unit
                } else {
                    return res
                        .status(ErrorStatus.PRICE_MODEL_ALREADY_EXIST.code)
                        .json(prepareErrorResponse(ErrorStatus.PRICE_MODEL_ALREADY_EXIST, null, extractLanguageFromRequest(req)));
                }
            }
        }
        if(req.requestModel?.unitAr){
            const checkIfPriceNameExist = await PriceQueries.getPriceByUnitAr(req.requestModel.unitAr);
            if (!isEmpty(checkIfPriceNameExist)) {
                if (req.params.id.valueOf() === checkIfPriceNameExist._id.valueOf()) {
                    delete req.requestModel.unit
                } else {
                    return res
                        .status(ErrorStatus.PRICE_MODEL_IN_ARABIC_ALREADY_EXIST.code)
                        .json(prepareErrorResponse(ErrorStatus.PRICE_MODEL_IN_ARABIC_ALREADY_EXIST, null, extractLanguageFromRequest(req)));
                }
            }
        }

        req.responseModel = await PriceQueries.updatePriceById(req.params.id, req.requestModel)
        next();
    } catch (error) {
        prepareErrorLog(error, updatePriceByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const deletePriceDataHandler = async (req, res, next) => {
    try {
        const priceIds = req.requestModel.priceIds;
        const deletePromises = priceIds.map(async (id) => await PriceQueries.deletePriceById(id));
        const deleteResults = await Promise.all(deletePromises);

        // Check if any IDs did not exist
        const notFoundIds = [];
        deleteResults.forEach((result, index) => {
            if (result === null) {
                notFoundIds.push(priceIds[index]);
            }
        });

        if (notFoundIds.length > 0) {
            // Some IDs were not found
            return res
                .status(ErrorStatus.SERVER_DOWN.code)
                .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, "The following price IDs do not exist: " + notFoundIds.join(", "), extractLanguageFromRequest(req)));
        }
        next();
    } catch (error) {
        prepareErrorLog(error, deletePriceDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const deletePriceByIdDataHandler = async (req,res,next)=>{
    try {
        const priceId = req.params.id;
        await PriceQueries.deletePriceById(priceId);
        next();
    } catch (error) {
        prepareErrorLog(error, deletePriceByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const getPriceBySearchPaginatedDataHandler = async (req, res, next) => {
    try {
        const list = await PriceQueries.getAllPricesBySearch(req);
        const userId = req.userFromToken?._id;
        const getUser = await UserQueries.getUserInfoById(userId);
        if (!userId) {
            req.responseModel = list;
            next();
        } else {
            req.responseModel = list;
            req.responseModel.admin = getUser;
            next();
        }

    } catch (error) {
        prepareErrorLog(error, getPriceBySearchPaginatedDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

module.exports = {
    getAllPricesDataHandler,
    createPriceDataHandler,
    getPriceByIdDataHandler,
    updatePriceByIdDataHandler,
    deletePriceDataHandler,
    deletePriceByIdDataHandler,
    createBulkPriceDataHandler,
    getPriceBySearchPaginatedDataHandler
}