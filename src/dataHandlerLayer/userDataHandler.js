const { isEmpty } = require("lodash");
const UserQueries = require("../dataBaseLayer/queries/user/userQueries");
const { ErrorStatus } = require("../constants/error/errorStatus");
const { prepareErrorResponse } = require("../errorHandlerLayer/errorHandler");
const { prepareErrorLog } = require("../helperKit/loggingKit");
const crypto = require("crypto");
const bcrypt = require("bcryptjs");
const { DefaultPassword } = require("../constants/general/general");
const mongoose = require("mongoose");
const { sendEmail, EmailTypes } = require("../helperKit/emailKit");
const { extractLanguageFromRequest } = require("./common/LanguageHeaderHandler");
const { isValidNumber } = require("libphonenumber-js");
const CartQueries = require("../dataBaseLayer/queries/cart/cartQueries");
const OrderQueries = require("../dataBaseLayer/queries/order/OrderQueries")
const AddressesQueries = require("../dataBaseLayer/queries/address/addressQueries")
const { calculateTotalAmount, createCustomerInStripe } = require("../helperKit/helperKit");
/**
 * GetUserByIdDataHandler prepares the user data needed from the database
 */
const getUserByIdDataHandler = async (req, res, next) => {
    try {
        const user = await UserQueries.getUserInfoById(req.params.id);
        if (isEmpty(user)) {
            return res
                .status(ErrorStatus.DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        } else {
            req.user = user;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, getUserByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const getUserByIdWithOrderDataHandler = async (req, res, next) => {
    try {
        const user = await UserQueries.getUserInfoById(req.params.id);
        if (isEmpty(user)) {
            return res
                .status(ErrorStatus.DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        } else {
            //get all orders for a user
            const getAllOrderByUser = await OrderQueries.getAllOrdersByUserPaginated(req.params.id, req);
            const calculatedAmount = await calculateTotalAmount(getAllOrderByUser);
            const getAllAddressesByUser = await AddressesQueries.getAllAddressPaginated(req, req.params.id);
            req.responseModel = {
                user: user,
                addresses: getAllAddressesByUser.docs,
                order: calculatedAmount.docs
            };
            next();
        }
    } catch (error) {
        prepareErrorLog(error, getUserByIdWithOrderDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}
/**
 * GetAllUsersDataHandler prepares the user data needed from the database
 */
const getAllUsersDataHandler = async (req, res, next) => {
    try {
        const users = await UserQueries.getAllUsersPaginated(req);
        if (isEmpty(users)) {
            return res
                .status(ErrorStatus.DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        } else {
            req.users = users;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, getAllUsersDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


/**
 * updateUserByIdFromAdminsDataHandler updates the user data needed from the database
 */
//admin update user
const updateUserByIdFromAdminsDataHandler = async (req, res, next) => {
    try {
        req.requestModel = {
            info: {
                general: {
                    name: req.body.name,
                    email: req.body.email
                }
            },
            active: req.body.active
        }
        if (isEmpty(req.requestModel)) {
            return res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(prepareErrorResponse(ErrorStatus.BAD_REQUEST, null, extractLanguageFromRequest(req)));
        }
        const email = req.requestModel.info?.general?.email?.toLowerCase().trim() ?? "";
        const name = req.requestModel.info?.general?.name;
        const userCheck = await UserQueries.getUserInfoById(req.params.id);
        if (!isEmpty(userCheck)) {
            if (email === userCheck.info?.general?.email) {
                delete req.requestModel.info.general.email;
            } else {
                const userCheck2 = await UserQueries.getUserInfoByEmail(email);
                if (!isEmpty(userCheck2)) {
                    return res
                        .status(ErrorStatus.USER_EMAIL_EXIST.code)
                        .json(prepareErrorResponse(ErrorStatus.USER_EMAIL_EXIST, null, extractLanguageFromRequest(req)));
                }
            }
            if (name === userCheck.info?.general?.name) {
                delete req.requestModel.info.general.name;
            } else {
                const userCheck3 = await UserQueries.getUserInfoByName(name);
                if (!isEmpty(userCheck3)) {
                    return res
                        .status(ErrorStatus.NAME_EXIST.code)
                        .json(prepareErrorResponse(ErrorStatus.NAME_EXIST, null, extractLanguageFromRequest(req)));
                }
            }
        } else {
            return res
                .status(ErrorStatus.DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }
        await UserQueries.updateUserInfoById(req.params.id, req.requestModel);
        next();
    } catch (error) {
        prepareErrorLog(error, updateUserByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};
//user update himself
const updateUserByIdDataHandler = async (req, res, next) => {
    try {
        if (isEmpty(req.requestModel)) {
            return res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(prepareErrorResponse(ErrorStatus.BAD_REQUEST, null, extractLanguageFromRequest(req)));
        }
        const email = req.requestModel.info?.general?.email?.toLowerCase().trim() ?? "";
        const userCheck = await UserQueries.getUserInfoById(req.userFromToken._id);
        if (!isEmpty(userCheck)) {
            if (email === userCheck.info?.general?.email) {
                delete req.requestModel.info.general.email;
            } else {
                const userCheck2 = await UserQueries.getUserInfoByEmail(email);
                if (!isEmpty(userCheck2)) {
                    return res
                        .status(ErrorStatus.USER_EMAIL_EXIST.code)
                        .json(prepareErrorResponse(ErrorStatus.USER_EMAIL_EXIST, null, extractLanguageFromRequest(req)));
                }
            }

        } else {
            return res
                .status(ErrorStatus.DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }
        await UserQueries.updateUserInfoById(req.userFromToken._id, req.requestModel);
        next();
    } catch (error) {
        prepareErrorLog(error, updateUserByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

//user update himself
const updateUserProfileDataHandler = async (req, res, next) => {
    try {
        if (isEmpty(req.requestModel)) {
            return res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(prepareErrorResponse(ErrorStatus.BAD_REQUEST, null, extractLanguageFromRequest(req)));
        }
        const email = req.requestModel.info?.general?.email?.toLowerCase().trim() ?? "";

        const userCheck = await UserQueries.getUserInfoById(req.userFromToken._id);
        if (!isEmpty(userCheck)) {
            if (email === userCheck.info?.general?.email) {
                delete req.requestModel.info.general.email;
            } else {
                const userCheck2 = await UserQueries.getUserInfoByEmail(email);
                if (!isEmpty(userCheck2)) {
                    return res
                        .status(ErrorStatus.USER_EMAIL_EXIST.code)
                        .json(prepareErrorResponse(ErrorStatus.USER_EMAIL_EXIST, null, extractLanguageFromRequest(req)));
                }
            }
            if (!isEmpty(req.requestModel.info.general.phoneNumber)) {
                const isValid = isValidNumber(req.requestModel.info.general.phoneNumber);
                if (!isValid) {
                    return res
                        .status(ErrorStatus.SERVER_DOWN.code)
                        .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, "phone Number is Invalid", extractLanguageFromRequest(req)));
                }
            }
            if (!isEmpty(req.requestModel.info.general.alternativePhoneNumber)) {
                const isValid = isValidNumber(req.requestModel.info.general.alternativePhoneNumber);
                if (!isValid) {
                    return res
                        .status(ErrorStatus.SERVER_DOWN.code)
                        .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, "alternative phone Number is Invalid", extractLanguageFromRequest(req)));
                }
            }
        } else {
            return res
                .status(ErrorStatus.DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }
        await UserQueries.updateUserProfileForMobile(req.userFromToken._id, req.requestModel);
        next();
    } catch (error) {
        prepareErrorLog(error, updateUserProfileDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


/**
 * ChangePasswordDataHandler updates the user data needed from the database
 */
const changePasswordDataHandler = async (req, res, next) => {
    try {
        if (isEmpty(req.requestModel)) {
            return res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(prepareErrorResponse(ErrorStatus.BAD_REQUEST, null, extractLanguageFromRequest(req)));
        }
        const newPassword = req.requestModel.newPassword;
        const salt = await bcrypt.genSalt(10),
            hashedPassword = await bcrypt.hash(newPassword, salt);
        const fetchedUser = await UserQueries.getUserInfoById(req.userFromToken._id);
        if (isEmpty(fetchedUser)) {
            return res
                .status(ErrorStatus.DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }
        // const isVerified = bcrypt.compareSync(req.requestModel.currentPassword, fetchedUser.info?.general?.password)
        // if (!isVerified) {
        //     return res
        //         .status(ErrorStatus.WRONG_CURRENT_PASSWORD.code)
        //         .json(prepareErrorResponse(ErrorStatus.WRONG_CURRENT_PASSWORD, null, extractLanguageFromRequest(req)));
        // }
        sendEmail(fetchedUser, EmailTypes.ChangePassword);
        await UserQueries.changeUserPassword(req.userFromToken._id, hashedPassword);
        next();
    } catch (error) {
        prepareErrorLog(error, changePasswordDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

/**
 * CreateUserDataHandler creates a new user
 */
const createUserDataHandler = async (req, res, next) => {
    try {
        if (isEmpty(req.requestModel)) {
            return res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(prepareErrorResponse(ErrorStatus.BAD_REQUEST, null, extractLanguageFromRequest(req)));
        }
        const isValid = isValidNumber(req.requestModel.info.general.phoneNumber);
        if (isValid) {
            const userBody = req.newUser;
            const newPassword = userBody.info.general.password ?? DefaultPassword;
            const salt = await bcrypt.genSalt(10),
                hashedPassword = await bcrypt.hash(newPassword, salt);
            userBody.info.general.password = hashedPassword;
            userBody.info.general.walletBalance = 999999;

            userBody.info.general.email = userBody.info.general.email.toLowerCase();
            const userCheck = await UserQueries.getUserInfoByEmail(
                userBody.info.general.email
            );
            if (!isEmpty(userCheck)) {
                return res
                    .status(ErrorStatus.USER_EXIST.code)
                    .json(prepareErrorResponse(ErrorStatus.USER_EXIST, null, extractLanguageFromRequest(req)));
            }
            const createUserInStripe = await createCustomerInStripe(userBody.info.general.name, userBody.info.general.email, userBody.info.general.phoneNumber);
            userBody.info.general.verificationToken = await crypto.randomBytes(32).toString("hex");
            userBody.info.general.expireVerificationToken = Date.now() + 3600000;
            if (!isEmpty(createUserInStripe)) {
                userBody.info.general.stripe_id = createUserInStripe.id;
                const savedUser = await UserQueries.createUser(userBody);
                await CartQueries.createUserCart(savedUser._id, [])

                const user = await UserQueries.getUserInfoById(savedUser._id);
                req.user = user;
                sendEmail(user, EmailTypes.UserCreated);
            } else {
                return res
                    .status(ErrorStatus.STRIPE_ERROR_CREATE_CUSTOMER.code)
                    .json(prepareErrorResponse(ErrorStatus.STRIPE_ERROR_CREATE_CUSTOMER, null, extractLanguageFromRequest(req)));
            }

            next();
        } else {
            return res
                .status(ErrorStatus.SERVER_DOWN.code)
                .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, "phone Number is Invalid", extractLanguageFromRequest(req)));
        }


    } catch (error) {
        prepareErrorLog(error, createUserDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

/**
 * DeleteUserByIdDataHandler deletes the user data  from the database
 */
const deleteUserByIdDataHandler = async (req, res, next) => {
    try {
        if (!mongoose.Types.ObjectId.isValid(req.params.id))
            return res
                .status(ErrorStatus.DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        const result = await UserQueries.deleteUserInfoById(req.params.id);
        if (result?.deletedCount === 0) {
            return res
                .status(ErrorStatus.DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        } else {
            next();
        }
    } catch (error) {
        prepareErrorLog(error, deleteUserByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

/**
 * UploadUserImageDataHandler
 */
const uploadUserImageDataHandler = async (req, res, next) => {
    try {
        const userCheck = await UserQueries.getUserInfoById(req.userFromToken._id);
        if (isEmpty(userCheck)) {
            return res
                .status(ErrorStatus.DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }
        await UserQueries.updateUserImageUrl(req.userFromToken._id, req.fileLocation, req.file.originalname);
        next();
    } catch (error) {
        prepareErrorLog(error, uploadUserImageDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const getUserInfoByTokenDataHandler = async (req, res, next) => {
    try {
        const userId = req.userFromToken._id;
        const user = await UserQueries.getUserInfoById(userId);
        if (isEmpty(user)) {
            return res
                .status(ErrorStatus.DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        } else {
            req.user = user;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, getUserInfoByTokenDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const getUsersBySearchPaginatedDataHandler = async (req, res, next) => {
    try {
        const list = await UserQueries.getAllUsersBySearch(req);
        const userId = req.userFromToken?._id;
        const getUser = await UserQueries.getUserInfoById(userId);
        if (!userId) {
            req.responseModel = list;
            next();
        } else {
            req.responseModel = list;
            req.responseModel.admin = getUser;
            next();
        }

    } catch (error) {
        prepareErrorLog(error, getUsersBySearchPaginatedDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

module.exports = {
    getUserByIdDataHandler,
    getAllUsersDataHandler,
    updateUserByIdDataHandler,
    changePasswordDataHandler,
    createUserDataHandler,
    deleteUserByIdDataHandler,
    uploadUserImageDataHandler,
    updateUserByIdFromAdminsDataHandler,
    getUserInfoByTokenDataHandler,
    updateUserProfileDataHandler,
    getUserByIdWithOrderDataHandler,
    getUsersBySearchPaginatedDataHandler
};
