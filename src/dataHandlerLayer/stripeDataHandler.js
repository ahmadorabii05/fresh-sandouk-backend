const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("./common/LanguageHeaderHandler");
const {prepareErrorLog} = require("../helperKit/loggingKit");
const UserQueries = require("../dataBaseLayer/queries/user/userQueries");
require("dotenv").config();
const Stripe = require('stripe');
const {
    getTotalAmountForStripe,
    getCartInfoForStripe,
    stripeValidationBeforeCheckout
} = require("../helperKit/helperKit");
const zlib = require("zlib");
const {isEmpty} = require("lodash");
const AddressQueries = require("../dataBaseLayer/queries/address/addressQueries");
const { checkIfPriceValidForStripe } = require("../helperKit/stripeKit");

const stripe = Stripe(process.env.STRIPE_KEY);

const createCheckoutSheetDataHandler = async (req, res, next) => {
    try {
        const userInfo = await UserQueries.getUserInfoById(req.userFromToken._id);
        const amount = await getTotalAmountForStripe(req, res);
        const userId = req.userFromToken._id;
        let addressId;
        const ephemeralKey = await stripe.ephemeralKeys.create(
            {customer: userInfo.info.general.stripe_id},
            {apiVersion: '2022-11-15'}
        );
        if (!checkIfPriceValidForStripe(amount.amount)) {
            prepareErrorLog('Your order amount must be atleast 150 AED', createCheckoutSheetDataHandler.name);
            return res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, 'Your order amount must be atleast 150 AED', extractLanguageFromRequest(req)));
        } else {
            const getAddress = await AddressQueries.getAddressByUser(userId);

            if (!isEmpty(getAddress)) {

                const getIfThereIsDefaultAddress = await AddressQueries.getIfIsDefaultExist(
                    userId
                );
                if (!isEmpty(getIfThereIsDefaultAddress)) {
                    // Ensure that req.requestModel is defined before setting properties on it
                    addressId = getIfThereIsDefaultAddress._id;
                } else {
                    return res
                        .status(ErrorStatus.YOU_SHOULD_CREATE_ADDRESS_FIRST.code)
                        .json(
                            prepareErrorResponse(
                                ErrorStatus.YOU_SHOULD_CREATE_ADDRESS_FIRST,
                                null,
                                extractLanguageFromRequest(req)
                            )
                        );
                }
            } else {
                return res
                    .status(ErrorStatus.ADDRESS_DOES_NOT_EXIST.code)
                    .json(
                        prepareErrorResponse(
                            ErrorStatus.ADDRESS_DOES_NOT_EXIST,
                            null,
                            extractLanguageFromRequest(req)
                        )
                    );
            }
            await stripeValidationBeforeCheckout(userId, req, res);
            const productInfo = await getCartInfoForStripe(req, res, userId);
            // const test = productInfo.productsInCart.map(item => {
            //     if (item?.name !== "undefined" || item?.name !== undefined || !isEmpty(item.name)) {
            //         return new ProductResponseModel(item, req);
            //     }
            // });
            // const finalAmount = productInfo.totalPrice === 0 ? 200 : productInfo.totalPrice
            // if (productInfo.totalPrice < 200) {
            //     return res
            //         .status(ErrorStatus.BAD_REQUEST.code)
            //         .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, 'Your order amount must be atleast 150 AED', extractLanguageFromRequest(req)));
            // } else {
                const jsonString = JSON.stringify(req.requestModel.products);
                const compressedData = zlib.deflateSync(jsonString).toString('base64');
                const paymentIntent = await stripe.paymentIntents.create({
                    amount: parseInt(amount.amount * 100),
                    currency: 'aed',
                    metadata: {
                        userId: userInfo._id.toString(), // here you can set the metadata
                        // requestData:req.requestModel
                        products: compressedData,
                        scheduledDate: req?.body?.scheduledDate,
                        scheduledTime: req?.body?.scheduledTime,
                        deliveryInstruction: req.body?.deliveryInstruction,
                        packingInstruction: req.body?.packingInstruction,
                        addressId: addressId.toString()
                    },
                    customer: userInfo.info.general.stripe_id,
                    // In the latest version of the API, specifying the `automatic_payment_methods` parameter is optional because Stripe enables its functionality by default.
                    automatic_payment_methods: {
                        enabled: true,
                    },
                });
                req.responseModel = {
                    paymentIntent: paymentIntent.client_secret,
                    ephemeralKey: ephemeralKey.secret,
                    customer: userInfo.info.general.stripe_id,
                    publishableKey: `${process.env.STRIPE_API}`
                }
            // }
        }
        next()
    } catch (error) {
        prepareErrorLog(error, createCheckoutSheetDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const createRefundDataHandler = async (req,res,next)=>{
    try {
        const orderId = req.requestModel.orderId;
                    // Refund the charge
                    await stripe.refunds.create({payment_intent: orderId}, (refundErr, refund) => {
                        if (refundErr) {
                            console.error(refundErr);
                        } else {
                            next()
                        }
                    });

    } catch (error) {
        prepareErrorLog(error, createRefundDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

module.exports = {
    createCheckoutSheetDataHandler,
    createRefundDataHandler
}
