const categoryQueries = require("../dataBaseLayer/queries/category/categoryQueries");
const UserQueries = require("../dataBaseLayer/queries/user/userQueries");
const SubCategoriesQueries = require("../dataBaseLayer/queries/subCategory/subCategoryQueries")
const { isEmpty } = require("lodash");
const { ErrorStatus } = require("../constants/error/errorStatus");
const { prepareErrorResponse } = require("../errorHandlerLayer/errorHandler");
const { extractLanguageFromRequest } = require("./common/LanguageHeaderHandler");
const { prepareErrorLog } = require("../helperKit/loggingKit");
const { deleteFileInAWS } = require("../helperKit/deleteFileFromAWSKit");
const path = require("path");
const { getIdsNotInArray, getFileStorePathForFile } = require("../helperKit/helperKit");
const { uploadFile } = require("../helperKit/uploadFile");
const { isAdminRole } = require("../services/userService");


const createCategoryDataHandler = async (req, res, next) => {
    try {
        const requestModel = req.requestModel;
        if (req.files.file?.length > 0) {
            if (getFileStorePathForFile(req.files.file[0].mimetype.split("/")[1])) {
                const category = await categoryQueries.createCategory(requestModel);
                const createdCategory = await category.save();
                const uploadedFile = await uploadFile(res, req.files.file[0], createdCategory._id, req.baseUrl);
                req.category = await categoryQueries.updateImageUrl(createdCategory._id, uploadedFile);
                // loop over banner images if exists and then update bannerImageUrls in db
                var categoryBannerFiles = []
                var index = 0;
                for (const bannerFile of req.files.bannerFiles) {
                    const uploadedBannerFile = await uploadFile(res, bannerFile, createdCategory._id, `${req.baseUrl}/banners`);
                    categoryBannerFiles.push({
                        actionUrl: req.body.bannerFilesActionUrl[index].actionUrl,
                        imageUrl: uploadedBannerFile
                    });
                    index++;
                }
                req.category = await categoryQueries.updateBanners(createdCategory._id, categoryBannerFiles);
                next();
            } else {
                return res
                    .status(ErrorStatus.ONLY_IMAGES.code)
                    .json(prepareErrorResponse(ErrorStatus.ONLY_IMAGES, null, extractLanguageFromRequest(req)));
            }
        } else {
            return res
                .status(ErrorStatus.FILE_MISSING.code)
                .json(prepareErrorResponse(ErrorStatus.FILE_MISSING, null, extractLanguageFromRequest(req)));
        }
    } catch (error) {
        prepareErrorLog(error, createCategoryDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


const createBulkCategoryDataHandler = async (req, res, next) => {
    try {
        const requestModel = req.requestModel;

        // Assuming requestModel contains an array of categories
        const categories = requestModel.category;

        // Iterate over the categories and create each one
        for (const categoryData of categories) {
            const subCategoryIds = [];

            if (categoryData?.subCategories?.length > 0) {
                // Iterate over subCategories and get their IDs by name
                for (const subCategory of categoryData.subCategories) {
                    // Assuming you have a function to retrieve subCategory by name
                    const subCategoryInfo = await SubCategoriesQueries.getSubCategoryByName(subCategory.name);

                    if (subCategoryInfo && subCategoryInfo._id) {
                        subCategoryIds.push(subCategoryInfo._id);
                    } else {
                        return res
                            .status(ErrorStatus.SUB_CATEGORY_DOESNT_EXIST.code)
                            .json(prepareErrorResponse(ErrorStatus.SUB_CATEGORY_DOESNT_EXIST, subCategory.name, extractLanguageFromRequest(req)));
                    }
                }
            }
            const newCategory = {
                name: categoryData.name,
                nameAr: categoryData.nameAr,
                priority: categoryData.priority,
                color: categoryData.color,
                imageUrl: "",
                subCategories: subCategoryIds,
            };
            await categoryQueries.createCategory(newCategory);
        }

        next();
    } catch (error) {
        prepareErrorLog(error, createBulkCategoryDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}


const getAllCategoryDataHandler = async (req, res, next) => {
    try {
        let getUser;
        const isAdmin = isAdminRole(req.userFromToken);
        if (req.userFromToken?._id) {
            getUser = await UserQueries.getUserInfoById(req.userFromToken?._id)
        }
        if (req.header('client') == 'admin' || isAdminRole(getUser)) {
            req.responseModel = await categoryQueries.getAllCategories(req);
            req.responseModel.admin = await UserQueries.getUserInfoById(req.userFromToken?._id)
        } else {
            req.responseModel = await categoryQueries.getAllCategoriesPaginated(req, isAdmin);
        }

        next();

    } catch (error) {
        prepareErrorLog(error, getAllCategoryDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }

}

const getCategoryByIdDataHandler = async (req, res, next) => {
    try {
        const id = req.params.id;
        const isAdmin = isAdminRole(req.userFromToken);
        if (req.userFromToken?._id) {
            req.requestModel = await categoryQueries.getCategoryById(id, isAdmin);
            req.admin = await UserQueries.getUserInfoById(req.userFromToken?._id);
        } else {
            req.requestModel = await categoryQueries.getCategoryById(id, isAdmin);
        }
        next();
    } catch (error) {
        prepareErrorLog(error, getCategoryByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const deleteCategoryByIdsDataHandler = async (req, res, next) => {
    try {
        const categoryIds = req.requestModel.categoryIds;

        for (const item of categoryIds) {
            const productIds = await categoryQueries.getCategoryByIdInfo(item);
            if (isEmpty(productIds)) {
                return res
                    .status(ErrorStatus.CATEGORY_DOESNT_EXIST.code)
                    .json(
                        prepareErrorResponse(
                            ErrorStatus.CATEGORY_DOESNT_EXIST,
                            null,
                            extractLanguageFromRequest(req)
                        )
                    );
            }
            const imageUrl = path.basename(productIds.imageUrl);
            if (productIds.subCategories.length > 0) {
                return res.status(ErrorStatus.CATEGORY_HAS_SUBCATEGORY.code).json(
                    prepareErrorResponse(
                        ErrorStatus.CATEGORY_HAS_SUBCATEGORY,
                        `Cannot Delete ${productIds.name}, since it has SubCategories`,
                        extractLanguageFromRequest(req)
                    )
                );
            }
            const timestampSegment = productIds.imageUrl.match(/\/(\d+)\//);
            if (timestampSegment && timestampSegment.length >= 2) {
                const timestampString = timestampSegment[1];
                await deleteFileInAWS(`kibson${req.baseUrl}`, timestampString + "/" + imageUrl);
                await categoryQueries.deleteCategoryById(item);
            }

        }

        next();
    } catch (error) {
        prepareErrorLog(error, deleteCategoryByIdsDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(
                prepareErrorResponse(
                    ErrorStatus.SERVER_DOWN,
                    null,
                    extractLanguageFromRequest(req)
                )
            );
    }
};

const deleteCategoryByIdDataHandler = async (req, res, next) => {
    try {
        const categoryId = req.params?.id;
        const productIds = await categoryQueries.getCategoryByIdInfo(categoryId);
        if (isEmpty(productIds)) {
            return res
                .status(ErrorStatus.CATEGORY_DOESNT_EXIST.code)
                .json(
                    prepareErrorResponse(
                        ErrorStatus.CATEGORY_DOESNT_EXIST,
                        null,
                        extractLanguageFromRequest(req)
                    )
                );
        }
        const imageUrl = path.basename(productIds.imageUrl);

        if (productIds.subCategories.length > 0) {
            return res.status(ErrorStatus.CATEGORY_HAS_SUBCATEGORY.code).json(
                prepareErrorResponse(
                    ErrorStatus.CATEGORY_HAS_SUBCATEGORY,
                    `Cannot Delete ${productIds.name}, since it has SubCategories`,
                    extractLanguageFromRequest(req)
                )
            );
        }

        const timestampSegment = productIds.imageUrl.match(/\/(\d+)\//);
        if (timestampSegment && timestampSegment.length >= 2) {
            const timestampString = timestampSegment[1];
            await deleteFileInAWS(`kibson${req.baseUrl}`, timestampString + "/" + imageUrl);
        }
        await categoryQueries.deleteCategoryById(categoryId);
        next();
    } catch (error) {
        prepareErrorLog(error, deleteCategoryByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(
                prepareErrorResponse(
                    ErrorStatus.SERVER_DOWN,
                    null,
                    extractLanguageFromRequest(req)
                )
            );
    }
}

const updateCategoryByIdDataHandler = async (req, res, next) => {
    try {
        const id = req.params.id;
        req.requestModel.imageUrl = req.fileLocation;
        const currentCategory = await categoryQueries.getCategoryById(id);
        if (isEmpty(currentCategory)) {
            return res
                .status(ErrorStatus.CATEGORY_DOESNT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.CATEGORY_DOESNT_EXIST, null, extractLanguageFromRequest(req)));
        }
        const subCategoryFromCurrentCategory = currentCategory?.subCategories;
        if (req.files.file?.length > 0) {
            if (getFileStorePathForFile(req.files.file[0].mimetype.split("/")[1])) {
                const imageUrl = path.basename(currentCategory.imageUrl);

                const timestampSegment = currentCategory.imageUrl.match(/\/(\d+)\//);
                if (timestampSegment && timestampSegment.length >= 2) {
                    const timestampString = timestampSegment[1];
                    await deleteFileInAWS(`kibson${req.baseUrl}`, timestampString + "/" + imageUrl);
                }
                req.requestModel.imageUrl = await uploadFile(res, req.files.file[0], id, `${req.baseUrl}`);
            } else {
                return res
                    .status(ErrorStatus.ONLY_IMAGES.code)
                    .json(prepareErrorResponse(ErrorStatus.ONLY_IMAGES, null, extractLanguageFromRequest(req)));
            }
        }

        if (req.files.bannerFiles?.length > 0) {
            var categoryBannerFiles = []
            var index = 0;
            for (const bannerFile of req.files.bannerFiles) {
                const uploadedBannerFile = await uploadFile(res, bannerFile, id, `${req.baseUrl}/banners`);
                categoryBannerFiles.push({
                    actionUrl: req.body.bannerFilesActionUrl[index].actionUrl,
                    imageUrl: uploadedBannerFile
                });
                index++;
            }
            req.requestModel.bannerImageUrls = await categoryQueries.updateBanners(id, categoryBannerFiles);
        } else {
            if (req.body.bannerFilesActionUrl?.length > 0) {
                // must not reset (the update was done on other properties)
            } else {
                // reset banners
                req.requestModel.bannerImageUrls = await categoryQueries.updateBanners(id, []);
            }
        }

        if (req.requestModel?.subCategories.length > 0) {
            req.requestModel.subCategories = await getIdsNotInArray(req.requestModel.subCategories, subCategoryFromCurrentCategory);
            for (const item of req.requestModel.subCategories) {
                await categoryQueries.removeSubCategory(item);
            }
        }
        if (req.requestModel?.subCategories === "undefined" || req.requestModel?.subCategories?.length === 0) {
            req.requestModel.subCategories = []
        }
        req.category = await categoryQueries.updateCategoryById(id, req.requestModel)
        next();


    } catch (error) {
        prepareErrorLog(error, updateCategoryByIdDataHandler.name);
        return res.status(ErrorStatus.SERVER_DOWN.code).json(
            prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req))
        );
    }
}

const getCategoryBySearchPaginatedDataHandler = async (req, res, next) => {
    try {
        const isAdmin = isAdminRole(req.userFromToken);
        const list = await categoryQueries.getAllCategoriesBySearch(req, isAdmin);
        const userId = req.userFromToken?._id;
        const getUser = await UserQueries.getUserInfoById(userId);
        if (!userId) {
            req.responseModel = list;
            next();
        } else {
            req.responseModel = list;
            req.responseModel.admin = getUser;
            next();
        }

    } catch (error) {
        prepareErrorLog(error, getCategoryBySearchPaginatedDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

module.exports = {
    createCategoryDataHandler,
    getAllCategoryDataHandler,
    getCategoryByIdDataHandler,
    deleteCategoryByIdsDataHandler,
    updateCategoryByIdDataHandler,
    deleteCategoryByIdDataHandler,
    createBulkCategoryDataHandler,
    getCategoryBySearchPaginatedDataHandler
}