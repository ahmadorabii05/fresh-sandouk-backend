const {prepareErrorLog} = require("../helperKit/loggingKit");
const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("./common/LanguageHeaderHandler");
const {countryInfo} = require("../../country");
const CountryQueries = require("../dataBaseLayer/queries/country/countryQueries")

const uploadCountriesDataHandler = async (req, res, next) => {
    try {
        const info = countryInfo();
        await Promise.all(
            info.map(async item => {
                const name = {
                    code: item.code,
                    country_en: item.country_en,
                    country_ar: item.country_ar
                };
                await CountryQueries.createCountry(name);
            })
        );
        next();
    } catch (error) {
        prepareErrorLog(error, uploadCountriesDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(
                prepareErrorResponse(
                    ErrorStatus.SERVER_DOWN,
                    null,
                    extractLanguageFromRequest(req)
                )
            );
    }
};


const getAllCountriesDataHandler = async (req,res,next)=>{
    try {
        req.responseModel=await CountryQueries.getAllCountries();
        next();
    }catch (error) {
        prepareErrorLog(error, getAllCountriesDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(
                prepareErrorResponse(
                    ErrorStatus.SERVER_DOWN,
                    null,
                    extractLanguageFromRequest(req)
                )
            );
    }
}

module.exports = {
    uploadCountriesDataHandler,
    getAllCountriesDataHandler
}