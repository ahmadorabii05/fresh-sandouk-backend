const {prepareErrorLog} = require("../helperKit/loggingKit");
const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("./common/LanguageHeaderHandler");
const tagsQueries = require("../dataBaseLayer/queries/tags/tagsQueries");
const productQueries = require("../dataBaseLayer/queries/product/productQueries")
const UserQueries = require("../dataBaseLayer/queries/user/userQueries");
const {isEmpty} = require("lodash");
const {
    handleSpecialCharsInFileName,
    getFileStorePathForFile,

} = require("../helperKit/helperKit");
const {uploadFile} = require("../helperKit/uploadFile");
const path = require("path");
const {deleteFileInAWS} = require("../helperKit/deleteFileFromAWSKit");

const createTagsDataHandler = async (req, res, next) => {
    try {
        if (req.file) {
            if (!handleSpecialCharsInFileName(req.file)) {
                if (getFileStorePathForFile(req.file.mimetype.split("/")[1])) {
                    const tags = await tagsQueries.createTag(req.requestModel);
                    const createdTags = await tags.save();
                    const uploadedFile = await uploadFile(res, req.file, createdTags._id, req.baseUrl);
                    req.responseModel = await tagsQueries.updateImageUrl(createdTags._id, uploadedFile);
                    next();
                } else {
                    return res
                        .status(ErrorStatus.ONLY_IMAGES.code)
                        .json(prepareErrorResponse(ErrorStatus.ONLY_IMAGES, null, extractLanguageFromRequest(req)));
                }
            } else {
                return res
                    .status(ErrorStatus.FILE_HAS_SPECIAL_CHARACTERS.code)
                    .json(prepareErrorResponse(ErrorStatus.FILE_HAS_SPECIAL_CHARACTERS, null, extractLanguageFromRequest(req)));
            }
        } else {
            return res
                .status(ErrorStatus.FILE_MISSING.code)
                .json(prepareErrorResponse(ErrorStatus.FILE_MISSING, null, extractLanguageFromRequest(req)));
        }
    } catch (error) {
        prepareErrorLog(error, createTagsDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}


const createBulkTagsDataHandler = async (req, res, next) => {
    try {

        const tags = req.requestModel.tags;

        for (const tagsData of tags) {
            tagsData.name = tagsData.name.trim().toUpperCase();
            tagsData.nameAr= tagsData?.nameAr || "";
            tagsData.imageUrl=[];
            await tagsQueries.createTag(tagsData);
        }
        next();

    } catch (error) {
        prepareErrorLog(error, createBulkTagsDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const getTagsDataHandler = async (req, res, next) => {
    try {
        const {id} = req.params;
        const tagInfo = await tagsQueries.getTagsInfoById(id);
        if (isEmpty(tagInfo)) {
            return res
                .status(ErrorStatus.TAG_DOESNT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.TAG_DOESNT_EXIST, null, extractLanguageFromRequest(req)));
        }
        req.responseModel = tagInfo;
        next();
    } catch (error) {
        prepareErrorLog(error, getTagsDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const getAllTagsDataHandler = async (req, res, next) => {
    try {
        req.responseModel = await tagsQueries.getAllTagsPaginated(req);
        next();
    } catch (error) {
        prepareErrorLog(error, getAllTagsDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const updateTagsDataHandler = async (req, res, next) => {
    try {
        const {id} = req.params;

        const tagInfo = await tagsQueries.getTagsInfoById(id);

        if (isEmpty(tagInfo)) {
            return res
                .status(ErrorStatus.TAG_DOESNT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.TAG_DOESNT_EXIST, null, extractLanguageFromRequest(req)));
        }
        if (req.file) {
            if (!handleSpecialCharsInFileName(req.file)) {
                if (getFileStorePathForFile(req.file.mimetype.split("/")[1])) {
                    const imageUrl = path.basename(tagInfo.imageUrl);
                    await deleteFileInAWS(`kibson${req.baseUrl}`, imageUrl);
                    req.requestModel.imageUrl = await uploadFile(res, req.file, id, `${req.baseUrl}`);
                } else {
                    return res
                        .status(ErrorStatus.ONLY_IMAGES.code)
                        .json(prepareErrorResponse(ErrorStatus.ONLY_IMAGES, null, extractLanguageFromRequest(req)));
                }
            }
        }
        req.responseModel = await tagsQueries.updateUserInfoById(id, req.requestModel);
        next();
    } catch (error) {
        prepareErrorLog(error, updateTagsDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}


const deleteTagsByIdDataHandler = async (req, res, next) => {
    try {
        const {tagIds} = req.requestModel;

        for (const item of tagIds) {
            const getTagInfo = await tagsQueries.getTagsInfoById(item);
            const imageUrl = path.basename(getTagInfo.imageUrl);
            const timestampSegment = getTagInfo.imageUrl.match(/\/(\d+)\//);
            if (timestampSegment && timestampSegment.length >= 2) {
                const timestampString = timestampSegment[1];
                await deleteFileInAWS(`kibson${req.baseUrl}`, timestampString + "/" + imageUrl);
            }
            await productQueries.deleteTag(item);
            await tagsQueries.deleteTagById(item);
        }
        next();
    } catch (error) {
        prepareErrorLog(error, deleteTagsByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const deleteSingleTagDataHandler = async (req, res, next) => {
    try {
        const {id} = req.params;
        const getTagInfo = await tagsQueries.getTagsInfoById(id);
        const imageUrl = path.basename(getTagInfo.imageUrl);

        const timestampSegment = getTagInfo.imageUrl.match(/\/(\d+)\//);
        if (timestampSegment && timestampSegment.length >= 2) {
            const timestampString = timestampSegment[1];
            await deleteFileInAWS(`kibson${req.baseUrl}`, timestampString + "/" + imageUrl);
        }
        await productQueries.deleteTag(id);
        await tagsQueries.deleteTagById(id);
        next();
    } catch (error) {
        prepareErrorLog(error, deleteSingleTagDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const getTagsBySearchPaginatedDataHandler = async (req, res, next) => {
    try {
        const list = await tagsQueries.getAllTagsBySearch(req);
        const userId = req.userFromToken?._id;
        const getUser = await UserQueries.getUserInfoById(userId);
        if (!userId) {
            req.responseModel = list;
            next();
        } else {
            req.responseModel = list;
            req.responseModel.admin = getUser;
            next();
        }

    } catch (error) {
        prepareErrorLog(error, getTagsBySearchPaginatedDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

module.exports = {
    createTagsDataHandler,
    getTagsDataHandler,
    getAllTagsDataHandler,
    updateTagsDataHandler,
    deleteTagsByIdDataHandler,
    deleteSingleTagDataHandler,
    createBulkTagsDataHandler,
    getTagsBySearchPaginatedDataHandler
}