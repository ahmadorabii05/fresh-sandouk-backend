const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("./common/LanguageHeaderHandler");
const {prepareErrorLog} = require("../helperKit/loggingKit");
const unzipper = require('unzipper'); // You need to install this package via npm or yarn
const ProductQueries = require("../dataBaseLayer/queries/product/productQueries")
const CategoryQueries = require("../dataBaseLayer/queries/category/categoryQueries");
const SubCategoryQueries = require("../dataBaseLayer/queries/subCategory/subCategoryQueries");
const {isEmpty} = require("lodash");
const ImageQueries = require("../dataBaseLayer/queries/image/imageQueries");
const { uploadFileViaZipFile, uploadFile} = require("../helperKit/uploadFile");
const uploadZipFileDataHandler = async (req, res, next) => {
    try {
        // console.log(req.requestModel);
        if (req.requestModel?.rejectedImages?.length > 0) {
            req.requestModel.rejected = req.requestModel.rejectedImages
            next();
        } else {
            req.requestModel.rejected = [];
            const zipFileBuffer = req.file.buffer; // Contains the zip file data as a Buffer
            let imageFileNames = [];
            unzipper.Open.buffer(zipFileBuffer)
                .then(async (d) => {
                    console.log(d);
                    for (const file of d.files) {
                        const pathParts = file.path.split('/');
                        // Check if the file is an image (modify the list of image extensions as needed)
                        imageFileNames.push({
                                name: pathParts[pathParts.length - 1],
                                buffer: await file.buffer()
                            }
                        )
                    }

                    // Extract image file names without folder structure
                    // const imageFileNames = d.files.map(async (imageFile) => {
                    //     // Split the path by '/' and get the last part (the file name)
                    //     const pathParts = imageFile.path.split('/');
                    //     return {
                    //         name: pathParts[pathParts.length - 1],
                    //         buffer: await imageFile.buffer()
                    //     }
                    // });
                    //check if the image name same as the product
                    var counter = 0
                    for (const image of imageFileNames) {
                        const lastDotIndex = image.name.lastIndexOf('.');


                        // Use the part of the string before the last dot as the new filename
                        const newFileName = image.name.substring(0, lastDotIndex);
                        const getProductName = await ProductQueries.getProductByNameIgnoreCases(newFileName)
                        // const getProductName = await ProductQueries.getProductByNameIgnoreCases(newFileName)
                        // const getSubCategory = await SubCategoryQueries.getSubCategoryByNameIgnoreCases(newFileName)
                        if (isEmpty(getProductName)) {
                            console.log("There is no product named " + newFileName);
                            // return res
                            //     .status(ErrorStatus.SERVER_DOWN.code)
                            //     .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, "There is no product named " + image.name, extractLanguageFromRequest(req)));
                        } else {
                            // const category = await CategoryQueries.createCategory(requestModel);
                            // const createdCategory = await category.save();
                            // console.log(getSubCategory, "i am here")
                            // counter++
                            // const uploadedFile = await uploadFileViaZipFile(req, res, image, getSubCategory._id, "kibson/subCategories");
                            // await SubCategoryQueries.updateImageUrl(getSubCategory._id, uploadedFile);
                            const savedImage = await ImageQueries.createImage({productId: getProductName._id});
                            const fileName = savedImage._id;
                            savedImage.imageUrl = await uploadFileViaZipFile(req, res, image, fileName, "kibson/products");
                            await savedImage.save();
                            await ProductQueries.updateImageUrlFromZip(getProductName._id, savedImage._id)
                        }
                    }
                })
                .catch((err) => {
                    console.error('Error processing zip file:', err);
                    return res
                        .status(ErrorStatus.SERVER_DOWN.code)
                        .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, "Error processing the zip file.", extractLanguageFromRequest(req)));
                }).finally(() => {
                // Call next() here, inside the .finally() block to ensure it's executed after processing is complete.
                next()
            });

        }
    } catch (error) {
        prepareErrorLog(error, uploadZipFileDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

module.exports = {
    uploadZipFileDataHandler
}