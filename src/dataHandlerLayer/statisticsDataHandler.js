const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("./common/LanguageHeaderHandler");
const {prepareErrorLog} = require("../helperKit/loggingKit");
const OrderQueries = require("../dataBaseLayer/queries/order/OrderQueries");
const SalesQueries = require("../dataBaseLayer/queries/stock/stockQueries");
const UsersQueries = require("../dataBaseLayer/queries/user/userQueries");
const RoleQueries = require("../dataBaseLayer/queries/role/roleQueries")
const ProductQueries = require("../dataBaseLayer/queries/product/productQueries");

const getStatisticsDataHandler = async (req, res, next) => {
    try {
        const {dateFrom, dateTo} = req.requestModel;

        // update all product discountValues that are null to ''
        // used for cleanup
        await ProductQueries.updateAllNullDiscountValuesToEmpty();

        //get all orders
        const totalOrders = await OrderQueries.getOrderCount(dateFrom, dateTo);
        // //get all sales
        // const totalSales = await SalesQueries.getTotalSales(dateFrom, dateTo);
        //get the role Id for the role user
        const roleId = await RoleQueries.getRoleByName("user");
        //get number of users
        const totalUsers = await UsersQueries.getTotalUsers(roleId._id);
        req.responseModel={
            totalSales: 0,
            totalOrders:totalOrders,
            totalUsers:totalUsers
        }
        next();
    } catch (error) {
        prepareErrorLog(error, getStatisticsDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

module.exports = {
    getStatisticsDataHandler
}