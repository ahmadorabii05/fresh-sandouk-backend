const productQueries = require("../dataBaseLayer/queries/product/productQueries");
const StockQueries = require("../dataBaseLayer/queries/stock/stockQueries")
const TagsQueries = require("../dataBaseLayer/queries/tags/tagsQueries")
const {isEmpty} = require("lodash");
const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("./common/LanguageHeaderHandler");
const {
    setFavoriteProductFlag,
    setFavoriteProductFlagById
} = require("../helperKit/helperKit");
const {uploadFile} = require("../helperKit/uploadFile");
const {prepareErrorLog} = require("../helperKit/loggingKit");
const subCategoryQueries = require("../dataBaseLayer/queries/subCategory/subCategoryQueries");
const userQueries = require("../dataBaseLayer/queries/user/userQueries");
const {deleteFileInAWS} = require("../helperKit/deleteFileFromAWSKit");
const path = require("path");
const ImageQueries = require("../dataBaseLayer/queries/image/imageQueries");
const BrandQueries = require("../dataBaseLayer/queries/brand/brandQueries")
const CountryQueries = require("../dataBaseLayer/queries/country/countryQueries");
const PriceModelQueries = require("../dataBaseLayer/queries/price/priceQueries")
const {isAdminRole} = require("../services/userService");
const {CreateProductRequestModel} = require("./models/requestModel/product/CreateProductRequestModel");
const {CreateBulkProductDetailsRequestModel} = require("./models/requestModel/product/CreateBulkProductDetailsRequestModel");

const createProductDataHandler = async (req, res, next) => {
    try {
        const requestModel = req.requestModel;
        const isAdmin = isAdminRole(req.userFromToken);
        const checkSKU = await productQueries.getProductBySKU(requestModel.sku);

        if (!isEmpty(checkSKU)) {
            return res
                .status(ErrorStatus.SKU_ALREADY_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.SKU_ALREADY_EXIST, null, extractLanguageFromRequest(req)));
        }
        //check if the brand Exist
        const checkBrand = await BrandQueries.getBrandById(requestModel?.brandId);

        if (isEmpty(checkBrand)) {
            return res
                .status(ErrorStatus.BRAND_DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.BRAND_DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }
        if (requestModel.nameAr) {
            await productQueries.getProductByNameAr(requestModel.nameAr);
        }

        const subCategoryInfo = await subCategoryQueries.getSubCategoryById(requestModel.subCategoryId);

        if (isEmpty(subCategoryInfo)) {
            return res
                .status(ErrorStatus.SUB_CATEGORY_DOESNT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.SUB_CATEGORY_DOESNT_EXIST, null, extractLanguageFromRequest(req)));
        }


        const product = await productQueries.createProduct(requestModel);
        await StockQueries.createProductInStock({
            productId: product._id,
            quantity: requestModel.quantity,
            quantitySold: 0
        })
        const createdProduct = await product.save();
        const imageUrls = []; // Initialize an array to store the file URLs
        for (let i = 0; i < req.files.length; i++) {
            const file = req.files[i];
            const savedImage = await ImageQueries.createImage({productId: createdProduct._id});
            const fileName = savedImage._id;
            savedImage.imageUrl = await uploadFile(res, file, fileName, req.baseUrl);
            await savedImage.save();
            imageUrls.push(savedImage._id); // Save the file URL to the imageUrls array
        }

        createdProduct.imageUrls = imageUrls; // Assign the imageUrls array to the createdProduct.imageUrls
        await createdProduct.save(); // Save the updated createdProduct with the associated file URLs
        await subCategoryQueries.addProduct(requestModel.subCategoryId, createdProduct._id);

        req.responseModel = await productQueries.getProductById(createdProduct._id, isAdmin);
        next();
    } catch (error) {
        prepareErrorLog(error, createProductDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const createBulkProductDataHandler = async (req, res, next) => {
    try {
        const products = req.requestModel.products;
        const tagsIds = [];

        for (const productsData of products) {
            const productRequestModel = new CreateBulkProductDetailsRequestModel(productsData);
            const newProduct = new CreateProductRequestModel({});
            const checkSKU = await productQueries.getProductBySKU(productRequestModel.sku);
            if (!isEmpty(checkSKU)) {
                return res
                    .status(ErrorStatus.SKU_ALREADY_EXIST.code)
                    .json(prepareErrorResponse(ErrorStatus.SKU_ALREADY_EXIST, productRequestModel.sku + " already exist", extractLanguageFromRequest(req)));
            }

            const checkPriceModel = await PriceModelQueries.getPriceByUnit(productRequestModel?.priceModelName);
            if (isEmpty(checkPriceModel)) {
                return res
                    .status(ErrorStatus.PRICE_MODEL_DOES_NOT_EXIST.code)
                    .json(prepareErrorResponse(ErrorStatus.PRICE_MODEL_DOES_NOT_EXIST, productRequestModel.priceModelName + " does not exist", extractLanguageFromRequest(req)));
            } else {
                newProduct.priceModel = checkPriceModel._id;
            }
            //check if the brand Exist
            const checkBrandName = await BrandQueries.getBrandSourceName(productRequestModel?.brandName);

            if (isEmpty(checkBrandName)) {
                return res
                    .status(ErrorStatus.BRAND_DOES_NOT_EXIST.code)
                    .json(prepareErrorResponse(ErrorStatus.BRAND_DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
            } else {
                newProduct.brandId = checkBrandName._id;
            }
            const subCategoryInfo = await subCategoryQueries.getSubCategoryName(productRequestModel?.subCategoryName);

            if (isEmpty(subCategoryInfo)) {
                return res
                    .status(ErrorStatus.SUB_CATEGORY_DOESNT_EXIST.code)
                    .json(prepareErrorResponse(ErrorStatus.SUB_CATEGORY_DOESNT_EXIST, null, extractLanguageFromRequest(req)));
            } else {
                newProduct.subCategoryId = subCategoryInfo._id;
            }
            const countryDetails = await CountryQueries.getCountryByName(productRequestModel.country);

            if (isEmpty(countryDetails)) {
                return res
                    .status(ErrorStatus.COUNTRY_NAME_DOES_NOT_EXIST.code)
                    .json(prepareErrorResponse(ErrorStatus.COUNTRY_NAME_DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
            } else {
                newProduct.country = countryDetails._id;
            }

            if (productRequestModel?.tagsName) {
                for (const tags of productRequestModel.tagsName) {
                    const getTagsDetails = await TagsQueries.getTagsInfoByName(tags);
                    if (isEmpty(getTagsDetails)) {
                        return res
                            .status(ErrorStatus.TAG_DOESNT_EXIST.code)
                            .json(prepareErrorResponse(ErrorStatus.TAG_DOESNT_EXIST, null, extractLanguageFromRequest(req)));
                    } else {
                        tagsIds.push(getTagsDetails._id);
                    }
                }
            }
            newProduct.name = productRequestModel.name;
            newProduct.price = productRequestModel.price;
            newProduct.description = productRequestModel.description;
            newProduct.sku = productRequestModel.sku;
            newProduct.discountValue = productRequestModel.discountValue;
            newProduct.generalDescription = productRequestModel.generalDescription;
            newProduct.ingredient = productRequestModel.ingredient;
            newProduct.storage = productRequestModel.storage;
            newProduct.nameAr = productRequestModel.nameAr;
            newProduct.generalDescriptionAr = productRequestModel.generalDescriptionAr;
            newProduct.ingredientAr = productRequestModel.ingredientAr;
            newProduct.storageAr = productRequestModel.storageAr;
            newProduct.descriptionAr = productRequestModel.descriptionAr;
            newProduct.nutrition = productRequestModel.nutrition;
            newProduct.quantity = productRequestModel.quantity;
            newProduct.tags = tagsIds;
            newProduct.imageUrls = [];
            const product = await productQueries.createProduct(newProduct);
            await StockQueries.createProductInStock({
                productId: product._id,
                quantity: newProduct.quantity,
                quantitySold: 0
            })
            const createdProduct = await product.save();
            await subCategoryQueries.addProduct(newProduct.subCategoryId, createdProduct._id);

        }

        next();
    } catch (error) {
        prepareErrorLog(error, createBulkProductDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const deleteNutritionInProductDataHandler = async (req, res, next) => {
    try {
        const {productId, nutritionId} = req.requestModel;

        const isAdmin = isAdminRole(req.userFromToken);
        const product = await productQueries.getProductByIdInfo(id, isAdmin);
        if (isEmpty(product)) {
            return res
                .status(ErrorStatus.PRODUCT_DOESNT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.PRODUCT_DOESNT_EXIST, null, extractLanguageFromRequest(req)));
        }

        req.responseModel = await productQueries.removeNutrition(productId, nutritionId);
        next()

    } catch (error) {
        prepareErrorLog(error, deleteNutritionInProductDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const deleteProductByIdsDataHandler = async (req, res, next) => {
    try {
        const {productIds} = req.requestModel;
        const isAdmin = isAdminRole(req.userFromToken);
        for (const item of productIds) {
            const productInfo = await productQueries.getProductByIdInfo(item, isAdmin);
            if (isEmpty(productInfo)) {
                return res
                    .status(ErrorStatus.PRODUCT_DOESNT_EXIST.code)
                    .json(prepareErrorResponse(ErrorStatus.PRODUCT_DOESNT_EXIST, null, extractLanguageFromRequest(req)));
            }
            const imageUrls = productInfo.imageUrls;
            for (const items of imageUrls) {
                const imageUrl = path.basename(items.imageUrl);
                const timestampSegment = items.imageUrl.match(/\/(\d+)\//);
                if (timestampSegment && timestampSegment.length >= 2) {
                    const timestampString = timestampSegment[1];
                    await deleteFileInAWS(`kibson${req.baseUrl}`, timestampString + "/" + imageUrl);
                }
            }
            await productQueries.deleteProductById(item);
            await subCategoryQueries.removeProductFromAllSubCategories(item);
        }
        next();

    } catch (error) {
        prepareErrorLog(error, deleteProductByIdsDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const deleteProductByIdDataHandler = async (req, res, next) => {
    try {
        const productId = req.params?.id;
        const isAdmin = isAdminRole(req.userFromToken);
        const productInfo = await productQueries.getProductByIdInfo(productId, isAdmin);
        if (isEmpty(productInfo)) {
            return res
                .status(ErrorStatus.PRODUCT_DOESNT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.PRODUCT_DOESNT_EXIST, null, extractLanguageFromRequest(req)));
        }
        const imageUrls = productInfo.imageUrls;
        for (const item of imageUrls) {
            const imageUrl = path.basename(item.imageUrl);
            const timestampSegment = item.imageUrl.match(/\/(\d+)\//);
            if (timestampSegment && timestampSegment.length >= 2) {
                const timestampString = timestampSegment[1];
                await deleteFileInAWS(`kibson${req.baseUrl}`, timestampString + "/" + imageUrl);
            }
        }
        await productQueries.deleteProductById(productId);
        await subCategoryQueries.removeProductFromAllSubCategories(productId);
        next();
    } catch (error) {
        prepareErrorLog(error, deleteProductByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const forceDeleteProductByIdDataHandler = async (req, res, next) => {
    try {
        const productId = req.params?.id;
        const isAdmin = isAdminRole(req.userFromToken);
        const productInfo = await productQueries.getProductByIdInfo(productId, isAdmin);
        if (isEmpty(productInfo)) {
            return res
                .status(ErrorStatus.PRODUCT_DOESNT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.PRODUCT_DOESNT_EXIST, null, extractLanguageFromRequest(req)));
        }
        const imageUrls = productInfo.imageUrls;
        for (const item of imageUrls) {
            const imageUrl = path.basename(item.imageUrl);
            const timestampSegment = item.imageUrl.match(/\/(\d+)\//);
            if (timestampSegment && timestampSegment.length >= 2) {
                const timestampString = timestampSegment[1];
                await deleteFileInAWS(`kibson${req.baseUrl}`, timestampString + "/" + imageUrl);
            }
        }
        await productQueries.forceDeleteProductById(productId);
        await subCategoryQueries.removeProductFromAllSubCategories(productId);
        next();
    } catch (error) {
        prepareErrorLog(error, deleteProductByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const updateProductByIdDataHandler = async (req, res, next) => {
    try {
        const {id} = req.params;
        const subCategoryId = req.requestModel?.subCategoryId;
        const isAdmin = isAdminRole(req.userFromToken);
        const productInfo = await productQueries.getProductByIdInfo(id, isAdmin);

        if (isEmpty(productInfo)) {
            return res
                .status(ErrorStatus.PRODUCT_DOESNT_EXIST.code)
                .json(
                    prepareErrorResponse(
                        ErrorStatus.PRODUCT_DOESNT_EXIST,
                        null,
                        extractLanguageFromRequest(req)
                    )
                );
        }

        if (req.requestModel?.sku) {
            const checkSKU = await productQueries.getProductBySKU(req.requestModel?.sku);
            if (!isEmpty(checkSKU)) {
                if (checkSKU.sku === productInfo.sku) {
                    delete req.requestModel.sku;
                } else {
                    return res
                        .status(ErrorStatus.SKU_ALREADY_EXIST.code)
                        .json(
                            prepareErrorResponse(
                                ErrorStatus.SKU_ALREADY_EXIST,
                                null,
                                extractLanguageFromRequest(req)
                            )
                        );
                }
            }
        }

        if (!isEmpty(subCategoryId)) {
            const subCategoryInfo = await subCategoryQueries.getSubCategoryById(
                subCategoryId
            );
            if (isEmpty(subCategoryInfo)) {
                res
                    .status(ErrorStatus.SUB_CATEGORY_DOESNT_EXIST.code)
                    .json(
                        prepareErrorResponse(
                            ErrorStatus.SUB_CATEGORY_DOESNT_EXIST,
                            null,
                            extractLanguageFromRequest(req)
                        )
                    );
            }
            if (subCategoryInfo.productIds.indexOf(id) === -1) {
                await subCategoryQueries.removeProductFromAllSubCategories(id);
                await subCategoryQueries.addProduct(subCategoryId, id);
            }
        }

        // await checkProductNames(id, productInfo?.name, productInfo?.nameAr, req, res);
        if (req.requestModel?.quantity) {
            await StockQueries.updateProductQuantityFromProductById(id, req.requestModel?.quantity)
        }

        if (req.files.length > 0) {
            const imageUrls = []; // Initialize an array to store the file URLs
            for (const file of req.files) {
                const savedImage = await ImageQueries.createImage({productId: id});
                const fileName = savedImage._id;
                savedImage.imageUrl = await uploadFile(
                    res,
                    file,
                    fileName,
                    req.baseUrl
                );
                await savedImage.save();
                imageUrls.push(savedImage._id); // Save the file URL to the imageUrls array
            }

            req.requestModel.imageUrls = imageUrls; // Assign the imageUrls array to the createdProduct.imageUrls
            for (const imageUrl of productInfo.imageUrls) {
                await ImageQueries.deleteImageById(imageUrl._id);
                const filename = path.basename(imageUrl.imageUrl);
                const timestampSegment = imageUrl.imageUrl.match(/\/(\d+)\//);
                if (timestampSegment && timestampSegment.length >= 2) {
                    const timestampString = timestampSegment[1];
                    await deleteFileInAWS(`kibson${req.baseUrl}`, timestampString + "/" + filename);
                }
            }
        }

        req.responseModel = await productQueries.updateProductById(
            id,
            req.requestModel
        );
        next();
    } catch (error) {
        prepareErrorLog(error, updateProductByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(
                prepareErrorResponse(
                    ErrorStatus.SERVER_DOWN,
                    null,
                    extractLanguageFromRequest(req)
                )
            );
    }
};

const bulkUpdateProductDataHandler = async (req, res, next) => {
    try {
        const productsToUpdate = req.body.products ?? [];
        const result = await productQueries.updateBulkPrice(productsToUpdate);

        req.responseModel = {updatedCount: result.modifiedCount};
        next();
    } catch (error) {
        prepareErrorLog(error, bulkUpdateProductDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const getProductByIdDataHandler = async (req, res, next) => {
    try {
        const {id} = req.params;
        const userId = req.userFromToken?._id;
        const isAdmin = isAdminRole(req.userFromToken);
        const product = await productQueries.getProductByIdInfo(id, isAdmin);
        if (isEmpty(product)) {
            return res
                .status(ErrorStatus.PRODUCT_DOESNT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.PRODUCT_DOESNT_EXIST, null, extractLanguageFromRequest(req)));
        }
        const getUser = await userQueries.getUserInfoById(userId)
        if (!userId) {
            req.responseModel = product;
            next();
        } else {
            if (isAdminRole(getUser)) {
                const product = await productQueries.getProductById(id, true);
                product.subCategories = await subCategoryQueries.findProductInSubCategory(id);
                const stockQuantity = await StockQueries.checkIfProductIdExist(id);
                product.stockQuantity = stockQuantity.quantity;
                req.responseModel = product;
                req.responseModel.admin = getUser;
            } else {
                req.responseModel = await setFavoriteProductFlagById(userId, product, res, req);
            }
            next();
        }

    } catch (error) {
        prepareErrorLog(error, getProductByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const getProductsPaginatedDataHandler = async (req, res, next) => {
    try {
        const isAdmin = isAdminRole(req.userFromToken);
        const products = await productQueries.getAllProducts(req, isAdmin);
        const userId = req.userFromToken?._id;
        const getUser = await userQueries.getUserInfoById(userId);
        if (!userId) {
            req.responseModel = products;
            next();
        } else {
            req.responseModel = await setFavoriteProductFlag(userId, products, res);
            req.responseModel.admin = getUser;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, getProductsPaginatedDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const getProductsBySearchPaginatedDataHandler = async (req, res, next) => {
    try {
        const isAdmin = isAdminRole(req.userFromToken);
        var products = await productQueries.getAllProductsBySearch(req, isAdmin);
        const userId = req.userFromToken?._id;
        const getUser = await userQueries.getUserInfoById(userId);
        if (!userId) {
            req.responseModel = products;
            next();
        } else {
            for (var prod of products.docs) {
                const stockQuantity = await StockQueries.checkIfProductIdExist(prod.id);
                prod.stockQuantity = stockQuantity.quantity;
            }
            req.responseModel = await setFavoriteProductFlag(userId, products, res);
            req.responseModel.admin = getUser;
            next();
        }

    } catch (error) {
        prepareErrorLog(error, getProductsBySearchPaginatedDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const getMyFavoriteProductsDataHandler = async (req, res, next) => {
    try {
        const userId = req.userFromToken?._id;
        req.responseModel = await userQueries.getMyFavoriteProduct(userId, req);
        next();
    } catch (error) {
        prepareErrorLog(error, getMyFavoriteProductsDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

module.exports = {
    createProductDataHandler,
    deleteNutritionInProductDataHandler,
    deleteProductByIdsDataHandler,
    updateProductByIdDataHandler,
    getProductByIdDataHandler,
    getProductsPaginatedDataHandler,
    getMyFavoriteProductsDataHandler,
    deleteProductByIdDataHandler,
    getProductsBySearchPaginatedDataHandler,
    createBulkProductDataHandler,
    forceDeleteProductByIdDataHandler,
    bulkUpdateProductDataHandler
}
