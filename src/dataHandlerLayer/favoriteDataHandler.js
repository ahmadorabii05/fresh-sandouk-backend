
const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("./common/LanguageHeaderHandler");
const {prepareErrorLog} = require("../helperKit/loggingKit");
const productQueries = require("../dataBaseLayer/queries/product/productQueries")
const userQueries = require("../dataBaseLayer/queries/user/userQueries")
const {isEmpty} = require("lodash");

const createFavoriteDataHandler = async (req, res, next) => {
    try {
        const productIds = req.requestModel?.productIds;
        const userId = req.userFromToken._id;
        const getUserInfo = await userQueries.getUserInfoById(userId);
        const productIdsFromUser = getUserInfo?.info?.productSettings;

        const getProductInfoPromises = productIds.map(id => productQueries.getProductByIdInfo(id));
        const productInfos = await Promise.all(getProductInfoPromises);

        const nonExistingProductIds = productInfos.filter(p => isEmpty(p)).map(p => p._id);

        if (nonExistingProductIds.length > 0) {
            return res
                .status(ErrorStatus.PRODUCT_DOESNT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.PRODUCT_DOESNT_EXIST, null, extractLanguageFromRequest(req)));
        }

        let updatedProductSettings = [...productIdsFromUser];

        for (const productId of productIds) {
            const productIdString = productId.toString();
            const index = updatedProductSettings.findIndex(id => id.toString() === productIdString);
            if (index !== -1) {
                updatedProductSettings.splice(index, 1);
            } else {
                updatedProductSettings.push(productId);
            }
        }

        await userQueries.updateUserProductById(userId, updatedProductSettings);

        next();
    } catch (error) {
        prepareErrorLog(error, createFavoriteDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};



module.exports={
    createFavoriteDataHandler
}