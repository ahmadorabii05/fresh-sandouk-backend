const cartQueries = require("../dataBaseLayer/queries/cart/cartQueries");
const { ErrorStatus } = require("../constants/error/errorStatus");
const { prepareErrorResponse } = require("../errorHandlerLayer/errorHandler");
const { extractLanguageFromRequest } = require("./common/LanguageHeaderHandler");

const { prepareErrorLog } = require("../helperKit/loggingKit");
const UserQueries = require("../dataBaseLayer/queries/user/userQueries");
const { calculateFinalPriceAfterDiscount } = require("../helperKit/productKit");


const addToCartDataHandler = async (req, res, next) => {
    try {
        const userId = req.userFromToken._id;
        const { productIds } = req.requestModel;
        const find = await cartQueries.getCartInfoByUserIdForAddToCart(userId);
        let cart = [];
        if (!find) {
            await cartQueries.createUserCart(userId, productIds);
            let cart = await cartQueries.findUserCart(userId);
            const { totalPrice, totalOriginPrice, productIds: cartProductIds } = cart[0];

            req.create = true;
            req.responseModel = {
                totalPrice,
                totalOriginPrice,
                cartProductIds,
                totalCount: productIds.length
            };
            next();
        } else {
            for (const product of productIds) {
                const productIndex = find.productIds?.findIndex(p => p.productId.valueOf() === (product.productId.valueOf()));
                if (productIndex === -1) {
                    await cartQueries.addProductToCart(userId, product.productId, product.quantity);
                    cart = await cartQueries.findUserCart(userId);
                    req.updateCart = true;
                } else {
                    cart = await cartQueries.addToCart(userId, product.productId, product.quantity); // update cart with the latest document
                    cart = await cartQueries.findUserCart(userId);

                    req.addToCart = true;
                }
            }
            const { totalPrice, totalOriginPrice, productIds: cartProductIds } = cart[0];
            req.responseModel = {
                totalPrice,
                totalOriginPrice,
                cartProductIds,
                totalCount: productIds.length

            };
            req.addToCart = true;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, addToCartDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const decreaseQuantityDataHandler = async (req, res, next) => {
    try {
        const userId = req.userFromToken._id;
        const { productId } = req.requestModel;
        let cart = await cartQueries.findUserCart(userId.valueOf());
        if (cart?.length > 0) {
            const productIndex = cart[0].productIds.findIndex(p => p._id.toString() === productId.toString());
            if (productIndex !== -1) {
                const quantity = cart[0].productIds[productIndex].quantity;
                if (quantity > 1) {
                    cart = await cartQueries.decreaseProductQuantity(userId.valueOf(), productId);
                    cart = await cartQueries.findUserCart(userId.valueOf());
                    req.updateCart = true;
                } else {
                    cart = await cartQueries.removeProductFromCart(userId.valueOf(), productId);
                    cart = await cartQueries.findUserCart(userId.valueOf());
                    req.updateCart = true;
                }
                if (cart?.length > 0) {
                    const { totalPrice, totalOriginPrice, productIds: cartProductIds } = cart[0];
                    req.responseModel = {
                        totalPrice,
                        totalOriginPrice,
                        cartProductIds,
                        totalCount: cartProductIds.length
                    };
                    next()
                } else {
                    req.responseModel = {
                        totalPrice: 0,
                        totalOriginPrice: 0,
                        cartProductIds: [],
                        totalCount: 0
                    };
                    next();
                }
            } else {
                return res
                    .status(ErrorStatus.PRODUCT_DOESNT_EXIST_IN_CART.code)
                    .json(prepareErrorResponse(ErrorStatus.PRODUCT_DOESNT_EXIST_IN_CART, null, extractLanguageFromRequest(req)));
            }
        } else {
            return res
                .status(ErrorStatus.ADD_PRODUCT_TO_CART_FIRST.code)
                .json(prepareErrorResponse(ErrorStatus.ADD_PRODUCT_TO_CART_FIRST, null, extractLanguageFromRequest(req)));
        }
    } catch (error) {
        prepareErrorLog(error, decreaseQuantityDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


const deleteItemFromCartDataHandler = async (req, res, next) => {
    try {
        const { productIds } = req.requestModel;
        const userId = req.userFromToken?._id;
        const removeProductFromCart = await cartQueries.removeProductsFromCartArray(userId, productIds);

        if (!removeProductFromCart) {
            // Cart not found for the user
            res
                .status(ErrorStatus.CART_NOT_FOUND.code)
                .json(prepareErrorResponse(ErrorStatus.CART_NOT_FOUND, null, extractLanguageFromRequest(req)));
        }
        next();
    } catch (error) {
        prepareErrorLog(error, deleteItemFromCartDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}


const getCartInfoDataHandler = async (req, res, next) => {
    try {
        const userId = req.userFromToken._id;
        let totalPrice = 0;
        let totalOriginalPrice = 0;
        const cartInfo = await cartQueries.getCartInfo(userId);
        if (cartInfo?.productIds?.length < 0) {
            req.responseModel = {
                productsInCart: [],
                totalItems: 0,
                totalPrice: 0,
                totalOriginalPrice: 0
            }
            next();
        } else {
            let totalItems = cartInfo.productIds?.length;
            const productsInCartPromises = cartInfo?.productIds?.map(async (item) => {
                const product = item.productId;
                if (product) {
                    const price = product.price;
                    let finalPriceAfterDiscount = price;
                    if (product.discountValue) {
                        const discountValue = parseFloat(product.discountValue) || 0;
                        finalPriceAfterDiscount = calculateFinalPriceAfterDiscount(price, discountValue);
                    }
                    totalPrice += finalPriceAfterDiscount * item.quantity;
                    totalOriginalPrice += product.price * item.quantity;
                    const userInfo = await UserQueries.getUserInfoById(userId);
                    const favoriteProductIds = userInfo?.info?.productSettings?.map((product) => product.toString()) || [];
                    if (favoriteProductIds.includes(item.productId._id.toString())) {
                        product.isFavorite = true;
                    }
                    return {
                        _id: product._id,
                        name: product.name,
                        imageUrls: product.imageUrls,
                        tags: product.tags,
                        description: product.description,
                        country: product.country,
                        price: price,
                        isFavorite: product.isFavorite,
                        priceModel: product.priceModel,
                        isDiscount: product.isDiscount,
                        discountValue: product.discountValue,
                        brand: product.brandId,
                        nutrition: product.nutrition,
                        generalDescription: product.generalDescription,
                        ingredient: product.ingredient,
                        storage: product.storage,
                        finalPriceAfterDiscount: finalPriceAfterDiscount,
                        quantityInCart: item.quantity,
                    };
                }
            });
            const productsInCart = await Promise.all(productsInCartPromises);
            req.responseModel = {
                productsInCart: productsInCart,
                totalItems: totalItems,
                totalPrice: totalPrice,
                totalOriginalPrice: totalOriginalPrice
            }
            next();
        }

    } catch (error) {
        prepareErrorLog(error, getCartInfoDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

module.exports = {
    addToCartDataHandler,
    decreaseQuantityDataHandler,
    deleteItemFromCartDataHandler,
    getCartInfoDataHandler
}