const {PaginationResponseModel} = require("../presentationLayer/models/responseModel/pagination/PaginationResponseModel");
const {SuccessStatus} = require("../constants/success/successStatus");
const {prepareSuccessResponse} = require("../presentationLayer/common/presenter");
const {InventoryResponseModel} = require("../presentationLayer/models/responseModel/inventory/InventoryResponseModel");


const getInventoryReportPresenter = async (req,res,next)=>{
    let stock = new PaginationResponseModel(req.responseModel);
    stock.docs = stock?.docs?.map((stock) => new InventoryResponseModel(stock,req));
    const responseModel = stock;
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}


module.exports = {
    getInventoryReportPresenter
}