const {isEmpty} = require("lodash");
const UserQueries = require("../dataBaseLayer/queries/user/userQueries");
const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {prepareErrorLog} = require("../helperKit/loggingKit");
const {sendEmail, EmailTypes} = require("../helperKit/emailKit");
const crypto = require("crypto");
const bcrypt = require("bcryptjs");
const {CreateUserRequestModel} = require("./models/requestModel/user/CreateUserRequestModel");
const RoleQueries = require("../dataBaseLayer/queries/role/roleQueries");
const {extractLanguageFromRequest} = require("./common/LanguageHeaderHandler");
// const {sendPushNotificationUser, PushNotificationTypes} = require("../helperKit/pushNotificationsKit");
const {DefaultPassword} = require("../constants/general/general");
// const {verifySMSCode} = require("../helperKit/fireBase-AuthKit");
const {isValidNumber} = require("libphonenumber-js");
const CartQueries = require("../dataBaseLayer/queries/cart/cartQueries")
const {createCustomerInStripe} = require("../helperKit/helperKit");
/**
 * Responsible for fetching the data from the database and preparing the needed data
 * for the next step
 * @param {request will include a property called requestModel prepared from the previous step} req
 * @param {response will be used to return an immediate response if needed} res
 * @param {goes to the next step of the process} next
 */

/**
 * LoginDataHandler prepares the user data needed from the database
 */
const loginDataHandler = async (req, res, next) => {
    try {
        const requestModel = req.requestModel;
        const user = await UserQueries.getUserInfoByEmail(requestModel.email);
        if (isEmpty(user)) {
                return res
                    .status(ErrorStatus.DOES_NOT_EXIST.code)
                    .json(prepareErrorResponse(ErrorStatus.DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
            } else {
            if(user.active){
                req.user = user;
                next();
            }else{
                return res
                    .status(ErrorStatus.NON_ACTIVE_USER.code)
                    .json(prepareErrorResponse(ErrorStatus.NON_ACTIVE_USER, null, extractLanguageFromRequest(req)));
            }

            }
    } catch (error) {
        prepareErrorLog(error, loginDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

/**
 * SignUpDataHandler
 */
const signUpDataHandler = async (req, res, next) => {
    try {
        const requestModel = req.requestModel;
        const user = await UserQueries.getUserInfoByEmail(requestModel.email);
        //check this later on

        // if(requestModel.otp){
        //    if(!await verifySMSCode(requestModel.otp)){
        //        return res
        //            .status(ErrorStatus.OTP_TOKEN.code)
        //            .json(prepareErrorResponse(ErrorStatus.OTP_TOKEN, null, extractLanguageFromRequest(req)));
        //    }
        // }
        const isValid = isValidNumber(requestModel.phoneNumber);
        if (isValid) {
            if (!isEmpty(user)) {
                return res
                    .status(ErrorStatus.USER_EMAIL_EXIST.code)
                    .json(prepareErrorResponse(ErrorStatus.USER_EMAIL_EXIST, null, extractLanguageFromRequest(req)));
            } else {

                const userRole = await RoleQueries.getRoleByName("user");
                requestModel.role = userRole?._id || -1;
                const userBody = new CreateUserRequestModel({});
                userBody.fromSignUp(requestModel);
                const newPassword = userBody.info.general.password ?? DefaultPassword;
                const salt = await bcrypt.genSalt(10),
                    hashedPassword = await bcrypt.hash(newPassword, salt);
                userBody.info.general.password = hashedPassword;
                userBody.info.general.email = userBody.info.general.email.toLowerCase().trim();
                userBody.info.general.verificationToken = crypto.randomBytes(32).toString("hex");
                userBody.info.general.expireVerificationToken = Date.now() + 3600000;
                userBody.info.general.walletBalance = 999999;
                userBody.info.general.name=requestModel.name;
                const createUserInStripe = await createCustomerInStripe(userBody.info.general.name,userBody.info.general.email,userBody.info.general.phoneNumber);
                if(!isEmpty(createUserInStripe)){
                    userBody.info.general.stripe_id=createUserInStripe.id;
                    const savedUser = await UserQueries.createUser(userBody);
                    const userFinal = await UserQueries.getUserInfoById(savedUser._id);
                    await CartQueries.createUserCart(savedUser._id,[])
                    req.user = userFinal;
                }else{
                    return res
                        .status(ErrorStatus.STRIPE_ERROR_CREATE_CUSTOMER.code)
                        .json(prepareErrorResponse(ErrorStatus.STRIPE_ERROR_CREATE_CUSTOMER, null, extractLanguageFromRequest(req)));
                }

                // sendEmail(userFinal, EmailTypes.UserCreated)
                // const pushNotificationUsername = await UserModel.getUserInfoById(savedUser._id);
                //
                // let Info = {
                //     sentBy: pushNotificationUsername.info.general.userName,
                //     itemId: savedUser._id,
                //     type: "verify-email",
                //     actionApplied: "null"
                // };
                // sendPushNotificationUser(savedUser._id, PushNotificationTypes.verifyEmail, {
                //     body: `${Info}`
                // }).then(async () => {
                //     await createNotificationDataHandler(savedUser._id, savedUser._id, "verify-email", savedUser._id,
                //         "null", [])
                // })

                next();
            }
        } else {
            return res
                .status(ErrorStatus.SERVER_DOWN.code)
                .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, "phone Number is Invalid", extractLanguageFromRequest(req)));

        }
    } catch (error) {
        prepareErrorLog(error, signUpDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

/**
 * SocialLoginDataHandler
 */
//TODO I have to come back to handle the username
const socialLoginDataHandler = async (req, res, next) => {
    try {
        const requestModel = req.requestModel;
        if (requestModel.email) {
            const user = await UserQueries.getUserInfoByEmail(requestModel.email);
            if (isEmpty(user)) {
                const userRole = await RoleQueries.getRoleByName("user");
                requestModel.role = userRole?._id || -1;
                const userBody = new CreateUserRequestModel({});
                userBody.fromSocial(requestModel);
                const newPassword = userBody.info.general.password ?? DefaultPassword;
                const salt = await bcrypt.genSalt(10),
                    hashedPassword = await bcrypt.hash(newPassword, salt);
                userBody.info.general.password = hashedPassword;

                userBody.info.general.email = userBody.info.general.email.toLowerCase();
                // userBody.info.general.verificationToken = await crypto.randomBytes(32).toString("hex");
                userBody.info.general.accountVerification = true;
                userBody.info.general.verificationToken = undefined;
                userBody.info.general.expireVerificationToken = undefined;
                userBody.info.general.walletBalance = 999999;
                const createUserInStripe = await createCustomerInStripe(userBody.info.general.name,userBody.info.general.email,userBody.info.general.phoneNumber);
                if(!isEmpty(createUserInStripe)){
                    userBody.info.general.stripe_id=createUserInStripe.id;
                    const savedUser = await UserQueries.createUser(userBody);
                    await CartQueries.createUserCart(savedUser._id,[])
                }else{
                    return res
                        .status(ErrorStatus.STRIPE_ERROR_CREATE_CUSTOMER.code)
                        .json(prepareErrorResponse(ErrorStatus.STRIPE_ERROR_CREATE_CUSTOMER, null, extractLanguageFromRequest(req)));
                }


                //create bucket
                const userFinal = await UserQueries.getUserInfoById(savedUser._id);
                req.user = userFinal;
                // sendEmail(userBody, EmailTypes.UserCreated);
                next();
            } else {
                await UserQueries.updateSavedUserSocialLoginTypeById(user._id, requestModel.socialLoginType);
                req.user = user;
                next();
            }

        } else if (requestModel.socialLoginId) {
            const socialLoginId = await UserQueries.socialLoginId(requestModel.socialLoginId);
            if (isEmpty(socialLoginId)) {
                const userRole = await RoleQueries.getRoleByName("user");
                requestModel.role = userRole?._id || -1;
                const userBody = new CreateUserRequestModel({});
                userBody.fromSocial(requestModel);
                const newPassword = userBody.info.general.password ?? DefaultPassword;
                const salt = await bcrypt.genSalt(10),
                    hashedPassword = await bcrypt.hash(newPassword, salt);
                userBody.info.general.password = hashedPassword;
                // userBody.info.general.verificationToken = await crypto.randomBytes(32).toString("hex");
                userBody.info.general.accountVerification = true;
                userBody.info.general.verificationToken = undefined;
                userBody.info.general.expireVerificationToken = undefined;
                userBody.info.general.walletBalance = 999999;

                const savedUser = await UserQueries.createUser(userBody);
                await CartQueries.createUserCart(savedUser._id,[])

                //create bucket
                const userFinal = await UserQueries.getUserInfoById(savedUser._id);
                req.user = userFinal;
                next();
            } else {
                await UserQueries.updateSavedUserSocialLoginTypeById(socialLoginId._id, requestModel.socialLoginType);
                req.user = socialLoginId;
                next();
            }
        } else {
            return res
                .status(ErrorStatus.MISSING_DATA.code)
                .json(prepareErrorResponse(ErrorStatus.MISSING_DATA, null, extractLanguageFromRequest(req)));
        }
    } catch (error) {
        prepareErrorLog(error, socialLoginDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

/**
 * ForgotPasswordDataHandler prepares the user data needed from the database
 */
const forgotPasswordDataHandler = async (req, res, next) => {
    try {
        const requestModel = req.requestModel;
        const user = await UserQueries.getUserInfoByEmail(requestModel.email);
        if (isEmpty(user)) {
            return res
                .status(ErrorStatus.DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        } else {
            user.info.general.resetToken = await crypto.randomBytes(32).toString("hex");
            user.info.general.expireResetToken = Date.now() + 3600000;
            // user.info.general.verifyCode = Math.floor(Math.random() * 90000) + 10000;
            await user.save();
            sendEmail(user, EmailTypes.ForgotPassword);
            req.user = user;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, forgotPasswordDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

/**
 * changePasswordDataHandler prepares the user data needed from the database
 */
const changePasswordDataHandler = async (req, res, next) => {
    try {
        const bodyReceived = req.requestModel;
        const user = await UserQueries.getUserInfoByEmail(bodyReceived.email);
        if (isEmpty(user)) {
            return res
                .status(ErrorStatus.DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }
                const salt = await bcrypt.genSalt(10);
                const hashedPassword = await bcrypt.hash(bodyReceived.password, salt);
                user.info.general.password = hashedPassword;
                user.info.general.resetToken = undefined;
                user.info.general.expireResetToken = undefined;
                await user.save();
                sendEmail(user, EmailTypes.ChangePassword);

                next();

    } catch (error) {
        prepareErrorLog(error, changePasswordDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const accountVerificationDataHandler = async (req, res, next) => {
    try {
        const bodyReceived = req.requestModel;
        const user = await UserQueries.getUserByVerificationToken(bodyReceived.sentToken);
        if (isEmpty(user)) {
            return res
                .status(ErrorStatus.REQUEST_EXPIRED.code)
                .json(prepareErrorResponse(ErrorStatus.REQUEST_EXPIRED, null, extractLanguageFromRequest(req)));
        }
        user.info.general.accountVerification = true;
        user.info.general.verificationToken = undefined;
        user.info.general.expireVerificationToken = undefined;
        await user.save();
        next();
    } catch (error) {
        prepareErrorLog(error, accountVerificationDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


/**
 * VerifyCodeDataHandler prepares the user data needed from the database
 */
const verifyCodeDataHandler = async (req, res, next) => {
    try {
        const bodyReceived = req.requestModel;
        const user = await UserQueries.getUserByVerificationCode(bodyReceived.email, bodyReceived.verifyCode);
        const checkUserExist = await UserQueries.getUserInfoByEmail(bodyReceived.email);

        if (isEmpty(checkUserExist)) {
            return res
                .status(ErrorStatus.DOES_NOT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.DOES_NOT_EXIST, null, extractLanguageFromRequest(req)));
        }
        if (isEmpty(user)) {
            return res
                .status(ErrorStatus.REQUEST_EXPIRED.code)
                .json(prepareErrorResponse(ErrorStatus.REQUEST_EXPIRED, null, extractLanguageFromRequest(req)));
        } else {
            next();
        }
    } catch (error) {
        prepareErrorLog(error, forgotPasswordDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


module.exports = {
    loginDataHandler,
    signUpDataHandler,
    socialLoginDataHandler,
    forgotPasswordDataHandler,
    changePasswordDataHandler,
    accountVerificationDataHandler,
    verifyCodeDataHandler
};
