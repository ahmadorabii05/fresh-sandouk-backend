const UserQueries = require("../dataBaseLayer/queries/user/userQueries");
const {isEmpty} = require("lodash");
const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("./common/LanguageHeaderHandler");
const bcrypt = require("bcryptjs");
const {sendEmail, EmailTypes} = require("../helperKit/emailKit");
const {prepareErrorLog} = require("../helperKit/loggingKit");


const resetPasswordDataHandler = async (req,res,next)=>{
    try {
        const bodyReceived = req.requestModel;
        const user = await UserQueries.getUserByResetToken(bodyReceived.sentToken);
        if (isEmpty(user)) {
            return res
                .status(ErrorStatus.REQUEST_EXPIRED.code)
                .json(prepareErrorResponse(ErrorStatus.REQUEST_EXPIRED, null, extractLanguageFromRequest(req)));
        }
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(bodyReceived.password, salt);
        user.info.general.password = hashedPassword;
        user.info.general.resetToken = undefined;
        user.info.general.expireResetToken = undefined;
        await user.save();
        sendEmail(user, EmailTypes.ChangePassword, extractLanguageFromRequest(req));
        next();
    } catch (error) {
        prepareErrorLog(error, resetPasswordDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

module.exports = {
    resetPasswordDataHandler
}