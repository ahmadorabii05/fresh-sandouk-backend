const subCategoryQueries = require("../dataBaseLayer/queries/subCategory/subCategoryQueries");
const CategoryQueries = require("../dataBaseLayer/queries/category/categoryQueries");
const UserQueries = require("../dataBaseLayer/queries/user/userQueries")
const ProductQueries = require("../dataBaseLayer/queries/product/productQueries")
const {isEmpty} = require("lodash");
const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("./common/LanguageHeaderHandler");
const {
    getFileStorePathForFile,
    getIdsNotInArray
} = require("../helperKit/helperKit");
const {uploadFile} = require("../helperKit/uploadFile");
const {prepareErrorLog} = require("../helperKit/loggingKit");
const path = require("path");
const {deleteFileInAWS} = require("../helperKit/deleteFileFromAWSKit");
const categoryQueries = require("../dataBaseLayer/queries/category/categoryQueries");
const SubCategoriesQueries = require("../dataBaseLayer/queries/subCategory/subCategoryQueries");
const CategoryModel = require("../dataBaseLayer/models/category/CategoryModel");
const { isAdminRole } = require("../services/userService");

const createSubCategoryDataHandler = async (req, res, next) => {
    try {
        const requestModel = req.requestModel;
        //check category
        const category = await CategoryQueries.getCategoryById(requestModel.categoryId);

        if (isEmpty(category)) {
            return res
                .status(ErrorStatus.CATEGORY_DOESNT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.CATEGORY_DOESNT_EXIST, null, extractLanguageFromRequest(req)));
        }

        if (req.file) {
            if (getFileStorePathForFile(req.file.mimetype.split("/")[1])) {
                const subCategory = await subCategoryQueries.createSubCategory(requestModel);
                const createdSubCategory = await subCategory.save();
                const uploadedFile = await uploadFile(res, req.file, createdSubCategory._id, req.baseUrl);
                await CategoryQueries.updateSubCategory(requestModel.categoryId, createdSubCategory._id);
                req.subCategory = await subCategoryQueries.updateImageUrl(createdSubCategory._id, uploadedFile);

                next();
            } else {
                return res
                    .status(ErrorStatus.ONLY_IMAGES.code)
                    .json(prepareErrorResponse(ErrorStatus.ONLY_IMAGES, null, extractLanguageFromRequest(req)));
            }
        } else {
            return res
                .status(ErrorStatus.FILE_MISSING.code)
                .json(prepareErrorResponse(ErrorStatus.FILE_MISSING, null, extractLanguageFromRequest(req)));
        }

    } catch (error) {
        prepareErrorLog(error, createSubCategoryDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


const createBulkSubCategoryDataHandler = async (req, res, next) => {
    try {
        const requestModel = req.requestModel;

        // Assuming requestModel contains an array of categories
        const Subcategories = requestModel.subCategories;

        for (const subCategoriesData of Subcategories) {
            const productIds = [];
            const category = await CategoryModel.findOne({ name: { $regex: new RegExp(subCategoriesData.categoryName, 'i') } });
            if(isEmpty(category)){
                return res
                    .status(ErrorStatus.CATEGORY_DOESNT_EXIST.code)
                    .json(prepareErrorResponse(ErrorStatus.CATEGORY_DOESNT_EXIST, subCategoriesData.categoryName +" not exist as Category", extractLanguageFromRequest(req)));
            }
            if (subCategoriesData?.productNames?.length > 0) {
                // Iterate over subCategories and get their IDs by name
                for (const product of subCategoriesData.productNames) {
                    // Assuming you have a function to retrieve subCategory by name
                    const ProductInfo = await ProductQueries.getProductByName(product.name);

                    if (ProductInfo && ProductInfo._id) {
                        productIds.push(ProductInfo._id);
                    } else {
                        return res
                            .status(ErrorStatus.PRODUCT_DOESNT_EXIST.code)
                            .json(prepareErrorResponse(ErrorStatus.PRODUCT_DOESNT_EXIST, `${product.name}` + " not exist as Product", extractLanguageFromRequest(req)));
                    }
                }
            }
            const newCategory = {
                name: subCategoriesData.name,
                nameAr: subCategoriesData.nameAr,
                categoryId:category._id,
                imageUrl: "",
                productIds: productIds,
            };
          const subCategory =  await SubCategoriesQueries.createSubCategory(newCategory);
            const createdSubCategory = await subCategory.save();
            await CategoryQueries.updateSubCategory(category._id, createdSubCategory._id);

        }
        next()
    } catch (error) {
        prepareErrorLog(error, createBulkSubCategoryDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const getAllSubCategoryPaginatedDataHandler = async (req, res, next) => {
    try {
        const isAdmin = isAdminRole(req.userFromToken);
        req.responseModel = await subCategoryQueries.getAllSubCategoriesPaginated(req, isAdmin);
        if (req.userFromToken?._id) {
            req.responseModel.admin = await UserQueries.getUserInfoById(req.userFromToken._id)
        }
        next();

    } catch (error) {
        prepareErrorLog(error, getAllSubCategoryPaginatedDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const getSubCategoryByIdDataHandler = async (req, res, next) => {
    try {
        const subCategoryId = req.params.id;
        const subCategoryInfo = await subCategoryQueries.getSubCategoryById(subCategoryId);
        if (isEmpty(subCategoryInfo))
            res
                .status(ErrorStatus.SUB_CATEGORY_DOESNT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.SUB_CATEGORY_DOESNT_EXIST, null, extractLanguageFromRequest(req)));
        req.responseModel = subCategoryInfo;
        if (req.userFromToken?._id) {
            req.responseModel.admin = await UserQueries.getUserInfoById(req.userFromToken._id);
        }
        next();

    } catch (error) {
        prepareErrorLog(error, getSubCategoryByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const deleteSubCategoryByIdDataHandler = async (req, res, next) => {
    try {
        const subCategoryIds = req.requestModel.subCategoryIds;

        for (const item of subCategoryIds) {
            const productIds = await subCategoryQueries.getSubCategoryByIdInfo(item);
            const imageUrl = path.basename(productIds.imageUrl);
            if (productIds.productIds.length > 0) {
                return res.status(ErrorStatus.CATEGORY_HAS_SUBCATEGORY.code).json(
                    prepareErrorResponse(
                        ErrorStatus.CATEGORY_HAS_SUBCATEGORY,
                        `Cannot Delete ${productIds.name}, since it has products`,
                        extractLanguageFromRequest(req)
                    )
                );
            }

            const timestampSegment = productIds.imageUrl.match(/\/(\d+)\//);
            if (timestampSegment && timestampSegment.length >= 2) {
                const timestampString = timestampSegment[1];
                await deleteFileInAWS(`kibson${req.baseUrl}`, timestampString + "/" + imageUrl);
                await subCategoryQueries.deleteSubCategoryById(item);
                await CategoryQueries.removeSubCategory(item);
            }
        }
        next();
    } catch (error) {
        prepareErrorLog(error, deleteSubCategoryByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const deleteSingleSubCategoryByIdDataHandler = async (req, res, next) => {
    try {
        const subCategoryId = req.params.id;
        const productIds = await subCategoryQueries.getSubCategoryByIdInfo(subCategoryId);
        if (isEmpty(productIds)) {
            return res
                .status(ErrorStatus.SUB_CATEGORY_DOESNT_EXIST.code)
                .json(prepareErrorResponse(ErrorStatus.SUB_CATEGORY_DOESNT_EXIST, null, extractLanguageFromRequest(req)));
        }
        const imageUrl = path.basename(productIds.imageUrl);
        if (productIds.productIds.length > 0) {
            return res.status(ErrorStatus.CATEGORY_HAS_SUBCATEGORY.code).json(
                prepareErrorResponse(
                    ErrorStatus.CATEGORY_HAS_SUBCATEGORY,
                    `Cannot Delete ${productIds.name}, since it has products`,
                    extractLanguageFromRequest(req)
                )
            );
        }

        const timestampSegment = productIds.imageUrl.match(/\/(\d+)\//);
        if (timestampSegment && timestampSegment.length >= 2) {
            const timestampString = timestampSegment[1];
            await deleteFileInAWS(`kibson${req.baseUrl}`, timestampString + "/" + imageUrl);
        }
        await subCategoryQueries.deleteSubCategoryById(subCategoryId);
        await CategoryQueries.removeSubCategory(subCategoryId);

        next();
    } catch (error) {
        prepareErrorLog(error, deleteSingleSubCategoryByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const updateSubCategoryByIdDataHandler = async (req, res, next) => {
    try {
        const id = req.params.id;
        const currentInfo = req.requestModel;
        const currentSubCategory = await subCategoryQueries.getSubCategoryById(id);

        const getSubCategoryFromCategory = await categoryQueries.getCategoryByIdInfo(currentInfo.categoryId);

        if (isEmpty(getSubCategoryFromCategory)) {
            return res.status(ErrorStatus.CATEGORY_DOESNT_EXIST.code).json(
                prepareErrorResponse(ErrorStatus.CATEGORY_DOESNT_EXIST, null, extractLanguageFromRequest(req))
            );
        }

        if (isEmpty(currentSubCategory)) {
            return res.status(ErrorStatus.SUB_CATEGORY_DOESNT_EXIST.code).json(
                prepareErrorResponse(ErrorStatus.SUB_CATEGORY_DOESNT_EXIST, null, extractLanguageFromRequest(req))
            );
        }
        if (getSubCategoryFromCategory.subCategories.indexOf(id) === -1) {
            await categoryQueries.removeSubCategory(id);
            await categoryQueries.addSubCategory(currentInfo.categoryId, id);
        }

        const productIdsFromCurrentCategory = currentSubCategory?.productIds;


        if (req.file) {
            if (getFileStorePathForFile(req.file.mimetype.split("/")[1])) {
                const imageUrl = path.basename(currentSubCategory.imageUrl);
                const timestampSegment = currentSubCategory.imageUrl.match(/\/(\d+)\//);
                if (timestampSegment && timestampSegment.length >= 2) {
                    const timestampString = timestampSegment[1];
                    await deleteFileInAWS(`kibson${req.baseUrl}`, timestampString + "/" + imageUrl);
                }
                req.requestModel.imageUrl = await uploadFile(res, req.file, id, `${req.baseUrl}`);
            } else {
                return res
                    .status(ErrorStatus.ONLY_IMAGES.code)
                    .json(prepareErrorResponse(ErrorStatus.ONLY_IMAGES, null, extractLanguageFromRequest(req)));
            }
        }

        if (req.requestModel?.productIds.length > 0) {
            req.requestModel.productIds = await getIdsNotInArray(req.requestModel.productIds, productIdsFromCurrentCategory)
        }
        req.responseModel = await subCategoryQueries.updateSubCategoryById(id, req.requestModel)
        next();
    } catch (error) {
        prepareErrorLog(error, updateSubCategoryByIdDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const getSubCategoryBySearchPaginatedDataHandler = async (req, res, next) => {
    try {
        const isAdmin = isAdminRole(req.userFromToken);
        const list = await subCategoryQueries.getAllSubCategoriesBySearch(req, isAdmin);
        const userId = req.userFromToken?._id;
        const getUser = await UserQueries.getUserInfoById(userId);
        if (!userId) {
            req.responseModel = list;
            next();
        } else {
            req.responseModel = list;
            req.responseModel.admin = getUser;
            next();
        }

    } catch (error) {
        prepareErrorLog(error, getSubCategoryBySearchPaginatedDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

module.exports = {
    createSubCategoryDataHandler,
    getAllSubCategoryPaginatedDataHandler,
    deleteSubCategoryByIdDataHandler,
    getSubCategoryByIdDataHandler,
    updateSubCategoryByIdDataHandler,
    deleteSingleSubCategoryByIdDataHandler,
    createBulkSubCategoryDataHandler,
    getSubCategoryBySearchPaginatedDataHandler,
}