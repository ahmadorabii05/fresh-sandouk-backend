const Joi = require("joi");


const createCheckoutSheetValidationScheme = Joi.object().keys({
    amount: Joi.number().required().messages({
        "any.required": "amount is required",
        "string.base": "amount must be a number",
        "string.empty": "amount must not be empty",
    })
}).messages({
    "object.unknown": "Invalid field(s) found"
})

const createRefundValidationScheme = Joi.object().keys({
    orderId: Joi.string().required().messages({
        "any.required": "OrderId is required",
        "string.base": "OrderId must be a string",
        "string.empty": "OrderId must not be empty",
    })
}).messages({
    "object.unknown": "Invalid field(s) found"
})

module.exports={
    createCheckoutSheetValidationScheme,
    createRefundValidationScheme
}