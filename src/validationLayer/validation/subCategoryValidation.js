const Joi = require("joi");


const createSubCategoryValidationScheme = Joi.object().keys({
    name: Joi.string().required(),
    nameAr: Joi.string().optional(),
    categoryId:Joi.string().required().messages({
        'any.required': 'Select Category first',
        'string.empty': 'Select Category first',
    }),
    productIds: Joi.array().optional(),
    isActive: Joi.boolean().optional()
});


const createBulkSubCategoryArrayValidationScheme = Joi.object().keys({
    name: Joi.string().required().messages({
        'any.required': 'Select name first',
        'string.empty': 'Select name first',
    }),
    nameAr: Joi.string().optional(),
    categoryName:Joi.string().required().messages({
        'any.required': 'Select Category first',
        'string.empty': 'Select Category first',
    }),
    productNames: Joi.array().optional(),
    isActive: Joi.boolean().optional()
});

const createBulkSubCategoryValidationScheme = Joi.object().keys({
    subCategories: Joi.array().items(createBulkSubCategoryArrayValidationScheme),
});

const deleteSubCategoryValidationScheme = Joi.object().keys({
    subCategoryIds: Joi.array().required().messages({
        'any.required': 'Select SubCategory first',
        'string.empty': 'Select SubCategory first',
    }),
});

const updateSubCategoryValidationScheme = Joi.object().keys({
    name: Joi.string().optional(),
    nameAr: Joi.string().optional(),
    categoryId:Joi.string().required().messages({
        'any.required': 'Select Category first',
        'string.empty': 'Select Category first',
    }),
    productIds: Joi.array().optional(),
    isActive: Joi.boolean().optional()
});

module.exports = {
    createSubCategoryValidationScheme,
    deleteSubCategoryValidationScheme,
    updateSubCategoryValidationScheme,
    createBulkSubCategoryValidationScheme
}