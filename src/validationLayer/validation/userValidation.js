const Joi = require("joi");
const { emailValidation, passwordValidation} = require("./common/commonValidation");

const updateUserByIdValidationScheme = Joi.object().keys({
    info: {
        general: {
            email: Joi.string().optional().custom(emailValidation),
            name: Joi.string().optional(),
            password: Joi.string().custom(passwordValidation).optional(),
            role: Joi.string().optional(),
            phoneNumber:Joi.string().optional().allow(""),
        },
    },
    active: Joi.boolean()
});


const updateUserProfileValidationScheme = Joi.object().keys({
    info: {
        general: {
            email: Joi.string().optional().custom(emailValidation),
            firstName: Joi.string().optional(),
            lastName: Joi.string().optional(),
            alternativeEmail: Joi.string().optional().custom(emailValidation),
            phoneNumber:Joi.string().optional(),
            alternativePhoneNumber:Joi.string().optional(),
            dateOfBirth: Joi.date().optional().allow(null), // Assuming date format is used for dateOfBirth
            nationality:Joi.string().optional().allow(""),

        },
    },
});

const changePasswordValidationScheme = Joi.object().keys({
    newPassword: Joi.string().custom(passwordValidation),
});

const createUserValidationScheme = Joi.object().keys({
    info: {
        general: {
            role: Joi.string().required(),
            email: Joi.string().required().custom(emailValidation),
            name: Joi.string().required(),
            password: Joi.string().custom(passwordValidation),
            accountVerification:Joi.optional(),
            phoneNumber: Joi.string().required()
        },
    },
    active: Joi.boolean()
});
const updateUserPushNotificationValidationScheme = Joi.object().keys({
    pushNotificationToken: Joi.string().required(),
});


module.exports = {
    updateUserByIdValidationScheme,
    changePasswordValidationScheme,
    createUserValidationScheme,
    updateUserPushNotificationValidationScheme,
    updateUserProfileValidationScheme
};
