const Joi = require("joi");


const createFavoriteValidationScheme = Joi.object().keys({
    productIds: Joi.array().required().messages({
        'any.required': 'Select Product first',
        'string.empty': 'Select Product first',
    }),
});

module.exports = {
    createFavoriteValidationScheme
}