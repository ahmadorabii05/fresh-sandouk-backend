const Joi = require("joi");
const {addressTitleValidation} = require("./common/commonValidation");

const createAddressValidationScheme = Joi.object().keys({
    address: Joi.string().trim().required().messages({
        "any.required": "address is required",
        "string.base": "address must be a string",
        "string.empty": "address must not be empty",
    }),
    addressTitle:Joi.string().custom(addressTitleValidation),
    contactNumber:Joi.string().required().messages({
        "any.required": "contactNumber is required",
        "string.base": "contactNumber must be a string",
        "string.empty": "contactNumber must not be empty",
    }),
    street:Joi.string().required().messages({
        "any.required": "street is required",
        "string.base": "street must be a string",
        "string.empty": "street must not be empty",
    }),
    nearestLand:Joi.string().optional().messages({
        "string.base": "nearestLand must be a string",
        "string.empty": "nearestLand must not be empty",
    }),
    longtitude :Joi.number().optional(),
    latitude :Joi.number().optional(),
    isDefaultAddress:Joi.boolean().optional()
}).messages({
    "object.unknown": "Invalid field(s) found"
})

const updateAddressValidationScheme = Joi.object().keys({
    address: Joi.string().trim().optional(),
    addressTitle:Joi.string().optional().custom(addressTitleValidation),
    contactNumber:Joi.string().optional(),
    street:Joi.string().optional(),
    nearestLand:Joi.string().optional(),
    isDefaultAddress:Joi.boolean().optional(),
    longtitude :Joi.number().optional(),
    latitude :Joi.number().optional()

}).messages({
    "object.unknown": "Invalid field(s) found"
})

const deleteAddressValidationScheme = Joi.object().keys({
    addressIds: Joi.array().items(Joi.string()).required().messages({
        "any.required": "addressIds is required",
        "array.base": "addressIds must be an array",
        "array.empty": "addressIds must not be empty",
        "string.base": "Each addressId in the array must be a string",
    }),
}).messages({
    "object.unknown": "Invalid field(s) found"
})

module.exports = {
    createAddressValidationScheme,
    updateAddressValidationScheme,
    deleteAddressValidationScheme
}