const Joi = require("joi");

const createProductValidationScheme = Joi.object().keys({
    name: Joi.string().required(),

    nameAr: Joi.string().optional(),

    country: Joi.string().required().messages({
        'any.required': 'product country is required',
        'string.empty': 'product country should not be empty',
    }),

    price: Joi.number().required(),
    priceModel: Joi.string().required(),
    description: Joi.string().required(),
    descriptionAr: Joi.string().optional(),
    sku: Joi.string().required().messages({
        'any.required': 'Stock Keeping Unit is required',
        'string.empty': 'Stock Keeping Unit should not be empty',
    }),
    discountValue: Joi.string().optional().allow(""),
    tags: Joi.array().optional(),
    generalDescription: Joi.string().optional().allow(""),
    ingredient: Joi.string().optional().allow(""),
    storage: Joi.string().optional().allow(""),

    generalDescriptionAr: Joi.string().optional().allow(""),
    ingredientAr: Joi.string().optional().allow(""),
    storageAr: Joi.string().optional().allow(""),
    brandId: Joi.string().required().messages({
        'any.required': 'Select brand first',
        'string.empty': 'Select brand first',
    }),
    subCategoryId: Joi.string().required().messages({
        'any.required': 'Select SubCategory first',
        'string.empty': 'Select SubCategory first',
    }),
    nutrition: Joi.array().optional().default([]),
    quantity: Joi.number().optional(),
});


const createBulkProductArrayValidationScheme = Joi.object().keys({
    name: Joi.string().required(),

    nameAr: Joi.string().optional(),

    country: Joi.string().required().messages({
        'any.required': 'product country is required',
        'string.empty': 'product country should not be empty',
    }),

    price: Joi.number().required(),
    priceModelName: Joi.string().required(),
    description: Joi.string().required(),
    descriptionAr: Joi.string().optional(),
    sku: Joi.string().required().messages({
        'any.required': 'Stock Keeping Unit is required',
        'string.empty': 'Stock Keeping Unit should not be empty',
    }),
    discountValue: Joi.string().optional().allow(""),
    tagsName: Joi.array().optional(),
    generalDescription: Joi.string().optional().allow(""),
    ingredient: Joi.string().optional().allow(""),
    storage: Joi.string().optional().allow(""),

    generalDescriptionAr: Joi.string().optional().allow(""),
    ingredientAr: Joi.string().optional().allow(""),
    storageAr: Joi.string().optional().allow(""),
    brandName: Joi.string().required().messages({
        'any.required': 'Select brand first',
        'string.empty': 'Select brand first',
    }),
    subCategoryName: Joi.string().required().messages({
        'any.required': 'Select SubCategory first',
        'string.empty': 'Select SubCategory first',
    }),
    nutrition: Joi.array().optional().default([]),
    quantity: Joi.number().optional(),
});

const createBulkProductValidationScheme = Joi.object().keys({
    products: Joi.array().items(createBulkProductArrayValidationScheme)
})

const updateProductByIdValidationScheme = Joi.object().keys({

    name: Joi.string().optional(),
    nameAr: Joi.string().optional(),

    country: Joi.string().optional(),
    price: Joi.string().optional(),

    barcode: Joi.string().optional(),

    priceModel: Joi.string().optional(),
    description: Joi.string().optional(),

    descriptionAr: Joi.string().optional(),
    sku: Joi.string().optional(),
    discountValue: Joi.string().optional().allow(""),
    generalDescription: Joi.string().optional().allow(""),
    ingredient: Joi.string().optional().allow(""),
    storage: Joi.string().optional().allow(""),

    generalDescriptionAr: Joi.string().optional().allow(""),
    ingredientAr: Joi.string().optional().allow(""),
    storageAr: Joi.string().optional().allow(""),
    brandId: Joi.string().optional(),
    subCategoryId: Joi.string().optional(),
    tags: Joi.array().optional(),
    quantity: Joi.number().optional(),
    sort_order: Joi.number().optional(),
    nutrition: Joi.array().optional().default([]),

    isActive: Joi.boolean().optional(),
    isFeatured:Joi.boolean().optional(),
})
const deleteNutritionProductValidationScheme = Joi.object().keys({
    productId: Joi.string().required().messages({
        'any.required': 'Select Product first',
        'string.empty': 'Select Product first',
    }),
    nutritionId: Joi.string().required().messages({
        'any.required': 'Select Nutrition record',
        'string.empty': 'Select Nutrition record',
    })
})


const deleteProductByIdsValidationScheme = Joi.object().keys({
    productIds: Joi.array().required().messages({
        'any.required': 'Select Product first',
        'string.empty': 'Select Product first',
    })
})
module.exports = {
    createProductValidationScheme,
    deleteNutritionProductValidationScheme,
    deleteProductByIdsValidationScheme,
    updateProductByIdValidationScheme,
    createBulkProductValidationScheme

}
