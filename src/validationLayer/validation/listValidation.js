const Joi = require("joi");


const createListValidationScheme =  Joi.object().keys({
    name: Joi.string().required().messages({
        'string.base': 'Name must be a string',
        'any.required': 'Name is required',
        'string.empty':'Name must not be empty'

    }),
    productIds: Joi.array().items(Joi.string()).optional().messages({
        'array.base': 'Product IDs must be an array',
        'array.items': 'Product IDs must be string',

    }),
})

const editListValidationScheme =Joi.object().keys({
    name: Joi.string().required().messages({
        'string.base': 'Name must be a string',
        'any.required': 'Name is required',
    }),
    listId: Joi.string().required().messages({
        'string.base': 'Selected List must be a string',
        'any.required': 'List is required',
        'string.empty':'List must not be empty'

    }),
})

const addProductToListValidationScheme = Joi.object().keys({
    productId: Joi.string().required().messages({
        'string.base': 'product must be a string',
        'any.required': 'product is required',
    }),
    listId: Joi.string().required().messages({
        'string.base': 'Selected List must be a string',
        'any.required': 'List is required',
        'string.empty':'List must not be empty'

    }),
})

const deleteListValidationScheme =Joi.object().keys({
    listId: Joi.string().required().messages({
        'string.base': 'Selected List must be a string',
        'any.required': 'List is required',
        'string.empty':'List must not be empty'
    }),
})
module.exports={
    createListValidationScheme,
    editListValidationScheme,
    addProductToListValidationScheme,
    deleteListValidationScheme
}