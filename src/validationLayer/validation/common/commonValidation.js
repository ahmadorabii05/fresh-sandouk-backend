const {getLocalizedStringForKey} = require("../../../helperKit/localizationKit");

/**
 *
 * @param {email value} value
 * @param {Joi helpers to be able to add customized error messages} helpers
 * @returns {the email if valid or the error message if not}
 */
const emailValidation = (value, helpers) => {
    let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (value.match(regexEmail)) {
        return value;
    } else {
        return helpers.message(getLocalizedStringForKey("INVALID_EMAIL","en"));
    }
};

/**
 *
 * @param {password value} value
 * @param {Joi helpers to be able to add customized error messages} helpers
 * @returns {the password if valid or the error message if not}
 */
const passwordValidation = (value, helpers) => {
    if (value) {
        if (value.length < 8) {
            return helpers.message(getLocalizedStringForKey("PASSWORD_MUST_BE_MORE_THAN_8_CHARACTERS","en"));
        } else {
            return value;
        }
    } else {
        return helpers.message(getLocalizedStringForKey("IS_REQUIRED","en"));
    }
};

/**
 *
 * @param {userName value} value
 * @param {Joi helpers to be able to add customized error messages} helpers
 * @returns {the userName if valid or the error message if not}
 */
const userNameValidation = (value, helpers) => {
    if (value) {
        if (value.length < 6) {
            return helpers.message(getLocalizedStringForKey("USERNAME_MUST_BE_AT_LEAST_6_CHARACTER","en"));
        } else {
            return value;
        }
    }
};

/**
 *
 * @param {permission value} value
 * @param {Joi helpers to be able to add customized error messages} helpers
 * @returns {the permission if valid or the error message if not}
 */
const permissionValidation = (value, helpers) => {
    const permission = ["create", "read", "update", "delete"];
    let invalid = false;
    value.forEach((item) => {
        if (!permission.includes(item)) {
            invalid = true;
        }
    });
    if (invalid) {
        return helpers.message(getLocalizedStringForKey("INVALID_PERMISSION","en"));
    } else {
        return value;
    }
};

const addressTitleValidation = (value,helpers)=>{
    const permission = ["Home", "Office", "Hotel", "Other"];
    const invalid = !permission.includes(value);
    if (invalid) {
        return helpers.message(getLocalizedStringForKey("INVALID_ADDRESS","en"));
    } else {
        return value;
    }
}




const genderValidation = (value, helpers) => {
    const gender = ["male", "female", "prefer_not_to_disclose", "none"];
    const invalid = !gender.includes(value);
    if (invalid) {
        return helpers.message(getLocalizedStringForKey("INVALID_GENDER","en"));
    } else {
        return value;
    }
};

const bannerLocationValidation = (value, helpers) => {
    const items = ["home_page_section_1", "home_page_section_2", "home_page_section_3", "product_details", "filters_page", "checkout"];
    const invalid = !items.includes(value);
    if (invalid) {
      return helpers.message(getLocalizedStringForKey("INVALID_BANNER_LOCATION", "en"));
    } else {
      return value;
    }
  };

const isHexColor = (value) => {
    // Regular expression to match a valid hex color code (# followed by 6 hexadecimal characters)
    const hexColorRegex = /^#([0-9A-Fa-f]{6})$/;
    return hexColorRegex.test(value);
};

module.exports = {
    emailValidation,
    passwordValidation,
    permissionValidation,
    genderValidation,
    userNameValidation,
    addressTitleValidation,
    bannerLocationValidation,
    isHexColor
};
