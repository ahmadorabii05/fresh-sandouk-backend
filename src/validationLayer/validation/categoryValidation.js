const Joi = require("joi");
const { isHexColor } = require("./common/commonValidation");

const createCategoryValidationScheme = Joi.object().keys({
    name: Joi.string().required().messages({
        'any.required': 'Select name first',
        'string.empty': 'Select name first',
    }),
    nameAr: Joi.string().optional(),
    color: Joi.string().custom((value, helpers) => {
        if (!isHexColor(value)) {
            return helpers.message("Invalid hex color code");
        }
        return value; // Return the valid color value
    }).required(),
    subCategories: Joi.array().optional(),
    priority: Joi.number().required(),
    isActive:Joi.boolean().optional()
});

const createBulkCategoryValidationScheme = Joi.object().keys({
    category: Joi.array().items(createCategoryValidationScheme),
});

const deleteCategoryValidationScheme = Joi.object().keys({
    categoryIds: Joi.array().required()
})


const updateCategoryValidationScheme = Joi.object().keys({
    name: Joi.string().optional(),
    nameAr: Joi.string().optional(),
    color: Joi.string().custom((value, helpers) => {
        if (!isHexColor(value)) {
            return helpers.message("Invalid hex color code");
        }
        return value; // Return the valid color value
    }).optional(),
    subCategories: Joi.array().optional().allow(""),
    priority: Joi.number().required(),
    isActive: Joi.boolean().optional()
});
module.exports = {
    createCategoryValidationScheme,
    deleteCategoryValidationScheme,
    updateCategoryValidationScheme,
    createBulkCategoryValidationScheme
}