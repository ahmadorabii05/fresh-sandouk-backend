const Joi = require("joi");


const addToCartValidationScheme = Joi.object().keys({
    productIds: Joi.array().items(
        Joi.object().keys({
            productId: Joi.string().required().messages({
                'any.required': 'Select Product first',
                'string.empty': 'Select Product first',
            }),
            quantity: Joi.number().integer().min(1).required().messages({
                'any.required': 'quantity is required',
                'number.base': 'quantity must be a number',
                'number.integer': 'quantity must be an integer',
                'number.min': 'quantity must be at least 1',
            }),
        })
    ).min(1).required(),
});


const decreaseToCartValidationScheme = Joi.object().keys({
    productId: Joi.string().required().messages({
        'any.required': 'Select Product first',
        'string.empty': 'Select Product first',
    })
});

const deleteItemFromCartValidationScheme = Joi.object().keys({
    productIds: Joi.array()
        .min(1)
        .required()
        .messages({
            'any.required': 'Select Product(s) first',
            'array.min': 'Select Product(s) first',
        }),
});
module.exports={
    addToCartValidationScheme,
    decreaseToCartValidationScheme,
    deleteItemFromCartValidationScheme
}