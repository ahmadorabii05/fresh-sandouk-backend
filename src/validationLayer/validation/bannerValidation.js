const Joi = require("joi");
const { bannerLocationValidation } = require("./common/commonValidation");

const createBannerValidationScheme = Joi.object().keys({
    actionUrl: Joi.optional(),
    location: Joi.string().custom(bannerLocationValidation),
    priority: Joi.number().optional(),
}).messages({
    "object.unknown": "Invalid field(s) found"
})

const updateBannerValidationScheme = Joi.object().keys({
    actionUrl: Joi.optional(),
    location: Joi.string().custom(bannerLocationValidation),
    priority: Joi.number().optional(),
}).messages({
    "object.unknown": "Invalid field(s) found"
})

const deleteBannerValidationScheme = Joi.object().keys({
    bannerIds: Joi.array().items(Joi.string()).required().messages({
        "any.required": "brandIds is required",
        "array.base": "brandIds must be an array",
        "array.empty": "brandIds must not be empty",
        "string.base": "Each brandId in the array must be a string",
    }),
}).messages({
    "object.unknown": "Invalid field(s) found"
})

module.exports = {
    createBannerValidationScheme,
    updateBannerValidationScheme,
    deleteBannerValidationScheme
}