const Joi = require("joi");


const createStockValidationScheme = Joi.object().keys({
    productId: Joi.string().trim().required().messages({
        "any.required": "productId is required",
        "string.base": "productId must be a string",
        "string.empty": "productId must not be empty",
    }),
    quantity:Joi.number().required().messages({
        "any.required": "quantity is required",
        "string.base": "quantity must be a Number",
        "string.empty": "quantity must not be empty",
    })
}).messages({
    "object.unknown": "Invalid field(s) found"
})


const updateStockValidationScheme = Joi.object().keys({
    quantity:Joi.number().required().messages({
        "any.required": "quantity is required",
        "string.base": "quantity must be a Number",
        "string.empty": "quantity must not be empty",
    })
}).messages({
    "object.unknown": "Invalid field(s) found"
})

module.exports = {
    createStockValidationScheme,
    updateStockValidationScheme
}