const Joi = require("joi");

const createPriceValidationScheme = Joi.object().keys({
    unit: Joi.string().required().messages({
        "any.required": "Unit is required",
        "string.base": "Unit must be a string",
        "string.empty": "Unit must not be empty",
    }),
    unitAr:Joi.string().trim().optional()
}).messages({
    "object.unknown": "Invalid field(s) found"
})


const createBulkPriceValidationScheme = Joi.object().keys({
    priceModel:Joi.array().items(createPriceValidationScheme)
})
const updatePriceValidationScheme = Joi.object().keys({
    unit: Joi.string().trim().optional().messages({
        "any.required": "Unit is required",
        "string.base": "Unit must be a string",
        "string.empty": "Unit must not be empty",
    }),
    unitAr:Joi.string().trim().optional().messages({
        "string.base": "unitAr must be a string",
        "string.empty": "unitAr must not be empty",
    })

}).messages({
    "object.unknown": "Invalid field(s) found"
})

const deletePriceValidationScheme = Joi.object().keys({
    priceIds: Joi.array().items(Joi.string()).required().messages({
        "any.required": "priceIds is required",
        "array.base": "priceIds must be an array",
        "array.empty": "priceIds must not be empty",
        "string.base": "Each priceId in the array must be a string",
    }),
}).messages({
    "object.unknown": "Invalid field(s) found"
})

module.exports = {
    createPriceValidationScheme,
    updatePriceValidationScheme,
    deletePriceValidationScheme,
    createBulkPriceValidationScheme
}