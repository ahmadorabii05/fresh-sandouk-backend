const Joi = require("joi");

const createBrandValidationScheme = Joi.object().keys({
    source: Joi.string().trim().required().messages({
        "any.required": "source is required",
        "string.base": "source must be a string",
        "string.empty": "source must not be empty",
    }),
    sourceAr:Joi.string().optional()
}).messages({
    "object.unknown": "Invalid field(s) found"
})

const createBulkBrandValidationScheme = Joi.object().keys({
    brands:Joi.array().items(createBrandValidationScheme)
})

const updateBrandValidationScheme = Joi.object().keys({
    source: Joi.string().trim().optional().messages({
        "any.required": "source is required",
        "string.base": "source must be a string",
        "string.empty": "source must not be empty",
    }),
    sourceAr:Joi.string().optional().allow("")
}).messages({
    "object.unknown": "Invalid field(s) found"
})

const deleteBrandValidationScheme = Joi.object().keys({
    brandIds: Joi.array().items(Joi.string()).required().messages({
        "any.required": "brandIds is required",
        "array.base": "brandIds must be an array",
        "array.empty": "brandIds must not be empty",
        "string.base": "Each brandId in the array must be a string",
    }),
}).messages({
    "object.unknown": "Invalid field(s) found"
})

module.exports = {
    createBrandValidationScheme,
    updateBrandValidationScheme,
    deleteBrandValidationScheme,createBulkBrandValidationScheme
}