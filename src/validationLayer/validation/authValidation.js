const Joi = require("joi");
const { emailValidation,passwordValidation} = require("./common/commonValidation");

const loginValidationScheme = Joi.object().keys({
    email:  Joi.string().required().custom(emailValidation),
    password: Joi.string().required().custom(passwordValidation),
});


const socialLoginValidationScheme = Joi.object().keys({
    email: Joi.string().optional().allow("").custom(emailValidation),
    name: Joi.string().optional(),
    phoneNumber: Joi.string().optional(),
    password: Joi.string().custom(passwordValidation),
    socialLoginToken: Joi.string().required(),
    socialLoginType: Joi.string().required(),
    socialLoginId:Joi.string().optional()
});

const signUpValidationScheme = Joi.object().keys({
    email: Joi.string().required().custom(emailValidation),
    name:Joi.string().required(),
    password: Joi.string().custom(passwordValidation),
    phoneNumber: Joi.string().required(),
    otp:Joi.string().optional().allow("")
});

const forgotPasswordValidationScheme = Joi.object().keys({
    email: Joi.string().required().custom(emailValidation),
});

const changePasswordValidationScheme = Joi.object().keys({
    email: Joi.string().required().custom(emailValidation),
    password: Joi.string().custom(passwordValidation),


});

const accountVerificationValidationScheme = Joi.object().keys({
    sentToken: Joi.string().required(),
});

const verifyCodeValidationScheme = Joi.object().keys({
    verifyCode: Joi.number().required(),
    email: Joi.string().required().custom(emailValidation),
});



module.exports = {
    loginValidationScheme,
    signUpValidationScheme,
    socialLoginValidationScheme,
    forgotPasswordValidationScheme,
    changePasswordValidationScheme,
    accountVerificationValidationScheme,
    verifyCodeValidationScheme,
};

