const Joi = require("joi");


const createOrderValidationScheme = Joi.object().keys({
    scheduledDate: Joi.string().trim().required().pattern(/^([A-Za-z]+)\s(\d{1,2})\/(\d{1,2})\/(\d{4})$/)
        .messages({
            "any.required": "scheduledDate is required",
            "string.base": "scheduledDate must be a string",
            "string.empty": "scheduledDate must not be empty",
            "string.pattern.base": "scheduledDate must be in the format 'Wednesday 14/4/1996'",
        }),
    scheduledTime: Joi.string().required().messages({
        "any.required": "scheduledTime is required",
        "string.base": "scheduledTime must be a string",
        "string.empty": "scheduledTime must not be empty",
    }),
    packingInstruction: Joi.string().optional(),
    deliveryInstruction: Joi.string().optional()
}).messages({
    "object.unknown": "Invalid field(s) found"
})

module.exports = {
    createOrderValidationScheme
}