const Joi = require("joi");
const {passwordValidation} = require("./common/commonValidation");


const resetPasswordValidationScheme = Joi.object().keys({
    sentToken: Joi.string().required(),
    password: Joi.string().custom(passwordValidation),
});

module.exports = {
    resetPasswordValidationScheme
}