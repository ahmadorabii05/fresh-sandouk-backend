const Joi = require("joi");


const getStatisticsValidationScheme = Joi.object().keys({
    dateFrom:  Joi.date().iso().required().messages({
        "any.required": "dateFrom is required",
        "date.base": "dateFrom must be a valid date in ISO 8601 format",
        "date.isoDate": "dateFrom must be in ISO 8601 format (e.g., 'YYYY-MM-DDTHH:mm:ss.sssZ')",
    }),
    dateTo:  Joi.date().iso().required().messages({
        "any.required": "dateTo is required",
        "date.base": "dateTo must be a valid date in ISO 8601 format",
        "date.isoDate": "dateTo must be in ISO 8601 format (e.g., 'YYYY-MM-DDTHH:mm:ss.sssZ')",
    }),
}).messages({
    "object.unknown": "Invalid field(s) found"
})

module.exports={
    getStatisticsValidationScheme
}