const Joi = require("joi");
const { permissionValidation } = require("./common/commonValidation");

const createRoleValidationScheme = Joi.object().keys({
    name: Joi.string().required(),
    permissions: Joi.object().required(),
});

const updateRoleValidationScheme = Joi.object().keys({
    name: Joi.string(),
    permissions: {
        permissions: Joi.array().custom(permissionValidation),
    },
});

module.exports = {
    createRoleValidationScheme,
    updateRoleValidationScheme,
};
