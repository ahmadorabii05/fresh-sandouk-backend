const Joi = require("joi");


const createTagsValidationScheme = Joi.object().keys({
    name: Joi.string().required(),
    nameAr:Joi.string().optional()
});
const createBulkTagsArrayValidationScheme = Joi.object().keys({
    name: Joi.string().required().messages({
        'any.required': 'Tag name is required',
        'string.empty': 'Tag name must not be empty',
    }),
    nameAr:Joi.string().optional()
});

const createBulkTagsValidationScheme= Joi.object().keys({
    tags:Joi.array().items(createBulkTagsArrayValidationScheme)
})
const updateTagsValidationScheme=Joi.object().keys({
    name:Joi.string().optional(),
    nameAr:Joi.string().optional()
})
const deleteTagsValidationScheme = Joi.object().keys({
    tagIds:Joi.array().required().messages({
        'any.required': 'Select at least one Tag',
        'string.empty': 'Select at least one Tag',
    })
})
module.exports = {
    createTagsValidationScheme,
    updateTagsValidationScheme,
    deleteTagsValidationScheme,
    createBulkTagsValidationScheme
}