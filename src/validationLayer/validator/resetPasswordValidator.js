const {ErrorStatus} = require("../../constants/error/errorStatus");
const {prepareErrorResponse} = require("../../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("../../dataHandlerLayer/common/LanguageHeaderHandler");
const {prepareErrorLog} = require("../../helperKit/loggingKit");
const {ResetPasswordRequestModel} = require("../../dataHandlerLayer/models/requestModel/auth/ResetPasswordRequestModel");
const {resetPasswordValidationScheme} = require("../validation/resetPasswordValidation");


const resetPasswordValidator = async (req,res,next)=>{
    try {
        const bodyReceived = new ResetPasswordRequestModel(req.body);
        const result = resetPasswordValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        } else {
            req.requestModel = bodyReceived;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, resetPasswordValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null));
    }
}


module.exports = {
    resetPasswordValidator
}