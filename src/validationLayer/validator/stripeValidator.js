const {ErrorStatus} = require("../../constants/error/errorStatus");
const {prepareErrorResponse} = require("../../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("../../dataHandlerLayer/common/LanguageHeaderHandler");
const {prepareErrorLog} = require("../../helperKit/loggingKit");
const {CreateCheckoutSheetRequestModel} = require("../../dataHandlerLayer/models/requestModel/stripe/CreateCheckoutSheetRequestModel");
const {createCheckoutSheetValidationScheme, createRefundValidationScheme} = require("../validation/stripeValidation");
const {CreateRefundRequestModel} = require("../../dataHandlerLayer/models/requestModel/stripe/CreateRefundRequestModel")

const  createCheckoutSheetValidator = async (req, res, next) => {
    try {
        const bodyReceived = new CreateCheckoutSheetRequestModel(req.body);
        const result = createCheckoutSheetValidationScheme.validate(bodyReceived);
        if (result.error) {
            return  res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, createCheckoutSheetValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const  createRefundValidator = async (req, res, next) => {

    try {
        const bodyReceived = new CreateRefundRequestModel(req.body);
        const result = createRefundValidationScheme.validate(bodyReceived);
        if (result.error) {
            return  res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, createRefundValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};
module.exports={
    createCheckoutSheetValidator,
    createRefundValidator
}