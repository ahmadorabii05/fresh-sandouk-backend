const { ErrorStatus } = require("../../constants/error/errorStatus");
const { prepareErrorResponse } = require("../../errorHandlerLayer/errorHandler");
const { extractLanguageFromRequest } = require("../../dataHandlerLayer/common/LanguageHeaderHandler");
const { prepareErrorLog } = require("../../helperKit/loggingKit");
const { CreateBannerRequestModel } = require("../../dataHandlerLayer/models/requestModel/banner/CreateBannerRequestModel");
const { createBannerValidationScheme, deleteBannerValidationScheme, updateBannerValidationScheme } = require("../validation/bannerValidation");
const { DeleteBannerRequestModel } = require("../../dataHandlerLayer/models/requestModel/banner/DeleteBannerRequestModel");
const { UpdateBannerRequestModel } = require("../../dataHandlerLayer/models/requestModel/banner/UpdateBannerRequestModel");


const createBannerValidator = async (req, res, next) => {
    try {
        const bodyReceived = new CreateBannerRequestModel(req.body);
        const result = createBannerValidationScheme.validate(bodyReceived);
        if (result.error) {
            return res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, createBannerValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const deleteBannerValidator = async (req, res, next) => {
    try {
        const bodyReceived = new DeleteBannerRequestModel(req.body);
        const result = deleteBannerValidationScheme.validate(bodyReceived);
        if (result.error) {
            return res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, deleteBannerValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


const updateBannerByIdValidator = async (req, res, next) => {
    try {
        const bodyReceived = new UpdateBannerRequestModel(req.body);
        const result = updateBannerValidationScheme.validate(bodyReceived);
        if (result.error) {
            return res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, updateBannerByIdValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


module.exports = {
    createBannerValidator,
    deleteBannerValidator,
    updateBannerByIdValidator
}