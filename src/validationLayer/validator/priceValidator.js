const {ErrorStatus} = require("../../constants/error/errorStatus");
const {prepareErrorResponse} = require("../../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("../../dataHandlerLayer/common/LanguageHeaderHandler");
const {prepareErrorLog} = require("../../helperKit/loggingKit");
const {CreatePriceRequestModel} = require("../../dataHandlerLayer/models/requestModel/price/CreatePriceRequestModel");
const {createPriceValidationScheme, deletePriceValidationScheme, updatePriceValidationScheme,
    createBulkPriceValidationScheme
} = require("../validation/priceValidation");
const {DeletePriceRequestModel} = require("../../dataHandlerLayer/models/requestModel/price/DeletePriceRequestModel");
const {UpdatePriceRequestModel} = require("../../dataHandlerLayer/models/requestModel/price/UpdatePriceRequestModel");
const {CreateBulkPriceRequestModel} = require("../../dataHandlerLayer/models/requestModel/price/CreateBulkPriceRequestModel");


const  createPriceValidator = async (req, res, next) => {
    try {
        const bodyReceived = new CreatePriceRequestModel(req.body);
        const result = createPriceValidationScheme.validate(bodyReceived);
        if (result.error) {
            return  res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, createPriceValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


const  createBulkPriceValidator = async (req, res, next) => {
    try {
        const bodyReceived = new CreateBulkPriceRequestModel(req.body);
        const validationResult = createBulkPriceValidationScheme.validate(bodyReceived, { abortEarly: false });

        if (validationResult.error) {
            const errorDetails = validationResult.error.details.map((errorItem) => {
                const path = errorItem.path;
                let position = -1;

                // Extract the array index from the error path
                if (Array.isArray(path) && path.length >= 3 && path[0] === 'priceModel' && path[2] === 'unit') {
                position = parseInt(path[1]);
            }

            return {
                objectPosition: position, // Index of the object in the array
                message: errorItem.message,
            };
        });

           return res.status(ErrorStatus.BAD_REQUEST.code).json(
                prepareErrorResponse(ErrorStatus.SUB_CATEGORY_NAME_SPECIAL_CHARACTER, errorDetails, extractLanguageFromRequest(req)),
            );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, createBulkPriceValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const  deletePriceValidator = async (req, res, next) => {
    try {
        const bodyReceived = new DeletePriceRequestModel(req.body);
        const result = deletePriceValidationScheme.validate(bodyReceived);
        if (result.error) {
            return  res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, deletePriceValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


const  updatePriceByIdValidator = async (req, res, next) => {
    try {
        const bodyReceived = new UpdatePriceRequestModel(req.body);
        const result = updatePriceValidationScheme.validate(bodyReceived);
        if (result.error) {
            return  res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, updatePriceByIdValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


module.exports = {
    createPriceValidator,
    deletePriceValidator,
    updatePriceByIdValidator,
    createBulkPriceValidator
}