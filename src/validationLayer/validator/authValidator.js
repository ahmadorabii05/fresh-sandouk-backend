const { ErrorStatus } = require("../../constants/error/errorStatus");
const {
    prepareErrorResponse,
} = require("../../errorHandlerLayer/errorHandler");
const {
    LoginRequestModel,
} = require("../../dataHandlerLayer/models/requestModel/auth/LoginRequestModel");
const {
    loginValidationScheme,
    forgotPasswordValidationScheme,
    socialLoginValidationScheme,
    signUpValidationScheme,
    accountVerificationValidationScheme, verifyCodeValidationScheme,
} = require("../validation/authValidation");
const { prepareErrorLog } = require("../../helperKit/loggingKit");
const {
    ForgotAndPasswordRequestModel,
} = require("../../dataHandlerLayer/models/requestModel/auth/ForgotAndPasswordRequestModel");
const {
    changePasswordValidationScheme,
} = require("../validation/authValidation");
const {
    ChangePasswordRequestModel,
} = require("../../dataHandlerLayer/models/requestModel/auth/ChangePasswordRequestModel");
const {
    AccountVerificationRequestModel
} = require("../../dataHandlerLayer/models/requestModel/auth/AccountVerificationRequestModel");
const { SocialLoginRequestModel } = require("../../dataHandlerLayer/models/requestModel/auth/SocialLoginRequestModel");
const { SignUpRequestModel } = require("../../dataHandlerLayer/models/requestModel/auth/SignUpRequestModel");
const {VerifyCodeRequestModel} = require("../../dataHandlerLayer/models/requestModel/auth/VerifyCodeRequestModel");
const {extractLanguageFromRequest} = require("../../dataHandlerLayer/common/LanguageHeaderHandler");

const loginValidator = (req, res, next) => {
    try {
        const bodyReceived = new LoginRequestModel(req.body);
        const result = loginValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        } else {
            req.requestModel = bodyReceived;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, loginValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const signUpValidator = (req, res, next) => {
    try {
        const bodyReceived = new SignUpRequestModel(req.body);
        const result = signUpValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        } else {
            req.requestModel = bodyReceived;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, signUpValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const socialLoginValidator = (req, res, next) => {
    try {
        const bodyReceived = new SocialLoginRequestModel(req.body);
        const result = socialLoginValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        } else {
            req.requestModel = bodyReceived;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, socialLoginValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const forgotPasswordValidator = (req, res, next) => {
    try {
        const bodyReceived = new ForgotAndPasswordRequestModel(req.body);
        const result =
            forgotPasswordValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        } else {
            req.requestModel = bodyReceived;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, forgotPasswordValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null));
    }
};

const changePasswordValidator = (req, res, next) => {
    try {
        const bodyReceived = new ChangePasswordRequestModel(req.body);
        const result = changePasswordValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        } else {
            req.requestModel = bodyReceived;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, changePasswordValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null));
    }
};

const accountVerificationValidator = (req, res, next) => {
    try {
        const bodyReceived = new AccountVerificationRequestModel(req.body);
        const result = accountVerificationValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        } else {
            req.requestModel = bodyReceived;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, accountVerificationValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null));
    }
};


const verifyCodeValidator = (req, res, next) => {
    try {
        const bodyReceived = new VerifyCodeRequestModel(req.body);
        const result = verifyCodeValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        } else {
            req.requestModel = bodyReceived;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, signUpValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null));
    }
};



module.exports = {
    loginValidator,
    signUpValidator,
    socialLoginValidator,
    forgotPasswordValidator,
    changePasswordValidator,
    accountVerificationValidator,
    verifyCodeValidator
};
