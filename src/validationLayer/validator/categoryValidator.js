const {CategoryRequestModel} = require("../../dataHandlerLayer/models/requestModel/category/CategoryRequestModel");
const {createCategoryValidationScheme, deleteCategoryValidationScheme, updateCategoryValidationScheme,
    createBulkCategoryValidationScheme
} = require("../validation/categoryValidation");
const {ErrorStatus} = require("../../constants/error/errorStatus");
const {prepareErrorResponse} = require("../../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("../../dataHandlerLayer/common/LanguageHeaderHandler");
const {prepareErrorLog} = require("../../helperKit/loggingKit");
const {DeleteCategoryByIdsRequestModel} = require("../../dataHandlerLayer/models/requestModel/category/DeleteCategoryByIdsRequestModel");
const {hasSpecialChars} = require("../../helperKit/helperKit");
const {CreateBulkCategoryRequestModel} = require("../../dataHandlerLayer/models/requestModel/category/CreateBulkCategoryRequestModel");


const  createCategoryValidator = async (req, res, next) => {
    try {
        const bodyReceived = new CategoryRequestModel(req.body);
        const result = createCategoryValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        } else {
            if(!hasSpecialChars(bodyReceived.name)){
                req.requestModel = bodyReceived;
                next();
            }else{
                return res
                    .status(ErrorStatus.CATEGORY_NAME_SPECIAL_CHARACTER.code)
                    .json(prepareErrorResponse(ErrorStatus.CATEGORY_NAME_SPECIAL_CHARACTER, null, extractLanguageFromRequest(req)));
            }

        }
    } catch (error) {
        prepareErrorLog(error, createCategoryValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


const  createBulkCategoryValidator = async (req, res, next) => {
    try {
        const bodyReceived = new CreateBulkCategoryRequestModel(req.body);
        const validationResult = createBulkCategoryValidationScheme.validate(bodyReceived, { abortEarly: false });

        if (validationResult.error) {
            const errorDetails = validationResult.error.details.map((errorItem) => {
                const path = errorItem.path;
                let position = -1;

                // Extract the array index from the error path
                if (Array.isArray(path) && path.length >= 3 && path[0] === 'category' && path[2] === 'name') {
                    position = parseInt(path[1]);
                }

                return {
                    objectPosition: position, // Index of the object in the array
                    message: errorItem.message,
                };
            });

            res.status(ErrorStatus.BAD_REQUEST.code).json(
                prepareErrorResponse(ErrorStatus.CATEGORY_DOESNT_EXIST, errorDetails, extractLanguageFromRequest(req)),
            );
        } else {
            if(!hasSpecialChars(bodyReceived.name)){
                req.requestModel = bodyReceived;
                next();
            }else{
                return res
                    .status(ErrorStatus.CATEGORY_NAME_SPECIAL_CHARACTER.code)
                    .json(prepareErrorResponse(ErrorStatus.CATEGORY_NAME_SPECIAL_CHARACTER, null, extractLanguageFromRequest(req)));
            }

        }
    } catch (error) {
        prepareErrorLog(error, createBulkCategoryValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const  deleteCategoryByIdsValidator = async (req, res, next) => {
    try {
        const bodyReceived = new DeleteCategoryByIdsRequestModel(req.body);
        const result = deleteCategoryValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        } else {
            req.requestModel = bodyReceived;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, createCategoryValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const updateCategoryByIdValidator = async (req,res,next)=>{
    try {
        const bodyReceived = new CategoryRequestModel(req.body);
        const result = updateCategoryValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        } else {
            if(!hasSpecialChars(bodyReceived.name)){
                req.requestModel = bodyReceived;
                next();
            }else{
                return res
                    .status(ErrorStatus.CATEGORY_NAME_SPECIAL_CHARACTER.code)
                    .json(prepareErrorResponse(ErrorStatus.CATEGORY_NAME_SPECIAL_CHARACTER, null, extractLanguageFromRequest(req)));
            }

        }
    } catch (error) {
        prepareErrorLog(error, updateCategoryByIdValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

module.exports={
    createCategoryValidator,
    deleteCategoryByIdsValidator,
    updateCategoryByIdValidator,
    createBulkCategoryValidator
}