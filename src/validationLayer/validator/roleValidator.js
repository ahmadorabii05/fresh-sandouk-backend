const { ErrorStatus } = require("../../constants/error/errorStatus");
const {
    prepareErrorResponse,
} = require("../../errorHandlerLayer/errorHandler");
const {
    createRoleValidationScheme,
    updateRoleValidationScheme,
} = require("../validation/roleValidation");
const { prepareErrorLog } = require("../../helperKit/loggingKit");
const {
    RoleRequestModel,
} = require("../../dataHandlerLayer/models/requestModel/role/RoleRequestModel");
const {extractLanguageFromRequest} = require("../../dataHandlerLayer/common/LanguageHeaderHandler");

const  createRoleValidator = async (req, res, next) => {
    try {
        const bodyReceived = new RoleRequestModel(req.body);
        const result = createRoleValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        } else {
            req.requestModel = bodyReceived;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, createRoleValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const updateRoleValidator = async (req, res, next) => {
    try {
        const result = updateRoleValidationScheme.validate(req.body);
        const bodyReceived = new RoleRequestModel(req.body);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result.error?.message, extractLanguageFromRequest(req))
                );
        } else {
            req.requestModel = bodyReceived;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, updateRoleValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

module.exports = {
    createRoleValidator,
    updateRoleValidator,
};
