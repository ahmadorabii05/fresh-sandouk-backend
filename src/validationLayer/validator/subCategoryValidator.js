const {ErrorStatus} = require("../../constants/error/errorStatus");
const {prepareErrorResponse} = require("../../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("../../dataHandlerLayer/common/LanguageHeaderHandler");
const {hasSpecialChars} = require("../../helperKit/helperKit");
const {prepareErrorLog} = require("../../helperKit/loggingKit");
const {SubCategoryRequestModel} = require("../../dataHandlerLayer/models/requestModel/subCategory/SubCategoryRequestModel");
const {createSubCategoryValidationScheme, deleteSubCategoryValidationScheme, updateSubCategoryValidationScheme,
    createBulkSubCategoryValidationScheme
} = require("../validation/subCategoryValidation");
const {DeleteSubCategoryRequestModel} = require("../../dataHandlerLayer/models/requestModel/subCategory/DeleteSubCategoryRequestModel");
const {CreateBulkSubCategoryRequestModel} = require("../../dataHandlerLayer/models/requestModel/subCategory/CreateBulkSubCategoryRequestModel");

const  createSubCategoryValidator = async (req, res, next) => {
    try {
        const bodyReceived = new SubCategoryRequestModel(req.body);
        const result = createSubCategoryValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        } else {
            if(!hasSpecialChars(bodyReceived.name)){
                req.requestModel = bodyReceived;
                next();
            }else{
                return res
                    .status(ErrorStatus.SUB_CATEGORY_NAME_SPECIAL_CHARACTER.code)
                    .json(prepareErrorResponse(ErrorStatus.SUB_CATEGORY_NAME_SPECIAL_CHARACTER, null, extractLanguageFromRequest(req)));
            }

        }
    } catch (error) {
        prepareErrorLog(error, createSubCategoryValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const  createBulkSubCategoryValidator = async (req, res, next) => {
    try {
        const bodyReceived = new CreateBulkSubCategoryRequestModel(req.body);
        const validationResult = createBulkSubCategoryValidationScheme.validate(bodyReceived, { abortEarly: false });

        if (validationResult.error) {
            const errorDetails = validationResult.error.details.map((errorItem) => {
                const path = errorItem.path;
                let position = -1;

                // Extract the array index from the error path
                if (Array.isArray(path) && path.length >= 3 && path[0] === 'subCategories' && (path[2] === 'categoryName' || path[2] === "name")) {
                    position = parseInt(path[1]);
                }

                return {
                    objectPosition: position, // Index of the object in the array
                    message: errorItem.message,
                };
            });

            res.status(ErrorStatus.BAD_REQUEST.code).json(
                prepareErrorResponse(ErrorStatus.SUB_CATEGORY_NAME_SPECIAL_CHARACTER, errorDetails, extractLanguageFromRequest(req)),
            );
        } else {
            if(!hasSpecialChars(bodyReceived.name)){
                req.requestModel = bodyReceived;
                next();
            }else{
                return res
                    .status(ErrorStatus.SUB_CATEGORY_NAME_SPECIAL_CHARACTER.code)
                    .json(prepareErrorResponse(ErrorStatus.SUB_CATEGORY_NAME_SPECIAL_CHARACTER, null, extractLanguageFromRequest(req)));
            }

        }
    } catch (error) {
        prepareErrorLog(error, createBulkSubCategoryValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const  deleteSubCategoryByIdValidator = async (req, res, next) => {
    try {
        const bodyReceived = new DeleteSubCategoryRequestModel(req.body);
        const result = deleteSubCategoryValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, deleteSubCategoryByIdValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const updateSubCategoryByIdValidator = async (req,res,next)=>{
    try {
        const bodyReceived = new SubCategoryRequestModel(req.body);
        const result = updateSubCategoryValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        } else {
            if(!hasSpecialChars(bodyReceived.name)){
                req.requestModel = bodyReceived;
                next();
            }else{
                return res
                    .status(ErrorStatus.CATEGORY_NAME_SPECIAL_CHARACTER.code)
                    .json(prepareErrorResponse(ErrorStatus.CATEGORY_NAME_SPECIAL_CHARACTER, null, extractLanguageFromRequest(req)));
            }

        }
    } catch (error) {
        prepareErrorLog(error, updateSubCategoryByIdValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

module.exports={
    createSubCategoryValidator,
    deleteSubCategoryByIdValidator,
    updateSubCategoryByIdValidator,
    createBulkSubCategoryValidator
}