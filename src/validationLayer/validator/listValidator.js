const {ErrorStatus} = require("../../constants/error/errorStatus");
const {prepareErrorResponse} = require("../../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("../../dataHandlerLayer/common/LanguageHeaderHandler");
const {prepareErrorLog} = require("../../helperKit/loggingKit");
const {CreateListRequestModel} = require("../../dataHandlerLayer/models/requestModel/list/CreateListRequestModel");
const {createListValidationScheme, editListValidationScheme, addProductToListValidationScheme,
    deleteListValidationScheme
} = require("../validation/listValidation");
const {EditListRequestModel} = require("../../dataHandlerLayer/models/requestModel/list/EditListRequestModel");
const {AddProductToListRequestModel} = require("../../dataHandlerLayer/models/requestModel/list/AddProductToListRequestModel");
const {DeleteListRequestModel} = require("../../dataHandlerLayer/models/requestModel/list/DeleteListRequestModel");


const  createListValidator = async (req, res, next) => {
    try {
        const bodyReceived = new CreateListRequestModel(req.body);
        const result = createListValidationScheme.validate(bodyReceived);
        if (result.error) {
            return  res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, createListValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


const editListNameValidator = async (req,res,next)=>{
    try {
        const bodyReceived = new EditListRequestModel(req.body);
        const result = editListValidationScheme.validate(bodyReceived);
        if (result.error) {
            return  res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, editListNameValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}


const addProductToListValidator = async (req,res,next)=>{
    try {
        const bodyReceived = new AddProductToListRequestModel(req.body);
        const result = addProductToListValidationScheme.validate(bodyReceived);
        if (result.error) {
            return  res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    }catch (error) {
        prepareErrorLog(error, addProductToListValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const deleteListValidator = async (req,res,next)=>{
    try {
        const bodyReceived = new DeleteListRequestModel(req.body);
        const result = deleteListValidationScheme.validate(bodyReceived);
        if (result.error) {
            return  res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    }catch (error) {
        prepareErrorLog(error, deleteListValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}
module.exports = {
    createListValidator,
    editListNameValidator,
    addProductToListValidator,
    deleteListValidator
}