const {ErrorStatus} = require("../../constants/error/errorStatus");
const {
    prepareErrorResponse,
} = require("../../errorHandlerLayer/errorHandler");
const {
    UserRequestModel,
} = require("../../dataHandlerLayer/models/requestModel/user/UserRequestModel");
const {
    CreateUserRequestModel,
} = require("../../dataHandlerLayer/models/requestModel/user/CreateUserRequestModel");
const {
    updateUserByIdValidationScheme,
    changePasswordValidationScheme,
    createUserValidationScheme, updateUserProfileValidationScheme,
} = require("../validation/userValidation");
const {prepareErrorLog} = require("../../helperKit/loggingKit");
const {extractLanguageFromRequest} = require("../../dataHandlerLayer/common/LanguageHeaderHandler");
const {UpdateUserProfileForMobileRequestModel} = require("../../dataHandlerLayer/models/requestModel/user/UpdateUserProfileForMobileRequestModel");


const updateUserByIdFromMobileValidator = (req, res, next) => {
    try {
        const fixedBody = {
            info: {
                general: {
                    ...req.body,
                },
            },
        };
        const bodyReceived = new UserRequestModel(fixedBody);
        const result = updateUserByIdValidationScheme.validate(fixedBody);
        if (result.error) {
            return res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(
                        ErrorStatus.BAD_REQUEST,
                        result?.error?.message, extractLanguageFromRequest(req)
                    )
                );
        } else {
            req.requestModel = bodyReceived;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, updateUserByIdFromMobileValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(
                prepareErrorResponse(
                    ErrorStatus.SERVER_DOWN,
                    null, extractLanguageFromRequest(req)
                )
            );
    }
};

const updateUserProfileFromMobileValidator = (req, res, next) => {
    try {
        const fixedBody = {
            info: {
                general: {
                    ...req.body,
                },
            },
        };
        const bodyReceived = new UpdateUserProfileForMobileRequestModel(fixedBody);
        const result = updateUserProfileValidationScheme.validate(bodyReceived);
        if (result.error) {
            return res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(
                        ErrorStatus.BAD_REQUEST,
                        result?.error?.message, extractLanguageFromRequest(req)
                    )
                );
        } else {
            req.requestModel = bodyReceived;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, updateUserByIdFromMobileValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(
                prepareErrorResponse(
                    ErrorStatus.SERVER_DOWN,
                    null, extractLanguageFromRequest(req)
                )
            );
    }
};



const updateUserByIdValidator = (req, res, next) => {
    try {
        // work around must fix a proper model for it
        const fixedBody = {
            info: {
                general: {
                    email: req.body.email,
                    name: req.body.name,
                    phoneNumber: req.body.phoneNumber,
                },
            },
            active: req.body.active,
        };
        const bodyReceived = new UserRequestModel(fixedBody);
        const result = updateUserByIdValidationScheme.validate(fixedBody);
        if (result.error) {
            return res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(
                        ErrorStatus.BAD_REQUEST,
                        result?.error?.message, extractLanguageFromRequest(req)
                    )
                );
        } else {
            req.requestModel = fixedBody;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, updateUserByIdValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(
                prepareErrorResponse(
                    ErrorStatus.SERVER_DOWN,
                    null, extractLanguageFromRequest(req)
                )
            );
    }
};

const changePasswordValidator = (req, res, next) => {
    try {
        const bodyReceived = req.body;
        const result = changePasswordValidationScheme.validate(bodyReceived);
        if (result.error) {
            return res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(
                        ErrorStatus.BAD_REQUEST,
                        result?.error?.message, extractLanguageFromRequest(req)
                    )
                );
        } else {
            req.requestModel = bodyReceived;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, changePasswordValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(
                prepareErrorResponse(
                    ErrorStatus.SERVER_DOWN,
                    null, extractLanguageFromRequest(req)
                )
            );
    }
};

const createUserValidator = (req, res, next) => {
    try {
        // work around must fix a proper model for it
        const fixedBody = {
            info: {
                general: {
                    ...req.body,
                },
            },
        };

            const bodyReceived = new UserRequestModel(fixedBody);
            const result = createUserValidationScheme.validate(fixedBody);
            if (result.error) {
                return res
                    .status(ErrorStatus.BAD_REQUEST.code)
                    .json(
                        prepareErrorResponse(
                            ErrorStatus.BAD_REQUEST,
                            result.error?.message, extractLanguageFromRequest(req)
                        )
                    );
            } else {
                req.requestModel = bodyReceived;
                req.newUser = new CreateUserRequestModel(bodyReceived);
                next();
            }
    } catch (error) {
        prepareErrorLog(error, createUserValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(
                prepareErrorResponse(
                    ErrorStatus.SERVER_DOWN,
                    null, extractLanguageFromRequest(req)
                )
            );
    }
};


module.exports = {
    updateUserByIdValidator,
    updateUserByIdFromMobileValidator,
    changePasswordValidator,
    createUserValidator,
    updateUserProfileFromMobileValidator
};
