const {ErrorStatus} = require("../../constants/error/errorStatus");
const {prepareErrorResponse} = require("../../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("../../dataHandlerLayer/common/LanguageHeaderHandler");
const {prepareErrorLog} = require("../../helperKit/loggingKit");
const {CreateStockRequestModel} = require("../../dataHandlerLayer/models/requestModel/stock/CreateStockRequestModel");
const {createStockValidationScheme, updateStockValidationScheme} = require("../validation/stockValidation");
const {UpdateStockRequestModel} = require("../../dataHandlerLayer/models/requestModel/stock/UpdateStockRequestModel");


const  createProductInStockValidator = async (req, res, next) => {
    try {
        const bodyReceived = new CreateStockRequestModel(req.body);
        const result = createStockValidationScheme.validate(bodyReceived);
        if (result.error) {
            return  res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, createProductInStockValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const  updateProductInStockValidator = async (req, res, next) => {
    try {
        const bodyReceived = new UpdateStockRequestModel(req.body);
        const result = updateStockValidationScheme.validate(bodyReceived);
        if (result.error) {
            return  res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, updateProductInStockValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


module.exports = {
    createProductInStockValidator,
    updateProductInStockValidator
}