const {ErrorStatus} = require("../../constants/error/errorStatus");
const {prepareErrorResponse} = require("../../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("../../dataHandlerLayer/common/LanguageHeaderHandler");
const {prepareErrorLog} = require("../../helperKit/loggingKit");
const {CreateBrandRequestModel} = require("../../dataHandlerLayer/models/requestModel/brand/CreateBrandRequestModel");
const {createBrandValidationScheme, deleteBrandValidationScheme, updateBrandValidationScheme,
    createBulkBrandValidationScheme
} = require("../validation/brandValidation");
const {DeleteBrandRequestModel} = require("../../dataHandlerLayer/models/requestModel/brand/DeleteBrandRequestModel");
const {UpdateBrandRequestModel} = require("../../dataHandlerLayer/models/requestModel/brand/UpdateBrandRequestModel");
const {CreateBulkBrandRequestModel} = require("../../dataHandlerLayer/models/requestModel/brand/CreateBulkBrandRequestModel");


const  createBrandValidator = async (req, res, next) => {
    try {
        const bodyReceived = new CreateBrandRequestModel(req.body);
        const result = createBrandValidationScheme.validate(bodyReceived);
        if (result.error) {
            return  res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, createBrandValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


const  createBulkBrandValidator = async (req, res, next) => {
    try {
        const bodyReceived = new CreateBulkBrandRequestModel(req.body);
        const validationResult = createBulkBrandValidationScheme.validate(bodyReceived, { abortEarly: false });
        if (validationResult.error) {
            const errorDetails = validationResult.error.details.map((errorItem) => {
                const path = errorItem.path;
                let position = -1;

                // Extract the array index from the error path
                if (Array.isArray(path) && path.length >= 3 && path[0] === 'brands' && path[2] === 'source') {
                    position = parseInt(path[1]);
                }

                return {
                    objectPosition: position, // Index of the object in the array
                    message: errorItem.message,
                };

            });
            return res.status(ErrorStatus.BAD_REQUEST.code).json(
                prepareErrorResponse(ErrorStatus.SUB_CATEGORY_NAME_SPECIAL_CHARACTER, errorDetails, extractLanguageFromRequest(req)),
            );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, createBulkBrandValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const  deleteBrandValidator = async (req, res, next) => {
    try {
        const bodyReceived = new DeleteBrandRequestModel(req.body);
        const result = deleteBrandValidationScheme.validate(bodyReceived);
        if (result.error) {
            return  res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, deleteBrandValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


const  updateBrandByIdValidator = async (req, res, next) => {
    try {
        const bodyReceived = new UpdateBrandRequestModel(req.body);
        const result = updateBrandValidationScheme.validate(bodyReceived);
        if (result.error) {
            return  res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, updateBrandByIdValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


module.exports = {
    createBrandValidator,
    deleteBrandValidator,
    updateBrandByIdValidator,
    createBulkBrandValidator
}