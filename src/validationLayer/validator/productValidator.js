const {ErrorStatus} = require("../../constants/error/errorStatus");
const {prepareErrorResponse} = require("../../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("../../dataHandlerLayer/common/LanguageHeaderHandler");
const {hasSpecialChars} = require("../../helperKit/helperKit");
const {prepareErrorLog} = require("../../helperKit/loggingKit");
const {CreateProductRequestModel} = require("../../dataHandlerLayer/models/requestModel/product/CreateProductRequestModel");
const {createProductValidationScheme, deleteNutritionProductValidationScheme, deleteProductByIdsValidationScheme, updateProductByIdValidationScheme, createBulkProductValidationScheme} = require("../validation/productValidation");
const {DeleteNutritionRequestModel} = require("../../dataHandlerLayer/models/requestModel/nutrition/DeleteNutritionRequestModel");
const {DeleteProductByIdsRequestModel} = require("../../dataHandlerLayer/models/requestModel/product/DeleteProductByIdsRequestModel");
const {UpdateProductRequestModel} = require("../../dataHandlerLayer/models/requestModel/product/UpdateProductRequestModel");
const {CreateBulkProductRequestModel} = require("../../dataHandlerLayer/models/requestModel/product/CreateBulkProductRequestModel");

const createProductValidator = async (req, res, next) => {
    try {
        const bodyReceived = new CreateProductRequestModel(req.body);
        const result = createProductValidationScheme.validate(bodyReceived);
        if (result.error) {
            return res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req)));
        } else {
            if (!hasSpecialChars(bodyReceived.name)) {
                req.requestModel = bodyReceived;
                next();
            } else {
                return res
                    .status(ErrorStatus.PRODUCT_NAME_SPECIAL_CHARACTER.code)
                    .json(prepareErrorResponse(ErrorStatus.PRODUCT_NAME_SPECIAL_CHARACTER, null, extractLanguageFromRequest(req)));
            }

        }
    } catch (error) {
        prepareErrorLog(error, createProductValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const createBulkProductValidator = async (req, res, next) => {
    try {
        const bodyReceived = new CreateBulkProductRequestModel(req.body);
        const validationResult = createBulkProductValidationScheme.validate(bodyReceived, { abortEarly: false });

        if (validationResult.error) {
            const errorDetails = validationResult.error.details.map((errorItem) => {
                const path = errorItem.path;
                let position = -1;

                // Extract the array index from the error path
                if (Array.isArray(path)) {
                    position = parseInt(path[1]);
                }

                return {
                    objectPosition: position, // Index of the object in the array
                    message: errorItem.message,
                };
            });

            res.status(ErrorStatus.BAD_REQUEST.code).json(
                prepareErrorResponse(ErrorStatus.SUB_CATEGORY_NAME_SPECIAL_CHARACTER, errorDetails, extractLanguageFromRequest(req)),
            );
        }else{
            req.requestModel = bodyReceived;
            next()
        }
    } catch (error) {
        prepareErrorLog(error, createBulkProductValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const deleteNutritionInProductValidator = async (req, res, next) => {
    try {
        const bodyReceived = new DeleteNutritionRequestModel(req.body);
        const result = deleteNutritionProductValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next()
    } catch (error) {
        prepareErrorLog(error, deleteNutritionInProductValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const deleteProductByIdsValidator = async (req, res, next) => {
    try {
        const bodyReceived = new DeleteProductByIdsRequestModel(req.body);
        const result = deleteProductByIdsValidationScheme.validate(bodyReceived);
        if (result.error) {
            return res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next()
    } catch (error) {
        prepareErrorLog(error, deleteProductByIdsValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}


const updateProductByIdValidator = async (req, res, next) => {
    try {
        const bodyReceived = new UpdateProductRequestModel(req.body);
        bodyReceived.sort_order = req.body.sort_order ? parseInt(req.body.sort_order) : 1;
        const result = updateProductByIdValidationScheme.validate(bodyReceived);
        if (result.error) {
            return res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next()

    } catch (error) {
        prepareErrorLog(error, updateProductByIdValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}
module.exports = {
    createProductValidator,
    deleteNutritionInProductValidator,
    deleteProductByIdsValidator,
    updateProductByIdValidator,
    createBulkProductValidator
}
