const {CreateStockRequestModel} = require("../../dataHandlerLayer/models/requestModel/stock/CreateStockRequestModel");
const {ErrorStatus} = require("../../constants/error/errorStatus");
const {prepareErrorResponse} = require("../../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("../../dataHandlerLayer/common/LanguageHeaderHandler");
const {prepareErrorLog} = require("../../helperKit/loggingKit");
const {createOrderValidationScheme} = require("../validation/orderValidation");
const {CreateOrderRequestModel} = require("../../dataHandlerLayer/models/requestModel/order/CreateOrderRequestModel");


const  createOrderValidator = async (req, res, next) => {
    try {
        const bodyReceived = new CreateOrderRequestModel(req.body);
        const result = createOrderValidationScheme.validate(bodyReceived);
        if (result.error) {
            return  res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, createOrderValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

module.exports = {
    createOrderValidator
}