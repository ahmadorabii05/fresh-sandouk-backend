const {ErrorStatus} = require("../../constants/error/errorStatus");
const {prepareErrorResponse} = require("../../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("../../dataHandlerLayer/common/LanguageHeaderHandler");
const {prepareErrorLog} = require("../../helperKit/loggingKit");
const {CreateAddressRequestModel} = require("../../dataHandlerLayer/models/requestModel/address/CreateAddressRequestModel");
const {createAddressValidationScheme, deleteAddressValidationScheme, updateAddressValidationScheme} = require("../validation/addressValidation");
const {DeleteAddressRequestModel} = require("../../dataHandlerLayer/models/requestModel/address/DeleteAddressRequestModel");
const {UpdateAddressRequestModel} = require("../../dataHandlerLayer/models/requestModel/address/UpdateAddressRequestModel");


const  createAddressValidator = async (req, res, next) => {
    try {
        const bodyReceived = new CreateAddressRequestModel(req.body);
        const result = createAddressValidationScheme.validate(bodyReceived);
        if (result.error) {
            return  res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, createAddressValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const  deleteAddressValidator = async (req, res, next) => {
    try {
        const bodyReceived = new DeleteAddressRequestModel(req.body);
        const result = deleteAddressValidationScheme.validate(bodyReceived);
        if (result.error) {
            return  res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, deleteAddressValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


const  updateAddressByIdValidator = async (req, res, next) => {
    try {
        const bodyReceived = new UpdateAddressRequestModel(req.body);
        const result = updateAddressValidationScheme.validate(bodyReceived);
        if (result.error) {
            return  res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, updateAddressByIdValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


module.exports = {
    createAddressValidator,
    deleteAddressValidator,
    updateAddressByIdValidator
}