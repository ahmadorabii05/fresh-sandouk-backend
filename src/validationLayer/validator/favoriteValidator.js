const {ErrorStatus} = require("../../constants/error/errorStatus");
const {prepareErrorResponse} = require("../../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("../../dataHandlerLayer/common/LanguageHeaderHandler");
const {prepareErrorLog} = require("../../helperKit/loggingKit");
const {createFavoriteValidationScheme} = require("../validation/favoriteValidation");
const {CreateFavoriteRequestModel} = require("../../dataHandlerLayer/models/requestModel/Favorite/CreateFavoriteRequestModel");


const  createFavoriteValidator = async (req, res, next) => {
    try {
        const bodyReceived = new CreateFavoriteRequestModel(req.body);
        const result = createFavoriteValidationScheme.validate(bodyReceived);
        if (result.error) {
          return  res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }
        req.requestModel = bodyReceived;
        next();
    } catch (error) {
        prepareErrorLog(error, createFavoriteValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

module.exports = {
    createFavoriteValidator
}