const {ErrorStatus} = require("../../constants/error/errorStatus");
const {prepareErrorResponse} = require("../../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("../../dataHandlerLayer/common/LanguageHeaderHandler");
const {prepareErrorLog} = require("../../helperKit/loggingKit");
const {addToCartValidationScheme, decreaseToCartValidationScheme, deleteItemFromCartValidationScheme} = require("../validation/cartValidation");
const {AddToCartRequestModel} = require("../../dataHandlerLayer/models/requestModel/cart/AddToCartRequestModel");
const {DecreaseToCartRequestModel}=require("../../dataHandlerLayer/models/requestModel/cart/DecreaseToCartRequestModel")
const {RemoveItemsFromCartRequestModel} = require("../../dataHandlerLayer/models/requestModel/cart/RemoveItemsFromCartRequestModel");

const  addToCartValidator = async (req, res, next) => {
    try {
        const bodyReceived = new AddToCartRequestModel(req.body);
        const result = addToCartValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }else{
            req.requestModel = bodyReceived;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, addToCartValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


const  decreaseToCartValidator = async (req, res, next) => {
    try {
        const bodyReceived = new DecreaseToCartRequestModel(req.body);
        const result = decreaseToCartValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }else{
            req.requestModel = bodyReceived;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, decreaseToCartValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


const  deleteItemFromCartValidator = async (req, res, next) => {
    try {
        const bodyReceived = new RemoveItemsFromCartRequestModel(req.body);
        const result = deleteItemFromCartValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        }else{
            req.requestModel = bodyReceived;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, deleteItemFromCartValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};
module.exports = {
    addToCartValidator,
    decreaseToCartValidator,
    deleteItemFromCartValidator
}