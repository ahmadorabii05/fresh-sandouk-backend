const {} = require("../validation/roleValidation");
const {ErrorStatus} = require("../../constants/error/errorStatus");
const {prepareErrorResponse} = require("../../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("../../dataHandlerLayer/common/LanguageHeaderHandler");
const {prepareErrorLog} = require("../../helperKit/loggingKit");
const {createTagsValidationScheme, updateTagsValidationScheme, deleteTagsValidationScheme,
    createBulkTagsValidationScheme
} = require("../validation/tagsValidation");
const {CreateTagsRequestModel} = require("../../dataHandlerLayer/models/requestModel/tags/CreateTagsRequestModel");
const {DeleteTagsRequestModel} = require("../../dataHandlerLayer/models/requestModel/tags/DeleteTagsRequestModel");
const {CreateBulkTagsRequestModel} = require("../../dataHandlerLayer/models/requestModel/tags/CreateBulkTagsRequestModel");


const  createTagsValidator = async (req, res, next) => {
    try {
        const bodyReceived = new CreateTagsRequestModel(req.body);
        const result = createTagsValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        } else {
            req.requestModel = bodyReceived;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, createTagsValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const  createBulkTagsValidator = async (req, res, next) => {
    try {
        const bodyReceived = new CreateBulkTagsRequestModel(req.body);
        const validationResult = createBulkTagsValidationScheme.validate(bodyReceived, { abortEarly: false });

        if (validationResult.error) {
            const errorDetails = validationResult.error.details.map((errorItem) => {
                const path = errorItem.path;
                let position = -1;

                // Extract the array index from the error path
                if (Array.isArray(path) && path.length >= 3 && path[0] === 'tags' && path[2] === 'name') {
                    position = parseInt(path[1]);
                }

                return {
                    objectPosition: position, // Index of the object in the array
                    message: errorItem.message,
                };
            });

            res.status(ErrorStatus.BAD_REQUEST.code).json(
                prepareErrorResponse(ErrorStatus.SUB_CATEGORY_NAME_SPECIAL_CHARACTER, errorDetails, extractLanguageFromRequest(req)),
            );
        } else {
            req.requestModel = bodyReceived;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, createBulkTagsValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const updateTagsValidator = async (req,res,next)=>{
    try {
        const bodyReceived = new CreateTagsRequestModel(req.body);
        const result = updateTagsValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        } else {
            req.requestModel = bodyReceived;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, updateTagsValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const deleteTagsValidator = async (req,res,next)=>{
    try {
        const bodyReceived = new DeleteTagsRequestModel(req.body);
        const result = deleteTagsValidationScheme.validate(bodyReceived);
        if (result.error) {
            res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(
                    prepareErrorResponse(ErrorStatus.BAD_REQUEST, result?.error?.message, extractLanguageFromRequest(req))
                );
        } else {
            req.requestModel = bodyReceived;
            next();
        }
    } catch (error) {
        prepareErrorLog(error, deleteTagsValidator.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}


module.exports = {
    createTagsValidator,
    updateTagsValidator,
    deleteTagsValidator,
    createBulkTagsValidator
}