const mongoose = require("mongoose");

/**
 * Prepares connection to the database
 */
const prepareDbConnection = async () => {

    // mongoose.connect(
    //     process.env.DB_URL,
    //     {
    //         useNewUrlParser: true,
    //         useUnifiedTopology: true,
    //         //   useFindAndModify: false,
    //         //   useCreateIndex: true,
    //     },
    //     (err) => {
    //         if (err) return console.log("Connection to DB Failed");
    //         console.log("Connection to DB Successful");
    //         console.log(`Connecting to DB: ${process.env.DB_NAME}`);
    //         console.log(`Connection URL: ${process.env.DB_URL}`);
    //     }
    // );
    const dbURL = process.env.DB_URL;

    if (!dbURL) {
        console.error("DB_URL environment variable is not set.");
        return;
    }

    try {
        await mongoose.connect(process.env.DB_URL, {
            // useNewUrlParser: true,
            // useUnifiedTopology: true,
            // useNewUrlParser: true,
            // useMongoClient: true
            // useFindAndModify: false,
            // useCreateIndex: true,
        });
        console.log("Connection to DB Successful");
        console.log(`Connecting to DB: ${process.env.DB_NAME}`);
        console.log(`Connection URL: ${process.env.DB_URL}`);
    } catch (err) {
        console.error("Connection to DB Failed", err);
    }
};

module.exports = {
    prepareDbConnection,
};
