const AddressModel = require("../../models/address/AddressModel");

const createAddress = async (body) => {
    return await AddressModel(body).save();
}

const getAddressById = async (id) => {
    return await AddressModel.findById({_id: id})
}

const deleteAddressById = async (id) => {
    return await AddressModel.findByIdAndDelete({_id: id})
}

const getAddressByUser = async (userId) => {
    return await AddressModel.findOne({userId: userId});
}

const updateAddressById = async (id, body) => {
    const query = {};
    if (body.addressTitle) {
        query["addressTitle"] = body.addressTitle;
    }
    if (body.address) {
        query["address"] = body.address;
    }
    if (body.longtitude) {
        query["longtitude"] = body.longtitude;
    }
    if (body.latitude) {
        query["latitude"] = body.latitude;
    }
    if(body.isDefaultAddress === true){
        query["isDefaultAddress"] = body.isDefaultAddress;
    }
    if(body.isDefaultAddress === false){
        query["isDefaultAddress"] = body.isDefaultAddress;
    }
    if (body.nearestLand) {
        query["nearestLand"] =
            body.nearestLand;
    }
    if (body.contactNumber) {
        query["contactNumber"] = body.contactNumber;
    }
    if (body.street) {
        query["street"] = body.street;
    }


    return AddressModel.findByIdAndUpdate({_id: id}, {...query}, {new: true});
}

const getAllAddressPaginated = async (req,userId) => {
    const {limit, page} = req.query;
    const paginationOptions = {
        limit: parseInt(limit) || 10,
        page: parseInt(page) || 1,
        sort: {"address": "asc"},
        lean: true,
    }
    const query = {
        "userId": userId
    }
    return await AddressModel.paginate(query, paginationOptions)
}

const updateAllAddressToDefaultAddressToFalse = async (userId)=>{
    await AddressModel.updateMany({ userId }, { $set: { isDefaultAddress: false } });
}

const getIfIsDefaultExist = async (userId)=>{
    return await AddressModel.exists({ userId, isDefaultAddress: true });
}

const getLatestRecord = async (userId)=>{
    return await AddressModel.findOne({ userId }).sort({ createdAt: -1 }).limit(1);
}

module.exports = {
    createAddress,
    getAddressById,
    deleteAddressById,
    updateAddressById,
    getAllAddressPaginated,
    getAddressByUser,
    updateAllAddressToDefaultAddressToFalse,
    getIfIsDefaultExist,
    getLatestRecord,

}