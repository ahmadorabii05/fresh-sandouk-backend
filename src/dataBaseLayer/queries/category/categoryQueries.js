const CategoryModel = require("../../models/category/CategoryModel");
const SubCategory = require("../../models/subCategory/SubCategoryModel")
const Product = require("../../models/products/ProductsModel")
const Tags = require("../../models/tags/TagsModel")
const ImageUrls = require("../../models/images/ImageModel");
const CountryTag = require("../../models/country/CountryModel");
const PriceModel = require("../../models/priceModel/PriceModel");
const BrandModel = require("../../models/brand/BrandModel")
const mongoose = require("mongoose");
/**
 *
 * @returns {saved category}
 * @param body
 */
const createCategory = async (body) => {
    return await CategoryModel(body).save();
};

/**
 *
 * @param {id to find the category by} id
 * @returns {potential category if found}
 */
const getCategoryById = async (id, isAdmin) => {

    let isActiveFilter = {};
    if (!isAdmin) {
        isActiveFilter = {
            isActive: true
        }
    }

    return await CategoryModel.findOne({
        _id: id,
    }).populate({
        path: "subCategories",
        model: SubCategory,
        match: { ...isActiveFilter },
        populate: {
            path: "productIds",
            model: Product,
            populate: [
                {
                    path: 'imageUrls',
                    model: ImageUrls,
                },
                {
                    path: 'country',
                    model: CountryTag,
                },
                {
                    path: 'priceModel',
                    model: PriceModel,
                },
                {
                    path: 'brandId',
                    model: BrandModel,
                },
                {
                    path: 'tags',
                    model: Tags,
                },

            ],
        },
    });
};


const getCategoryByIdInfo = async (id) => {
    return CategoryModel.findOne({
        _id: id,
    });
}

const pushSubCategory = async (id, subCategoryId) => {
    return await CategoryModel.findByIdAndUpdate({ _id: id }, {
        $push: {
            subCategoies: subCategoryId
        }
    })
}
/**
 *
 * @returns { updates category }
 * @param id
 * @param category
 */
const updateCategoryById = async (id, category) => {
    const query = {};
    if (category?.name) {
        query["name"] = category.name;
    }
    if (category?.imageUrl) {
        query["imageUrl"] = category.imageUrl;
    }
    if (category?.nameAr) {
        query["nameAr"] = category.nameAr;
    }
    if (category?.color) {
        query["color"] = category.color;
    }
    if (category.isActive === "false") {
        query["isActive"] = category.isActive;
    }
    if (category.isActive === "true") {
        query["isActive"] = category.isActive;
    }

    query["priority"] = category.priority;

    if (Array.isArray(category?.subCategories) && category.subCategories.every(mongoose.Types.ObjectId.isValid)) {
        query["$set"] = { "subCategories": category.subCategories };
    } else {
        query["$set"] = { "subCategories": [] };
    }
    if (typeof category === "object") {
        return await CategoryModel
            .findByIdAndUpdate({ _id: id }, { ...query }, { new: true })
            .populate({
                path: "subCategories",
                model: SubCategory,
                populate: {
                    path: "productIds",
                    model: Product,
                    populate: [
                        {
                            path: 'imageUrls',
                            model: ImageUrls,
                        },
                        {
                            path: 'country',
                            model: CountryTag,
                        },
                        {
                            path: 'priceModel',
                            model: PriceModel,
                        },
                        {
                            path: 'brandId',
                            model: BrandModel,
                        },
                        {
                            path: 'tags',
                            model: Tags,
                        },

                    ]
                }
            });
    } else {
        return null;
    }
};


const updateSubCategory = async (id, subCategoryId) => {
    return CategoryModel.findByIdAndUpdate({
        _id: id
    }, {
        $push: {
            "subCategories": subCategoryId
        }
    })
}

const updateImageUrl = async (id, imageUrl) => {
    return await CategoryModel.findByIdAndUpdate({ _id: id }, {
        imageUrl: imageUrl
    }, { new: true })
}

const updateBanners = async (id, banners) => {
    return await CategoryModel.findByIdAndUpdate({ _id: id }, {
        banners: banners
    }, { new: true })
}

/**
 *
 * @returns { updates category }
 * @param name
 */
const getCategoryByName = async (name) => {
    return CategoryModel.findOne({ name: name });
};

const getCategoryByNameAr = async (nameAr) => {
    return CategoryModel.findOne({ nameAr: nameAr });
}

const addSubCategory = async (id, subCategory) => {
    return await CategoryModel.findByIdAndUpdate({
        _id: id
    }, {
        $push: {
            subCategories: subCategory
        }
    })
}
const removeSubCategories = async (id, subCategory) => {
    return await CategoryModel.findByIdAndUpdate({
        _id: id
    }, {
        $pull: {
            subCategories: subCategory
        }
    })
}

/**
 *
 * @returns { list of categorys }
 */
const getCategories = async () => {
    return await CategoryModel.find();
};

/**
 * Deletes category by id
 */
const deleteCategoryById = async (id) => {
    return CategoryModel.deleteOne({
        _id: id,
    });
};

const getAllCategoriesPaginated = async (req, isAdmin) => {
    const { page, limit } = req.query;

    let isActiveFilter = {};
    if (!isAdmin) {
        isActiveFilter = {
            isActive: true
        }
    }

    const options = {
        page: parseInt(page) || 1,
        limit: parseInt(limit) || 5,
        sort: { priority: 1 },
        populate: [
            {
                path: 'subCategories',
                model: SubCategory,
                match: { ...isActiveFilter },
                options: { sort: { name: 1 } },
                // populate: [
                //     {
                //         path: 'productIds',
                //         model: Product,
                //         populate: [
                //             {
                //                 path: 'imageUrls',
                //                 model: ImageUrls,
                //             },
                //             {
                //                 path: 'country',
                //                 model: CountryTag,
                //             },
                //             {
                //                 path: 'priceModel',
                //                 model: PriceModel,
                //             },
                //             {
                //                 path: 'brandId',
                //                 model: BrandModel,
                //             },
                //             {
                //                 path: 'tags',
                //                 model: Tags,
                //             },

                //         ],
                //     },
                // ],
            },
        ],
        lean: true,
    };

    return await CategoryModel.paginate({ ...isActiveFilter }, options);
};


const getAllCategories = async (req) => {
    // const {page, limit} = req.query;
    // const options = {
    //     page: parseInt(page) || 1,
    //     limit: parseInt(limit) || 5,
    //     populate: [
    //         {
    //             path: 'subCategories',
    //             model: SubCategory,
    //             populate: [
    //                 {
    //                     path: 'productIds',
    //                     model: Product,
    //                     populate: {
    //                         path: 'tags',
    //                         model: Tags
    //                     }
    //                 }
    //             ]
    //         },
    //     ],
    //     lean: true,
    // };
    return await CategoryModel.find().sort({ priority: 1 }).populate({
        path: 'subCategories',
        model: SubCategory,
        populate: {
            path: 'productIds',
            model: Product,
            populate: [
                {
                    path: 'imageUrls',
                    model: ImageUrls,
                },
                {
                    path: 'country',
                    model: CountryTag,
                },
                {
                    path: 'priceModel',
                    model: PriceModel,
                },
                {
                    path: 'brandId',
                    model: BrandModel,
                },
                {
                    path: 'tags',
                    model: Tags,
                },
            ],
        },
    });

};


// Remove the subcategory ID from the parent category's subcategories array
const removeSubCategory = async (subCategoryId) => {
    return await CategoryModel.findOneAndUpdate(
        { subCategories: subCategoryId },
        { $pull: { subCategories: subCategoryId } },
        { new: true }
    );
}

const getAllCategoriesBySearch = async (req, isAdmin) => {
    const { page, limit, searchQuery } = req.query;

    const options = {
        limit: parseInt(limit) || 10,
        page: parseInt(page) || 1,
        sort: { "name": "asc" },
        lean: true,
    };

    const query = {
        // when indexing is applied
        // $text: { $search: searchQuery },
        $or: [
            { name: { $regex: searchQuery, $options: "i" } },
            { nameAr: { $regex: searchQuery, $options: "i" } },
        ]
    };

    let isActiveFilter = {};
    if (!isAdmin) {
        isActiveFilter = {
            isActive: true
        }
    }

    return await CategoryModel.paginate({ ...query, ...isActiveFilter }, options);
};

const getCategoryByNameIgnoreCases = async (name) => {
    return CategoryModel.findOne({ name: new RegExp(name, 'i') });
};

const updateAllToIsActiveTrue = async () => {
    return await CategoryModel.updateMany({}, {
        isActive: true
    });
}

module.exports = {
    createCategory,
    getCategoryById,
    getCategoryByNameIgnoreCases,
    updateCategoryById,
    getCategoryByName,
    getCategories,
    deleteCategoryById,
    getAllCategories,
    getAllCategoriesPaginated,
    getCategoryByIdInfo,
    updateImageUrl,
    addSubCategory,
    removeSubCategories,
    removeSubCategory,
    pushSubCategory,
    updateSubCategory,
    getCategoryByNameAr,
    getAllCategoriesBySearch,
    updateAllToIsActiveTrue,
    updateBanners
};
