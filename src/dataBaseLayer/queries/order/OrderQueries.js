const OrderModel = require("../../models/orders/OrdersModels");
const CountryModel = require("../../models/country/CountryModel");
const PriceModel = require("../../models/priceModel/PriceModel");
const ImageUrlModel = require("../../models/images/ImageModel");
const TagsModel = require("../../models/tags/TagsModel")
const BrandModel = require("../../models/brand/BrandModel")

const createOrder = async (body) => {
    return await OrderModel(body).save();
}

const getOrderById = async (id) => {
    return OrderModel.findById({ _id: id })
        .populate('userId') // Add this line to populate the userId field
        .populate({
            path: 'products',
            populate: {
                path: 'productId',
                populate: [
                    { path: 'tags', model: TagsModel },
                    { path: 'country', model: CountryModel },
                    { path: 'priceModel', model: PriceModel },
                    { path: 'imageUrls', model: ImageUrlModel },
                    { path: "brandId", model: BrandModel }
                ]
            }
        }).populate('addressId');
}

const getAllOrdersForAdminPaginated = async (req) => {
    const { page, limit } = req.query;
    const options = {
        limit: parseInt(limit) || 10,
        page: parseInt(page) || 1,
        sort: { createdAt: -1 },
        populate: [
            {
                path: 'products',
                populate: {
                    path: 'productId',
                    populate: [
                        { path: 'tags', model: TagsModel },
                        { path: 'country', model: CountryModel },
                        { path: 'priceModel', model: PriceModel },
                        { path: 'imageUrls', model: ImageUrlModel },
                        { path: 'brandId', model: BrandModel },
                    ],
                },
            },
            { path: 'userId' },
        ],
    };

    return OrderModel.paginate({}, options);
}
const getAllOrdersByUserPaginated = async (userId, req) => {
    const { page, limit } = req.query;
    const options = {
        limit: parseInt(limit) || 10,
        page: parseInt(page) || 1,
        populate: [
            {
                path: 'products',
                populate: {
                    path: 'productId',
                    populate: [
                        { path: 'tags', model: TagsModel },
                        { path: 'country', model: CountryModel },
                        { path: 'priceModel', model: PriceModel },
                        { path: 'imageUrls', model: ImageUrlModel },
                        { path: 'brandId', model: BrandModel },
                    ],
                },
            },
            { path: 'userId' }
        ],
    };

    return OrderModel.paginate({ userId: userId }, options);
};


const getOrderCount = async (dateFrom, dateTo) => {
    const totalOrders = await OrderModel.aggregate([
        {
            $match: {
                createdAt: {
                    $gte: new Date(dateFrom),
                    $lte: new Date(dateTo),
                },
            },
        },
        {
            $count: 'totalOrders',
        },
    ]);
    if (totalOrders.length === 0) {
        return 0; // No orders within the date range.
    }

    return totalOrders[0].totalOrders;
}

const getAllOrdersBySearch = async (req) => {
    const { page, limit, searchQuery } = req.query;

    const options = {
        limit: parseInt(limit) || 10,
        page: parseInt(page) || 1,
        populate: [
            {
                path: 'products',
                populate: {
                    path: 'productId',
                    populate: [
                        { path: 'tags', model: TagsModel },
                        { path: 'country', model: CountryModel },
                        { path: 'priceModel', model: PriceModel },
                        { path: 'imageUrls', model: ImageUrlModel },
                        { path: 'brandId', model: BrandModel },
                    ],
                },
            },
            { path: 'userId' },
        ],
    };

    const query = {
        // when indexing is applied
        // $text: { $search: searchQuery },
        $or: [
            { "userId.info.general.name": { $regex: searchQuery, $options: "i" } },
            { "userId.info.general.email": { $regex: searchQuery, $options: "i" } },
        ]
    };
    return await OrderModel.paginate(query, options);
};

const getOrderByCreateIdFromStripe = async (id) => {
    return await OrderModel.findOne({ createIdFromStripe: id }).exec();
}

const updateStatusOrder = async (id) => {
    return await OrderModel.findOneAndUpdate({ createIdFromStripe: id }, {
        "refund": true
    }).exec();
}

const updateStatusOrderFromAdmin = async (id, status) => {
    return await OrderModel.findOneAndUpdate({ _id: id }, {
        status: status
    }).exec();
}

const updateOrderQuantity = async (orderId, productId, newQuantity) => {
    const order = await OrderModel.findById(orderId);

    const productIndex = order.products.findIndex(product => product.productId.equals(productId));

    if (productIndex !== -1) {
        order.products[productIndex].quantity = newQuantity;
        await order.save();

        return order;
    }
}

module.exports = {
    createOrder,
    getOrderById,
    getAllOrdersForAdminPaginated,
    getAllOrdersByUserPaginated,
    getOrderCount,
    getAllOrdersBySearch,
    getOrderByCreateIdFromStripe,
    updateStatusOrder,
    updateOrderQuantity,
    updateStatusOrderFromAdmin

}
