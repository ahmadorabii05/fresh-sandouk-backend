const UserModel = require("../../models/user/userModel");
const {applyActiveFilters} = require("../common/filterQueries");
const CartModel = require("../../models/cart/CartModel")
const mongoose = require("mongoose");
const { calculateFinalPriceAfterDiscount } = require("../../../helperKit/productKit");

/**
 *
 * @param {email to find the user by} email
 * @returns {potential user if found}
 */
const getUserInfoByEmail = async (email) => {
    const checked_email = email.toLowerCase();
    return await UserModel.findOne({
        "info.general.email": checked_email,
    }).populate("info.general.role");
};

const getUserByResetToken = async (sentToken) => {
    return await UserModel.findOne({
        ["info.general.resetToken"]: sentToken,
        ["info.general.expireResetToken"]: { $gt: Date.now() },
    });
};
/**
 *
 * @returns {potential user if found}
 * @param name
 */
const getUserInfoByName = async (name) => {
    return await UserModel.findOne({
        "info.general.name": name,
    }).populate("info.general.role");
};
/**
 *
 * @param {id to find the user by} id
 * @returns {potential user if found}
 */
const getUserInfoById = async (id) => {
    return await UserModel.findOne({
        _id: id,
    }).populate("info.general.role");
};

const getTotalUsers = async (role)=>{
    const roleId = mongoose.Types.ObjectId(role); // Replace with the actual ObjectId of the 'user' role

    return await UserModel.countDocuments({
        'info.general.role': roleId, // Match the role name 'user'
        // 'info.general.accountVerification': true, // Match isActive as true
        active: true
    });
}

const getAllUsers = async (isActive) => {
    const query = {
        ...applyActiveFilters(isActive),
    }
    return UserModel.find(query).sort("info.general.fullName");
};
/**
 *
 * @returns {potential users if found}
 */
const getAllUsersPaginated = async (req) => {
    const {limit, page, isActive} = req.query;
    const query = {
        ...applyActiveFilters(isActive),
    };
    const paginationOptions = {
        limit: parseInt(limit) || 10,
        page: parseInt(page) || 1,
        sort: {"info.general.name": "asc"},
        populate: [
            {
                path: "info.general.role",
            },
        ],
        lean: true,
    }
    return await UserModel.paginate(query, paginationOptions);
};

const updateUserInfoById = async (id, body) => {
    const query = {};
    if (body.info?.general?.email) {
        query["info.general.email"] = body.info.general.email;
    }
    if (body.info?.general?.name) {
        query["info.general.name"] = body.info.general.name;
    }
    if (body.info?.general?.accountVerification) {
        query["info.general.accountVerification"] =
            body.info.general.accountVerification;
    }
    if (body.info?.general?.role) {
        query["info.general.role"] = body.info.general.role;
    }
    if (body.info?.general?.phoneNumber ) {
        query["info.general.phoneNumber"] =  body.info.general.phoneNumber;
    }

    if (body.active != null) {
        query["active"] = body.active;
    }
    return UserModel.updateOne({_id: id}, {...query});
};

const updateUserProfileForMobile = async (id, body) => {
    const query = {};
    if (body.info?.general?.email) {
        query["info.general.email"] = body.info.general.email;
    }
    if (body.info?.general?.alternativeEmail) {
        query["info.general.alternativeEmail"] = body.info.general.alternativeEmail;
    }
    if (body.info?.general?.firstName) {
        query["info.general.firstName"] =
            body.info.general.firstName;
    }
    if (body.info?.general?.lastName) {
        query["info.general.lastName"] = body.info.general.lastName;
    }
    if (body.info?.general?.phoneNumber ) {
        query["info.general.phoneNumber"] =  body.info.general.phoneNumber;
    }
    if (body.info?.general?.alternativePhoneNumber ) {
        query["info.general.alternativePhoneNumber"] =  body.info.general.alternativePhoneNumber;
    }
    if (body.info?.general?.dateOfBirth ) {
        query["info.general.dateOfBirth"] =  body.info.general.dateOfBirth;
    }
    if (body.info?.general?.nationality ) {
        query["info.general.nationality"] =  body.info.general.nationality;
    }

    if (body.active != null) {
        query["active"] = body.active;
    }
    return UserModel.updateOne({_id: id}, {...query});
};

const updateUserProductById = async (userId, updatedInfo) => {
    return await UserModel.findByIdAndUpdate(
        {_id: userId},
        {$set: {"info.productSettings": updatedInfo}},
    );
};
/**
 *
 * @param {id to find the user by} id
 * @param newPassword
 * @returns { updated user }
 */
const changeUserPassword = async (id, newPassword) => {
    return UserModel.updateOne(
        {
            _id: id,
        },
        {
            $set:{ "info.general.password": newPassword},
        }
    );
};

/**
 *
 * @returns { saved user }
 * @param body
 */
const createUser = async (body) => {
    return await UserModel(body).save();
};

/**
 *
 * @param {id to find the user by} id
 * @param socialLoginType
 * @returns { updated user }
 */
const updateSavedUserSocialLoginTypeById = async (id, socialLoginType) => {
    const query = {};
    if (socialLoginType) {
        query["info.general.socialLoginType"] = socialLoginType;
    }
    return UserModel.updateOne({_id: id}, {...query});
};

/**
 *
 * @param {id to find the user by} id
 * @param role
 * @returns { updated user }
 */
const updateUserRole = async (id, role) => {
    return UserModel.updateOne(
        {
            _id: id,
        },
        {
            "info.general.role": role,
        }
    );
};

const updateUserPushNotificationToken = async (id, notificationToken) => {
    return UserModel.updateOne(
        {
            _id: id,
        },
        {
            "info.notificationSettings.pushNotificationToken": notificationToken,
        }
    );
};

const updateUserImageUrl = async (id, url,ImageName) => {
    return UserModel.updateOne(
        {
            _id: id,
        },
        {
            "info.general.imageUrl": url,
            "info.general.imageProfileName":ImageName
        }
    );
};

const getUserByVerificationCode = async (email, verifyCode) => {
    return UserModel.findOne({
        ["info.general.email"]: email,
        ["info.general.verifyCode"]: verifyCode,
        ["info.general.expireResetToken"]: {$gt: Date.now()},
    });
};

const getUserByVerificationToken = async (sentToken) => {
    return UserModel.findOne({
        ["info.general.verificationToken"]: sentToken,
        ["info.general.expireVerificationToken"]: { $gt: Date.now() },
    });
};

const deleteUserInfoById = async (id) => {
    return UserModel.updateOne({_id: id}, {$set: {active: false}});
    // hard delete logic

    // return await UserModel.deleteOne({
    //   _id: id,
    // });
};

const deleteUserByID = async (id) => {
    return UserModel.deleteOne({
        _id: id,
    });
}

const socialLoginId = async (loginId)=>{
    return UserModel.findOne({"info.general.socialLoginId" : loginId})
}

const updatePhoneNumberCode = async (email, verifyCode) => {
    return UserModel.findOne({
        ["info.general.email"]: email,
        ["info.general.verifyCode"]: verifyCode,
        ["info.general.expireResetToken"]: {$gt: Date.now()},
    });
};

const getMyFavoriteProduct = async (userId, req) => {
    const { limit, page } = req.query;

    const options = {
        limit: parseInt(limit) || 10,
        page: parseInt(page) || 1,
        // You can specify additional options for the paginate() method here
    };

    const pipeline = [
        { $match: { _id: mongoose.Types.ObjectId(userId) } },
        {
            $lookup: {
                from: 'products',
                localField: 'info.productSettings',
                foreignField: '_id',
                as: 'info.productSettings',
            },
        },
        { $unwind: '$info.productSettings' },
        {
            $lookup: {
                from: 'brands',
                localField: 'info.productSettings.brandId',
                foreignField: '_id',
                as: 'info.productSettings.brand',
            },
        },
        { $unwind: '$info.productSettings.brand' },
        {
            $lookup: {
                from: 'images',
                localField: 'info.productSettings.imageUrls',
                foreignField: '_id',
                as: 'info.productSettings.imageUrls',
            },
        },
        {
            $lookup: {
                from: 'prices',
                localField: 'info.productSettings.priceModel',
                foreignField: '_id',
                as: 'info.productSettings.priceModel',
            },
        },
        {
            $lookup: {
                from: 'countries',
                localField: 'info.productSettings.country',
                foreignField: '_id',
                as: 'info.productSettings.country',
            },
        },
        {
            $lookup: {
                from: 'tags',
                localField: 'info.productSettings.tags',
                foreignField: '_id',
                as: 'info.productSettings.tags',
            },
        },
        {
            $group: {
                _id: '$_id',
                productSettings: { $push: '$info.productSettings' },
            },
        },
        {
            $project: {
                _id: 0,
                productSettings: {
                    $slice: ['$productSettings', (options.page - 1) * options.limit, options.limit],
                },
            },
        },
    ];


    const cart = await CartModel.findOne({ userId }).lean();
    const result = await UserModel.aggregate(pipeline);
    if(result.length > 0){
        return result[0].productSettings?.map((product) => {
            const productInCart = cart?.productIds?.find(
                (item) => item.productId.toString() === product._id.toString()
            );
            // const discountPercentage = product.isDiscount ? parseInt(product.discountValue) || 0 : 0;
            const discountValue = parseFloat(product.discountValue) || 0;
            const finalPrice = calculateFinalPriceAfterDiscount(product.price, discountValue);
            return {
                ...product,
                inCart: !!productInCart,
                quantityInCart: productInCart ? productInCart.quantity : 0,
                finalPriceAfterDiscount: finalPrice.toFixed(2), // Round the final price to 2 decimal places
            };
        });
    }else{
        return []
    }

};

const deductPriceFromWallet = async (id,price)=>{

    return UserModel.findByIdAndUpdate({_id:id},{
        $inc:{"info.general.walletBalance":-price}
    })
}

const addPriceFromWallet = async (id,price)=>{

    return UserModel.findByIdAndUpdate({_id:id},{
        $inc:{"info.general.walletBalance":+price}
    })
}

const getAllUsersBySearch = async (req) => {
    const {page, limit, searchQuery} = req.query;

    const options = {
        limit: parseInt(limit) || 10,
        page: parseInt(page) || 1,
        sort: {"info.general.name": "asc"},
        populate: [
            {
                path: "info.general.role",
            },
        ],
        lean: true,
    };

    const query = {
        // when indexing is applied
        // $text: { $search: searchQuery },
        $or: [
            { "info.general.name": { $regex: searchQuery, $options: "i" } },
            { "info.general.email": { $regex: searchQuery, $options: "i" } },
        ]
    };
    return await UserModel.paginate(query, options);
};

module.exports = {
    getUserInfoByEmail,
    getUserInfoById,
    getAllUsers,
    getAllUsersPaginated,
    updateUserInfoById,
    changeUserPassword,
    createUser,
    updateSavedUserSocialLoginTypeById,
    updateUserRole,
    deleteUserInfoById,
    getUserByVerificationCode,
    getUserByVerificationToken,
    updateUserPushNotificationToken,
    updateUserImageUrl,
    deleteUserByID,
    getUserInfoByName,
    socialLoginId,
    updateUserProductById,
    getMyFavoriteProduct,
    deductPriceFromWallet,
    updateUserProfileForMobile,
    getTotalUsers,
    getAllUsersBySearch,
    getUserByResetToken,
    addPriceFromWallet
};
