const BrandModel = require("../../models/brand/BrandModel");

const createBrand = async (body) => {
    return await BrandModel(body).save();
}

const getBrandById = async (id) => {
    return await BrandModel.findById({_id: id})
}

const deleteBrandById = async (id) => {
    return await BrandModel.findByIdAndDelete({_id: id})
}

const getBrandBySource = async (unit) => {
    return await BrandModel.findOne({source: unit});
}

const getBrandSourceName = async (name)=>{
    return await BrandModel.findOne({source: { $regex: new RegExp(name, 'i') }});

}
const getBrandBySourceAr = async (unit) => {
    return await BrandModel.findOne({sourceAr: unit});
}
const updateBrandById = async (id, unit) => {
    const query = {};
    if (unit?.source) {
        query["source"] = unit?.source;
    }
    if (unit?.sourceAr) {
        query["sourceAr"] = unit?.sourceAr;
    }
    if(unit?.imageUrl){
        query["imageUrl"]=unit?.imageUrl;
    }
    if (typeof unit === "object") {
        return BrandModel.findByIdAndUpdate({_id: id}, {
            ...query
        }, {new: true});
    }
}

const getAllBrandPaginated = async (req) => {
    const {limit, page} = req.query;
    const paginationOptions = {
        limit: parseInt(limit) || 10,
        page: parseInt(page) || 1,
        sort: {"source": "asc"},
        lean: true,
    }

    return await BrandModel.paginate({}, paginationOptions)
}

const updateBrandImageUrl = async (id, imageUrl) => {
    return await BrandModel.findByIdAndUpdate({_id: id}, {
        imageUrl: imageUrl
    }, {new: true})
}

const getAllBrandsBySearch = async (req) => {
    const {page, limit, searchQuery} = req.query;

    const options = {
        limit: parseInt(limit) || 10,
        page: parseInt(page) || 1,
        sort: {"source": "asc"},
        lean: true,
    };

    const query = {
        // when indexing is applied
        // $text: { $search: searchQuery },
        $or: [
            { source: { $regex: searchQuery, $options: "i" } },
            { sourceAr: { $regex: searchQuery, $options: "i" } },
        ]
    };
    return await BrandModel.paginate(query, options);
};

module.exports = {
    createBrand,
    getBrandById,
    deleteBrandById,
    updateBrandById,
    getAllBrandPaginated,
    getBrandBySource,
    getBrandBySourceAr,
    updateBrandImageUrl,
    getBrandSourceName,
    getAllBrandsBySearch
}