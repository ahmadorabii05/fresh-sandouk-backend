const StockModel = require("../../models/stock/StockModel");
const TagsModel = require("../../models/tags/TagsModel");
const CountryModel = require("../../models/country/CountryModel");
const PriceModel = require("../../models/priceModel/PriceModel");
const ImageUrlModel = require("../../models/images/ImageModel");
const BrandModel = require("../../models/brand/BrandModel");
const {extractLanguageFromRequest} = require("../../../dataHandlerLayer/common/LanguageHeaderHandler");

const createProductInStock = async (body) => {
    return await StockModel(body).save();
}

const getStockById = async (id) => {
    return await StockModel.findById(id).populate({
        path: "productId",
        select: "tags imageUrls country PriceModel",
    });
};

const getStockByProductId = async (productId)=>{
    return await StockModel.findOne({productId:productId})
}


const updateStockById = async (id,quantity)=>{
    return StockModel.findOneAndUpdate({productId:id},{
        $inc: {
            quantity: -quantity,
            quantitySold: quantity
        }
    },{new:true})
}

const updateProductQuantityById = async (id,quantity)=>{
    return await  StockModel.findOneAndUpdate({productId:id},{
        $inc: {
            quantity: quantity,
        }
    },{new:true})
}

const updateProductQuantityByIdFixed = async (id,quantity, quantitySold)=>{
    return await  StockModel.findOneAndUpdate({productId:id},{
        quantity: quantity,
        quantitySold: quantitySold
    },{new:true})
}

const updateProductFromStripe = async (id,quantity)=>{
    return await  StockModel.findOneAndUpdate({productId:id},{
        $inc: {
            quantity: quantity,
            quantitySold:-quantity
        },

    },{new:true})
}
const updateProductQuantityFromProductById = async (id,quantity)=>{
    return await  StockModel.findOneAndUpdate({productId:id},{
        $set: {
            quantity: quantity,
        }
    },{new:true})
}


const checkIfProductIdExist = async (productId)=>{
    return await StockModel.findOne({productId:productId})
}
const getAllProductInStock = async (req) => {
    const { limit, page, search } = req.query;
    const query = {};


    const paginationOptions = {
        limit: parseInt(limit) || 10,
        page: parseInt(page) || 1,
        lean: true,
        populate: {
            path: 'productId',
            populate: [
                { path: 'tags', model: TagsModel },
                { path: 'country', model: CountryModel },
                { path: 'priceModel', model: PriceModel },
                { path: 'imageUrls', model: ImageUrlModel },
                { path: "brandId", model: BrandModel }
            ]
        }
    };

    const result = await StockModel.paginate(query, paginationOptions);

    // Filter products based on search term
    if (search) {
        result.docs = result.docs.filter(doc => {
            const product = doc.productId;
            if(extractLanguageFromRequest(req) === "en"){
                return product && product.name.toLowerCase().includes(search.toLowerCase());
            }else{
                return product && product.nameAr.toLowerCase().includes(search.toLowerCase());
            }

        });
    }

    return result;
};

const getTotalSales = async (dateFrom, dateTo) => {
    const salesAggregation = await StockModel.aggregate([
        {
            $lookup: {
                from: 'products', // Use the name of your Products collection
                localField: 'productId',
                foreignField: '_id',
                as: 'product',
            },
        },
        {
            $unwind: '$product',
        },
        {
            $match: {
                createdAt: {
                    $gte: new Date(dateFrom),
                    $lte: new Date(dateTo),
                },
            },
        },
        {
            $addFields: {
                discountValueNumeric: {
                    $cond: {
                        if: { $eq: ['$product.discountValue', ''] }, // Check if discountValue is empty
                        then: 0, // Set to 0 if empty
                        else: { $toDouble: '$product.discountValue' }, // Convert to numeric type if not empty
                    },
                },
            },
        },
        {
            $project: {
                _id: 0,
                productName: '$product.name',
                price: '$product.price',
                quantitySold: '$quantitySold',
                discountPercentage: '$discountValueNumeric', // Use the parsed discountValueNumeric
                totalDiscount: {
                    $multiply: ['$product.price', '$quantitySold', { $divide: ['$discountValueNumeric', 100] }],
                },
                totalSales: {
                    $subtract: [
                        { $multiply: ['$product.price', '$quantitySold'] },
                        {
                            $multiply: [
                                { $multiply: ['$product.price', '$quantitySold', { $divide: ['$discountValueNumeric', 100] }] },
                                { $divide: ['$discountValueNumeric', 100] },
                            ],
                        },
                    ],
                },
            },
        },
        {
            $group: {
                _id: null,
                totalSales: { $sum: '$totalSales' },
                totalDiscount: { $sum: '$totalDiscount' },
            },
        },
    ]);

    if (salesAggregation.length === 0) {
        return {
            totalSales: 0,
            totalDiscount: 0,
        };
    }

    return {
        totalSales: salesAggregation[0].totalSales,
        totalDiscount: salesAggregation[0].totalDiscount,
    };
}
module.exports = {
    createProductInStock,
    getStockById,
    updateStockById,
    getAllProductInStock,
    checkIfProductIdExist,
    getStockByProductId,
    updateProductQuantityById,
    updateProductQuantityFromProductById,
    getTotalSales,
    updateProductFromStripe,
    updateProductQuantityByIdFixed
}