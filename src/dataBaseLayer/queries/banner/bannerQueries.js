const BannerModel = require("../../models/banner/BannerModel");

const createBanner = async (body) => {
    return await BannerModel(body).save();
}

const getBannerById = async (id) => {
    return await BannerModel.findById({ _id: id })
}

const deleteBannerById = async (id) => {
    return await BannerModel.findByIdAndDelete({ _id: id })
}

const updateBannerById = async (id, unit) => {
    const query = {};
    if (unit?.actionUrl) {
        query["actionUrl"] = unit?.actionUrl;
    }
    if (unit?.location) {
        query["location"] = unit?.location;
    }
    if (unit?.priority != null) {
        query["priority"] = unit?.priority;
    }
    if (unit?.imageUrl) {
        query["imageUrl"] = unit?.imageUrl;
    }
    if (typeof unit === "object") {
        return BannerModel.findByIdAndUpdate({ _id: id }, {
            ...query
        }, { new: true });
    }
}

const getAllBannerPaginated = async (req) => {
    const { limit, page } = req.query;
    const paginationOptions = {
        limit: parseInt(limit) || 10,
        page: parseInt(page) || 1,
        lean: true,
    }

    return await BannerModel.paginate({}, paginationOptions)
}

const updateBannerImageUrl = async (id, imageUrl) => {
    return await BannerModel.findByIdAndUpdate({ _id: id }, {
        imageUrl: imageUrl
    }, { new: true })
}
module.exports = {
    createBanner,
    getBannerById,
    deleteBannerById,
    updateBannerById,
    getAllBannerPaginated,
    updateBannerImageUrl
}