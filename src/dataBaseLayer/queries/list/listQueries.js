const ListModel = require("../../models/list/ListModel")
const TagModel = require("../../models/tags/TagsModel")
const TagsModel = require("../../models/tags/TagsModel");
const CountryTag = require("../../models/country/CountryModel");
const ImageUrls = require("../../models/images/ImageModel");
const PriceModel = require("../../models/priceModel/PriceModel");
const BrandModel = require("../../models/brand/BrandModel");
const getListByNameAndUserId = async (name, userId) => {
    return await ListModel.findOne({name, userId});
};

const createList = async (body) => {
    const newList = await ListModel(body).save();
    await newList
        .populate({
            path: "productIds",
            populate: [
                {
                    path: 'tags',
                    model: TagsModel,
                },
                {
                    path: 'country',
                    model: CountryTag,
                },
                {
                    path: 'imageUrls',
                    model: ImageUrls,
                },
                {
                    path: 'priceModel',
                    model: PriceModel
                }
                ,
                {
                    path: 'brandId',
                    model: BrandModel
                }]
        })
    return newList;
};

const updateListName = async (listId, name) => {
    return await ListModel.findOneAndUpdate(
        {_id: listId},
        {name: name},
        {new: true}
    );
}

const getPaginatedListsForUser = async (userId, req) => {
    const {limit, page} = req?.query;
    const options = {
        page: parseInt(page) || 1,
        limit: parseInt(limit) || 10,
        sort: {createdAt: -1}, // Sort by createdAt field in descending order
        populate: {
            path: "productIds",
            populate: [
                {
                    path: 'tags',
                    model: TagsModel,
                },
                {
                    path: 'country',
                    model: CountryTag,
                },
                {
                    path: 'imageUrls',
                    model: ImageUrls,
                },
                {
                    path: 'priceModel',
                    model: PriceModel
                }
                ,
                {
                    path: 'brandId',
                    model: BrandModel
                }]
        }
    };

    const query = {userId};

    return await ListModel.paginate(query, options);

};



const addProductToList = async (listId, productId) => {
    return await ListModel.findByIdAndUpdate(
        listId,
        {$addToSet: {productIds: productId}},
        {new: true}
    ).populate({path: "productIds", populate: {path: "tags", model: TagModel}});
}
const removeProductFromList = async (listId, productId) => {
    return await ListModel.findByIdAndUpdate(
        listId,
        {$pull: {productIds: productId}},
        {new: true}
    ).populate({path: "productIds", populate: {path: "tags", model: TagModel}});
}

const getListById = async (listId) => {
    return await ListModel.findById({_id: listId}) .populate({
        path: "productIds",
        populate: [
            {
                path: 'tags',
                model: TagsModel,
            },
            {
                path: 'country',
                model: CountryTag,
            },
            {
                path: 'imageUrls',
                model: ImageUrls,
            },
            {
                path: 'priceModel',
                model: PriceModel
            }
            ,
            {
                path: 'brandId',
                model: BrandModel
            }]
    })
};

const deleteList = async (listId) => {
    return await ListModel.findByIdAndDelete({_id: listId});
}
module.exports = {
    getListByNameAndUserId,
    createList,
    updateListName,
    getPaginatedListsForUser,
    addProductToList,
    removeProductFromList,
    getListById,
    deleteList
}