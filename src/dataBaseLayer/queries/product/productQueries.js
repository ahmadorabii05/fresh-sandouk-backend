const ProductModel = require("../../models/products/ProductsModel");
const TagsModel = require("../../models/tags/TagsModel");
const ImageUrls = require("../../models/images/ImageModel");
const CountryTag = require("../../models/country/CountryModel");
const PriceModel = require("../../models/priceModel/PriceModel");
const BrandModel = require("../../models/brand/BrandModel")
const CategoryModel = require("../../models/category/CategoryModel")
const CartModel = require("../../models/cart/CartModel")
const SubCategoryModel = require("../../models/subCategory/SubCategoryModel")
const StockModel = require("../../models/stock/StockModel")
const mongoose = require("mongoose");
const {calculateFinalPriceAfterDiscount} = require("../../../helperKit/productKit");

const createProduct = async (body) => {
    return await ProductModel(body).save();
};

const getProductById = async (id, isAdmin) => {

    let filters = {
        _id: id
    }
    if (!isAdmin) {
        filters = {
            ...filters,
            isActive: true
        }
    }
    return await ProductModel.findById(filters)
        .populate({
            path: "imageUrls",
            model: ImageUrls
        }) // Populates the imageUrls field with referenced documents from the 'Images' model
        .populate({
            path: "country",
            model: CountryTag
        }) // Populates the country field with referenced documents from the 'Countries' model
        .populate({
            path: "priceModel",
            model: PriceModel
        }) // Populates the pricingModel field with referenced documents from the 'PriceModel' model
        .populate({
            path: "tags",
            model: TagsModel
        }) // Populates the tags field with referenced documents from the 'Tag' model
        .populate({
            path: "brandId",
            model: BrandModel
        })
        .exec();
}

const getProductByIdInfo = async (id, isAdmin, userId) => {
    let filters = {
        _id: id,
        isDeleted: false
    };
    if (!isAdmin) {
        filters = {
            ...filters,
            isActive: true
        }
    }
    const product = await ProductModel.findOne(filters).populate({
        path: "imageUrls",
        model: ImageUrls
    }) // Populates the imageUrls field with referenced documents from the 'Images' model
        .populate({
            path: "country",
            model: CountryTag
        }) // Populates the country field with referenced documents from the 'Countries' model
        .populate({
            path: "priceModel",
            model: PriceModel
        }) // Populates the pricingModel field with referenced documents from the 'PriceModel' model
        .populate({
            path: "tags",
            model: TagsModel
        })
        .populate({
            path: "brandId",
            model: BrandModel
        });
    if (product) {
        const quantity = await StockModel.findOne({productId: id})

        // const discountPercentage = product.isDiscount ? parseInt(product.discountValue) || 0 : 0;
        const discountValue = parseFloat(product.discountValue) || 0;
        const finalPrice = calculateFinalPriceAfterDiscount(product.price, discountValue);

        const subCategory = await SubCategoryModel.find({productIds: id});


        if (userId) {
            const cartAggregate = await CartModel.aggregate([
                {$match: {userId: mongoose.Types.ObjectId(userId)}},
                {$unwind: "$productIds"},
                {$match: {"productIds.productId": mongoose.Types.ObjectId(id)}},
                {$group: {_id: null, quantityInCart: {$sum: "$productIds.quantity"}}},
            ]);
            const quantityInCart = cartAggregate.length > 0 ? cartAggregate[0].quantityInCart : 0;
            return {
                ...product.toObject(),
                quantityInCart,
                stockQuantity: quantity,
                subCategories: subCategory,
                finalPriceAfterDiscount: finalPrice.toFixed(2), // Round the final price to 2 decimal places
            };
        } else {
            return {
                ...product.toObject(),
                stockQuantity: quantity,
                subCategories: subCategory,
                finalPriceAfterDiscount: finalPrice.toFixed(2), // Round the final price to 2 decimal places};
            }
        }
    } else {
        return product
    }

}

const removeNutrition = async (id, nutritionId) => {
    return ProductModel.findByIdAndUpdate(
        id,
        {$pull: {nutrition: {_id: nutritionId}}},
        {new: true} // Return the updated product
    );
}

const updateProductById = async (id, Products) => {
    const query = {};
    if (Products?.name) {
        query["name"] = Products.name;
    }
    if (Products?.isDiscount === true) {
        query["isDiscount"] = Products.isDiscount;
    }
    if (Products?.isDiscount === false) {
        query["isDiscount"] = Products.isDiscount;
    }
    if (Products?.nameAr) {
        query["nameAr"] = Products.nameAr;
    }
    if (Products?.country) {
        query["country"] = Products.country;
    }
    if (Products?.price) {
        query["price"] = Products.price;
    }
    if (Products?.priceModel) {
        query["priceModel"] = Products.priceModel;
    }
    if (Products?.description) {
        query["description"] = Products.description;
    }
    if (Products?.descriptionAr) {
        query["descriptionAr"] = Products.descriptionAr;
    }

    if (Products?.discountValue) {
        if ((Products?.discountValue == "null" || Products?.discountValue == null)) {
        } else {
            query["discountValue"] = Products.discountValue;
        }
    }
    console.log('SKKU', Products);
    if (Products?.sku && Products.sku !== 'undefined') {
        query["sku"] = Products.sku;
    }

    if (Products?.barcode) {
        query["barcode"] = Products.barcode;
    }

    if (Products?.sort_order) {
        query["sort_order"] = Products.sort_order;
    }

    if (Products?.tags) {
        query["$set"] = {tags: Products.tags};
    } else {
        query["$set"] = {tags: []};
    }

    if (Products?.brandId) {
        query["brandId"] = Products.brandId;
    }

    if (Products?.generalDescription) {
        query["generalDescription"] = Products.generalDescription;
    }

    if (Products?.ingredient) {
        query["ingredient"] = Products.ingredient;
    }

    if (Products?.storage) {
        query["storage"] = Products.storage;
    }

    if (Products?.generalDescriptionAr) {
        query["generalDescriptionAr"] = Products.generalDescriptionAr;
    }

    if (Products?.ingredientAr) {
        query["ingredientAr"] = Products.ingredientAr;
    }

    if (Products?.storageAr) {
        query["storageAr"] = Products.storageAr;
    }

    if (Products?.imageUrls) {
        query["imageUrls"] = Products.imageUrls;
    }

    if (Products.isActive === "false") {
        query["isActive"] = Products.isActive;
    }

    if (Products.isActive === "true") {
        query["isActive"] = Products.isActive;
    }

    if (Products.isFeatured === "false") {
        query["isFeatured"] = Products.isFeatured;
    }
    if (Products.isFeatured === "true") {
        query["isFeatured"] = Products.isFeatured;
    }

    if (Products?.nutrition) {
        query["nutrition"] = Products.nutrition;
    }
    if (typeof Products === "object") {
        return await ProductModel.findByIdAndUpdate({_id: id}, {...query}, {new: true});
    } else return null;
};

const updateImageUrl = async (id, imageUrl) => {
    return await ProductModel.findByIdAndUpdate({_id: id}, {
        imageUrl: imageUrl
    }, {new: true})
}

const updateBulkPrice = async (items) => {
    const updateQueries = [];
    items.forEach(async (item) => {
        updateQueries.push({
            updateOne: {
                filter: {sku: item.barcode},
                update: {price: item.price},
            },
        });
    });

    console.log(items, "hunaaa");
    return await ProductModel.bulkWrite(updateQueries);
}

const deleteTag = async (tagId) => {
    return await ProductModel.updateMany(
        {
            tags: tagId
        }, {
            $pull: {tags: tagId}
        }
    )

}

const getProductByName = async (name) => {
    return ProductModel.findOne({name: name});
};

const getProductByNameIgnoreCases = async (name) => {
    return ProductModel.findOne({name: new RegExp(name, 'i')});
};

const getProductBySKU = async (sku) => {
    return ProductModel.findOne({sku: sku})
}

const getProductByNameAr = async (name) => {
    return ProductModel.findOne({nameAr: name});
};

const getProducts = async () => {
    return await ProductModel.find();
};

const deleteProductById = async (id) => {
    return ProductModel.findOneAndUpdate(
        {_id: id}, // Find the product by its Id
        {$set: {isDeleted: true}}, // Update the isDeleted field to true
        {new: true} // Return the updated product
    );
};

const forceDeleteProductById = async (id) => {
    return ProductModel.deleteOne({_id: id});
};

const getAllProducts = async (req, isAdmin) => {
    const userId = req.userFromToken?._id.valueOf();
    let {page, limit, tag, price, sort_by_name, subcategoryId, brandName, search, countryName, isActive} = req.query;

    let {categoryId} = req.query;
    const options = {
        page: parseInt(page) || 1,
        limit: parseInt(limit) || 1000,
        lean: true,
        populate: [
            {
                path: 'tags',
                model: TagsModel,
            },
            {
                path: 'country',
                model: CountryTag,
            },
            {
                path: 'imageUrls',
                model: ImageUrls,
            },
            {
                path: 'priceModel',
                model: PriceModel
            },
            {
                path: 'brandId',
                model: BrandModel
            }
        ],
    };

    let filter = {isDeleted: false};

    // Create an array to store individual search criteria
    const searchCriteria = [];

    let tagFilter = {isDeleted: false};
    if (isActive !== null && typeof isActive !== "undefined") {
        // Now isActive is not null or undefined
        if (isActive === 'true') {
            tagFilter.isActive = true; // Set as true
        } else if (isActive === 'false') {
            tagFilter.isActive = false; // Set as false
        } else {
            // Handle the case when isActive has a value that's neither 'true' nor 'false'
            // You might want to log a warning or handle this case according to your needs
        }
    } else {
        // Do nothing, keep tagFilter.isActive as it is
        // tagFilter.isActive remains unchanged
    }

    if (!isAdmin) {
        tagFilter.isActive = true
    }

    // Step 1: Find the SubCategories belonging to the given categoryId
    if (categoryId) {
        const category = await CategoryModel.findById(categoryId).lean();
        if (!category) {
            // If the categoryId is not found, return an empty result
            return {
                docs: [],
                totalDocs: 0,
                totalPages: 0,
                page: options.page,
                limit: options.limit,
            };
        }

        const subcategoryIds = category.subCategories || [];

        // Step 2: Extract productIds from the found SubCategories
        const subcategories = await SubCategoryModel.find({_id: {$in: subcategoryIds}}).lean();
        const productIds = subcategories.flatMap((subcategory) => subcategory.productIds || []);

        tagFilter._id = {$in: productIds};
    }


    if (subcategoryId) {
        const subcategory = await SubCategoryModel.findById(subcategoryId).lean();
        const productIds = subcategory?.productIds || [];
        tagFilter._id = {$in: productIds};
    }
    if (search) {
        const productNameOrDescriptionFilter = {
            $or: [
                {name: {$regex: search, $options: 'i'}},
            ],
        };

        searchCriteria.push(productNameOrDescriptionFilter)
    }
    // Combine all search criteria using the $or operator
    if (searchCriteria.length > 0) {
        filter = {
            $or: searchCriteria,
        };
    }

    let sortOrder = {
        sort_order: 1,  // Always sort by sort_order
    };

    if(!isAdmin){
        sortOrder.isFeatured = -1 // Prioritize isFeatured true first
    }


    if (sort_by_name) {
        sortOrder.name = sort_by_name === 'asc' ? 1 : -1;
    }

    if (price) {
        sortOrder.price = price === 'asc' ? 1 : -1;
    }

    if (Object.keys(sortOrder).length === 0) {
        sortOrder.price = -1;
    }

    options.sort = sortOrder;

    if (tag) {
        const tagObj = await TagsModel.findOne({name: tag});
        if (tagObj) {
            tagFilter.tags = tagObj._id;
        } else {
            // If tag name is not found, return empty result
            return {
                docs: [],
                totalDocs: 0,
                totalPages: 0,
                page: options.page,
                limit: options.limit,
            };
        }
    }

    if (brandName) {
        // Find the brand by its name in the Brands model
        const brand = await BrandModel.findOne({source: brandName}).lean();
        if (brand) {
            tagFilter.brandId = brand._id;
        } else {
            // If the brand name is not found, return an empty result
            return {
                docs: [],
                totalDocs: 0,
                totalPages: 0,
                page: options.page,
                limit: options.limit,
            };
        }
    }

    if (countryName) {
        // Find the country by its name in the CountryTag model
        const country = await CountryTag.findOne({country_en: countryName}).lean();
        if (country) {
            tagFilter.country = country._id;
        } else {
            // If the country name is not found, return an empty result
            return {
                docs: [],
                totalDocs: 0,
                totalPages: 0,
                page: options.page,
                limit: options.limit,
            };
        }
    }


    const products = await ProductModel.paginate({...tagFilter, ...filter}, options);
    // Fetch cart items
    let cartItems = [];
    if (userId) {
        cartItems = await CartModel.aggregate([
            {$match: {userId: new mongoose.Types.ObjectId(userId)}},
            {
                $lookup: {
                    from: 'products',
                    localField: 'productIds.productId',
                    foreignField: '_id',
                    as: 'product',
                },
            },
            {$unwind: '$product'},
            {
                $addFields: {
                    quantityInCart: {
                        $arrayElemAt: [
                            {
                                $filter: {
                                    input: '$productIds',
                                    as: 'pi',
                                    cond: {$eq: ['$$pi.productId', '$product._id']},
                                },
                            },
                            0,
                        ],
                    },
                },
            },
            {
                $project: {
                    product: '$product', // Include all fields from the product object
                    quantityInCart: "$quantityInCart.quantity", // Include the quantityInCart field
                },
            },
        ]);
    }

    const mergedProducts = await Promise.all(products.docs.map(async (product) => {
        const cartProduct = cartItems.find((cp) => String(cp.product._id) === String(product._id));
        // const discountPercentage = product.isDiscount ? parseInt(product.discountValue) || 0 : 0;
        const discountValue = parseFloat(product.discountValue) || 0;
        const finalPrice = calculateFinalPriceAfterDiscount(product.price, discountValue);

        // Fetch stock information
        const stockInfo = await StockModel.findOne({productId: product._id}).lean();

        return {
            ...product,
            quantityInCart: cartProduct?.quantityInCart || 0,
            finalPriceAfterDiscount: finalPrice.toFixed(2),
            stockQuantity: stockInfo?.quantity || 0,
            subCategories: await SubCategoryModel.find({productIds: product._id}).lean()

        };
    }));


    return {
        ...products,
        docs: mergedProducts,
    };
};

const getAllProductsBySearch = async (req, isAdmin) => {
    const userId = req.userFromToken?._id.valueOf();
    let {page, limit, tag, price, sort_by_name} = req.query;
    const {searchQuery} = req.query; // Assuming you have a "searchQuery" field in the request body for the product name or description search.

    const options = {
        page: parseInt(page) || 1,
        limit: parseInt(limit) || 1000,
        lean: true,
        populate: [
            {path: 'country', model: CountryTag},
            {path: 'imageUrls', model: ImageUrls},
            {path: 'priceModel', model: PriceModel},
            {path: 'brandId', model: BrandModel},
            {path: 'tags', model: TagsModel},
        ],
    };

    let filter = {isDeleted: false};

    // Create an array to store individual search criteria
    const searchCriteria = [];

    // Search by tag name
    if (tag) {
        const tagObj = await TagsModel.findOne({name: tag});
        if (tagObj) {
            searchCriteria.push({tags: tagObj._id});
        }
    }

    // Search by product name or description
    if (searchQuery) {
        const tagObjs = await TagsModel.find({name: {$regex: new RegExp(searchQuery, 'i')}});
        const countryObjs = await CountryTag.find({country_en: {$regex: new RegExp(searchQuery, 'i')}});

        const tagIds = tagObjs?.map(tagObj => tagObj._id);
        const countryIds = countryObjs?.map(countryObj => countryObj._id);

        if (tagIds.length > 0) {
            searchCriteria.push({tags: {$in: tagIds}});
        }

        if (countryIds.length > 0) {
            searchCriteria.push({country: {$in: countryIds}});
        }

        const subcategoryObjs = await SubCategoryModel.find({name: {$regex: new RegExp(searchQuery, 'i')}});

        const subcategoryProductIds = subcategoryObjs.reduce((accumulator, subcategoryObj) => {
            return accumulator.concat(subcategoryObj.productIds);
        }, []);

        if (subcategoryProductIds.length > 0) {
            searchCriteria.push({_id: {$in: subcategoryProductIds}});
        }


        const productNameOrDescriptionFilter = {
            $or: [
                {name: {$regex: searchQuery, $options: 'i'}},
                // {description: {$regex: searchQuery, $options: 'i'}},
            ],
        };
        searchCriteria.push(productNameOrDescriptionFilter);

        const productSkuOrBarcodeFilter = {
            $or: [

                {sku: {$regex: new RegExp(searchQuery, 'i')}},
                {barcode: {$regex: new RegExp(searchQuery, 'i')}},

            ]
        }
        searchCriteria.push(productSkuOrBarcodeFilter);
    }
    // Combine all search criteria using the $or operator
    if (searchCriteria.length > 0) {
        filter = {
            $or: searchCriteria,
        };
    }
    let sortOrder = {
        isFeatured: -1,  // Prioritize isFeatured true first
        sort_order: 1,  // Always sort by sort_order
    };

    if (sort_by_name) {
        sortOrder.name = sort_by_name === 'asc' ? 1 : -1;
    }

    if (price) {
        sortOrder.price = price === 'asc' ? 1 : -1;
    }

    if (Object.keys(sortOrder).length === 0) {
        sortOrder.price = -1;
    }

    options.sort = sortOrder;
    console.log('flag 2', options)

    let isActiveFilter = {isDeleted: false};
    if (!isAdmin) {
        isActiveFilter = {
            ...isActiveFilter,
            isActive: true
        }
    }

    const products = await ProductModel.paginate({...filter, ...isActiveFilter}, options);

    // Fetch cart items
    let cartItems = [];
    if (userId) {
        cartItems = await CartModel.aggregate([
            {$match: {userId:new mongoose.Types.ObjectId(userId)}},
            {
                $lookup: {
                    from: 'products',
                    localField: 'productIds.productId',
                    foreignField: '_id',
                    as: 'product',
                },
            },
            {$unwind: '$product'},
            {
                $addFields: {
                    quantityInCart: {
                        $arrayElemAt: [
                            {
                                $filter: {
                                    input: '$productIds',
                                    as: 'pi',
                                    cond: {$eq: ['$$pi.productId', '$product._id']},
                                },
                            },
                            0,
                        ],
                    },
                },
            },
            {
                $project: {
                    product: '$product', // Include all fields from the product object
                    quantityInCart: '$quantityInCart.quantity', // Include the quantityInCart field
                },
            },
        ]);
    }
    const mergedProducts = products.docs.map((product) => {
        const cartProduct = cartItems.find((cp) => String(cp.product._id) === String(product._id));
        // const discountPercentage = product.isDiscount ? parseInt(product.discountValue) || 0 : 0;
        const discountValue = parseFloat(product.discountValue) || 0;
        const finalPrice = calculateFinalPriceAfterDiscount(product.price, discountValue);
        return {
            ...product,
            quantityInCart: cartProduct?.quantityInCart || 0,
            finalPriceAfterDiscount: finalPrice.toFixed(2), // Round the final price to 2 decimal places
        };
    });
    return {
        ...products,
        docs: mergedProducts,
    };
};

const updateImageUrlFromZip = async (id, imageUrl) => {
    return await ProductModel.findByIdAndUpdate(
        id,
        {$push: {imageUrls: imageUrl}},
        {new: true} // to return the updated document
    );
    // to return the updated document)
}

const updateAllToIsDeletedFalse = async () => {
    return await ProductModel.updateMany({}, {isDeleted: false});
}

const updateAllNullDiscountValuesToEmpty = async () => {
    return await ProductModel.updateMany({discountValue: "null"}, {discountValue: ''});
}

module.exports = {
    updateAllToIsDeletedFalse,
    updateImageUrlFromZip,
    createProduct,
    updateProductById,
    updateBulkPrice,
    getProductByName,
    getProducts,
    deleteProductById,
    getAllProducts,
    getProductByIdInfo,
    updateImageUrl,
    removeNutrition,
    deleteTag,
    getProductByNameAr,
    getProductById,
    getProductBySKU,
    getAllProductsBySearch,
    getProductByNameIgnoreCases,
    updateAllNullDiscountValuesToEmpty,
    forceDeleteProductById
};
