const SubCategoryModel = require("../../models/subCategory/SubCategoryModel");
const Tags = require("../../models/tags/TagsModel")
const CategoryModel = require("../../models/category/CategoryModel")
const ImageUrls = require("../../models/images/ImageModel");
const CountryTag = require("../../models/country/CountryModel");
const PriceModel = require("../../models/priceModel/PriceModel");
const BrandModel = require("../../models/brand/BrandModel")
/**
 *
 * @returns {saved subCategory}
 * @param body
 */
const createSubCategory = async (body) => {
    return await SubCategoryModel(body).save();
};

/**
 *
 * @param {id to find the subCategory by} id
 * @returns {potential subCategory if found}
 */
const getSubCategoryById = async (id) => {
    const subCategory = await SubCategoryModel.findOne({
        _id: id,
    })
        .populate({
            path: "productIds",
            model: "Products",
            populate: [
                {
                    path: "imageUrls",
                    model: ImageUrls,
                },
                {
                    path: "country",
                    model: CountryTag,
                },
                {
                    path: "priceModel",
                    model: PriceModel,
                },
                {
                    path: "brandId",
                    model: BrandModel,
                },
                {
                    path: "tags",
                    model: Tags,
                },
            ],
        });

    if (!subCategory) {
        return null; // Subcategory not found
    }

    // Find the parent category in CategoryModel based on the subCategory ID
    subCategory.category = await CategoryModel.findOne({
        subCategories: id,
    });
    return subCategory;
};


const getSubCategoryByIdInfo = async (id) => {
    return SubCategoryModel.findOne({
        _id: id,
    });
}
/**
 *
 * @returns { updates subCategory }
 * @param id
 * @param subCategory
 */
const updateSubCategoryById = async (id, subCategory) => {
    const query = {};
    if (subCategory?.name) {
        query["name"] = subCategory.name;
    }
    if (subCategory?.nameAr) {
        query["nameAr"] = subCategory.nameAr;
    } if (subCategory?.color) {
        query["color"] = subCategory.color;
    }
    if (subCategory?.imageUrl) {
        query["imageUrl"] = subCategory.imageUrl;
    }
    if (subCategory?.productIds?.length > 0) {
        query["$set"] = { "productIds": subCategory.productIds };
    }
    if (subCategory?.isActive === "false") {
        query["isActive"] = subCategory.isActive;
    }
    if (subCategory?.isActive === "true") {
        query["isActive"] = subCategory.isActive;
    }

    if (typeof subCategory === "object") {
        return await SubCategoryModel.findByIdAndUpdate({ _id: id }, { ...query }, { new: true });
    } else return null;
};

const updateImageUrl = async (id, imageUrl) => {
    return await SubCategoryModel.findByIdAndUpdate({ _id: id }, {
        imageUrl: imageUrl
    }, { new: true })
}

/**
 *
 * @returns { updates subCategory }
 * @param name
 */
const getSubCategoryByName = async (name) => {
    return SubCategoryModel.findOne({ name: name });
};

const getSubCategoryName = async (name) => {
    return SubCategoryModel.findOne({ name: { $regex: new RegExp(name, 'i') } });
}

const getSubCategoryByNameAr = async (nameAr) => {
    return SubCategoryModel.findOne({ nameAr: nameAr });
};


const removeProductFromAllSubCategories = async (productId) => {
    return SubCategoryModel.updateMany(
        { productIds: productId },
        { $pull: { productIds: productId } }
    )
}
/**
 *
 * @returns { list of subCategorys }
 */
const getSubCategories = async () => {
    return await SubCategoryModel.find();
};

const addProduct = async (id, product) => {
    return await SubCategoryModel.findByIdAndUpdate({
        _id: id
    }, {
        $push: {
            productIds: product
        }
    })
}
const removeProduct = async (id, product) => {
    return await SubCategoryModel.findByIdAndUpdate({
        _id: id
    }, {
        $pull: {
            productIds: product
        }
    })
}

/**
 * Deletes SubCategory by id
 */
const deleteSubCategoryById = async (id) => {
    return SubCategoryModel.deleteOne({
        _id: id,
    });
};

const getAllSubCategoriesPaginated = async (req, isAdmin) => {
    const { page, limit } = req.query;
    const options = {
        page: parseInt(page) || 1,
        limit: parseInt(limit) || 5,
        // populate: [
        //     {
        //         path: "productIds",
        //         model: "Products",
        //         populate: [
        //             {
        //                 path: "imageUrls",
        //                 model: ImageUrls,
        //             },
        //             {
        //                 path: "country",
        //                 model: CountryTag,
        //             },
        //             {
        //                 path: "priceModel",
        //                 model: PriceModel,
        //             },
        //             {
        //                 path: "brandId",
        //                 model: BrandModel,
        //             },
        //             {
        //                 path: "tags",
        //                 model: Tags,
        //             },
        //         ],
        //     },
        // ],
        lean: true,
    };

    let isActiveFilter = {};
    if (!isAdmin) {
        isActiveFilter = {
            isActive: true
        }
    }

    // Fetch subcategories without populating the categoryId
    const subCategoriesPaginated = await SubCategoryModel.paginate({ ...isActiveFilter }, options);

    // Manually populate the parent category for each subcategory
    const subCategoriesWithParentCategory = await Promise.all(
        subCategoriesPaginated.docs.map(async (subCategory) => {
            const populatedSubCategory = { ...subCategory };

            // Find the parent category in CategoryModel based on the subCategory ID
            populatedSubCategory.category = await CategoryModel.findOne({
                subCategories: subCategory._id,
            });

            return populatedSubCategory;
        })
    );

    // Replace the docs with the subcategories including the parent category
    subCategoriesPaginated.docs = subCategoriesWithParentCategory;

    return subCategoriesPaginated;
};

const findProductInSubCategory = async (productId) => {
    return SubCategoryModel.find({ productIds: productId })
}

const getAllSubCategoriesBySearch = async (req, isAdmin) => {
    const { page, limit, searchQuery } = req.query;

    const options = {
        limit: parseInt(limit) || 10,
        page: parseInt(page) || 1,
        sort: { "name": "asc" },
        lean: true,
    };

    const query = {
        // when indexing is applied
        // $text: { $search: searchQuery },
        $or: [
            { name: { $regex: searchQuery, $options: "i" } },
            { nameAr: { $regex: searchQuery, $options: "i" } },
        ]
    };

    let isActiveFilter = {};
    if (!isAdmin) {
        isActiveFilter = {
            isActive: true
        }
    }

    return await SubCategoryModel.paginate({ ...query, ...isActiveFilter }, options);
};

const getSubCategoryByNameIgnoreCases = async (name) => {
    return SubCategoryModel.findOne({ name: new RegExp(name, 'i') });
};

const updateAllToIsActiveTrue = async () => {
    return await SubCategoryModel.updateMany({}, {
        isActive: true
    });
}

module.exports = {
    createSubCategory,
    getSubCategoryById,
    getSubCategoryByNameIgnoreCases,
    updateSubCategoryById,
    getSubCategoryByName,
    getSubCategories,
    deleteSubCategoryById,
    getAllSubCategoriesPaginated,
    getSubCategoryByIdInfo,
    updateImageUrl,
    removeProduct,
    addProduct,
    removeProductFromAllSubCategories,
    getSubCategoryByNameAr,
    findProductInSubCategory,
    getSubCategoryName,
    getAllSubCategoriesBySearch,
    updateAllToIsActiveTrue
};
