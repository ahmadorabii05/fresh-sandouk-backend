const RoleModel = require("../../models/role/RoleModel");

/**
 *
 *@param {body} roleBody
 * @returns {saved role}
 */
const createRole = async (body) => {
    return await RoleModel(body).save();
};

/**
 *
 * @param {id to find the role by} id
 * @returns {potential role if found}
 */
const getRoleById = async (id) => {
    const role = await RoleModel.findOne({
        _id: id,
    }).populate();
    return role;
};

/**
 *
 * @param {id to find the role by} userId
 * @returns { updates role }
 */
const updateRoleById = async (id, role) => {
    const query = {};
    if (role?.name) {
        query["name"] = role.name;
    }
    if (role?.permissions?.permissions?.length > 0) {
        query["permissions.permissions"] = role.permissions.permissions;
    }
    if (typeof role === "object") {
        return await RoleModel.updateOne({ _id: id }, { ...query });
    } else return null;
};

/**
 *
 * @param {id to find the role by} userId
 * @returns { updates role }
 */
const getRoleByName = async (name) => {
    return await RoleModel.findOne({ name: name });
};

/**
 *
 * @returns { list of roles }
 */
const getRoles = async () => {
    return await RoleModel.find();
};

/**
 * Deletes role by id
 */
const deleteRoleById = async (id) => {
    return await RoleModel.deleteOne({
        _id: id,
    });
};

module.exports = {
    createRole,
    getRoleById,
    updateRoleById,
    getRoleByName,
    getRoles,
    deleteRoleById,
};
