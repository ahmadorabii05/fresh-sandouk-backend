const PriceModel = require("../../models/priceModel/PriceModel");

const createPrice = async (body) => {
    return await PriceModel(body).save();
}

const getPriceById = async (id) => {
    return await PriceModel.findById({_id: id})
}

const deletePriceById = async (id) => {
    return await PriceModel.findByIdAndDelete({_id: id})
}

const getPriceByUnit = async (unit) => {
    return await PriceModel.findOne({ "unit" : { "$regex" : unit.toLowerCase(), "$options" : "i"}});
}

const getPriceByUnitAr = async (unitAr) => {
    return await PriceModel.findOne({unitAr: unitAr});
}
const updatePriceById = async (id, Price) => {
    const query = {};
    if (Price?.unit) {
        query["unit"] = Price.unit
    }
    if (Price?.unitAr) {
        query["unitAr"] = Price.unitAr
    }
    if (typeof Price === "object") {
        return PriceModel.findByIdAndUpdate({_id: id}, {
            ...query
        }, {new: true});
    } else return null;

}

const getAllPricePaginated = async (req) => {
    const {limit, page} = req.query;
    const paginationOptions = {
        limit: parseInt(limit) || 10,
        page: parseInt(page) || 1,
        sort: {"unit": "asc"},
        lean: true,
    }

    return await PriceModel.paginate({}, paginationOptions)
}

const getAllPricesBySearch = async (req) => {
    const {page, limit, searchQuery} = req.query;

    const options = {
        limit: parseInt(limit) || 10,
        page: parseInt(page) || 1,
        sort: {"unit": "asc"},
        lean: true,
    };

    const query = {
        // when indexing is applied
        // $text: { $search: searchQuery },
        $or: [
            { unit: { $regex: searchQuery, $options: "i" } },
            { unitAr: { $regex: searchQuery, $options: "i" } },
        ]
    };
    return await PriceModel.paginate(query, options);
};

module.exports = {
    createPrice,
    getPriceById,
    deletePriceById,
    updatePriceById,
    getAllPricePaginated,
    getPriceByUnit,
    getPriceByUnitAr,
    getAllPricesBySearch
}