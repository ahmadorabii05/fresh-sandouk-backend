const applyActiveFilters = (isActive) => {
    let query = {};
    if (isActive != null ) {
        query = {
            active: (isActive === 'true') ? true : false,
        }
    }
    return query;
};


module.exports = {
    applyActiveFilters,
};
