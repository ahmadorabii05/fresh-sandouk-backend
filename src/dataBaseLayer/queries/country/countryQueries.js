const CountryModel = require("../../models/country/CountryModel");


const createCountry = async (body) => {
    return await CountryModel(body).save();
};

const getAllCountries = async ()=>{
    return await CountryModel.find();
}

const getCountryByName = async (name)=>{
    return await CountryModel.findOne({country_en:{ $regex: new RegExp(name, 'i') }})
}

module.exports = {
    createCountry,
    getAllCountries,
    getCountryByName
}