const ImageModel = require("../../models/images/ImageModel");

const createImage = async (body) => {
    return await ImageModel(body).save();
}

const getImageById = async (id) => {
    return await ImageModel.findById({_id: id})
}

const deleteImageById = async (id) => {
    return await ImageModel.findByIdAndDelete({_id: id})
}

const getImageByUnit = async (unit) => {
    return await ImageModel.findOne({unit: unit});
}
// const updateImageById = async (id, body) => {
//     const query = {};
//
//     if(body.imageUr)
//     return ImageModel.findByIdAndUpdate({_id: id}, {
//         $set: {
//             "unit": unit
//         }
//     }, {new: true});
// }

const getAllImagePaginated = async (req) => {
    const {limit, page} = req.query;
    const paginationOptions = {
        limit: parseInt(limit) || 10,
        page: parseInt(page) || 1,
        lean: true,
    }

    return await ImageModel.paginate({}, paginationOptions)
}
module.exports = {
    createImage,
    getImageById,
    deleteImageById,
    // updateImageById,
    getAllImagePaginated,
    getImageByUnit
}