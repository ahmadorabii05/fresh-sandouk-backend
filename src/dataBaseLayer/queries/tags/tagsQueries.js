const TagsModel = require("../../models/tags/TagsModel");

//create tag
const createTag = async (body) => {
    return await TagsModel(body).save();
};

//get tags Info by name
const getTagsInfoByName = async (name) => {
    return await TagsModel.findOne({name: name})
}
const getTagsInfoByNameAr = async (name)=>{
    return await TagsModel.findOne({nameAr:name})
}
//deleteTag
const deleteTagById = async (id)=>{
    return await TagsModel.findByIdAndDelete({_id:id})
}

const getTagsInfoById = async (id) => {
    return await TagsModel.findById({_id: id})
}


const updateImageUrl = async (id, imageUrl) => {
    return await TagsModel.findByIdAndUpdate({_id: id}, {
        imageUrl: imageUrl
    }, {new: true})
}

const updateUserInfoById = async (id, body) => {
    const query = {};
    if (body.name) {
        query["name"] = body.name;
    }
    if (body.nameAr) {
        query["nameAr"] = body.nameAr;
    }
    if (body.imageUrl) {
        query["imageUrl"] = body.imageUrl;
    }
    return TagsModel.findByIdAndUpdate({_id: id}, {...query},{new:true});

}
const getAllTagsPaginated = async (req)=>{
    const {limit, page} = req.query;
    const paginationOptions = {
        limit: parseInt(limit) || 10,
        page: parseInt(page) || 1,
        sort: {"name": "asc"},
        lean: true,
    }
    return await TagsModel.paginate({},paginationOptions)

}

const getAllTagsBySearch = async (req) => {
    const {page, limit, searchQuery} = req.query;

    const options = {
        limit: parseInt(limit) || 10,
        page: parseInt(page) || 1,
        sort: {"name": "asc"},
        lean: true,
    };

    const query = {
        // when indexing is applied
        // $text: { $search: searchQuery },
        $or: [
            { name: { $regex: searchQuery, $options: "i" } },
            { nameAr: { $regex: searchQuery, $options: "i" } },
        ]
    };
    return await TagsModel.paginate(query, options);
};

module.exports = {
    createTag,
    getTagsInfoByName,
    getTagsInfoById,
    updateImageUrl,
    getAllTagsPaginated,
    updateUserInfoById,
    deleteTagById,
    getTagsInfoByNameAr,
    getAllTagsBySearch
}