const CartModel = require("../../models/cart/CartModel");
const mongoose = require("mongoose");
const TagModel = require("../../models/tags/TagsModel")
const PriceModel = require("../../models/priceModel/PriceModel");
const BrandModel = require("../../models/brand/BrandModel");
const CountryModel = require("../../models/country/CountryModel");
const ImageUrls = require("../../models/images/ImageModel")
const findUserCart = async (userId) => {
    const userIdObj = mongoose.Types.ObjectId(userId);
    const result = await CartModel.aggregate([
        {
            $match: { userId: userIdObj }
        },
        {
            $match: { productIds: { $ne: [] } }
        },
        {
            $unwind: "$productIds"
        },
        {
            $lookup: {
                from: "products",
                localField: "productIds.productId",
                foreignField: "_id",
                as: "product"
            }
        },
        {
            $unwind: "$product"
        },
        {
            $lookup: {
                from: "tags",
                localField: "product.tags",
                foreignField: "_id",
                as: "product.tags"
            }
        },
        {
            $group: {
                _id: "$_id",
                userId: { $first: "$userId" },
                productIds: {
                    $push: {
                        _id: "$productIds.productId",
                        quantity: "$productIds.quantity",
                        name: "$product.name",
                        imageUrl: "$product.imageUrl",
                        price: "$product.price",
                        discountValue: "$product.discountValue",
                        isDiscount: "$product.isDiscount",
                        tags: "$product.tags"
                    }
                }
            }
        },
        {
            $project: {
                _id: 0,
                userId: 1,
                productIds: {
                    $map: {
                        input: "$productIds",
                        as: "product",
                        in: {
                            _id: "$$product._id",
                            quantity: "$$product.quantity",
                            name: "$$product.name",
                            imageUrl: "$$product.imageUrl",
                            price: "$$product.price",
                            discountValue: "$$product.discountValue",
                            isDiscount: "$$product.isDiscount",
                            tags: "$$product.tags",
                            finalPriceAfterDiscount: {
                                $cond: {
                                    if: { $and: [{ $ne: ["$$product.discountValue", ""] }, { $ne: ["$$product.discountValue", null] }] }, // Check if discountValue is not empty or null
                                    then: {
                                        $subtract: [
                                            "$$product.price",
                                            {
                                                $cond: {
                                                    if: { $gt: [{ $toDouble: "$$product.discountValue" }, 0] }, // Check if discountValue is a positive number
                                                    then: { $toDouble: "$$product.discountValue" }, // Convert discountValue to double
                                                    else: 0 // Use 0 if discountValue is not a positive number
                                                }
                                            }
                                        ]
                                    },
                                    else: "$$product.price" // If discountValue is empty or null, use original price
                                }
                            }



                        }
                    }
                },
                totalOriginPrice: {
                    $sum: {
                        $map: {
                            input: "$productIds",
                            as: "product",
                            in: {
                                $multiply: [
                                    "$$product.quantity",
                                    "$$product.price"
                                ]
                            }
                        }
                    }
                }
            }
        },
        {
            $unwind: "$productIds"
        },
        {
            $group: {
                _id: null,
                userId: { $first: "$userId" },
                productIds: { $push: "$productIds" },
                totalPrice: {
                    $sum: {
                        $cond: [
                            "$productIds.finalPriceAfterDiscount",
                            {
                                $multiply: [
                                    "$productIds.quantity",
                                    "$productIds.finalPriceAfterDiscount"
                                ]
                            },
                            {
                                $multiply: [
                                    "$productIds.quantity",
                                    "$productIds.price"
                                ]
                            }
                        ]
                    }
                },
                totalOriginPrice: { $first: "$totalOriginPrice" }
            }
        }
    ]);

    return result;
};


const createUserCart = (userId, productIds) => {
    const cartProducts = productIds.map(({productId, quantity}) => ({
        productId: productId,
        quantity: quantity > 100 ? 100 : quantity,
    }));
    return CartModel.create({userId, productIds: cartProducts});
};


const addToCart = (userId, productId, quantity) => {
    const updatedQuantity = quantity > 100 ? 100 : quantity;
    return CartModel.updateOne(
        {userId, 'productIds.productId': productId},
        {$set: {'productIds.$.quantity': updatedQuantity}},
        {new: true}
    );
};


const addProductToCart = (userId, productId, quantity) => {
    return CartModel.updateOne(
        {userId},
        {$push: {productIds: {productId: productId, quantity}}},
        {new: true}
    );
};

const decreaseProductQuantity = (userId, productId) => {
    return CartModel.findOneAndUpdate(
        {userId, 'productIds.productId': productId},
        {$inc: {'productIds.$.quantity': -1}},
        {new: true}
    );
};

const removeProductFromCart =async (userId, productId) => {
    return await CartModel.findOneAndUpdate(
        {userId},
        {$pull: {productIds: {productId: productId}}},
        {new: true}
    );
};

// Find the user's cart and update it using the $pull operator
const removeProductsFromCartArray = async (userId, productIds) => {
    return await CartModel.findOneAndUpdate(
        {userId},
        {$pull: {productIds: {productId: {$in: productIds}}}},
        {new: true}
    );
}

const getCartAggregateByUserId = async (userId) => {
    return await CartModel.aggregate([
        {
            $match: {
                userId: mongoose.Types.ObjectId(userId)
            }
        },
        {
            $unwind: "$productIds"
        },
        {
            $lookup: {
                from: "products",
                localField: "productIds.productId",
                foreignField: "_id",
                as: "product"
            }
        },
        {
            $unwind: "$product"
        },
        {
            $group: {
                _id: "$product._id",
                quantityInCart: {$sum: "$productIds.quantity"}
            }
        }
    ]);
};

const getCartInfoByUserIdForAddToCart = async (userId) => {
    return await CartModel.findOne({userId: userId})
}

const getCartInfo = async (userId) => {
    return await CartModel.findOne({userId: userId})
        .populate({
            path: "productIds.productId",
            populate: [
                {path: "country", model: CountryModel},
                {path: "priceModel", model: PriceModel},
                {path: "brandId", model: BrandModel},
                {path: "imageUrls", model: ImageUrls},
                {
                    path: "tags",
                    model: TagModel
                }
            ]
        })

};

const getUserCartWithoutPopulate = async (userId)=>{
    return CartModel.findOne({
        userId:userId
    })
}

const removeAllProductAfterOrder = async (userId)=>{
    return CartModel.findOneAndUpdate({userId:userId},{
        $set:{"productIds":[]}
    })
}

module.exports = {
    getCartInfoByUserIdForAddToCart,
    getCartInfo,
    removeProductsFromCartArray,
    getCartAggregateByUserId,
    findUserCart,
    createUserCart,
    addToCart,
    addProductToCart,
    decreaseProductQuantity,
    removeProductFromCart,
    getUserCartWithoutPopulate,
    removeAllProductAfterOrder
};
