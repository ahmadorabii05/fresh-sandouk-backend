const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
const aggregatePaginate = require("mongoose-aggregate-paginate-v2");

const WalletModel = new mongoose.Schema(
    {
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref:"Users"
        },
        orderIds:{
            orders:[{
                ref:"Orders",
                type:mongoose.Schema.Types.ObjectId,
            }]
        }
    },
    {
        timestamps: true,
    }
);

WalletModel.plugin(mongoosePaginate);
WalletModel.plugin(aggregatePaginate);
module.exports = mongoose.model("Wallet", WalletModel);
