const mongoose = require("mongoose");

const GeneralModel = new mongoose.Schema(
    {
        name: {
            type: String,
            trim: true,
        },
        firstName: {
            type: String,
            trim: true,
        },
        lastName: {
            type: String,
            trim: true,
        },
        imageUrl: {
            type: String,
            trim: true,
        },
        email: {
            type: String,
            trim: true,
        },
        stripe_id:{
            type:String,
            default:""
        },
        alternativeEmail: {
            type: String,
            trim: true,
        },
        password: {
            type: String,
            trim: true,
            required: true,
        },
        phoneNumber: {
            type: String,
            default: "",
            required: false,
        }, alternativePhoneNumber: {
            type: String,
            default: "",
            required: false,
        },
        accountVerification: {
            type: Boolean,
            default: false,
        },
        role: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Roles",
            required: false,
        }, dateOfBirth: {
            type: Date,
        },
        nationality: {
            type: String,
        },
        resetToken: String,
        verificationToken: String,
        expireResetToken: Date,
        expireVerificationToken: Date,
        verifyCode: Number,
        walletBalance:{
            type:Number,
            default:0
        }
    },
    {
        timestamps: true,
    }
);

module.exports = GeneralModel;
