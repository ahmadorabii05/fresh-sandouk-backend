const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
const aggregatePaginate = require("mongoose-aggregate-paginate-v2");

const PriceModel = new mongoose.Schema(
    {
        unit: {
            type: String
        },
        unitAr:{
            type:String
        }
    },
    {
        timestamps: true,
    }
);

PriceModel.plugin(mongoosePaginate);
PriceModel.plugin(aggregatePaginate);
module.exports = mongoose.model("Prices", PriceModel);
