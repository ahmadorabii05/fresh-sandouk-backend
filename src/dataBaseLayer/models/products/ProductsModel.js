const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
const aggregatePaginate = require("mongoose-aggregate-paginate-v2");
const NutritionSchema = require('../nutrition/NutritionModel');

const ProductsModel = new mongoose.Schema(
    {
        name: {
            type: String
        },
        nameAr: {
            type: String,
            default: ""
        },
        imageUrls: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: "Images"
        }],
        country: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Countries",
        },
        price: {
            type: Number
        },
        priceModel: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Prices",
        },
        brandId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Brands",
        },
        description: {
            type: String
        },
        descriptionAr: {
            type: String,
            default: ""
        },
        discountValue: {
            type: Number,
        },
        sku: {
            type: String,
            default: "",
        },
        sort_order: {
            type: Number,
            default: 1,
        },
        barcode: {
            type: String,
            default: "",
        },
        tags: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: "Tag",
            default: []
        }],
        nutrition: {
            type: [NutritionSchema],
            required: false,
            default: []
        },
        generalDescription: {
            type: String,
            default: ""
        },
        ingredient: {
            type: String,
            default: ""
        },
        storage: {
            type: String,
            default: ""
        },
        generalDescriptionAr: {
            type: String,
            default: ""
        },
        ingredientAr: {
            type: String,
            default: ""
        },
        storageAr: {
            type: String,
            default: ""
        },
        isDeleted: {
            type: Boolean,
            default: false
        },
        isFeatured: {
            type: Boolean,
            default: false
        },
        isActive: {
            type: Boolean,
            default: true
        }

    },
    {
        timestamps: true,
        strict: false // <-- add this option to allow unknown fields
    }
);

ProductsModel.plugin(mongoosePaginate);
ProductsModel.plugin(aggregatePaginate);
module.exports = mongoose.model("Products", ProductsModel);
