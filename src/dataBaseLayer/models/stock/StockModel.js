const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
const aggregatePaginate = require("mongoose-aggregate-paginate-v2");

const StockModel = new mongoose.Schema(
    {
        productId: {
            type: mongoose.Schema.Types.ObjectId,
            ref:"Products"
        },
        quantity:{
            type:Number,
            default:0
        },
        quantitySold:{
            type:Number,
            default:0
        }
    },
    {
        timestamps: true,
    }
);

StockModel.plugin(mongoosePaginate);
StockModel.plugin(aggregatePaginate);
module.exports = mongoose.model("Stock", StockModel);
