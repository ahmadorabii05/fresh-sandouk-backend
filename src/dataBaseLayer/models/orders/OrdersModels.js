const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
const aggregatePaginate = require("mongoose-aggregate-paginate-v2");

const OrdersModel = new mongoose.Schema(
    {
        keyOrder: {
            type: String,
            unique: true,
            required: true,
        },
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Users"
        },
        products: [{
            productId: {
                ref: "Products",
                type: mongoose.Schema.Types.ObjectId
            },
            quantity: {
                type: Number,
                default: 0
            }
        }],
        addressId: {
            ref: "Addresses",
            type: mongoose.Schema.Types.ObjectId
        },
        scheduledDate: {
            type: String
        },
        scheduledTime: {
            type: String
        },
        deliveryInstruction: {
            type: String,
        },
        packingInstruction: {
            type: String,
        },
        createIdFromStripe: {
            type: String
        },
        refund: {
            type: Boolean,
            default: false
        },
        status: {
            type: String,
            enum: ['New', 'Confirmed', 'Processing', 'Out for Delivery', 'Delivered', 'Closed' , 'Cancel'],
            default: 'New'
        },
    },
    {
        timestamps: true,
    }
);

OrdersModel.plugin(mongoosePaginate);
OrdersModel.plugin(aggregatePaginate);
module.exports = mongoose.model("Orders", OrdersModel);
