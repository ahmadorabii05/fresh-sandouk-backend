const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
const aggregatePaginate = require("mongoose-aggregate-paginate-v2");

const BannerModel = new mongoose.Schema(
    {
        actionUrl: {
            type: String
        },
        location: {
            type: String,
            enum: ["home_page_section_1", "home_page_section_2", "home_page_section_3", "product_details", "filters_page", "checkout"],
            default: "home_page_section_1",
            required: false,
        },
        priority: {
            type: Number
        },
        imageUrl: {
            type: String
        }
    },
    {
        timestamps: true,
    }
);

BannerModel.plugin(mongoosePaginate);
BannerModel.plugin(aggregatePaginate);
module.exports = mongoose.model("Banners", BannerModel);
