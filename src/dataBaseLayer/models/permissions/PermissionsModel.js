const mongoose = require("mongoose");

const PermissionsModel = new mongoose.Schema({
    permissions: [
        {
            type: String,
            enum: ["create", "read", "update", "delete"],
            default: [],
        },
    ],
});

module.exports = PermissionsModel;
