const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
const aggregatePaginate = require("mongoose-aggregate-paginate-v2");

const SubCategoryModel = new mongoose.Schema(
    {
        name: {
            type: String
        },
        nameAr: {
            type: String,
            default: ""
        },
        imageUrl: {
            type: String,
        },
        productIds: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: "Products",
            default: []
        }],
        isActive: {
            type: Boolean,
            default: true
        }

    },
    {
        timestamps: true,
    }
);

SubCategoryModel.plugin(mongoosePaginate);
SubCategoryModel.plugin(aggregatePaginate);
module.exports = mongoose.model("SubCategory", SubCategoryModel);
