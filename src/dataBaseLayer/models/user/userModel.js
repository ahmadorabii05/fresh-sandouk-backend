const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
const aggregatePaginate = require("mongoose-aggregate-paginate-v2");
const InfoModel = require("../info/infoModel");

const UserModel = new mongoose.Schema(
    {
        info: InfoModel,
        active: {
            type: Boolean,
            default: true
        }
    },
    {
        timestamps: true,
    }
);

UserModel.plugin(mongoosePaginate);
UserModel.plugin(aggregatePaginate);
module.exports = mongoose.model("Users", UserModel);
