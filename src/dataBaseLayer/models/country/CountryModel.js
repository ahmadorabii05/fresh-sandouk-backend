const mongoose = require("mongoose");

const CountryModel = new mongoose.Schema(
    {
        code: {
            type: String,
        },
        country_en: {
            type: String
        },
        country_ar: {
            type: String
        }
    },
    {
        timestamps: true,
    }
);


module.exports = mongoose.model("Countries", CountryModel);
