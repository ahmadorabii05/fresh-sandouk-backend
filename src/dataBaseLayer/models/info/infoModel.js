const mongoose = require("mongoose");
const GeneralModel = require("../general/GeneralModel");
// const NotificationSettingsModel = require("../notificationSettings/NotificationSettingsModel");
// const SocialSettingModel = require("../socialSetting/SocialSettingModel")


const InfoModel = new mongoose.Schema(
    {
        general: GeneralModel,
        productSettings: [{
            ref: "Products",
            type: mongoose.Schema.Types.ObjectId,
            default:[]
        }],

        // notificationSettings: NotificationSettingsModel,
        // socialSetting: SocialSettingModel,
    },
    {
        timestamps: true,
    }
);

module.exports = InfoModel;
