const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
const aggregatePaginate = require("mongoose-aggregate-paginate-v2");

const AddressModel = new mongoose.Schema(
    {
        addressTitle: {
            type: String
        },
        address:{
            type:String,
        },
        street:{
            type:String
        },
        nearestLand:{
            type:String
        },
        contactNumber:{
            type:String
        },
        userId:{
            ref:"Users",
            type:mongoose.Schema.Types.ObjectId
        },
        isDefaultAddress:{
            type:Boolean,
            default : false
        }, longtitude : {
            type:Number,
            default: 0
        },
        latitude :{
            type:Number,
            default: 0
        },
    },
    {
        timestamps: true,
    }
);

AddressModel.plugin(mongoosePaginate);
AddressModel.plugin(aggregatePaginate);
module.exports = mongoose.model("Addresses", AddressModel);
