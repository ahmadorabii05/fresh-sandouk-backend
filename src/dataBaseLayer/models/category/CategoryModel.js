const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
const aggregatePaginate = require("mongoose-aggregate-paginate-v2");

const CategoryBanners = new mongoose.Schema(
    {
        actionUrl: {
            type: String,
            trim: true,
        },
        imageUrl: {
            type: String,
            trim: true,
        },
    },
    {
        timestamps: true,
    }
);

const CategoryModel = new mongoose.Schema(
    {
        name: {
            type: String,
        },
        nameAr: {
            type: String,
            default: ""
        },
        imageUrl: {
            type: String
        },
        banners: [{
            type: CategoryBanners
        }],
        color: {
            type: String,
            default: "#FFFFFF"
        },
        subCategories: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: "SubCategory",
            default: []
        }],
        priority: {
            type: Number
        },
        isActive: {
            type: Boolean,
            default: true
        }
    },
    {
        timestamps: true,
    }
);


CategoryModel.plugin(mongoosePaginate);
CategoryModel.plugin(aggregatePaginate);
module.exports = mongoose.model("Category", CategoryModel);
