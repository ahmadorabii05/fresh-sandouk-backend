const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
const aggregatePaginate = require("mongoose-aggregate-paginate-v2");

const ListModel = new mongoose.Schema(
    {
        name: {
            type:String,
        },
        userId:{
            type: mongoose.Schema.Types.ObjectId,
            ref:"Users",
        },
        productIds: [{
            type: mongoose.Schema.Types.ObjectId,
            ref:"Products",
            default:[]
        }]
    },
    {
        timestamps: true,
    }
);


ListModel.plugin(mongoosePaginate);
ListModel.plugin(aggregatePaginate);
module.exports = mongoose.model("Lists", ListModel);
