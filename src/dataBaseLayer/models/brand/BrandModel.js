const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
const aggregatePaginate = require("mongoose-aggregate-paginate-v2");

const BrandModel = new mongoose.Schema(
    {
        source: {
            type: String
        },
        sourceAr:{
            type:String,
            default:""
        },
        imageUrl:{
            type:String
        }
    },
    {
        timestamps: true,
    }
);

BrandModel.plugin(mongoosePaginate);
BrandModel.plugin(aggregatePaginate);
module.exports = mongoose.model("Brands", BrandModel);
