const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
const aggregatePaginate = require("mongoose-aggregate-paginate-v2");

const CartModel = new mongoose.Schema(
    {
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Users",
        },
        productIds: [
            {
                productId: {
                    ref: "Products",
                    type: mongoose.Schema.Types.ObjectId,
                },
                quantity: {
                    type: Number,
                    default: 0,
                },
            },
        ],
    },
    {
        timestamps: true,
    }
);

CartModel.plugin(mongoosePaginate);
CartModel.plugin(aggregatePaginate);
module.exports = mongoose.model("Cart", CartModel);
