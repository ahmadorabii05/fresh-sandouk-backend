const mongoose = require("mongoose");


const ImageModel = new mongoose.Schema(
    {
        productId: {
            type: mongoose.Schema.Types.ObjectId,
            ref:"Products"
        },
        imageUrl:{
            type:String
        }
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model("Images", ImageModel);
