const mongoose = require('mongoose');

const NutritionSchema = new mongoose.Schema({
    description: {type: String, default: ""},
    per_100g_ml: {
        type: Number, require: false,
        default: ""
    },
    per_serving: {
        type: Number, require: false,
        default: ""
    },
    measure: {
        type: Number, require: false,
        default: ""
    },
    nrv: {type: Number, require: false},
    descriptionAr: {type: String, default: ""},
    per_100g_mlAr: {
        type: Number, require: false,
        default: ""
    },
    per_servingAr: {
        type: Number, require: false,
        default: ""
    },
    measureAr: {
        type: Number, require: false,
        default: ""
    },
    nrvAr: {
        type: Number, require: false,
        default: ""
    }
});

module.exports = NutritionSchema;
