const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
const aggregatePaginate = require("mongoose-aggregate-paginate-v2");

const TagsModel = new mongoose.Schema(
    {
        name: {
            type: String
        },
        nameAr:{
            type:String
        },
        imageUrl: {
            type: String,
        }
    },
    {
        timestamps: true,
    }
);

TagsModel.plugin(mongoosePaginate);
TagsModel.plugin(aggregatePaginate);
module.exports = mongoose.model("Tags", TagsModel);
