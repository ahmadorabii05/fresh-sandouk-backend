const express = require("express");
const path = require("path");
const fs = require("fs");
const https = require("https");
const helmet = require("helmet");
const morgan = require("morgan");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const _ = require("lodash");
const routes = require("../routes/index");
const { prepareDbConnection } = require("../dataBaseLayer/common/connections");
const bodyParser = require("body-parser");

// const { startNewsUpdaterCron } = require("../helperKit/cronJobKit");
// const { testingStuff } = require("../helperKit/htmlParserKit");

require("dotenv").config();

const app = express();
const port = process.env.PORT || 8080;
// const options = {
//     key: fs.readFileSync(`${__dirname}/privkey.pem`, { encoding: 'utf8' }),
//     cert: fs.readFileSync(`${__dirname}/fullchain.pem`, { encoding: 'utf8' })
// };
const prepareAppStartUp = () => {
    //.env file for DB_URL and port

    prepareDbConnection();
    // startNewsUpdaterCron();
    // testingStuff();

    //Using middlewares
    app.use(helmet());
    app.use(morgan("dev"));
    app.use(cors({ origin: "*", methods: "GET, POST, PATCH, DELETE,PUT" }));
    app.use(express.json({ limit: "20GB" }));
    app.use(cookieParser());
    //Public folder will be used for public uploads
    app.use(express.static(path.join(__dirname, "/public")));
    app.use('/images', express.static('images'));
    // app.use(bodyParser.raw({ type: 'application/json' }));
    app.use(bodyParser.json());



    /**
     * const path = require("path");
     *
     * const storage = multer.diskStorage({
     *     destination: path.join(__dirname, "./public/uploads"),
     *     filename: function (req, file, cb) {
     *         cb(null, new Date().toISOString() + file.originalname);
     *     },
     * });
     */


    routes(app);

    app.get("/", (req, res) => {
        res.send("Test Backend Fresh Sandouk");
    });

    const server = app.listen(port, () => {
        console.log(`Server is running on port ${port}`);
    });

    // const server = https.createServer(options, app);

    // server.listen(port, () => {
    //     console.log("Server starting on port: " + port)
    // });

    server.timeout = 36000000;
};


module.exports = {
    prepareAppStartUp,
};