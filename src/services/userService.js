const UserQueries = require("../dataBaseLayer/queries/user/userQueries")
const {isEmpty} = require("lodash");
const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("../dataHandlerLayer/common/LanguageHeaderHandler");

const isAdminRole = (user) => {
    return user?.info?.general?.role?.name == "admin";
}
const isAdmin = (req,res,next) => {
     if(req.userFromToken?.info?.general?.role?.name == "admin"){
         next();
     }else{
         return res
             .status(ErrorStatus.YOUR_ARE_NOT_AN_ADMIN.code)
             .json(
                 prepareErrorResponse(
                     ErrorStatus.YOUR_ARE_NOT_AN_ADMIN,
                     null, extractLanguageFromRequest(req)
                 )
             );
     }
}

const isVerified = async (req, res, next) => {
    const email = req.body.email;
    const userIsVerified1 = await UserQueries.getUserInfoByEmail(email);
    if (!isEmpty(userIsVerified1)) {
        if (userIsVerified1.info.general.accountVerification) {
            next();
        } else {
            return res
                .status(ErrorStatus.USER_NOT_VERIFIED.code)
                .json(prepareErrorResponse(ErrorStatus.USER_NOT_VERIFIED, null, extractLanguageFromRequest(req)));
        }
    }
}

module.exports = {
    isAdminRole,
    isVerified,
    isAdmin
}