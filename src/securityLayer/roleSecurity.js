const jwt = require("jsonwebtoken");
const { ErrorStatus } = require("../constants/error/errorStatus");
const { prepareErrorResponse } = require("../errorHandlerLayer/errorHandler");
const { prepareErrorLog } = require("../helperKit/loggingKit");
const { isAdminRole } = require("../services/userService");
const {extractLanguageFromRequest} = require("../dataHandlerLayer/common/LanguageHeaderHandler");

/**
 * Validates that the user's role is authorized for this endpoint
 */
const roleSecurity = async (req, res, next) => {
    try {
        const user = req.user;
        if (!user) {
            return res
                .status(ErrorStatus.NOT_AUTHORIZED.code)
                .json(prepareErrorResponse(ErrorStatus.NOT_AUTHORIZED,null, extractLanguageFromRequest(req)));
        } else {
            if (isAdminRole(user)) {
                req.userFromToken = user;
                next();
            } else {
                return res
                    .status(ErrorStatus.NOT_AUTHORIZED.code)
                    .json(prepareErrorResponse(ErrorStatus.NOT_AUTHORIZED, null, extractLanguageFromRequest(req)));
            }
        }
    } catch (error) {
        prepareErrorLog(error, tokenSecurity.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN,null, extractLanguageFromRequest(req)));
    }
};

module.exports = {
    roleSecurity,
};
