const jwt = require("jsonwebtoken");
const { ErrorStatus } = require("../constants/error/errorStatus");
const { prepareErrorResponse } = require("../errorHandlerLayer/errorHandler");
const UserQueries = require("../dataBaseLayer/queries/user/userQueries");
const { prepareErrorLog } = require("../helperKit/loggingKit");
const {extractLanguageFromRequest} = require("../dataHandlerLayer/common/LanguageHeaderHandler");

/**
 * Validates that the token received in the authorization header belongs to a user or not
 */
const tokenSecurity = async (req, res, next) => {
    try {
        const token = req.header("Authorization");
        if (!token) {
            return res
                .status(ErrorStatus.NOT_AUTHORIZED.code)
                .json(prepareErrorResponse(ErrorStatus.NOT_AUTHORIZED,null, extractLanguageFromRequest(req)));
        }
        const data = jwt.decode(
            token.replace("Bearer", "").trim(),
            process.env.JWT_SECRET
        );
        if (data) {
            const user = await UserQueries.getUserInfoById(
                data.user.id ?? data.user._id
            );
            if (!user)
                return res
                    .status(ErrorStatus.NOT_AUTHORIZED.code)
                    .json(prepareErrorResponse(ErrorStatus.NOT_AUTHORIZED,null,extractLanguageFromRequest(req)));
            req.userFromToken = user;
            next();
        } else {
            return res
                .status(ErrorStatus.NOT_AUTHORIZED.code)
                .json(prepareErrorResponse(ErrorStatus.NOT_AUTHORIZED,null,extractLanguageFromRequest(req)));
        }
    } catch (error) {
        prepareErrorLog(error, tokenSecurity.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN,null,extractLanguageFromRequest(req)));
    }
};

const tokenSecurityMightExist = async (req, res, next) => {
    try {
        const token = req.header("Authorization");
        if (!token) {
           next()
        }else{
            const data = jwt.decode(
                token.replace("Bearer", "").trim(),
                process.env.JWT_SECRET
            );
            if (data) {
                const user = await UserQueries.getUserInfoById(
                    data.user.id ?? data.user._id
                );
                if (!user)
                    return res
                        .status(ErrorStatus.NOT_AUTHORIZED.code)
                        .json(prepareErrorResponse(ErrorStatus.NOT_AUTHORIZED,null,extractLanguageFromRequest(req)));
                req.userFromToken = user;
                next();
            } else {
                return res
                    .status(ErrorStatus.NOT_AUTHORIZED.code)
                    .json(prepareErrorResponse(ErrorStatus.NOT_AUTHORIZED,null,extractLanguageFromRequest(req)));
            }
        }

    } catch (error) {
        prepareErrorLog(error, tokenSecurityMightExist.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN,null,extractLanguageFromRequest(req)));
    }
};

module.exports = {
    tokenSecurity,
    tokenSecurityMightExist
};
