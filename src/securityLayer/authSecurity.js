const { ErrorStatus } = require("../constants/error/errorStatus");
const { prepareErrorResponse } = require("../errorHandlerLayer/errorHandler");
const { generateJWT } = require("../helperKit/jwtKit");
const { prepareErrorLog } = require("../helperKit/loggingKit");
const { isPasswordEncryptionValid } = require("./common/encryption");
const {extractLanguageFromRequest} = require("../dataHandlerLayer/common/LanguageHeaderHandler");

const loginSecurity = async (req, res, next) => {
    try {
        const user = req.user;
        const requestModel = req.requestModel;
        const isValidPassword = await isPasswordEncryptionValid(
            requestModel.password,
            user?.info?.general?.password || ""
        );
        if (!isValidPassword) {
            return res
                .status(ErrorStatus.WRONG_PASSWORD.code)
                .json(prepareErrorResponse(ErrorStatus.WRONG_PASSWORD,null, extractLanguageFromRequest(req)));
        } else {
            const registeredToken = await generateJWT(user);

            res.cookie("token", registeredToken);
            req.responseModel = { token: registeredToken, user };
            next();
        }
    } catch (error) {
        prepareErrorLog(error, loginSecurity.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN,null, extractLanguageFromRequest(req)));
    }
};

const signUpSecurity = async (req, res, next) => {
    try {
        const user = req.user;
        const registeredToken = await generateJWT(user);
        res.cookie("token", registeredToken);
        req.responseModel = { token: registeredToken, user };
        next();
    } catch (error) {
        prepareErrorLog(error, signUpSecurity.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN,null, extractLanguageFromRequest(req)));
    }
};

const socialLoginSecurity = async (req, res, next) => {
    try {
        const user = req.user;
        const registeredToken = await generateJWT(user);
        res.cookie("token", registeredToken);
        req.responseModel = { token: registeredToken, user };
        next();
    } catch (error) {
        prepareErrorLog(error, socialLoginSecurity.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN,null, extractLanguageFromRequest(req)));
    }
};

const logoutSecurity = async (req, res, next) => {
    try {
        await res.clearCookie("token");
        next();
    } catch (error) {
        prepareErrorLog(error, loginSecurity.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN,null, extractLanguageFromRequest(req)));
    }
};

module.exports = {
    loginSecurity,
    signUpSecurity,
    socialLoginSecurity,
    logoutSecurity,
};
