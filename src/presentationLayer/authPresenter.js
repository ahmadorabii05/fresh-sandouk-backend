const { SuccessStatus } = require("../constants/success/successStatus");
const {
    LoginResponseModel,
} = require("../presentationLayer/models/responseModel/auth/LoginResponseModel");
const { prepareSuccessResponse } = require("./common/presenter");
const {PhoneNumberResponseModel} = require("./models/responseModel/phoneNumber/PhoneNumberResponseModel");

/**
 * Responsible for preparing the response to be sent to the frontend
 * @param {request will include a property called responseModel prepared from the dataHandler} req
 * @param {response will be used to return an immediate response if needed} res
 * @param {goes to the next step of the process} next
 */

/**
 * LoginPresenter prepares the response for the client side
 */
const loginPresenter = async (req, res, next) => {
    const responseModel = new LoginResponseModel(req.responseModel);
    req.statusCode = SuccessStatus.LOGIN_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.LOGIN_SUCCESSFUL,
        null,
        responseModel
    );
    next();
};

/**
 * SignUpPresenter prepares the response for the client side
 */
const signUpPresenter = async (req, res, next) => {
    const responseModel = new LoginResponseModel(req.responseModel);
    req.statusCode = SuccessStatus.SIGNUP_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.SIGNUP_SUCCESSFUL,
        null,
        responseModel
    );
    next();
};

/**
 * SocialLoginPresenter prepares the response for the client side
 */
const socialLoginPresenter = async (req, res, next) => {
    const responseModel = new LoginResponseModel(req.responseModel);
    req.statusCode = SuccessStatus.LOGIN_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.LOGIN_SUCCESSFUL,
        null,
        responseModel
    );
    next();
};

/**
 * LogoutPresenter prepares the response for the client side
 */
const logoutPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.LOGOUT_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.LOGOUT_SUCCESSFUL,
        null,
        null
    );
    next();
};

/**
 * ForgotPasswordPresenter prepares the response for the client side
 */
const forgotPasswordPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.FORGOT_RESET_PASSWORD_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.FORGOT_RESET_PASSWORD_SUCCESSFUL,
        null,
        null
    );
    next();
};

/**
 * AccountVerificationPresenter prepares the response for the client side
 */
const accountVerificationPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.ACCOUNT_VERIFIED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.ACCOUNT_VERIFIED_SUCCESSFULLY,
        null,
        null
    );
    next();
};

/**
 * ChangePasswordPresenter prepares the response for the client side
 */
const changePasswordPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.PASSWORD_SUCCESSFULLY_CHANGED.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.PASSWORD_SUCCESSFULLY_CHANGED,
        null,
        null
    );
    next();
};

/**
 * VerifyCodePresenter prepares the response for the client side
 */
const verifyCodePresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.VERIFY_CODE_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.VERIFY_CODE_SUCCESSFULLY,
        null,
        null
    );
    next();
};

const verifySMSCodeNumberPresenter = async (req,res,next)=>{
    const data = new PhoneNumberResponseModel(req.phoneNumber);
    req.statusCode = SuccessStatus.PHONE_NUMBER_MESSAGE_SEND_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.PHONE_NUMBER_MESSAGE_SEND_SUCCESSFULLY,
        "verification code sent Successfully to " + `${req.phoneNumber.phoneNumber}`,
        data
    );
    next();
}

module.exports = {
    verifySMSCodeNumberPresenter,
    loginPresenter,
    signUpPresenter,
    socialLoginPresenter,
    logoutPresenter,
    forgotPasswordPresenter,
    accountVerificationPresenter,
    changePasswordPresenter,
    verifyCodePresenter
};
