const {SuccessStatus} = require("../constants/success/successStatus");
const {prepareSuccessResponse} = require("./common/presenter");
const {CreateListResponseModel} = require("./models/responseModel/list/CreateListResponseModel");
const {EditListNameResponseModel} = require("./models/responseModel/list/EditListNameResponseModel");
const {PaginationResponseModel} = require("./models/responseModel/pagination/PaginationResponseModel");
const {GetListsResponseModel} = require("./models/responseModel/list/GetListsResponseModel");


const createListPresenter = async (req, res, next) => {
    const response = new CreateListResponseModel(req.responseModel,req);
    req.statusCode = SuccessStatus.LIST_CREATED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.LIST_CREATED_SUCCESSFULLY,
        null,
        response
    );
    next();
};


const editListNamePresenter = async (req,res,next)=>{
    const response = new EditListNameResponseModel(req.responseModel,req);
    req.statusCode = SuccessStatus.LIST_UPDATED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.LIST_UPDATED_SUCCESSFULLY,
        null,
        response
    );
    next();
}


const getListPresenter = async (req,res,next)=>{

    let lists = new PaginationResponseModel(req.responseModel);
    lists.docs = lists.docs.map((list) => new GetListsResponseModel(list,req));
    const responseModel = lists;
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}

const getListIdPresenter=async (req,res,next)=>{
    const responseModel = new GetListsResponseModel(req.responseModel,req);
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}


const addProductToListPresenter = async (req,res,next)=>{
    const response = new CreateListResponseModel(req.responseModel,req);
    req.statusCode = SuccessStatus.LIST_UPDATED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.LIST_UPDATED_SUCCESSFULLY,
        null,
        response
    );
    next();
}

const deleteListPresenter = async (req,res,next)=>{
    req.statusCode = SuccessStatus.LIST_DELETED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.LIST_DELETED_SUCCESSFULLY,
        null,
        null
    );
    next();
}

module.exports={
    createListPresenter,
    editListNamePresenter,
    getListPresenter,
    addProductToListPresenter,
    deleteListPresenter,
    getListIdPresenter
}