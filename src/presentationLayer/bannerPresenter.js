const { PaginationResponseModel } = require("./models/responseModel/pagination/PaginationResponseModel");
const { SuccessStatus } = require("../constants/success/successStatus");
const { prepareSuccessResponse } = require("./common/presenter");
const { BannerResponseModel } = require("./models/responseModel/banner/BannerResponseModel");


const getAllBannersPresenter = async (req, res, next) => {
    let sources = new PaginationResponseModel(req.responseModel);
    sources.docs = sources?.docs?.map((source) => new BannerResponseModel(source));
    const responseModel = sources;
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}

const createBannerPresenter = async (req, res, next) => {
    const responseModel = new BannerResponseModel(req.responseModel);
    req.statusCode = SuccessStatus.BANNER_ADDED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.BANNER_ADDED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}

const getBannerByIdPresenter = async (req, res, next) => {
    let source = new BannerResponseModel(req.responseModel);
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        source
    );
    next();
}

const updateBannerByIdPresenter = async (req, res, next) => {
    let source = new BannerResponseModel(req.responseModel);
    req.statusCode = SuccessStatus.BANNER_UPDATED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.BANNER_UPDATED_SUCCESSFULLY,
        null,
        source
    );
    next();
}

const deleteBannerPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.BANNER_DELETED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.BANNER_DELETED_SUCCESSFULLY,
        null,
        null
    );
    next();
}

module.exports = {
    getAllBannersPresenter,
    createBannerPresenter,
    getBannerByIdPresenter,
    updateBannerByIdPresenter,
    deleteBannerPresenter
}