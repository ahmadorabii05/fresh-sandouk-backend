const { PaginationResponseModel } = require("./models/responseModel/pagination/PaginationResponseModel");
const { SuccessStatus } = require("../constants/success/successStatus");
const { prepareSuccessResponse } = require("./common/presenter");
const { InvoiceResponseModel } = require("./models/responseModel/invoice/InvoiceResponseModel");
const { OrderByIdResponseModel } = require("./models/responseModel/order/OrderByIdResponseModel");


const getAllInvoiceReportPresenter = async (req, res, next) => {
    let invoice = new PaginationResponseModel(req.responseModel);
    invoice.docs = invoice?.docs?.map((invoice) => new InvoiceResponseModel(invoice, req));
    const responseModel = invoice;
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}

const getOrderByIdPresenter = async (req, res, next) => {
    const order = new OrderByIdResponseModel(req.responseModel, req);
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        order
    );
    next();
}

const editOrderQuantityPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.ORDER_UPDATED_SUCCESSFULLY,
        null,
        null
    );
    next();
}

const editOrderStatusPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.ORDER_UPDATED_SUCCESSFULLY,
        null,
        null
    );
    next();
}

const getTimeSlotPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        req.responseModel
    );
    next();
}

module.exports = {
    getAllInvoiceReportPresenter,
    getOrderByIdPresenter,
    editOrderQuantityPresenter,
    getTimeSlotPresenter,
    editOrderStatusPresenter
}