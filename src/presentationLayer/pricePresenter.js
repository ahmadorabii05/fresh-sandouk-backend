const {PaginationResponseModel} = require("./models/responseModel/pagination/PaginationResponseModel");
const {SuccessStatus} = require("../constants/success/successStatus");
const {prepareSuccessResponse} = require("./common/presenter");
const {PriceResponseModel} = require("./models/responseModel/price/PriceResponseModel");
const { isAdminRole } = require("../services/userService");


const getAllPricesPresenter = async (req,res,next)=>{
    let prices = new PaginationResponseModel(req.responseModel);
    prices.docs = prices?.docs?.map((price) => new PriceResponseModel(price,req));
    const responseModel = prices;
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}

const createPricePresenter = async (req,res,next)=>{
    req.statusCode = SuccessStatus.PRICE_MODEL_ADDED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.PRICE_MODEL_ADDED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}
const createBulkPricePresenter = async (req,res,next)=>{
    req.statusCode = SuccessStatus.PRICE_MODEL_BULK_ADDED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.PRICE_MODEL_BULK_ADDED_SUCCESSFULLY,
        null,
        null
    );
    next();
}
const getPriceByIdPresenter = async (req,res,next)=>{
    let price = new PriceResponseModel(req.responseModel,req);
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        price
    );
    next();
}

const updatePriceByIdPresenter = async (req,res,next)=>{
    let price = new PriceResponseModel(req.responseModel,req);
    req.statusCode = SuccessStatus.PRICE_MODEL_UPDATED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.PRICE_MODEL_UPDATED_SUCCESSFULLY,
        null,
        price
    );
    next();
}

const deletePricePresenter = async (req,res,next)=>{
    req.statusCode = SuccessStatus.PRICE_MODEL_DELETED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.PRICE_MODEL_DELETED_SUCCESSFULLY,
        null,
        null
    );
    next();
}

const deletePriceByIDPresenter = async (req,res,next)=>{
    req.statusCode = SuccessStatus.PRICE_MODEL_DELETE_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.PRICE_MODEL_DELETE_SUCCESSFULLY,
        null,
        null
    );
    next();
}

const getPriceBySearchPaginatedPresenter = async (req,res,next)=>{
    if(isAdminRole(req.responseModel?.admin)){
        let list = new PaginationResponseModel(req.responseModel);
        list.docs = list.docs.map((item) => new PriceResponseModel(item, req));
        const responseModel = list;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    }else{
        let list = new PaginationResponseModel(req.responseModel);
        list.docs = list.docs.map((item) => new PriceResponseModel(item, req));
        const responseModel = list;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    }

    next();
}

module.exports = {
    getAllPricesPresenter,
    createPricePresenter,
    getPriceByIdPresenter,
    updatePriceByIdPresenter,
    deletePricePresenter,
    deletePriceByIDPresenter,
    createBulkPricePresenter,
    getPriceBySearchPaginatedPresenter
}