const {SuccessStatus} = require("../constants/success/successStatus");
const {prepareSuccessResponse} = require("./common/presenter");


const createFavoritePresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.FAVORITE_UPDATED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.FAVORITE_UPDATED_SUCCESSFULLY,
        null,
        null
    );
    next();
};

module.exports = {
    createFavoritePresenter
}