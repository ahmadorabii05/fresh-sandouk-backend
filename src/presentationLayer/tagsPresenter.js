const {SuccessStatus} = require("../constants/success/successStatus");
const {prepareSuccessResponse} = require("./common/presenter");
const {TagsResponseModel} = require("./models/responseModel/tags/TagsResponseModel");
const {PaginationResponseModel} = require("./models/responseModel/pagination/PaginationResponseModel");
const { isAdminRole } = require("../services/userService");


const createTagsPresenter = async (req,res,next)=>{
    const responseModel = new TagsResponseModel(req.responseModel);
    req.statusCode = SuccessStatus.TAGS_CREATED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.TAGS_CREATED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}

const createBulkTagsPresenter = async (req,res,next)=>{
    req.statusCode = SuccessStatus.TAGS_BULK_CREATED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.TAGS_BULK_CREATED_SUCCESSFULLY,
        null,
        null
    );
    next();
}

const getTagsPresenter = async (req,res,next)=> {
    const responseModel = new TagsResponseModel(req.responseModel);
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}

const getAllTagsPresenter = async (req,res,next)=>{
    let tags = new PaginationResponseModel(req.responseModel);
    tags.docs = tags.docs.map((tag) => new TagsResponseModel(tag));
    const responseModel = tags;
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}

const updateTagsPresenter = async (req,res,next)=>{
    const responseModel = new TagsResponseModel(req.responseModel);
    req.statusCode = SuccessStatus.TAGS_UPDATED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.TAGS_UPDATED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}

const deleteTagsByIdPresenter = async (req,res,next)=>{
    req.statusCode = SuccessStatus.TAGS_DELETED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.TAGS_DELETED_SUCCESSFULLY,
        null,
        null
    );
    next();
}

const getTagsBySearchPaginatedPresenter = async (req,res,next)=>{
    if(isAdminRole(req.responseModel?.admin)){
        let list = new PaginationResponseModel(req.responseModel);
        list.docs = list.docs.map((item) => new TagsResponseModel(item, req));
        const responseModel = list;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    }else{
        let list = new PaginationResponseModel(req.responseModel);
        list.docs = list.docs.map((item) => new TagsResponseModel(item, req));
        const responseModel = list;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    }

    next();
}

module.exports={
    createTagsPresenter,
    getTagsPresenter,
    getAllTagsPresenter,
    updateTagsPresenter,
    deleteTagsByIdPresenter,
    createBulkTagsPresenter,
    getTagsBySearchPaginatedPresenter
}