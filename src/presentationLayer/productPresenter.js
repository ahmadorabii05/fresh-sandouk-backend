const { SuccessStatus } = require("../constants/success/successStatus");
const { prepareSuccessResponse } = require("./common/presenter");
const { ProductResponseModel } = require("./models/responseModel/products/ProductResponseModel");
const { PaginationResponseModel } = require("./models/responseModel/pagination/PaginationResponseModel");
const { MyFavoriteProductResponseModel } = require("./models/responseModel/products/MyFavoriteProductResponseModel");
const { isAdminRole } = require("../services/userService");
const { ProductsForAdminResponseModel } = require("./models/responseModel/products/ProductsForAdminResponseModel");

const deleteProductByIdsPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.PRODUCT_DELETED_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.PRODUCT_DELETED_SUCCESSFUL,
        null,
        null
    );
    next();
}

const deleteProductByIdPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.PRODUCT_DELETED_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.PRODUCT_DELETED_SUCCESSFUL,
        null,
        null
    );
    next();
}

const createProductPresenter = async (req, res, next) => {
    const responseModel = new ProductResponseModel(req.responseModel, req);
    req.statusCode = SuccessStatus.PRODUCT_CREATED_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.PRODUCT_CREATED_SUCCESSFUL,
        null,
        responseModel
    );
    next();
};

const createBulkProductPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.PRODUCT_BULK_CREATED_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.PRODUCT_BULK_CREATED_SUCCESSFUL,
        null,
        null
    );
    next();
};

const bulkUpdateProductPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.PRODUCT_BULK_UPDATED_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.PRODUCT_BULK_UPDATED_SUCCESSFUL,
        null,
        null
    );
    next();
};


const deleteNutritionInProductPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.NUTRITION_DELETED_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.NUTRITION_DELETED_SUCCESSFUL,
        null,
        null
    );
    next();
}

const updateProductByIdPresenter = async (req, res, next) => {
    const updatedProduct = req.responseModel
    req.statusCode = SuccessStatus.PRODUCT_UPDATED_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.PRODUCT_UPDATED_SUCCESSFUL,
        null,
        updatedProduct
    );
    next();
}


const getProductByIdPresenter = async (req, res, next) => {
    if (isAdminRole(req.responseModel.admin)) {
        const responseModel = new ProductsForAdminResponseModel(req.responseModel);
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    } else {

        const responseModel = new ProductResponseModel(req.responseModel, req);
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    }
    next();

}


const getAllProductsPaginatedPresenter = async (req, res, next) => {
    if (isAdminRole(req.responseModel?.admin)) {
        let products = new PaginationResponseModel(req.responseModel);
        products.docs = products.docs.map((product) => new ProductsForAdminResponseModel(product));
        const responseModel = products;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    } else {
        let products = new PaginationResponseModel(req.responseModel);
        products.docs = products.docs.map((product) => new ProductResponseModel(product, req));
        const responseModel = products;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    }

    next();
}

const getProductsBySearchPaginatedPresenter = async (req, res, next) => {
    if (isAdminRole(req.responseModel?.admin)) {
        let products = new PaginationResponseModel(req.responseModel);
        products.docs = products.docs.map((product) => new ProductsForAdminResponseModel(product));
        const responseModel = products;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    } else {
        let products = new PaginationResponseModel(req.responseModel);
        products.docs = products.docs.map((product) => new ProductResponseModel(product, req));
        const responseModel = products;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    }

    next();
}

const getMyFavoriteProductsPresenter = async (req, res, next) => {
    const myFavoriteProduct = req.responseModel?.map(item => {
        return new MyFavoriteProductResponseModel(item, req)
    });
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        myFavoriteProduct
    );
    next();
}
module.exports = {
    createProductPresenter,
    deleteNutritionInProductPresenter,
    deleteProductByIdsPresenter,
    updateProductByIdPresenter,
    getProductByIdPresenter,
    getAllProductsPaginatedPresenter,
    getMyFavoriteProductsPresenter,
    deleteProductByIdPresenter,
    getProductsBySearchPaginatedPresenter,
    createBulkProductPresenter,
    bulkUpdateProductPresenter
}
