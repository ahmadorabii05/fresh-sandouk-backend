const {PaginationResponseModel} = require("./models/responseModel/pagination/PaginationResponseModel");
const {SuccessStatus} = require("../constants/success/successStatus");
const {prepareSuccessResponse} = require("./common/presenter");
const {BrandResponseModel} = require("./models/responseModel/brand/BrandResponseModel");
const {BrandForAdminResponseModel} = require("./models/responseModel/brand/BrandForAdminResponseModel");
const { isAdminRole } = require("../services/userService");


const getAllBrandsPresenter = async (req,res,next)=>{
    let sources = new PaginationResponseModel(req.responseModel);
    sources.docs = sources?.docs?.map((source) => new BrandResponseModel(source,req));
    const responseModel = sources;
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}

const createBrandPresenter = async (req,res,next)=>{
    const responseModel = new BrandResponseModel(req.responseModel,req);
    req.statusCode = SuccessStatus.BRAND_ADDED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.BRAND_ADDED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}

const createBulkBrandPresenter = async (req,res,next)=>{
    req.statusCode = SuccessStatus.BRAND_BULK_ADDED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.BRAND_BULK_ADDED_SUCCESSFULLY,
        null,
        null
    );
    next();
}

const getBrandByIdPresenter = async (req,res,next)=>{
    let source = new BrandForAdminResponseModel(req.responseModel,req);
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        source
    );
    next();
}

const updateBrandByIdPresenter = async (req,res,next)=>{
    let source = new BrandResponseModel(req.responseModel,req);
    req.statusCode = SuccessStatus.BRAND_UPDATED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.BRAND_UPDATED_SUCCESSFULLY,
        null,
        source
    );
    next();
}

const deleteBrandPresenter = async (req,res,next)=>{
    req.statusCode = SuccessStatus.BRAND_DELETED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.BRAND_DELETED_SUCCESSFULLY,
        null,
        null
    );
    next();
}

const getBrandBySearchPaginatedPresenter = async (req,res,next)=>{
    if(isAdminRole(req.responseModel?.admin)){
        let brands = new PaginationResponseModel(req.responseModel);
        brands.docs = brands.docs.map((item) => new BrandForAdminResponseModel(item));
        const responseModel = brands;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    }else{
        let brands = new PaginationResponseModel(req.responseModel);
        brands.docs = brands.docs.map((item) => new BrandResponseModel(item, req));
        const responseModel = brands;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    }

    next();
}

module.exports = {
    getAllBrandsPresenter,
    createBrandPresenter,
    getBrandByIdPresenter,
    updateBrandByIdPresenter,
    deleteBrandPresenter,
    createBulkBrandPresenter,
    getBrandBySearchPaginatedPresenter
}