const {SuccessStatus} = require("../constants/success/successStatus");
const {prepareSuccessResponse} = require("./common/presenter");
const {ProductForCartResponseModel} = require("./models/responseModel/products/ProductForCartResponseModel");
const {CartResponseModel} = require("./models/responseModel/cart/CartResponseModel");


const addToCartPresenter = async (req, res, next) => {
    const responseModel = new ProductForCartResponseModel(req.responseModel);
    req.statusCode = SuccessStatus.CART_CREATED_SUCCESSFULLY.code;
    let message = "";
    if (req.create) {
        message = SuccessStatus.CART_CREATED_SUCCESSFULLY
    } else if (req.updateCart) {
        message = SuccessStatus.CART_UPDATED_SUCCESSFULLY
    } else {
        message = SuccessStatus.ITEM_ADD_SUCCESSFULLY

    }
    req.presenterModel = prepareSuccessResponse(
        message,
        null,
        responseModel
    );
    next();
};

const decreaseQuantityPresenter = async (req,res,next)=>{
    const responseModel = new ProductForCartResponseModel(req.responseModel);
    req.statusCode = SuccessStatus.UPDATED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.UPDATED_SUCCESSFULLY,
            null,
            responseModel
        );
        next();
}

const deleteItemFromCartPresenter =async (req,res,next)=>{
    req.statusCode = SuccessStatus.ITEM_DELETED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.ITEM_DELETED_SUCCESSFULLY,
        null,
        null
    );
    next();
}

const getCartInfoDataPresenter = async (req,res,next)=>{
    const responseModel = new CartResponseModel(req.responseModel,req);
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();

}
module.exports = {
    addToCartPresenter,
    decreaseQuantityPresenter,
    deleteItemFromCartPresenter,
    getCartInfoDataPresenter
}