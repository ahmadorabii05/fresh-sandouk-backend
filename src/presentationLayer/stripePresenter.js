const {SuccessStatus} = require("../constants/success/successStatus");
const {prepareSuccessResponse} = require("./common/presenter");
const {PaymentSheetResponseModel} = require("./models/responseModel/stripe/PaymentSheetResponseModel");


const createCheckoutSheetPresenter = async (req,res,next)=>{
    const responseModel = new PaymentSheetResponseModel(req.responseModel);
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}

const createRefundPresenter = async (req,res,next)=>{
    req.statusCode = SuccessStatus.ORDER_REFUNDED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.ORDER_REFUNDED_SUCCESSFULLY,
        null,
        null
    );
    next();
}

module.exports={
    createCheckoutSheetPresenter,
    createRefundPresenter
}