const {PaginationResponseModel} = require("./models/responseModel/pagination/PaginationResponseModel");
const {StockResponseModel} = require("./models/responseModel/stock/StockResponseModel");
const {SuccessStatus} = require("../constants/success/successStatus");
const {prepareSuccessResponse} = require("./common/presenter");
const {SalesResponseModel} = require("./models/responseModel/sales/SalesResponseModel");


const getAllSalesPresenter = async (req,res,next)=>{
    let stock = new PaginationResponseModel(req.responseModel);
    stock.docs = stock?.docs?.map((stock) => new SalesResponseModel(stock,req));
    const responseModel = stock;
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}

module.exports = {
    getAllSalesPresenter
}