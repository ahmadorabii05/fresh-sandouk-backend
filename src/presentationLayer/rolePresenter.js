const { SuccessStatus } = require("../constants/success/successStatus");
const {
    GetRoleResponseModel,
} = require("./models/responseModel/role/GetRoleResponseModel");
const { prepareSuccessResponse } = require("./common/presenter");

/**
 * CreateRolePresenter prepares the response
 */
const createRolePresenter = async (req, res, next) => {
    const responseModel = new GetRoleResponseModel(req.role);
    req.statusCode = SuccessStatus.ROLE_CREATED_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.ROLE_CREATED_SUCCESSFUL,
        null,
        responseModel
    );
    next();
};

/**
 * GetRolePresenter prepares the response
 */
const getRolePresenter = async (req, res, next) => {
    const responseModel = new GetRoleResponseModel(req.responseModel);
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
};

/**
 * GetAllRolesPresenter prepares the response
 */
const getAllRolesPresenter = async (req, res, next) => {
    const responseModel = req.responseModel.map(
        (data) => new GetRoleResponseModel(data)
    );
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
};

/**
 * UpdateRolePresenter prepares the response
 */
const updateRolePresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.UPDATED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.UPDATED_SUCCESSFULLY,
        null,
        null
    );
    next();
};

/**
 * DeleteRolePresenter prepares the response
 */
const deleteRolePresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.DELETED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.DELETED_SUCCESSFULLY,
        null,
        null
    );
    next();
};

module.exports = {
    createRolePresenter,
    getRolePresenter,
    updateRolePresenter,
    getAllRolesPresenter,
    deleteRolePresenter,
};
