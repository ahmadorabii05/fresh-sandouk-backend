const {SuccessStatus} = require("../constants/success/successStatus");
const {prepareSuccessResponse} = require("./common/presenter");
const {OrderResponseModel} = require("./models/responseModel/order/OrderResponseModel");
const {ProductResponseModel} = require("./models/responseModel/products/ProductResponseModel");
const {ErrorStatus} = require("../constants/error/errorStatus");
const { isAdminRole } = require("../services/userService");
const { PaginationResponseModel } = require("./models/responseModel/pagination/PaginationResponseModel");


const createOrderPresenter = async (req,res,next)=>{
    if(req.responseModel.outOfStock){
        const responseModel =req.responseModel.outOfStockProducts.map(item=>{
            return new ProductResponseModel(item,req);
        })
        req.statusCode = ErrorStatus.ITEM_OUT_OF_STOCK.code;
        req.presenterModel = prepareSuccessResponse(
            ErrorStatus.ITEM_OUT_OF_STOCK,
            "Item(s) out of Stock",
            responseModel
        );
    }else{
        const responseModel = new OrderResponseModel(req.responseModel,req);
        req.statusCode = SuccessStatus.ORDER_ADDED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.ORDER_ADDED_SUCCESSFULLY,
            null,
            responseModel
        );

    }
    next();

}

const getOrdersBySearchPaginatedPresenter = async (req,res,next)=>{
    if(isAdminRole(req.responseModel?.admin)){
        let list = new PaginationResponseModel(req.responseModel);
        list.docs = list.docs.map((item) => new OrderResponseModel(item, req));
        const responseModel = list;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    }else{
        let list = new PaginationResponseModel(req.responseModel);
        list.docs = list.docs.map((item) => new OrderResponseModel(item, req));
        const responseModel = list;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    }

    next();
}

module.exports = {
    createOrderPresenter,
    getOrdersBySearchPaginatedPresenter
}