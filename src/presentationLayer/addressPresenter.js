const {PaginationResponseModel} = require("./models/responseModel/pagination/PaginationResponseModel");
const {SuccessStatus} = require("../constants/success/successStatus");
const {prepareSuccessResponse} = require("./common/presenter");
const {AddressResponseModel} = require("./models/responseModel/address/AddressResponseModel");


const getAllAddressPresenter = async (req,res,next)=>{
    let address = new PaginationResponseModel(req.responseModel);
    address.docs = address?.docs?.map((address) => new AddressResponseModel(address));
    const responseModel = address;
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}

const createAddressPresenter = async (req,res,next)=>{
    const responseModel = new AddressResponseModel(req.responseModel);
    req.statusCode = SuccessStatus.ADDRESS_ADDED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.ADDRESS_ADDED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}

const getAddressByIdPresenter = async (req,res,next)=>{
    let address = new AddressResponseModel(req.responseModel);
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        address
    );
    next();
}

const updateAddressByIdPresenter = async (req,res,next)=>{
    let address = new AddressResponseModel(req.responseModel);
    req.statusCode = SuccessStatus.ADDRESS_UPDATED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.ADDRESS_UPDATED_SUCCESSFULLY,
        null,
        address
    );
    next();
}

const deleteAddressPresenter = async (req,res,next)=>{
    req.statusCode = SuccessStatus.ADDRESS_DELETED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.ADDRESS_DELETED_SUCCESSFULLY,
        null,
        null
    );
    next();
}

module.exports = {
    getAllAddressPresenter,
    createAddressPresenter,
    getAddressByIdPresenter,
    updateAddressByIdPresenter,
    deleteAddressPresenter
}