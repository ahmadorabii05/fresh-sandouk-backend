const {SuccessStatus} = require("../constants/success/successStatus");
const {prepareSuccessResponse} = require("./common/presenter");
const {ErrorStatus} = require("../constants/error/errorStatus");


const uploadZipFilePresenter = async (req,res,next)=>{
    if(req.requestModel.rejected.length > 0){
        req.statusCode = ErrorStatus.SERVER_DOWN.code;
        req.presenterModel = prepareSuccessResponse(
            ErrorStatus.SERVER_DOWN,
            "We can not find products name for these images name",
            req.requestModel.rejected
        );
        next();
    }else{
        req.statusCode = SuccessStatus.ALL_IMAGES_UPLOADED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.ALL_IMAGES_UPLOADED_SUCCESSFULLY,
            null,
            null
        );
        next();
    }
}

module.exports = {
    uploadZipFilePresenter
}