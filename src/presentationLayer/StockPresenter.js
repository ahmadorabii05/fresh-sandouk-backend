const {PaginationResponseModel} = require("./models/responseModel/pagination/PaginationResponseModel");
const {SuccessStatus} = require("../constants/success/successStatus");
const {prepareSuccessResponse} = require("./common/presenter");
const {StockResponseModel} = require("./models/responseModel/stock/StockResponseModel");




const createProductInStockPresenter = async (req,res,next)=>{
    const responseModel = new StockResponseModel(req.responseModel,req);
    req.statusCode = SuccessStatus.STOCK_ADDED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.STOCK_ADDED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}

const getProductInStockByIdPresenter = async (req,res,next)=>{
    let source = new StockResponseModel(req.responseModel,req);
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        source
    );
    next();
}


module.exports = {
    createProductInStockPresenter,
    getProductInStockByIdPresenter,
}