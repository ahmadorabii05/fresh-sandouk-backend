const {SuccessStatus} = require("../constants/success/successStatus");
const {prepareSuccessResponse} = require("./common/presenter");
const {CategoryResponseModel} = require("./models/responseModel/category/CategoryResponseModel");
const {PaginationResponseModel} = require("./models/responseModel/pagination/PaginationResponseModel");
const {CreateCategoryResponseModel} = require("./models/responseModel/category/CreateCategoryResponseModel");
const {CategoryFromAdminResponseModel} = require("./models/responseModel/category/CategoryFromAdminResponseModel");
const {isAdminRole} = require("../services/userService");


const createCategoryPresenter = async (req, res, next) => {
    const responseModel = new CreateCategoryResponseModel(req.category);
    req.statusCode = SuccessStatus.CATEGORY_CREATED_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.CATEGORY_CREATED_SUCCESSFUL,
        null,
        responseModel
    );
    next();
};

const createBulkCategoryPresenter = async (req,res,next)=>{
    req.statusCode = SuccessStatus.CATEGORIES_CREATED_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.CATEGORIES_CREATED_SUCCESSFUL,
        null,
        null
    );
    next();
}

const getAllCategoryPresenter = async (req, res, next) => {
    if(req.header('client') == 'admin' || isAdminRole(req.responseModel?.admin)) {
        const categories = req.responseModel.map((cat) => new CategoryFromAdminResponseModel(cat,req));
        const responseModel = categories;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        res.setHeader('Access-Control-Expose-Headers', 'Content-Range')
        res.setHeader('Content-Range', 'category 0-24/319')
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
        next();
    } else {
        let categories = new PaginationResponseModel(req.responseModel);
        categories.docs = categories.docs.map((category) => new CategoryResponseModel(category,req));
        const responseModel = categories;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
        next();
    }
}

const getCategoryByIdPresenter = async (req, res, next) => {
    if(isAdminRole(req?.admin)){
        let categoryById = new CategoryFromAdminResponseModel(req.requestModel, req)
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            categoryById
        );
    }else{
        let categoryById = new CategoryResponseModel(req.requestModel,req)
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            categoryById
        );
    }

    next();
}

const deleteCategoryByIdsPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.CATEGORY_DELETED_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.CATEGORY_DELETED_SUCCESSFUL,
        null,
        null
    );
    next();
}

const deleteCategoryByIdPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.CATEGORY_DELETED_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.CATEGORY_DELETED_SUCCESSFUL,
        null,
        null
    );
    next();
}


const updateCategoryByIdPresenter = async (req, res, next) => {
    const category = new CategoryResponseModel(req.category,req);
    req.statusCode = SuccessStatus.CATEGORY_UPDATED_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.CATEGORY_UPDATED_SUCCESSFUL,
        null,
        category
    );
    next();

}

const getCategoryBySearchPaginatedPresenter = async (req,res,next)=>{
    if(isAdminRole(req.responseModel?.admin)){
        let list = new PaginationResponseModel(req.responseModel);
        list.docs = list.docs.map((item) => new CategoryFromAdminResponseModel(item, req));
        const responseModel = list;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    }else{
        let list = new PaginationResponseModel(req.responseModel);
        list.docs = list.docs.map((item) => new CategoryResponseModel(item, req));
        const responseModel = list;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    }

    next();
}

module.exports = {
    createCategoryPresenter,
    getAllCategoryPresenter,
    getCategoryByIdPresenter,
    deleteCategoryByIdsPresenter,
    updateCategoryByIdPresenter,
    deleteCategoryByIdPresenter,
    createBulkCategoryPresenter,
    getCategoryBySearchPaginatedPresenter
}

