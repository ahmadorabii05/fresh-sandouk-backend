const {SuccessStatus} = require("../constants/success/successStatus");
const {prepareSuccessResponse} = require("./common/presenter");
const {PaginationResponseModel} = require("./models/responseModel/pagination/PaginationResponseModel");
const {SubCategoryResponseModel} = require("./models/responseModel/subCategory/SubCategoryResponseModel");
const {CreateSubCategoryResponseModel} = require("./models/responseModel/subCategory/CreateSubCategoryResponseModel");
const {isAdminRole} = require("../services/userService");
const {SubCategoryForAdminResponseModel} = require("./models/responseModel/subCategory/SubCategoryForAdminResponseModel");


const CreateSubCategoryPresenter = async (req, res, next) => {
    const responseModel = new CreateSubCategoryResponseModel(req.subCategory);
    req.statusCode = SuccessStatus.SUB_CATEGORY_CREATED_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.SUB_CATEGORY_CREATED_SUCCESSFUL,
        null,
        responseModel
    );
    next();
};


const CreateBulkSubCategoryPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.SUB_CATEGORY_BULK_CREATED_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.SUB_CATEGORY_BULK_CREATED_SUCCESSFUL,
        null,
        null
    );
    next();
};
const getAllSubCategoryPaginatedPresenter = async (req, res, next) => {
    if(isAdminRole(req.responseModel?.admin)){
        let subCategories = new PaginationResponseModel(req.responseModel);
        subCategories.docs = subCategories.docs.map((subCategory) => new SubCategoryForAdminResponseModel(subCategory,req));
        const responseModel = subCategories;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    }else{
        let subCategories = new PaginationResponseModel(req.responseModel);
        subCategories.docs = subCategories.docs.map((subCategory) => new SubCategoryResponseModel(subCategory,req));
        const responseModel = subCategories;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    }
    next();
}

const getSubCategoryByIdPresenter = async (req, res, next) => {
    if(isAdminRole(req.responseModel?.admin)){
        let SubcategoryById = new SubCategoryForAdminResponseModel(req.responseModel,req)
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            SubcategoryById
        );
    }else{
        let SubcategoryById = new SubCategoryResponseModel(req.responseModel,req)
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            SubcategoryById
        );
    }

    next();
}

const deleteSubCategoryByIdsPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.SUB_CATEGORY_DELETED_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.SUB_CATEGORY_DELETED_SUCCESSFUL,
        null,
        null
    );
    next();
}
const deleteSingleSubCategoryByIdsPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.SUB_CATEGORY_DELETED_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.SUB_CATEGORY_DELETED_SUCCESSFUL,
        null,
        null
    );
    next();
}

const updateSubCategoryByIdPresenter = async (req, res, next) => {
    const subCategory = new SubCategoryResponseModel(req.responseModel,req);
    req.statusCode = SuccessStatus.SUB_CATEGORY_UPDATED_SUCCESSFUL.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.SUB_CATEGORY_UPDATED_SUCCESSFUL,
        null,
        subCategory
    );
    next();

}

const getSubCategoryBySearchPaginatedPresenter = async (req,res,next)=>{
    if(isAdminRole(req.responseModel?.admin)){
        let list = new PaginationResponseModel(req.responseModel);
        list.docs = list.docs.map((item) => new SubCategoryForAdminResponseModel(item, req));
        const responseModel = list;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    }else{
        let list = new PaginationResponseModel(req.responseModel);
        list.docs = list.docs.map((item) => new SubCategoryResponseModel(item, req));
        const responseModel = list;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    }

    next();
}

module.exports = {
    CreateSubCategoryPresenter,
    getAllSubCategoryPaginatedPresenter,
    getSubCategoryByIdPresenter,
    deleteSubCategoryByIdsPresenter,
    updateSubCategoryByIdPresenter,
    deleteSingleSubCategoryByIdsPresenter,
    CreateBulkSubCategoryPresenter,
    getSubCategoryBySearchPaginatedPresenter
}

