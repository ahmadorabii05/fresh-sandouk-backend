const {SuccessStatus} = require("../constants/success/successStatus");
const {prepareSuccessResponse} = require("./common/presenter");
const {GetStatisticsResponseModel} = require("./models/responseModel/statistics/GetStatisticsResponseModel");


const getStatisticsPresenter = async (req,res,next)=>{
    let statistics = new GetStatisticsResponseModel(req.responseModel);
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        statistics
    );
    next();
}

module.exports = {
    getStatisticsPresenter
}