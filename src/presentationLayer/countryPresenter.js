const {SuccessStatus} = require("../constants/success/successStatus");
const {prepareSuccessResponse} = require("./common/presenter");
const {CountryResponseModel} = require("../dataHandlerLayer/models/requestModel/country/CountryResponseModel");


const uploadCountryPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.COUNTRIES_UPLOADED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.COUNTRIES_UPLOADED_SUCCESSFULLY,
        null,
        null
    );
    next();

}

const getAllCountriesPresenter = async (req,res,next)=>{
    const responseModel = req.responseModel?.map(item=>{
        return new CountryResponseModel(item,req);
    })

    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}

module.exports = {
    uploadCountryPresenter,
    getAllCountriesPresenter
}