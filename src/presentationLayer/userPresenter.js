const { SuccessStatus } = require("../constants/success/successStatus");
const {
    UserResponseModel,
} = require("./models/responseModel/user/UserResponseModel");
const {
    UserInfoResponseModel,
} = require("./models/responseModel/user/UserInfoResponseModel");
const { prepareSuccessResponse } = require("./common/presenter");
const {
    PaginationResponseModel,
} = require("./models/responseModel/pagination/PaginationResponseModel");
const {UserInfoWIthAllDetails} = require("./models/responseModel/user/UserInfoWIthAllDetails");
const { isAdminRole } = require("../services/userService");
/**
 * Responsible for preparing the response to be sent to the frontend
 * @param {request will include a property called responseModel prepared from the dataHandler} req
 * @param {response will be used to return an immediate response if needed} res
 * @param {goes to the next step of the process} next
 */

/**
 * GetUserByIdPresenter prepares the response for the client side
 */
const getUserByIdPresenter = async (req, res, next) => {
    const responseModel = new UserResponseModel(req.user);
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
};

/**
 * GetUserInfoByIdPresenter prepares the response for the client side
 */
const getUserInfoByIdPresenter = async (req, res, next) => {
    const responseModel = new UserInfoResponseModel(req.user);
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
};

/**
 * GetAllUsersPresenter prepares the response for the client side
 */
const getAllUsersPresenter = async (req, res, next) => {
    let users = new PaginationResponseModel(req.users);
    users.docs = users.docs.map((user) => new UserResponseModel(user));
    const responseModel = users;
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
};

/**
 * UpdateUserByIdPresenter prepares the response for the client side
 */
const updateUserByIdPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.UPDATED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.UPDATED_SUCCESSFULLY,
        null,
        null
    );
    next();
};

/**
 * ChangePasswordPresenter prepares the response for the client side
 */
const changePasswordPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.UPDATED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.UPDATED_SUCCESSFULLY,
        null,
        null
    );
    next();
};

/**
 * CreateUserByIdPresenter prepares the response for the client side
 */
const createUserByIdPresenter = async (req, res, next) => {
    const responseModel = new UserResponseModel(req.user);
    req.statusCode = SuccessStatus.ADDED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.ADDED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
};

/**
 * UploadUserImagePresenter
 */
const uploadUserImagePresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.UPLOADED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.UPLOADED_SUCCESSFULLY,
        null,
        null
    );
    next();
};


/**
 * UpdateUserPushNotificationTokenPresenter
 */
const updateUserPushNotificationTokenPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.UPDATED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.UPDATED_SUCCESSFULLY,
        null,
        null
    );
    next();
};




/**
 * DeleteUserByIdPresenter prepares the response for the client side
 */
const deleteUserByIdPresenter = async (req, res, next) => {
    req.statusCode = SuccessStatus.DELETED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.DELETED_SUCCESSFULLY,
        null,
        null
    );
    next();
};

const updateUserIsOnlinePresenter = (req,res,next) =>{
    req.statusCode = SuccessStatus.UPDATED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.UPDATED_SUCCESSFULLY,
        null,
        null
    );
    next();
}

const removeProfilePresenter = async (req,res,next)=>{
    req.statusCode = SuccessStatus.PROFILE_IMAGE_REMOVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.PROFILE_IMAGE_REMOVED_SUCCESSFULLY,
        null,
        null
    );
    next();
}

const getUserInfoByTokenPresenter = async (req,res,next)=>{
    const responseModel = new UserInfoResponseModel(req.user);
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}

const getUserByIdWithOrderPresenter = async (req,res,next)=>{
    const responseModel = new UserInfoWIthAllDetails(req.responseModel,req);
    req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
    req.presenterModel = prepareSuccessResponse(
        SuccessStatus.RECEIVED_SUCCESSFULLY,
        null,
        responseModel
    );
    next();
}

const getUsersBySearchPaginatedPresenter = async (req,res,next)=>{
    if(isAdminRole(req.responseModel?.admin)){
        let list = new PaginationResponseModel(req.responseModel);
        list.docs = list.docs.map((item) => new UserResponseModel(item, req));
        const responseModel = list;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    }else{
        let list = new PaginationResponseModel(req.responseModel);
        list.docs = list.docs.map((item) => new UserResponseModel(item, req));
        const responseModel = list;
        req.statusCode = SuccessStatus.RECEIVED_SUCCESSFULLY.code;
        req.presenterModel = prepareSuccessResponse(
            SuccessStatus.RECEIVED_SUCCESSFULLY,
            null,
            responseModel
        );
    }

    next();
}

module.exports = {
    removeProfilePresenter,
    getUserByIdPresenter,
    getUserInfoByIdPresenter,
    getAllUsersPresenter,
    updateUserByIdPresenter,
    changePasswordPresenter,
    createUserByIdPresenter,
    deleteUserByIdPresenter,
    uploadUserImagePresenter,
    updateUserPushNotificationTokenPresenter,
    updateUserIsOnlinePresenter,
    getUserInfoByTokenPresenter,
    getUserByIdWithOrderPresenter,
    getUsersBySearchPaginatedPresenter
};
