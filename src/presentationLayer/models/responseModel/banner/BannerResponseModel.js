const { APISignature } = require("../common/APISignature");
class BannerResponseModel extends APISignature {
    id = undefined;
    actionUrl = undefined;
    imageUrl = undefined;
    location = undefined;
    priority = undefined;
    constructor(values) {
        super(values);
        this.id = values?._id;
        this.actionUrl = values?.actionUrl;
        this.imageUrl = values?.imageUrl;
        this.location = values?.location;
        this.priority = values?.priority;
    }
}

module.exports = {
    BannerResponseModel
}
