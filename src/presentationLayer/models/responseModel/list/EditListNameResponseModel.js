const {ProductResponseModel} = require("../products/ProductResponseModel");
const {APISignature} = require("../common/APISignature");
const {extractLanguageFromRequest} = require("../../../../dataHandlerLayer/common/LanguageHeaderHandler");
class EditListNameResponseModel  extends APISignature{
    name=undefined;
    userId=undefined;
    productIds =undefined;

    constructor(values,req) {
        super(values);
        this.name=extractLanguageFromRequest(req) === "ar" ? values?.nameAr || values?.name : values?.name;
        this.userId=values?.userId;
        this.productIds = values?.productIds?.length > 0 ? values?.productIds.map(item=>{
            return new ProductResponseModel(item,req)
        }) : [];
    }

}

module.exports = {
    EditListNameResponseModel
}