const {APISignature} = require ("../common/APISignature");
const {ProductResponseModel} = require ("../products/ProductResponseModel");
const {extractLanguageFromRequest} = require("../../../../dataHandlerLayer/common/LanguageHeaderHandler");

class CreateListResponseModel extends APISignature{
    id=undefined;
    name=undefined;
    userId=undefined;
    productIds =undefined;
    constructor(values,req) {
        super(values);
        this.id=values?._id;
        this.name=extractLanguageFromRequest(req) === "ar" ? values?.nameAr || values?.name : values?.name;
        this.userId=values?.userId;
        this.productIds = values?.productIds.length > 0 ? values?.productIds.map(item=>{
            return new ProductResponseModel(item,req)
        }) : [];
    }
}

module.exports = {
    CreateListResponseModel
}