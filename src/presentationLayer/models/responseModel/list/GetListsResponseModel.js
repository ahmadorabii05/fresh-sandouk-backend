const {ProductResponseModel} = require("../products/ProductResponseModel");
const {extractLanguageFromRequest} = require("../../../../dataHandlerLayer/common/LanguageHeaderHandler");

class GetListsResponseModel {
    id=undefined;
    name = undefined;
    userId = undefined;
    productIds = undefined;

    constructor(values,req) {
        this.id=values?._id;
        this.name=extractLanguageFromRequest(req) === "ar" ? values?.nameAr || values?.name : values?.name;
        this.userId = values.userId;
        this.productIds = values?.productIds?.length > 0 ? values.productIds.map(item => {
            return new ProductResponseModel(item,req)
        }) : []
    }
}

module.exports={
    GetListsResponseModel
}