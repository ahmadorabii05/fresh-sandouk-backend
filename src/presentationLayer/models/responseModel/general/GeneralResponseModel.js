const { GetRoleResponseModel } = require("../role/GetRoleResponseModel");
const {APISignature} = require("../common/APISignature");
class GeneralResponseModel extends APISignature{
    email = undefined;
    name=undefined;
    firstName=undefined;
    lastName=undefined;
    alternativeEmail=undefined;
    alternativePhoneNumber=undefined;
    accountVerification = undefined;
    dateOfBirth=undefined;
    nationality=undefined;
    role = undefined;
    phoneNumber=undefined;
    stripe_id=undefined;
    constructor(values) {
        super(values);
        this.name=values?.name || "";
        this.email = values?.email || "";
        this.stripe_id=values?.stripe_id || "";
        this.imageUrl = values?.imageUrl || "";
        this.accountVerification = values?.accountVerification || false;
        this.role = new GetRoleResponseModel(values?.role);
        this.phoneNumber = values?.phoneNumber || "";
        this.firstName = values?.firstName || "";
        this.lastName = values?.lastName || "";
        this.alternativeEmail = values?.alternativeEmail || "";
        this.alternativePhoneNumber = values?.alternativePhoneNumber || "";
        this.dateOfBirth = values?.dateOfBirth || "";
        this.nationality = values?.nationality || "";
        this.walletBalance =values?.walletBalance || 0;


    }
}

module.exports = {
    GeneralResponseModel,
};
