const {APISignature} = require("../common/APISignature");
const {GetCategoryInventory} = require("../category/GetCategoryInventory");
const {SalesProductResponseModel} = require("../products/SalesProductResponseModel");

class SalesResponseModel extends APISignature{
    id=undefined;
    product=undefined;
    quantity=undefined;
    quantitySold=undefined;
    category=undefined;
    totalSoldAmount=undefined;
    constructor(values,req) {
        super(values);
        this.id=values?._id;
        this.product = new SalesProductResponseModel(values?.productId,req);
        this.quantity=values?.quantity;
        this.quantitySold=values?.quantitySold;
        this.category=values?.category ? new GetCategoryInventory(values?.category,req) : "";
        this.totalSoldAmount = values?.purchase || 0;
    }
}

module.exports = {
    SalesResponseModel
}