const {APISignature} = require("../common/APISignature");

class TagsResponseModel extends APISignature {
    id=undefined;
    name = undefined;
    nameAr = undefined;
    imageUrl = undefined;

    constructor(values) {
        super(values);
        this.id=values?._id;
        this.name = values?.name;
        this.nameAr = values?.nameAr;
        this.imageUrl = values?.imageUrl
    }

}

module.exports = {
    TagsResponseModel
}