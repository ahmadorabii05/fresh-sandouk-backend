const {ProductResponseModel} = require("../products/ProductResponseModel");
const {APISignature} = require("../common/APISignature");
const {extractLanguageFromRequest} = require("../../../../dataHandlerLayer/common/LanguageHeaderHandler");

class SubCategoryResponseModel extends APISignature{
    id=undefined;
    name = undefined;
    imageUrl = undefined;
    productIds=undefined;
    color=undefined;
    isActive=undefined;
    constructor(values,req) {
        super(values);
        this.id=values?._id;
        this.name =extractLanguageFromRequest(req) === "en" ? values?.name : values?.nameAr || "";
        this.imageUrl =values?.imageUrl;
        this.color=values?.color;
        this.productIds=values?.productIds?.length > 0 ? values?.productIds.map((item)=>{
            return new ProductResponseModel(item,req)
        }) : []
        this.isActive = values?.isActive
    }
}

module.exports = {
    SubCategoryResponseModel,
};
