const {ProductResponseModel} = require("../products/ProductResponseModel");
const {APISignature} = require("../common/APISignature");

class CreateSubCategoryResponseModel extends APISignature{
    id=undefined;
    name = undefined;
    nameAr = undefined;
    imageUrl = undefined;
    productIds=undefined;
    color=undefined;
    isActive=undefined;
    constructor(values) {
        super(values);
        this.id=values?._id;
        this.name = values?.name;
        this.nameAr=values?.nameAr || "";
        this.imageUrl =values?.imageUrl;
        this.color=values?.color;
        this.productIds=values?.productIds?.length > 0 ? values?.productIds.map((item)=>{
            return new ProductResponseModel(item)
        }) : []
        this.isActive = values?.isActive;
    }
}

module.exports = {
    CreateSubCategoryResponseModel,
};
