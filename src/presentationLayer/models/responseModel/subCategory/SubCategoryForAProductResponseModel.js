class SubCategoryForAProductResponseModel{
    id=undefined;
    name = undefined;
    nameAr=undefined;
    imageUrl = undefined;
    productIds=undefined;
    color=undefined;
    isActive=undefined;
    constructor(values) {
        this.id=values?._id;
        this.name =values?.name;
        this.nameAr=values?.nameAr;
        this.imageUrl =values?.imageUrl;
        this.color=values?.color;
        this.isActive = values?.isActive;
    }
}

module.exports = {
    SubCategoryForAProductResponseModel,
};
