

const {APISignature} = require("../common/APISignature");
const {ProductsForAdminResponseModel} = require("../products/ProductsForAdminResponseModel");

class SubCategoryForAdminResponseModel extends APISignature{
    id=undefined;
    name = undefined
    nameAr = undefined;
    imageUrl = undefined;
    color=undefined;
    productIds=undefined;
    category =undefined;
    isActive = undefined;
    constructor(values) {
        super(values);
        this.id=values?._id;
        this.name =values?.name ||  "";
        this.nameAr =values?.nameAr ||  "";
        this.imageUrl =values?.imageUrl;
        this.color=values?.color;
        this.productIds=values?.productIds?.length > 0 ? values?.productIds.map((item)=>{
            return new ProductsForAdminResponseModel(item)
        }) : [];
        this.category = values?.category;
        this.isActive = values?.isActive;
    }
}

module.exports = {
    SubCategoryForAdminResponseModel,
};
