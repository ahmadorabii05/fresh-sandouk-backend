const { APISignature } = require("../common/APISignature");
const {SubCategoryResponseModel} = require("../subCategory/SubCategoryResponseModel");
const {extractLanguageFromRequest} = require("../../../../dataHandlerLayer/common/LanguageHeaderHandler");


class CategoryResponseModel extends APISignature {
    id=undefined;
    name = undefined;
    color=undefined;
    subCategories=undefined;
    priority=undefined;
    isActive=undefined;
    banners=undefined;
    constructor(values,req) {
        super(values);
        this.id=values?.id;
        this.name =extractLanguageFromRequest(req) === "en" ? values?.name : values?.nameAr;
        this.imageUrl=values?.imageUrl;
        this.color=values?.color;
        this.subCategories=values?.subCategories.length > 0 ? values?.subCategories.map((item)=>{
            new SubCategoryResponseModel(item)
        }) : []
        this.priority = values?.priority;
        this.isActive = values?.isActive;
        this.banners = values?.banners ?? [];
    }
}

module.exports = {
    CategoryResponseModel,
};
