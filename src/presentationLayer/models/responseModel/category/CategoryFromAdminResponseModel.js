const { APISignature } = require("../common/APISignature");
const {SubCategoryForAdminResponseModel} = require("../subCategory/SubCategoryForAdminResponseModel");


class CategoryFromAdminResponseModel extends APISignature {
    id=undefined;
    name = undefined;
    nameAr=undefined;
    subCategories=undefined;
    imageUrl = undefined;
    color=undefined;
    priority=undefined;
    isActive=undefined;
    banners=undefined;
    constructor(values,req) {

        super(values);
        this.id=values?._id;
        this.name =   values?.name || "";
        this.nameAr =values?.nameAr || "";
        this.imageUrl=values?.imageUrl;
        this.color=values?.color;
        this.subCategories=values?.subCategories?.length > 0 ? values?.subCategories.map(item=>{
            return new SubCategoryForAdminResponseModel(item,req)
        })  : [];
        this.priority = values?.priority;
        this.isActive = values?.isActive;
        this.banners = values?.banners ?? [];

    }
}

module.exports = {
    CategoryFromAdminResponseModel,
};
