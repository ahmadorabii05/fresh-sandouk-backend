const { APISignature } = require("../common/APISignature");


class CreateCategoryResponseModel extends APISignature {
    id=undefined;
    name = undefined;
    nameAr=undefined;
    subCategories=undefined;
    color=undefined;
    imageUrl = undefined;
    priority=undefined;
    isActive=undefined;
    banners=undefined;
    constructor(values) {
        super(values);
        this.id=values?.id;
        this.name = values?.name || "";
        this.nameAr = values?.nameAr || "";
        this.color=values?.color;
        this.imageUrl=values?.imageUrl;
        this.subCategories=values?.subCategories?.length > 0 ? values?.subCategories : [];
        this.priority = values?.priority;
        this.isActive = values?.isActive;
        this.banners = values?.banners ?? [];
    }
}

module.exports = {
    CreateCategoryResponseModel,
};
