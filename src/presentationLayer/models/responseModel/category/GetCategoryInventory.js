const {extractLanguageFromRequest} = require("../../../../dataHandlerLayer/common/LanguageHeaderHandler");


class GetCategoryInventory{
    name=undefined;
    constructor(values,req) {
        this.name =  extractLanguageFromRequest(req) === "en" ? values?.name : values?.nameAr || "";

    }
}

module.exports = {
    GetCategoryInventory
}