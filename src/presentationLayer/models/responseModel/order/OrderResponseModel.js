const { APISignature } = require("../common/APISignature");
const { ProductResponseModel } = require("../products/ProductResponseModel");
const { AddressResponseModel } = require("../address/AddressResponseModel");


class OrderResponseModel extends APISignature {

    constructor(values, req) {
        super(values);
        this.id = values?._id;
        this.keyOrder = values?.keyOrder || "";
        this.userId = values?.userId;
        this.products = values?.products.length > 0 ? values?.products.map(item => {
            return new ProductResponseModel(item.productId, req)
        }) : [];
        this.address = new AddressResponseModel(values?.addressId);
        this.scheduledDate = values?.scheduledDate;
        this.scheduledTime = values?.scheduledTime;
        this.deliveryInstruction = values?.deliveryInstruction || "";
        this.packingInstruction = values?.packingInstruction || "";
        this.status = values?.status;
    }
}

module.exports = {
    OrderResponseModel
}