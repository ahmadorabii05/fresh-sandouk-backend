const {APISignature} = require("../common/APISignature");
const {AddressResponseModel} = require("../address/AddressResponseModel");
const {ProductForOrderResponseModel} = require("../products/ProductForOrderResponseModel");


class OrderPerUserResponseModel extends APISignature{

    constructor(values,req) {
        super(values);
        this.id = values?._id;
        // this.userId = values?.userId;
        this.keyOrder=values?.keyOrder || "";
        this.products = values?.products.length > 0 ? values?.products.map(item=>{
            return new ProductForOrderResponseModel(item ?? -1,req)
        }) : [];
        this.address = new AddressResponseModel(values?.addressId);
        this.scheduledDate=values?.scheduledDate;
        this.scheduledTime=values?.scheduledTime;
        this.deliveryInstruction =values?.deliveryInstruction || "";
        this.packingInstruction=values?.packingInstruction || "";
        this.totalAmount = values?.totalAmount || 0;
        this.status = values?.status;
    }
}

module.exports={
    OrderPerUserResponseModel
}