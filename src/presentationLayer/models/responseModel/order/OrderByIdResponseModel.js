const {APISignature} = require("../common/APISignature");
const {AddressResponseModel} = require("../address/AddressResponseModel");
const {ProductForOrderByIdResponseModel} = require("../products/ProductForOrderByIdResponseModel");


class OrderByIdResponseModel extends APISignature{
    id=undefined;
    keyOrder=undefined;
    customerName=undefined;
    address=undefined;
    scheduledDate=undefined;
    scheduledTime=undefined;
    totalAmount=undefined;
    status=undefined;
    deliveryInstruction = undefined;
    constructor(values,req) {
        super(values);
        this.id=values?._doc?._id || -1;
        this.keyOrder=values?._doc?.keyOrder || "";
        this.customerName=values?._doc?.userId?.info?.general?.name;
        this.address= new AddressResponseModel(values?._doc?.addressId) || "";
        this.scheduledDate=values?._doc?.scheduledDate;
        this.scheduledTime=values?._doc?.scheduledTime;
        this.totalAmount=values?._doc?.totalAmount;
        this.totalAmount = values?.totalAmount || 0;
        this.products= values?.products.length > 0 ? values?.products.map(item=>{
            return new ProductForOrderByIdResponseModel(item,req)
        }) : []
        this.status = values?.status;
        this.deliveryInstruction = values?.deliveryInstruction ?? "";
    }
}

module.exports={
    OrderByIdResponseModel
}