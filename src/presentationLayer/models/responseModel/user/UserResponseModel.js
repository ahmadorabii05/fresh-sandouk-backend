const { UserInfoResponseModel } = require("../user/UserInfoResponseModel");

class UserResponseModel extends UserInfoResponseModel {
    constructor(values) {
        super(values);
    }
}

module.exports = {
    UserResponseModel,
};
