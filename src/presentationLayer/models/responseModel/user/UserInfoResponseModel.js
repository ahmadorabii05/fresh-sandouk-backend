const { APISignature } = require("../common/APISignature");
const { InfoResponseModel } = require("../info/InfoResponseModel");

class UserInfoResponseModel extends APISignature {
    id = undefined;
    info = undefined;

    constructor(values) {
        super(values);
        this.id = values._id || -1;
        this.info = new InfoResponseModel(values.info) || "";
    }
}

module.exports = {
    UserInfoResponseModel,
};
