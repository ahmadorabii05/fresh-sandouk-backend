const {UserInfoResponseModel} = require("./UserInfoResponseModel");
const {AddressResponseModel} = require("../address/AddressResponseModel");
const {OrderPerUserResponseModel} = require("../order/OrderPerUserResponseModel");


class UserInfoWIthAllDetails {
    userInfo=undefined;
    orders=undefined;
    addresses=undefined;
    constructor(values,req) {
        this.userInfo=values?.user ? new UserInfoResponseModel(values?.user) : "";
        this.addresses = values?.addresses?.length > 0 ? values?.addresses.map(item=>{
            return new AddressResponseModel(item);
        }) : [];
        this.orders = values?.order.length > 0 ? values?.order.map(item=>{
            return new OrderPerUserResponseModel(item,req)
        }) : []
    }
}

module.exports={
    UserInfoWIthAllDetails
}