const {APISignature} = require("../common/APISignature");
const {ProductResponseModel} = require("../products/ProductResponseModel");
const {CategoryResponseModel} = require("../category/CategoryResponseModel");
const {InventoryProductResponseModel} = require("../products/InventoryProductResponseModel");

class StockResponseModel extends APISignature{
    constructor(values,req) {
        super(values);
        this.id=values?._id;
        this.product = new InventoryProductResponseModel(values?.productId,req);
        this.quantity=values?.quantity;
        this.quantitySold=values?.quantitySold;
        this.category=values?.category ? new CategoryResponseModel(values?.category,req) : "";
        this.purchase =values?.purchase || 0;
    }
}

module.exports = {
    StockResponseModel
}