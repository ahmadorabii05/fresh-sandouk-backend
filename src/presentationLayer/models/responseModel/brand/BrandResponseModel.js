const {APISignature} = require("../common/APISignature");
const {extractLanguageFromRequest} = require("../../../../dataHandlerLayer/common/LanguageHeaderHandler");

class BrandResponseModel extends APISignature {
    id = undefined;
    source = undefined;
    sourceAr=undefined;
    displayName=undefined;
    imageUrl=undefined;
    constructor(values,req) {
        super(values);
        this.id = values?._id;
        this.source = values?.source;
        this.sourceAr = values?.sourceAr;
        this.source =extractLanguageFromRequest(req) === "ar" ? values?.sourceAr || values?.source : values?.source;
        this.imageUrl = values?.imageUrl;
    }
}

module.exports = {
    BrandResponseModel
}
