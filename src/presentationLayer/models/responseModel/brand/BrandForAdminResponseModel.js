const {APISignature} = require("../common/APISignature");

class BrandForAdminResponseModel extends APISignature {
    id = undefined;
    source = undefined;
    sourceAr = undefined;
    imageUrl=undefined;
    constructor(values) {
        super(values);
        this.id = values?._id;
        this.source =values?.source;
        this.sourceAr = values?.sourceAr;
        this.imageUrl=values?.imageUrl;
    }
}

module.exports = {
    BrandForAdminResponseModel
}
