const {ProductInvoiceResponseModel} = require("../products/ProductInvoiceResponseModel");


class InvoiceResponseModel {
    invoiceNumber = undefined;
    customerName = undefined;
    dueDate = undefined;
    dueTime = undefined;
    products = undefined;
    totalAmount = undefined;
    status = undefined;

    constructor(values, req) {
        this.id = values?._id;
        this.invoiceNumber=values?.keyOrder;
        this.customerName = values?.userId?.info?.general?.name;
        this.dueDate = values?.scheduledDate;
        this.dueTime = values?.scheduledTime;
        this.totalAmount = values?.totalAmount;
        this.products = values?.products.length > 0 ? values?.products.map(item => {
           return new ProductInvoiceResponseModel(item, req)
        }) : []
        this.status = values?.status;
    }
}

module.exports = {
    InvoiceResponseModel
}