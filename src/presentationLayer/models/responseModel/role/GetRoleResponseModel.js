const { RoleResponseModel } = require("./RoleResponseModel");

class GetRoleResponseModel extends RoleResponseModel {
    id = undefined;

    constructor(values) {
        super(values);
        this.id = values?._id || -1;
    }
}

module.exports = {
    GetRoleResponseModel,
};
