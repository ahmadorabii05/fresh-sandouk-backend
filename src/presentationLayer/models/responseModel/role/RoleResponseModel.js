const { APISignature } = require("../common/APISignature");
const {
    PermissionsResponseModel,
} = require("../permissions/PermissionsResponseModel");

class RoleResponseModel extends APISignature {
    name = undefined;
    permissions = undefined;

    constructor(values) {
        super(values);
        this.name = values?.name || "";
        this.permissions = new PermissionsResponseModel(values?.permissions);
    }
}

module.exports = {
    RoleResponseModel,
};
