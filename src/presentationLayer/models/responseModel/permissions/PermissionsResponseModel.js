class PermissionsResponseModel {
    permissions = undefined;

    constructor(values) {
        this.permissions = values?.permissions || [];
    }
}

module.exports = {
    PermissionsResponseModel,
};
