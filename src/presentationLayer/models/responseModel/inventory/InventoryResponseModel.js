const {APISignature} = require("../common/APISignature");
const {InventoryProductResponseModel} = require("../products/InventoryProductResponseModel");
const {GetCategoryInventory} = require("../category/GetCategoryInventory");

class InventoryResponseModel extends APISignature{
    constructor(values,req) {
        super(values);
        this.id=values?._id;
        this.product = new InventoryProductResponseModel(values?.productId,req);
        this.quantity=values?.quantity;
        // this.quantitySold=values?.quantitySold;
        this.category=values?.category ? new GetCategoryInventory(values?.category,req) : "";
    }
}

module.exports = {
    InventoryResponseModel
}