class PhoneNumberResponseModel {
    phoneNumber = undefined;
    verificationCode = undefined;
    userId = undefined;

    constructor(values) {
        this.phoneNumber = values?.phoneNumber || "";
        this.verificationCode = values?.verificationCode || "";
        this.userId = values?.userId || "";
    }
}

module.exports = {
    PhoneNumberResponseModel,
};