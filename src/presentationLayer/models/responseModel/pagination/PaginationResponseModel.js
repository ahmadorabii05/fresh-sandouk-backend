class PaginationResponseModel {
    docs = undefined;
    totalDocs = undefined;
    limit = undefined;
    totalPages = undefined;
    page = undefined;
    pagingCounter = undefined;
    hasPrevPage = undefined;
    hasNextPage = undefined;
    prevPage = undefined;
    nextPage = undefined;

    constructor(values) {
        this.docs = values.docs || [];
        this.totalDocs = values.totalDocs || 0;
        this.limit = values?.limit || 0;
        this.totalPages = values?.totalPages || 0;
        this.page = values?.page || 0;
        this.pagingCounter = values?.pagingCounter || 0;
        this.hasPrevPage =
            typeof values?.hasPrevPage !== "undefined" ? values.hasPrevPage : "";
        this.hasNextPage =
            typeof values?.hasNextPage !== "undefined" ? values?.hasNextPage : "";
        this.prevPage =
            typeof values?.prevPage !== "undefined" ? values?.prevPage : "";
        this.nextPage =
            typeof values?.nextPage !== "undefined" ? values?.nextPage : "";
    }
}

module.exports = {
    PaginationResponseModel,
};
