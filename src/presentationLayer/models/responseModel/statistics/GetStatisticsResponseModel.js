class GetStatisticsResponseModel {
    totalSales = undefined;
    totalOrders = undefined;
    totalUsers=undefined;
    constructor(values) {
        this.totalSales = values?.totalSales || 0;
        this.totalOrders = values?.totalOrders || 0;
        this.totalUsers=values?.totalUsers || 0;
    }
}

module.exports = {
    GetStatisticsResponseModel
}