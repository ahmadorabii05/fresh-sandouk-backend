const { APISignature } = require("../common/APISignature");
const { extractLanguageFromRequest } = require("../../../../dataHandlerLayer/common/LanguageHeaderHandler");

class PriceResponseModel extends APISignature {
    id = undefined;
    unit = undefined;
    unitAr = undefined;
    displayName = undefined;
    constructor(values, req) {
        super(values);
        this.id = values?._id;
        this.unit = values?.unit;
        this.unitAr = values?.unitAr;
        this.displayName = extractLanguageFromRequest(req) === "en" ? values?.unit : values?.unitAr || values?.unit;
    }
}

module.exports = {
    PriceResponseModel
}
