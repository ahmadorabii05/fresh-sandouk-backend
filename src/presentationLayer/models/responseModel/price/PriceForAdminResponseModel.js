const {APISignature} = require("../common/APISignature");

class PriceForAdminResponseModel extends APISignature {
    id = undefined;
    unit = undefined;
    unitAr = undefined;
    constructor(values) {
        super(values);
        this.id = values?._id;
        this.unit = values?.unit || "";
        this.unitAr = values?.unitAr || "";
    }
}

module.exports = {
    PriceForAdminResponseModel
}
