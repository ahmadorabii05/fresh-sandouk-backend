const {ProductResponseModel} =require("../products/ProductResponseModel");

class CartResponseModel {
    products = undefined;
    totalPrice = undefined;
    totalItems = undefined;
    quantity = undefined;
    totalOriginalPrice=undefined;
    constructor(values,req) {
        this.products = values?.productsInCart ? values?.productsInCart.length > 0 ? values?.productsInCart.map((item => {
            return new ProductResponseModel(item,req)
        })) : [] : [];
        this.totalPrice = values?.totalPrice;
        this.totalItems = values?.totalItems;
        this.quantity = values?.quantity;
        this.totalOriginalPrice=values?.totalOriginalPrice;
    }

}

module.exports = {
    CartResponseModel
}