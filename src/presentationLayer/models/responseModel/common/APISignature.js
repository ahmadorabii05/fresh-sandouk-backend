class APISignature {
    createdAt = undefined;
    createdBy = undefined;
    updatedAt = undefined;
    updatedBy = undefined;
    active = undefined;

    constructor(values) {
        this.createdAt = values?.createdAt;
        this.createdBy = values?.createdBy;
        this.updatedAt = values?.updatedAt;
        this.updatedBy = values?.updatedBy;
        this.active = values?.active;
    }
}

module.exports = {
    APISignature,
};
