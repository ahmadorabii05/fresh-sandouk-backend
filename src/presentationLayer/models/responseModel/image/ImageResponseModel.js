const {APISignature} = require("../common/APISignature");


class ImageResponseModel extends APISignature {
    id = undefined;
    imageUrl = undefined;
    constructor(props) {
        super(props);
        this.id =props?._id || props?.id;
        this.imageUrl = props?.imageUrl;
    }

}

module.exports = {
    ImageResponseModel
}