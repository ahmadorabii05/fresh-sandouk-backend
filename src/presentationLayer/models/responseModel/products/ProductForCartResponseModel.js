const {APISignature} = require("../common/APISignature");
const {TagsResponseModel} = require("../tags/TagsResponseModel");

class ProductForCartResponseModel extends APISignature {

    quantity = undefined;
    totalPrice = undefined;
    totalOriginPrice = undefined;
    totalItems = undefined;

    constructor(values) {
        super(values);
        this.products = values?.cartProductIds ? values?.cartProductIds.length > 0 ? values?.cartProductIds.map((item => {
            return new ProductInCart(item)
        })) : [] : [];
        this.totalPrice = values?.totalPrice;
        this.totalOriginPrice = values?.totalOriginPrice;
        this.totalItems = values?.totalCount || 0;
    }
}

class ProductInCart {
    id = undefined;
    name = undefined;
    imageUrl = undefined;
    country = undefined;
    price = undefined;
    pricingModel = undefined;
    description = undefined;
    discountValue = undefined;
    isDiscount = undefined;
    tags = undefined;
    sku=undefined;
    isFavorite = undefined;
    finalPriceAfterDiscount=undefined;
    constructor(values) {
        this.id = values?._id;
        this.name = values?.name || "";
        this.imageUrl = values?.imageUrl;
        this.pricingModel = values?.pricingModel;
        this.price = values?.price;
        this.sku=values?.sku;
        this.description = values?.description;
        this.isDiscount = values?.isDiscount;
        this.discountValue = values?.discountValue;
        this.generalDescription = values?.generalDescription || "";
        this.country = values?.country;
        this.isFavorite = values?.isFavorite;
        this.tags = values?.tags?.length > 0 ? values?.tags.map(tag => {
            return new TagsResponseModel(tag)
        }) : []
        this.quantity = values?.quantity
        this.finalPriceAfterDiscount=values?.finalPriceAfterDiscount;
        this.isActive = values.isActive;
        this.stockQuantity =values?.stockQuantity;
        this.isDeleted = values.isDeleted;

    }
}

module.exports = {
    ProductForCartResponseModel,
};
