const {APISignature} = require("../common/APISignature");
const {NutritionRequestModel} = require("../../../../dataHandlerLayer/models/requestModel/nutrition/NutritionRequestModel");
const {TagsResponseModel} = require("../tags/TagsResponseModel");
const {ImageResponseModel} = require("../image/ImageResponseModel");
const {PriceForAdminResponseModel} = require("../price/PriceForAdminResponseModel");
const {CountryForAdminResponseModel} = require("../../../../dataHandlerLayer/models/requestModel/country/CountryForAdminResponseModel");
const {BrandForAdminResponseModel} = require("../brand/BrandForAdminResponseModel");
const {SubCategoryForAProductResponseModel} = require("../subCategory/SubCategoryForAProductResponseModel");


class ProductsForAdminResponseModel extends APISignature {
    id = undefined;
    name = undefined;
    nameAr = undefined;
    imageUrls = undefined;
    country = undefined;
    price = undefined;
    priceModel = undefined;
    description = undefined;
    descriptionAr = undefined;
    discountValue = undefined;
    isDiscount = undefined;
    tags = undefined;
    generalDescription = undefined;
    generalDescriptionAr = undefined;

    ingredient = undefined;
    ingredientAr = undefined;

    storage = undefined;
    storageAr = undefined

    nutrition = undefined;
    isFavorite = undefined;
    isFeatured = undefined;
    quantityInCart = undefined;
    finalPriceAfterDiscount = undefined;
    Sku = undefined;
    SortOrder = undefined;
    subCategories = undefined;
    stockQuantity = undefined;
    isDeleted = undefined;

    constructor(values) {
        super(values)
        this.id = values?._id ? values?._id : values?.id;
        this.barcode = values?.barcode || "";
        this.name = values?.name || "";
        this.nameAr = values?.nameAr || "";
        this.imageUrls = values?.imageUrls?.map(item => {
            return new ImageResponseModel(item)
        });
        this.subCategories = values?.subCategories?.length > 0 ? new SubCategoryForAProductResponseModel(values.subCategories[0]) : new SubCategoryForAProductResponseModel(values.subCategories);
        this.Sku = values?.sku;
        this.SortOrder = values?.sort_order;
        this.priceModel = values?.priceModel ? new PriceForAdminResponseModel(values.priceModel) : "";
        this.price = values?.price;
        this.description = values?.description;
        this.descriptionAr = values?.descriptionAr || "";
        this.isDiscount = values?.isDiscount;
        this.discountValue = values?.discountValue;
        this.generalDescription = values?.generalDescription;
        this.generalDescriptionAr = values?.generalDescriptionAr || "";
        this.ingredient = values?.ingredient;
        this.ingredientAr = values?.ingredientAr || "";
        this.storage = values?.storage || "";
        this.storageAr = values?.storageAr || "";
        this.country = new CountryForAdminResponseModel(values?.country);
        this.isFavorite = values?.isFavorite;
        this.tags = values?.tags?.length > 0 ? values?.tags.map(tag => {
            return new TagsResponseModel(tag)
        }) : []
        this.nutrition = values?.nutrition?.length > 0 ? values?.nutrition?.map(item => {
            return new NutritionRequestModel(item)
        }) : [];
        if (values?.quantityInCart > 0) {
            this.quantityInCart = values?.quantityInCart;
        }
        this.brand = new BrandForAdminResponseModel(values?.brandId || values?.brand);
        this.finalPriceAfterDiscount = values?.finalPriceAfterDiscount;
        this.isActive = values.isActive;
        this.isFeatured = values.isFeatured;
        this.stockQuantity = values?.stockQuantity;
        this.isDeleted = values.isDeleted;
    }
}

module.exports = {
    ProductsForAdminResponseModel,
};
