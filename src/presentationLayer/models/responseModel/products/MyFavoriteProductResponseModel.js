const {TagsResponseModel} = require("../tags/TagsResponseModel");
const {NutritionRequestModel} = require("../../../../dataHandlerLayer/models/requestModel/nutrition/NutritionRequestModel");
const {extractLanguageFromRequest} = require("../../../../dataHandlerLayer/common/LanguageHeaderHandler");
const {PriceResponseModel} = require("../price/PriceResponseModel");
const {CountryResponseModel} = require("../../../../dataHandlerLayer/models/requestModel/country/CountryResponseModel");
const {BrandResponseModel} = require("../brand/BrandResponseModel");


class MyFavoriteProductResponseModel{
    id = undefined;
    name = undefined;
    imageUrl = undefined;
    country = undefined;
    price = undefined;
    pricingModel = undefined;
    description = undefined;
    discountValue = undefined;
    isDiscount = undefined;
    tags = undefined;
    generalDescription = undefined;
    ingredient = undefined;
    storage = undefined
    nutrition = undefined;
    isFavorite = undefined;
    inCart=undefined;
    sku=undefined;
    quantityInCart=undefined;finalPriceAfterDiscount=undefined;

    constructor(values,req) {
        this.id = values?._id ? values?._id : values?.id;
        this.name = extractLanguageFromRequest(req) === "ar" ? (values?.nameAr ? values?.nameAr : values?.name) : values?.name ;
        this.imageUrls = values?.imageUrls;
        this.priceModel = values?.priceModel ? new PriceResponseModel(values.priceModel[0],req) : "";
        this.price = values?.price;
        this.description = extractLanguageFromRequest(req) === "ar" ? values?.descriptionAr || values?.description : values?.description ;
        this.isDiscount = values?.isDiscount;
        this.discountValue = values?.discountValue;
        this.sku=values?.sku;
        this.generalDescription = extractLanguageFromRequest(req) === "ar" ? values?.generalDescriptionAr || values?.generalDescription : values?.generalDescription ;
        this.ingredient = extractLanguageFromRequest(req) === "ar" ? values?.ingredientAr || values?.ingredient : values?.ingredient ;
        this.storage = extractLanguageFromRequest(req) === "ar" ? values?.storageAr || values?.storage : values?.storage ;
        this.country =  new CountryResponseModel(values?.country[0],req) ;
        this.isFavorite = values?.isFavorite;
        this.tags = values?.tags?.length > 0 ? values?.tags.map(tag=>{
            return new TagsResponseModel(tag)
        }) : []
        this.nutrition = values?.nutrition?.length > 0 ? values?.nutrition?.map(item=>{
            return new NutritionRequestModel(item)
        }): [];
        this.brand= new BrandResponseModel(values?.brand,req)
        this.inCart=values.inCart;
        this.quantityInCart=values?.quantityInCart;
        this.finalPriceAfterDiscount=values?.finalPriceAfterDiscount;
        this.isActive = values.isActive;
        this.stockQuantity =values?.stockQuantity;
        this.isDeleted = values.isDeleted;

    }
}

module.exports = {
    MyFavoriteProductResponseModel
}