const {ImageResponseModel} = require("../image/ImageResponseModel");
const {extractLanguageFromRequest} = require("../../../../dataHandlerLayer/common/LanguageHeaderHandler");


class ProductInvoiceResponseModel {
    id = undefined;
    productName = undefined;
    quantityBought = undefined;
    productPrice = undefined;
    sku=undefined;
    constructor(values,req) {
        this.id = values?._id;
        this.productName = extractLanguageFromRequest(req) === "ar" ? (values?.productId.nameAr || values?.productId.name || "") : values?.productId.name;
        this.imageUrls =values?.productId.imageUrls.length > 0 ? values?.productId.imageUrls.map(item=>{
            return  new ImageResponseModel(item);
        }) : []
        this.quantityBought = values?.quantity;
        this.productPrice = values?.totalPrice;
        this.sku=values?.sku;
        this.isActive = values?.isActive;
        this.stockQuantity =values?.stockQuantity;

    }
}

module.exports ={
    ProductInvoiceResponseModel
}