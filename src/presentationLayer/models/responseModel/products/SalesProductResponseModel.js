const {APISignature} = require("../common/APISignature");
const {extractLanguageFromRequest} = require("../../../../dataHandlerLayer/common/LanguageHeaderHandler");
const {ImageResponseModel} = require("../image/ImageResponseModel");
const {PriceResponseModel} = require("../price/PriceResponseModel");
const {BrandResponseModel} = require("../brand/BrandResponseModel");

class SalesProductResponseModel extends APISignature{
    constructor(values,req) {
        super(values);
        this.id = values?._id ? values?._id : values?.id;
        this.name = extractLanguageFromRequest(req) === "ar" ? (values?.nameAr || values?.name || "") : values?.name;
        this.imageUrls = values?.imageUrls?.map(item => {
            return new ImageResponseModel(item)
        });
        this.priceModel = values?.priceModel ? new PriceResponseModel(values.priceModel, req) : "";
        this.price = values?.price;
        this.sku=values?.sku;
        this.brand = new BrandResponseModel(values?.brandId || values?.brand,req);
        this.isActive = values.isActive;
        this.stockQuantity =values?.stockQuantity;
        this.isDeleted = values.isDeleted;
    }
}

module.exports = {
    SalesProductResponseModel
}