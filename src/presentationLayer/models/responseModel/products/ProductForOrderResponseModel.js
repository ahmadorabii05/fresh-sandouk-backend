const {APISignature} = require("../common/APISignature");
const {NutritionRequestModel} = require("../../../../dataHandlerLayer/models/requestModel/nutrition/NutritionRequestModel");
const {TagsResponseModel} = require("../tags/TagsResponseModel");
const {extractLanguageFromRequest} = require("../../../../dataHandlerLayer/common/LanguageHeaderHandler");
const {PriceResponseModel} = require("../price/PriceResponseModel");
const {ImageResponseModel} = require("../image/ImageResponseModel");
const {CountryInProductResponseModel} = require("../../../../dataHandlerLayer/models/requestModel/country/CountryInProductResponseModel");
const {BrandResponseModel} = require("../brand/BrandResponseModel");


class ProductForOrderResponseModel extends APISignature {
    id = undefined;
    name = undefined;
    imageUrls = undefined;
    country = undefined;
    price = undefined;
    priceModel = undefined;
    description = undefined;
    discountValue = undefined;
    isDiscount = undefined;
    tags = undefined;
    generalDescription = undefined;
    ingredient = undefined;
    storage = undefined
    nutrition = undefined;
    isFavorite = undefined;
    quantityInCart = undefined;
    finalPriceAfterDiscount = undefined;
    sku=undefined;
    isActive=undefined;
    constructor(values, req) {
        super(values);
        this.id = values?.productId._id ? values?.productId._id : values?.id;
        this.name = extractLanguageFromRequest(req) === "ar" ? (values?.productId.nameAr || values?.productId.name || "") : values?.productId.name;
        this.imageUrls = values?.productId.imageUrls?.map(item => {
            return new ImageResponseModel(item)
        });
        this.sku=values?.productId.sku;
        this.priceModel = values?.productId.priceModel ? new PriceResponseModel(values.productId.priceModel, req) : "";
        this.price = values?.productId.price;
        this.description = extractLanguageFromRequest(req) === "ar" ? values?.productId.descriptionAr || values?.productId.description || "" : values?.productId.description || "";
        this.isDiscount = values?.productId.isDiscount;
        this.discountValue = values?.productId.discountValue;
        this.generalDescription = extractLanguageFromRequest(req) === "ar" ? values?.productId.generalDescriptionAr || values.productId.generalDescription || "" : values?.productId.generalDescription || "";
        this.ingredient = extractLanguageFromRequest(req) === "ar" ? values?.productId.ingredientAr || values?.productId.ingredient || "" : values?.productId.ingredient || "";
        this.storage = extractLanguageFromRequest(req) === "ar" ? values?.productId.storageAr || values?.productId.storage || "" : values?.productId.storage || "";
        this.country = new CountryInProductResponseModel(values?.productId.country, req);
        this.isFavorite = values?.productId.isFavorite;
        this.tags = values?.productId.tags?.length > 0 ? values?.productId.tags.map(tag => {
            return new TagsResponseModel(tag)
        }) : []
        this.nutrition = values?.nutrition?.productId.length > 0 ? values?.productId.nutrition?.map(item => {
            return new NutritionRequestModel(item)
        }) : [];
        if (values?.productId.quantityInCart > 0) {
            this.quantityInCart = values?.productId.quantityInCart;
        }
        this.brand = new BrandResponseModel(values?.productId.brandId || values?.productId.brand,req);
        this.finalPriceAfterDiscount = values?.productId.finalPriceAfterDiscount;
        this.isActive = values?.productId.isActive;
        this.stockQuantity =values?.productId.stockQuantity;
        this.quantity = values?.quantity;
        this.isDeleted = values.productId.isDeleted;

    }
}

module.exports = {
    ProductForOrderResponseModel,
};
