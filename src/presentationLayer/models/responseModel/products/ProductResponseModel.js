const {APISignature} = require("../common/APISignature");
const {NutritionRequestModel} = require("../../../../dataHandlerLayer/models/requestModel/nutrition/NutritionRequestModel");
const {TagsResponseModel} = require("../tags/TagsResponseModel");
const {extractLanguageFromRequest} = require("../../../../dataHandlerLayer/common/LanguageHeaderHandler");
const {PriceResponseModel} = require("../price/PriceResponseModel");
const {ImageResponseModel} = require("../image/ImageResponseModel");
const {CountryInProductResponseModel} = require("../../../../dataHandlerLayer/models/requestModel/country/CountryInProductResponseModel");
const {BrandResponseModel} = require("../brand/BrandResponseModel");


class ProductResponseModel extends APISignature {
    id = undefined;
    name = undefined;
    barcode = undefined;
    isFeatured = undefined;
    sort_order = undefined;
    imageUrls = undefined;
    country = undefined;
    price = undefined;
    priceModel = undefined;
    description = undefined;
    discountValue = undefined;
    isDiscount = undefined;
    tags = undefined;
    generalDescription = undefined;
    ingredient = undefined;
    storage = undefined
    nutrition = undefined;
    isFavorite = undefined;
    quantityInCart = undefined;
    finalPriceAfterDiscount = undefined;
    sku = undefined;
    isActive = undefined;

    constructor(values, req) {
        super(values);
        this.id = values?._id ? values?._id : values?.id;
        this.name = extractLanguageFromRequest(req) === "ar" ? (values?.nameAr || values?.name || "") : values?.name;
        this.imageUrls = values?.imageUrls?.map(item => {
            return new ImageResponseModel(item)
        });
        this.sku = values?.sku;
        this.barcode = values?.barcode;
        this.isFeatrued = values?.isFeatrued;
        this.sort_order = values.sort_order ?? 1;
        this.priceModel = values?.priceModel ? new PriceResponseModel(values.priceModel, req) : "";
        this.price = values?.price;
        this.description = extractLanguageFromRequest(req) === "ar" ? values?.descriptionAr || values?.description || "" : values?.description || "";
        this.isDiscount = values?.isDiscount;
        this.discountValue = values?.discountValue;
        this.generalDescription = extractLanguageFromRequest(req) === "ar" ? values?.generalDescriptionAr || values.generalDescription || "" : values?.generalDescription || "";
        this.ingredient = extractLanguageFromRequest(req) === "ar" ? values?.ingredientAr || values?.ingredient || "" : values?.ingredient || "";
        this.storage = extractLanguageFromRequest(req) === "ar" ? values?.storageAr || values?.storage || "" : values?.storage || "";
        this.country = new CountryInProductResponseModel(values?.country, req);
        this.isFavorite = values?.isFavorite;
        this.tags = values?.tags?.length > 0 ? values?.tags.map(tag => {
            return new TagsResponseModel(tag)
        }) : []
        this.nutrition = values?.nutrition?.length > 0 ? values?.nutrition?.map(item => {
            return new NutritionRequestModel(item)
        }) : [];
        if (values?.quantityInCart > 0) {
            this.quantityInCart = values?.quantityInCart;
        }
        this.brand = new BrandResponseModel(values?.brandId || values?.brand, req);
        this.finalPriceAfterDiscount = values?.finalPriceAfterDiscount;
        this.isActive = values?.isActive;
        this.isFeatured = values?.isFeatured;
        this.stockQuantity = values?.stockQuantity;
        this.quantity = values?.quantity
    }
}

module.exports = {
    ProductResponseModel,
};
