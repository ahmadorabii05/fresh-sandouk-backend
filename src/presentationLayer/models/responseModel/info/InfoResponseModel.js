const { GeneralResponseModel } = require("../general/GeneralResponseModel");

class InfoResponseModel {
    general = undefined;
    constructor(values) {
        this.general = new GeneralResponseModel(values?.general);
    }
}

module.exports = {
    InfoResponseModel,
};
