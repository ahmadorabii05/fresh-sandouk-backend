class PaymentSheetResponseModel {
    paymentIntent = undefined;
    ephemeralKey = undefined;
    customer = undefined;
    publishableKey = undefined;

    constructor(values) {
        this.customer = values?.customer;
        this.paymentIntent = values?.paymentIntent;
        this.ephemeralKey = values?.ephemeralKey;
        this.publishableKey = values?.publishableKey;
    }
}

module.exports = {
    PaymentSheetResponseModel
}