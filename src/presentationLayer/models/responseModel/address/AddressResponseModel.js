const {APISignature} = require("../common/APISignature");

class AddressResponseModel extends APISignature{
    id=undefined;
    addressTitle = undefined;
    address = undefined;
    street = undefined;
    nearestLand = undefined;
    contactNumber = undefined;
    isDefaultAddress=undefined;
    longtitude = undefined;
    latitude =undefined;
    constructor(values) {
        super(values);
        this.id=values?._id;
        this.addressTitle = values?.addressTitle
        this.address = values?.address
        this.street = values?.street
        this.nearestLand = values?.nearestLand
        this.contactNumber = values?.contactNumber
        this.isDefaultAddress=values?.isDefaultAddress
        this.longtitude = values?.longtitude || 0;
        this.latitude = values?.latitude || 0;
    }
}

module.exports={
    AddressResponseModel
}