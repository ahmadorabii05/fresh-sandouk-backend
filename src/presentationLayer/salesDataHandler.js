const StockQueries = require("../dataBaseLayer/queries/stock/stockQueries");
const SubCategoryModel = require("../dataBaseLayer/models/subCategory/SubCategoryModel");
const CategoryModel = require("../dataBaseLayer/models/category/CategoryModel");
const {findCategoryId} = require("../helperKit/helperKit");
const {prepareErrorLog} = require("../helperKit/loggingKit");
const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("../dataHandlerLayer/common/LanguageHeaderHandler");


const getAllSalesDataHandler = async (req, res, next) => {
    try {
        const query = await StockQueries.getAllProductInStock(req);
        // Get the product IDs from the query result
        const productIds = query.docs.map(doc => doc.productId);
        // Fetch the subcategories that have at least one product with an ID in the productIds array
        const subcategories = await SubCategoryModel.find({
            productIds: { $in: productIds }
        }).populate({
            path: "productIds",
            match: { _id: { $in: productIds } }
        }).lean();

        // Fetch the categories that have subcategories matching the fetched subcategories
        const categories = await CategoryModel.find({
            subCategories: { $in: subcategories.map(subcat => subcat._id) }
        }).populate({
            path: "subCategories",
            populate: {
                path: "productIds"
            }
        }).lean();

        // Map the categories and subcategories to the products in the query result
        const productsWithCategories = query.docs.map(doc => {
            const product = doc;
            const categoryId = findCategoryId(categories, product);

            // Find the category and subcategory that match the IDs
            // Add the category and subcategory information to the product
            const purchase = product.productId.price * product.quantitySold;
            product.category = categories.find(cat => cat._id.equals(categoryId));
            product.purchase = purchase;
            return product;
        });
        req.responseModel = {
            docs:productsWithCategories,
            totalDocs : query.totalDocs,
            limit : query.limit,
            totalPages : query.totalPages,
            page : query.page,
            pagingCounter : query.pagingCounter,
            hasPrevPage : query.hasPrevPage,
            hasNextPage : query.hasNextPage,
            prevPage : query.prevPage,
            nextPage : query.nextPage,
        } ;
        next();
    } catch (error) {
        prepareErrorLog(error, getAllSalesDataHandler.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

module.exports = {
    getAllSalesDataHandler
}
