const BannerErrorStatus = {
    BANNER_ALREADY_EXIST: { key: "BANNER_ALREADY_EXIST", code: 404 },
    BANNER_DOES_NOT_EXIST: { key: "BANNER_DOES_NOT_EXIST", code: 404 },
    BANNER_ALREADY_EXIST_IN_ARABIC: { key: "BANNER_ALREADY_EXIST_IN_ARABIC", code: 404 }
}

module.exports = {
    BannerErrorStatus
}