const BrandErrorStatus={
    BRAND_ALREADY_EXIST:{key:"BRAND_ALREADY_EXIST",code:404},
    BRAND_DOES_NOT_EXIST:{key:"BRAND_DOES_NOT_EXIST",code:404},
    BRAND_ALREADY_EXIST_IN_ARABIC:{key:"BRAND_ALREADY_EXIST_IN_ARABIC",code:404}
}

module.exports = {
    BrandErrorStatus
}