const ListErrorStatus = {
    LIST_ALREADY_EXIST: {key: "LIST_ALREADY_EXIST", code: 404},
    LIST_DOES_NOT_EXIST:{key: "LIST_DOES_NOT_EXIST", code: 404}
}

module.exports = {
    ListErrorStatus
}