const TagsErrorStatus ={
    TAGS_NAME_ALREADY_EXIST:{key:"TAGS_NAME_ALREADY_EXIST",code:404},
    TAG_DOESNT_EXIST:{key:"TAG_DOESNT_EXIST",code:404},
    TAG_IN_ARABIC_ALREADY_EXIST:{key:"TAG_IN_ARABIC_ALREADY_EXIST",code:404}
}

module.exports = {
    TagsErrorStatus
}