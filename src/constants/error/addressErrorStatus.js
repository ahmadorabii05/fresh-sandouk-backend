const AddressErrorStatus={
    ADDRESS_ALREADY_EXIST:{key:"ADDRESS_ALREADY_EXIST",code:404},
    ADDRESS_DOES_NOT_EXIST:{key:"ADDRESS_DOES_NOT_EXIST",code:404},
    YOU_SHOULD_CREATE_ADDRESS_FIRST:{key:"YOU_SHOULD_CREATE_ADDRESS_FIRST",code:404}
}

module.exports = {
    AddressErrorStatus
}