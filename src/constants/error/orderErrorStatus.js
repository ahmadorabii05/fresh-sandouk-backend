const OrderErrorStatus = {
    YOU_DONT_HAVE_ENOUGH_BALANCE: {key: "YOU_DONT_HAVE_ENOUGH_BALANCE", code: 404},
    ORDER_DOES_NOT_EXIST:{key:"ORDER_DOES_NOT_EXIST",code:404}
}

module.exports = {
    OrderErrorStatus
}