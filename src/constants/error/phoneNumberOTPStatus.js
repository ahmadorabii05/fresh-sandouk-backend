const PhoneNumberOTPStatus = {
    OTP_TOKEN: { key: 'OTP_TOKEN', code: 400 },
};

module.exports = {
    PhoneNumberOTPStatus,
};