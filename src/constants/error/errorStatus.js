const {UserErrorStatus} = require("./userErrorStatus");
const {GeneralErrorStatus} = require("./generalErrorMessage");
const {RoleErrorStatus} = require("./roleErrorStatus");
const {PhoneNumberOTPStatus} = require("./phoneNumberOTPStatus");
const {CategoryErrorStatus} = require("./categoryErrorStatus");
const {SubCategoryErrorStatus} = require("./subCategoryErrorStatus");
const {ProductErrorStatus} = require("./productErrorStatus");
const {TagsErrorStatus} = require("./tagsErrorStatus");
const {CartErrorStatus} = require("./cartErrorStatus");
const {ListErrorStatus} = require("./listErrorStatus");
const {PriceErrorStatus} = require("./priceErrorStatus");
const {BrandErrorStatus} = require("./brandErrorStatus");
const {BannerErrorStatus} = require("./bannerErrorStatus");
const {AddressErrorStatus} = require("./addressErrorStatus");
const {StockErrorStatus} = require("./stockErrorStatus");
const {OrderErrorStatus} = require("./orderErrorStatus");
const {CountryErrorStatus} = require("./countryErrorStatus");
const {StripeErrorStatus} = require("./stripeErrorStatus");
const {UploadZipFileErrorStatus} = require("./uploadZipFileErrorStatus");


const ErrorStatus = {
    ...GeneralErrorStatus,
    ...UserErrorStatus,
    ...RoleErrorStatus,
    ...PhoneNumberOTPStatus,
    ...CategoryErrorStatus,
    ...SubCategoryErrorStatus,
    ...ProductErrorStatus,
    ...TagsErrorStatus,
    ...CartErrorStatus,
    ...ListErrorStatus,
    ...PriceErrorStatus,
    ...BrandErrorStatus,
    ...BannerErrorStatus,
    ...AddressErrorStatus,
    ...StockErrorStatus,
    ...OrderErrorStatus,
    ...CountryErrorStatus,
    ...StripeErrorStatus,
    ...UploadZipFileErrorStatus
}


module.exports = {
    ErrorStatus,
};
