const UserErrorStatus = {
    DOES_NOT_EXIST: { key: 'DOES_NOT_EXIST', code: 404 },
    WRONG_CURRENT_PASSWORD: { key: 'WRONG_CURRENT_PASSWORD', code: 400 },
    USER_EXIST: { key: 'USER_EXIST', code: 400 },
    NAME_EXIST:{key:'NAME_EXIST',code:400},
    USER_EMAIL_EXIST: { key: 'USER_EMAIL_EXIST', code: 400 },
    WRONG_PASSWORD: { key: 'WRONG_PASSWORD', code: 401 },
    REQUEST_EXPIRED: { key: 'REQUEST_EXPIRED', code: 400 },
    PASSWORD_MUST_BE_MORE_THAN_8_CHARACTERS : { key :"PASSWORD_MUST_BE_8_CHAR" ,code:400},
    NAME_MUST_BE_AT_LEAST_6_CHARACTER:{key :"NAME_MUST_BE_AT_LEAST_6_CHARACTER",code:400},
    USER_NOT_VERIFIED:{key:"USER_NOT_VERIFIED",code:404},
    NON_ACTIVE_USER:{key:"NON_ACTIVE_USER",code:404}
};

module.exports = {
    UserErrorStatus,
};
