
const UploadZipFileErrorStatus = {
    UPLOAD_ZIP_FILE:{key:"UPLOAD_ZIP_FILE",code:404},
    NO_ZIP_FILE:{key:"NO_ZIP_FILE",code:404}
}

module.exports = {
    UploadZipFileErrorStatus
}