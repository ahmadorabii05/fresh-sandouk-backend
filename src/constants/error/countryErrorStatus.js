
const CountryErrorStatus={
    COUNTRY_NAME_DOES_NOT_EXIST:{key:"COUNTRY_NAME_DOES_NOT_EXIST",code:404}
}

module.exports = {
    CountryErrorStatus
}