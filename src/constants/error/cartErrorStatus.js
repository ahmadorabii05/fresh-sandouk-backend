const CartErrorStatus={
    PRODUCT_DOESNT_EXIST_IN_CART:{key:"PRODUCT_DOESNT_EXIST_IN_CART",code:404},
    ADD_PRODUCT_TO_CART_FIRST:{key:"ADD_PRODUCT_TO_CART_FIRST",code:404},
    CART_NOT_FOUND:{key:"CART_NOT_FOUND",code:404},
    CART_IS_EMPTY:{key:"CART_IS_EMPTY",code:404}
}

module.exports = {
    CartErrorStatus
}