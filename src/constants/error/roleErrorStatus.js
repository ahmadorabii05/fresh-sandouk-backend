const RoleErrorStatus = {
    ROLE_EXIST: { key: 'ROLE_EXIST', code: 400 },
    ROLE_DOESNT_EXIST: { key: 'ROLE_DOESNT_EXIST', code: 404 },
    YOUR_ARE_NOT_AN_ADMIN:{key:"YOUR_ARE_NOT_AN_ADMIN",code:404}
};

module.exports = {
    RoleErrorStatus,
};
