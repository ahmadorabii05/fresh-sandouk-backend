const GeneralErrorStatus = {
    BAD_REQUEST: { key: 'BAD_REQUEST', code: 400 },
    NOT_AUTHORIZED: { key: 'NOT_AUTHORIZED', code: 401 },
    NOT_FOUND: { key: 'NOT_FOUND', code: 404 },
    SERVER_DOWN: { key: 'SERVER_DOWN', code: 500 },
    OUT_DATED: { key: 'OUT_DATED', code: 600 },
    FAILED_TO_UPLOAD: { key: 'FAILED_TO_UPLOAD', code: 400 },
    FILE_MISSING: { key: 'FILE_MISSING', code: 400 },
    FILE_TEXT_FAILED:{key:'Must Have even Text or Upload File',code:400},
    FILE_HAS_SPECIAL_CHARACTERS:{key:"FILE_HAS_SPECIAL_CHARACTERS",code:404},
    INVALID_PERMISSION: { key:"Invalid Permission",code:400},
    IS_REQUIRED: {key:"is required",code:400},
    INVALID_GENDER: {key :"Invalid Gender" ,code:400},
    INVALID_COUNTRY_CODE:{key:"INVALID_COUNTRY_CODE",code:400},
    INVALID_EMAIL:{key:"INVALID_EMAIL",code:404},
    INVALID_TYPE:{key:"INVALID TYPE",code:404},
    MISSING_DATA:{key:"MISSING_DATA",code:404},
    ONLY_IMAGES:{key:"ONLY_IMAGES",code:404},
};

module.exports = {
    GeneralErrorStatus,
};
