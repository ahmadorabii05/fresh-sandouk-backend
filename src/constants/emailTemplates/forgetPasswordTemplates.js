const { generalTemplate } = require("./common/generalTemplate");

require("dotenv");

const forgotPasswordTemplate = (fullName, resetToken) => {
    const token = `${process.env.FRONTEND_BASEURL}/reset-password/${resetToken}`;
    return `${generalTemplate(
        token,
        "Reset Password",
        fullName,
        "Forgot your password?",
        "We received a request to reset the password of your account",
        "To reset your password, click on the link below.",
        `${process.env.COPYRIGHT_LINK}`
    )}`;
};

module.exports = {
    forgotPasswordTemplate,
};
