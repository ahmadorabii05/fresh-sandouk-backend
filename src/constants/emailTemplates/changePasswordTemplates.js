const { generalTemplate } = require("./common/generalTemplate");

require("dotenv");

const changePasswordTemplate = (fullName) => {
    const link = `${process.env.FRONTEND_BASEURL}/login`;
    return `${generalTemplate(
        link,
        "Change Password",
        fullName,
        "Congratulations",
        "Your password has successfully been changed",
        "",
        `${process.env.COPYRIGHT_LINK}`
    )}`;
};

module.exports = {
    changePasswordTemplate,
};
