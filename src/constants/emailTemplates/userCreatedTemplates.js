const { generalTemplate } = require("./common/generalTemplate");

require("dotenv");

const userCreatedTemplate = (fullName, verificationToken) => {
    const link = `${process.env.FRONTEND_BASEURL}/account-verification/${verificationToken}`;
    return `${generalTemplate(
        link,
        "Account Verification",
        fullName,
        "Welcome on board,",
        "We would like to welcome you onto E-commerce application",
        "Please verify your account by following link below.",
        `${process.env.COPYRIGHT_LINK}`
    )}`;
};

module.exports = {
    userCreatedTemplate,
};
