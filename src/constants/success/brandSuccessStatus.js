const BrandSuccessStatus={
    BRAND_ADDED_SUCCESSFULLY:{message:"Brand added Successfully",code:200},
    BRAND_BULK_ADDED_SUCCESSFULLY:{message:"Brands added Successfully",code:200},
    BRAND_UPDATED_SUCCESSFULLY:{message:"Brand updated Successfully",code:200},
    BRAND_DELETED_SUCCESSFULLY:{message:"Brand(s) deleted Successfully",code:200},
}



module.exports = {
    BrandSuccessStatus
}