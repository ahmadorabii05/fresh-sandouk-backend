const CartSuccessStatus={
    CART_CREATED_SUCCESSFULLY:{message:"Cart created Successfully",code:200},
    CART_UPDATED_SUCCESSFULLY:{message:"Cart updated Successfully",code:200},
    ITEM_ADD_SUCCESSFULLY:{message:"Item added Successfully",code:200},
    ITEM_DELETED_SUCCESSFULLY:{message:"Item deleted Successfully",code:200}
}

module.exports = {
    CartSuccessStatus
}