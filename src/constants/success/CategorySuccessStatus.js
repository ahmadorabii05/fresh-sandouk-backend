const CategorySuccessStatus = {
    CATEGORY_CREATED_SUCCESSFUL: { code: 200, message: "Category created Successfully" },
    CATEGORY_DELETED_SUCCESSFUL:{code:200,message:"Categories deleted Successfully"},
    CATEGORY_UPDATED_SUCCESSFUL:{code:200,message:"Category updated Successfully"},
    CATEGORIES_CREATED_SUCCESSFUL:{code:200,message:"Categories created Successfully"}
};

module.exports = {
    CategorySuccessStatus,
};
