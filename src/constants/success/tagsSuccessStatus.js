const TagsSuccessStatus = {
    TAGS_CREATED_SUCCESSFULLY: {message: "Tag created Successfully", code: 200},
    TAGS_BULK_CREATED_SUCCESSFULLY: {message: "Tags created Successfully", code: 200},

    TAGS_UPDATED_SUCCESSFULLY: {message: "Tag Updated Successfully", code: 200},
    TAGS_DELETED_SUCCESSFULLY: {message: "Tag deleted Successfully", code: 200},
}

module.exports = {
    TagsSuccessStatus
}