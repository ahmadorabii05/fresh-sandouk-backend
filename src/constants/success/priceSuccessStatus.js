const PriceSuccessStatus={
    PRICE_MODEL_ADDED_SUCCESSFULLY:{message:"PriceModel added Successfully",code:200},
    PRICE_MODEL_BULK_ADDED_SUCCESSFULLY:{message:"PriceModels added Successfully",code:200},
    PRICE_MODEL_UPDATED_SUCCESSFULLY:{message:"PriceModel updated Successfully",code:200},
    PRICE_MODEL_DELETED_SUCCESSFULLY:{message:"PriceModel(s) deleted Successfully",code:200},
    PRICE_MODEL_DELETE_SUCCESSFULLY:{message:"PriceModel deleted Successfully",code:200},

}


module.exports = {
    PriceSuccessStatus
}