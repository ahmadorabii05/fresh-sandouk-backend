const OrderSuccessStatus = {
    ORDER_ADDED_SUCCESSFULLY: { message: "Order created Successfully", code: 200 },
    ORDER_UPDATED_SUCCESSFULLY: { message: "Order updated Successfully", code: 200 }
}

module.exports = {
    OrderSuccessStatus
}