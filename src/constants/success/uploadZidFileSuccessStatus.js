const UploadZidFileSuccessStatus = {
    ALL_IMAGES_UPLOADED_SUCCESSFULLY: {message: "All Images has been Uploaded Successfully", code: 200}
}

module.exports = {
    UploadZidFileSuccessStatus
}