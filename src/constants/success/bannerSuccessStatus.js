const BannerSuccessStatus = {
    BANNER_ADDED_SUCCESSFULLY: { message: "Banner added Successfully", code: 200 },
    BANNER_UPDATED_SUCCESSFULLY: { message: "Banner updated Successfully", code: 200 },
    BANNER_DELETED_SUCCESSFULLY: { message: "Banner(s) deleted Successfully", code: 200 },
}



module.exports = {
    BannerSuccessStatus
}