const { GeneralSuccessStatus } = require("./generalSuccessStatus");
const { UserSuccessStatus } = require("./userSuccessStatus");
const { RoleSuccessStatus } = require("./roleSuccessStatus");
const { CategorySuccessStatus } = require("./CategorySuccessStatus");
const { SubCategorySuccessStatus } = require("./subCategorySuccessResponse");
const { ProductSuccessStatus } = require("./productSuccessStatus");
const { TagsSuccessStatus } = require("./tagsSuccessStatus");
const { FavoriteSuccessStatus } = require("./favoriteSuccessStatus");
const { CartSuccessStatus } = require("./cartSuccessStatus");
const { ListSuccessStatus } = require("./listSuccessStatus");
const { CountrySuccessStatus } = require("./countrySuccessStatus");
const { PriceSuccessStatus } = require("./priceSuccessStatus");
const { BrandSuccessStatus } = require("./brandSuccessStatus");
const { BannerSuccessStatus } = require("./bannerSuccessStatus");
const { AddressSuccessStatus } = require("./addressSuccessStatus");
const { StockSuccessStatus } = require("./stockSuccessStatus");
const { OrderSuccessStatus } = require("./OrderSuccessStatus");
const { UploadZidFileSuccessStatus } = require("./uploadZidFileSuccessStatus");
const { StripeSuccessStatus } = require("./stripeSuccessStatus");


const SuccessStatus = {
    ...GeneralSuccessStatus,
    ...UserSuccessStatus,
    ...RoleSuccessStatus,
    ...CategorySuccessStatus,
    ...SubCategorySuccessStatus,
    ...ProductSuccessStatus,
    ...TagsSuccessStatus,
    ...FavoriteSuccessStatus,
    ...CartSuccessStatus,
    ...ListSuccessStatus,
    ...CountrySuccessStatus,
    ...PriceSuccessStatus,
    ...BrandSuccessStatus,
    ...AddressSuccessStatus,
    ...StockSuccessStatus,
    ...OrderSuccessStatus,
    ...BannerSuccessStatus,
    ...UploadZidFileSuccessStatus,
    ...StripeSuccessStatus
}


module.exports = {
    SuccessStatus,
};
