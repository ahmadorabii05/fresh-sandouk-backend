const SubCategorySuccessStatus = {
    SUB_CATEGORY_CREATED_SUCCESSFUL: { code: 200, message: "SubCategory created Successfully" },
    SUB_CATEGORY_DELETED_SUCCESSFUL:{code:200,message:"SubCategories deleted Successfully"},
    SUB_CATEGORY_UPDATED_SUCCESSFUL:{code:200,message:"SubCategory updated Successfully"},
    SUB_CATEGORY_BULK_CREATED_SUCCESSFUL: { code: 200, message: "SubCategories created Successfully" },

};

module.exports = {
    SubCategorySuccessStatus,
};