const GeneralSuccessStatus = {
    RECEIVED_SUCCESSFULLY: { code: 200, message: "Received Successfully" },
    ADDED_SUCCESSFULLY: { code: 200, message: "Added Successfully" },
    UPDATED_SUCCESSFULLY: { code: 203, message: "Updated Successfully" },
    DELETED_SUCCESSFULLY: { code: 200, message: "Deleted Successfully" },
    UPLOADED_SUCCESSFULLY: { code: 200, message: "Uploaded Successfully" },
    PROFILE_IMAGE_REMOVED_SUCCESSFULLY:{code:200,message:"Profile Image Removed Successfully"}
};

module.exports = {
    GeneralSuccessStatus,
};
