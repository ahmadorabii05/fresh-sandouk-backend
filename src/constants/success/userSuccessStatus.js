const UserSuccessStatus = {
    LOGIN_SUCCESSFUL: { code: 200, message: "User Logged In Successfully" },
    SIGNUP_SUCCESSFUL: { code: 200, message: "User Signed Up Successfully" },
    LOGOUT_SUCCESSFUL: { code: 200, message: "User Logged Out Successfully" },
    FORGOT_RESET_PASSWORD_SUCCESSFUL: { code: 200, message: "Successfully sent an email" },
    PASSWORD_SUCCESSFULLY_CHANGED: { code:200, message: "You have successfully changed your password" },
    ACCOUNT_VERIFIED_SUCCESSFULLY: { code:200, message: "Account Verified Successfully" },
    VERIFY_CODE_SUCCESSFULLY: { code:200, message: "Code Verified Successfully" },
    PHONE_NUMBER_MESSAGE_SEND_SUCCESSFULLY:{code:200,message:"verification code sent Successfully"}
};

module.exports = {
    UserSuccessStatus,
};