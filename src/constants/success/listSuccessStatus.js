const ListSuccessStatus={
    LIST_CREATED_SUCCESSFULLY:{message:"List created Successfully",code:200},
    LIST_UPDATED_SUCCESSFULLY :{message:"List updated Successfully",code:200},
    LIST_DELETED_SUCCESSFULLY:{message:"List deleted Successfully",code:200}
}

module.exports = {
    ListSuccessStatus
}