const AddressSuccessStatus={
    ADDRESS_ADDED_SUCCESSFULLY:{message:"Address added Successfully",code:200},
    ADDRESS_UPDATED_SUCCESSFULLY:{message:"Address updated Successfully",code:200},
    ADDRESS_DELETED_SUCCESSFULLY:{message:"Address(s) deleted Successfully",code:200},
}



module.exports = {
    AddressSuccessStatus
}