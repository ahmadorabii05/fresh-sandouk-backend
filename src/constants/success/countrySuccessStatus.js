const CountrySuccessStatus= {
    COUNTRIES_UPLOADED_SUCCESSFULLY: {message: "Countries uploaded Successfully", code: 200}
}

module.exports = {
    CountrySuccessStatus
}