const FavoriteSuccessStatus={
    FAVORITE_UPDATED_SUCCESSFULLY:{code:200,message:"Favorite Updated Successfully"}
}

module.exports = {
    FavoriteSuccessStatus
}