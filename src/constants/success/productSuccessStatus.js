const ProductSuccessStatus = {
    PRODUCT_CREATED_SUCCESSFUL: { code: 200, message: "Product created Successfully" },
    PRODUCT_BULK_CREATED_SUCCESSFUL: { code: 200, message: "Products created Successfully" },
    PRODUCT_BULK_UPDATED_SUCCESSFUL: { code: 200, message: "Products updated Successfully" },
    PRODUCT_DELETED_SUCCESSFUL: { code: 200, message: "Product deleted Successfully" },
    PRODUCT_UPDATED_SUCCESSFUL: { code: 200, message: "Product updated Successfully" },
    NUTRITION_DELETED_SUCCESSFUL: { code: 200, message: "Nutrition deleted Successfully" }
};

module.exports = {
    ProductSuccessStatus,
};
