const nodemailer = require("nodemailer");
require("dotenv").config();

const transporter = nodemailer.createTransport({
    service: "gmail",
    secure:false,
    auth: {
        user: process.env.MAILING_EMAIL,
        pass: process.env.MAILING_EMAIL_PASSWORD,
    },
});
const mailOptions = (sender, receiver, subject, text, html) => {
    return {
        from: sender ?? process.env.MAILING_EMAIL, // sender address
        to: receiver, // list of receivers
        subject: subject, // Subject line
        text: text, // plain text body
        html: html, // html body
    };
};
// send mail with defined transport object
const sendEmailService = async (mailOptions) => {
    await transporter.sendMail(mailOptions);
};

module.exports = {
    mailOptions,
    sendEmailService,
};
