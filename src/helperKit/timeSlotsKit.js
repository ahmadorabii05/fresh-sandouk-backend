const { DateTime } = require('luxon');
//city = {Dubai, Abu Dhabi} (String)
//timestamp = milliseconds  (Double)
function getTimeSlots(city, timestamp, numberOfOptions) {
    const currentDateTime = DateTime.fromSeconds(parseInt(timestamp));
    let cutOffDateTime;
    let slotsDubaioriginal = [
        { slot_no: 1, timing: '7 am to 11 am', cut_off: '18:00', delivery_day: '' },
        { slot_no: 2, timing: '11 am to 3 pm', cut_off: '07:00', delivery_day: '' },
        { slot_no: 3, timing: '4 pm to 9 pm', cut_off: '13:00', delivery_day: '' },
    ];
    let slotsDubai = [
        { slot_no: 1, timing: '7 am to 11 am', cut_off: '18:00', delivery_day: '' },
        { slot_no: 2, timing: '11 am to 3 pm', cut_off: '07:00', delivery_day: '' },
        { slot_no: 3, timing: '4 pm to 9 pm', cut_off: '13:00', delivery_day: '' },
    ];

    let slotsAbuDhabi = [
        { slot_no: 1, timing: '12 pm to 5 pm', cut_off: '22:00', delivery_day: '' },
    ];

    const validDeliveryDaysDubai = [1, 2, 3, 4, 5, 6, 7]; // all week days
    const validDeliveryDaysAbuDubai = [2, 4, 7]; // tuesday - thursday - sunday

    const timeFormat = "HH:mm";
    let deliveryDay;
    switch (city) {
        case 'Dubai':
            cutOffDateTime = currentDateTime.set({ hour: 18, minute: 0, second: 0, millisecond: 0 });
            if (currentDateTime < cutOffDateTime.set({ hour: 7 })) {
                deliveryDay = cutOffDateTime.toFormat('yyyy-MM-dd');
                slotsDubai = slotsDubai.slice(1);
                slotsDubai.forEach(slot => {
                    slot.delivery_day = deliveryDay;
                });
            } else if (currentDateTime < cutOffDateTime.set({ hour: 13 })) {
                deliveryDay = cutOffDateTime.toFormat('yyyy-MM-dd');
                slotsDubai = slotsDubai.slice(2);
                slotsDubai.forEach(slot => {
                    slot.delivery_day = deliveryDay;
                });
            } else if (currentDateTime < cutOffDateTime) {
                deliveryDay = cutOffDateTime.plus({ days: 1 }).toFormat('yyyy-MM-dd');
                slotsDubai.forEach(slot => {
                    slot.delivery_day = deliveryDay;
                });
            } else {
                deliveryDay = cutOffDateTime.plus({ days: 1 }).toFormat('yyyy-MM-dd');
                slotsDubai = slotsDubai.slice(1);
                slotsDubai.forEach(slot => {
                    slot.delivery_day = deliveryDay;
                });
            }


            for (let i = 0; i < numberOfOptions; i++) {
                slotsDubaioriginal.forEach(slot => {

                    const newSlot = {
                        slot_no: slot.slot_no,
                        timing: slot.timing,
                        cut_off: slot.cut_off,
                        delivery_day: DateTime.fromFormat(deliveryDay, 'yyyy-MM-dd').plus({ days: i + 1 }).toFormat('yyyy-MM-dd'),
                    };
                    slotsDubai.push(newSlot);
                });

            }

            return slotsDubai;

        case 'AbuDhabi':
            cutOffDateTime = currentDateTime.set({ hour: 22, minute: 0, second: 0, millisecond: 0 });
            if (currentDateTime < cutOffDateTime) {
                deliveryDay = cutOffDateTime.plus({ days: 1 }).toFormat('yyyy-MM-dd');
                slotsAbuDhabi.forEach(slot => {
                    slot.delivery_day = deliveryDay;
                });
            } else {
                deliveryDay = cutOffDateTime.plus({ days: 2 }).toFormat('yyyy-MM-dd');
                slotsAbuDhabi.forEach(slot => {
                    slot.delivery_day = deliveryDay;
                });
            }
            const newSlots = getNextDeliveryOptions(slotsAbuDhabi, numberOfOptions);
            updateDeliveryDay(newSlots, validDeliveryDaysAbuDubai);

            return slotsAbuDhabi;

        default:
            return [];
    }
}


function isDeliveryDayValid(validDeliveryDays, weekday) {
    const isValid = validDeliveryDays.includes(weekday);
    return isValid;
}

function updateDeliveryDay(slots, validDeliveryDays) {
    const usedDays = new Set();
    const updatedSlots = [];

    slots.forEach(slot => {
        let deliveryDateTemp = DateTime.fromFormat(slot.delivery_day, 'yyyy-MM-dd').weekday;
        let newDeliveryDay = slot.delivery_day;

        while (!isDeliveryDayValid(validDeliveryDays, deliveryDateTemp) || usedDays.has(newDeliveryDay)) {
            newDeliveryDay = DateTime.fromFormat(newDeliveryDay, 'yyyy-MM-dd').plus({ days: 1 }).toFormat('yyyy-MM-dd');
            deliveryDateTemp = DateTime.fromFormat(newDeliveryDay, 'yyyy-MM-dd').weekday;
        }


        slot.delivery_day = newDeliveryDay;
        updatedSlots.push(slot);
        usedDays.add(newDeliveryDay);

    });

    slots.length = 0;
    slots.push(...updatedSlots);


}




function getNextDeliveryOptions(slots, numberOfOptions) {


    const originalLength = slots.length;

    for (let i = 0; i < numberOfOptions; i++) {


        for (let j = 0; j < originalLength; j++) {
            const latestSlot = slots[slots.length - 1];
            const latestDate = latestSlot.delivery_day;
            const nextDate = DateTime.fromFormat(latestDate, 'yyyy-MM-dd').plus({ days: 1 }).toFormat('yyyy-MM-dd');
            slots.push({
                slot_no: latestSlot.slot_no,
                timing: slots[j].timing,
                cut_off: slots[j].cut_off,
                delivery_day: nextDate,
            })
        }

    }

    return slots;
}

module.exports = {
    getTimeSlots
}