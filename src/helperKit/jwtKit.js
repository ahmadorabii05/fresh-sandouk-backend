const jwt = require("jsonwebtoken");
require('dotenv').config();

const prepareJWTPayload = (user) => {
    return {
        user: {
            id: user.id,
            role: user.role,
        },
    };
};

const generateJWT = (user) => {
    const JWT_SECRET = process.env.JWT_SECRET || "E-COMMERCE_123";
    const jwtData = prepareJWTPayload(user);
    // jwtData.expiry = new Date().getTime() + 3600 * 9999;
    const jsonToken = jwt.sign(jwtData, JWT_SECRET);
    return jsonToken;
};

const jwtUserEncryption = (user) => {
    const JWT_SECRET = process.env.JWT_SECRET;
    const payload = { user };
    const token = jwt.sign(payload, JWT_SECRET);
    return token;
};

module.exports = {
    generateJWT,
    jwtUserEncryption,
};
