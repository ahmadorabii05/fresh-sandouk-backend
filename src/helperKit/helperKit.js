const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("../dataHandlerLayer/common/LanguageHeaderHandler");
const UserQueries = require("../dataBaseLayer/queries/user/userQueries")
const {prepareErrorLog} = require("./loggingKit");
const {ProductResponseModel} = require("../presentationLayer/models/responseModel/products/ProductResponseModel");
const cartQueries = require("../dataBaseLayer/queries/cart/cartQueries");
const productQueries = require("../dataBaseLayer/queries/product/productQueries")
const listQueries = require("../dataBaseLayer/queries/list/listQueries")
const CartQueries = require("../dataBaseLayer/queries/cart/cartQueries")
const OrderModel = require("../dataBaseLayer/models/orders/OrdersModels")
const StockQueries = require("../dataBaseLayer/queries/stock/stockQueries")
require("dotenv").config();
const {isEmpty} = require("lodash");
const Stripe = require('stripe');
const ProductQueries = require("../dataBaseLayer/queries/product/productQueries")
const {prepareSuccessResponse} = require("../presentationLayer/common/presenter");
const stripe = Stripe(process.env.STRIPE_KEY);
const unzipper = require('unzipper'); // You need to install this package via npm or yarn
const { calculateFinalPriceAfterDiscount } = require("./productKit");

const hasSpecialChars = (str) => {
    return /[^\w&\s]/.test(str);
}

const getIdsNotInArray = async (stringArray, objectArray) => {
    return stringArray.filter(str => {
        const obj = objectArray.find(obj => obj._id.toString() === str);
        return obj || true;
    });
};

const handleSpecialCharsInFileName = (file) => {
    const fileName = file.originalname;
    const extensionIndex = fileName.lastIndexOf('.');
    const fileNameWithoutExtension = fileName.substring(0, extensionIndex);
    return hasSpecialChars(fileNameWithoutExtension);
}

const getFileStorePathForFile = (fileName) => {
    return ['png', 'jpg', 'jpeg', 'jfif'].includes(fileName.toLowerCase());
}

const isImageFileName =(fileName) => {
    const imageExtensions = ['png', 'jpg', 'jpeg', 'jfif', 'gif', 'bmp']; // Add more extensions if needed

    // Get the file extension from the file name
    const fileExtension = fileName.split('.').pop().toLowerCase();

    // Check if the file extension is in the list of image extensions
    return imageExtensions.includes(fileExtension);
}

const checkIfExistInAnArray = (array, string) => {
    return array.includes(string)
}

const isActive = (req, res, next) => {
    if (req.userFromToken?.active) {
        next()
    } else {
        return res
            .status(ErrorStatus.NON_ACTIVE_USER.code)
            .json(prepareErrorResponse(ErrorStatus.NON_ACTIVE_USER, null, extractLanguageFromRequest(req)));
    }
}


const setFavoriteProductFlag = async (userId, products, res) => {
    try {
        const userInfo = await UserQueries.getUserInfoById(userId);
        const favoriteProductIds = userInfo?.info.productSettings?.map((product) => product.toString()) || [];

        products.docs = products.docs.map((product) => {
            if (favoriteProductIds.includes(product.id.toString())) {
                product.isFavorite = true;
            }
            return product;
        });

        return products;
    } catch (error) {
        prepareErrorLog(error, setFavoriteProductFlag.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const setFavoriteProductFlagNotPaginated = async (userId, products, res) => {
    try {
        const userInfo = await UserQueries.getUserInfoById(userId);
        const favoriteProductIds = userInfo?.info.productSettings?.map((product) => product.toString()) || [];

        products.docs = products.docs.map((product) => {
            if (favoriteProductIds.includes(product.id.toString())) {
                product.isFavorite = true;
            }
            return product;
        });

        return products;
    } catch (error) {
        prepareErrorLog(error, setFavoriteProductFlag.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};

const setFavoriteProductFlagById = async (userId, product, res, req) => {
    try {
        const user = userId.valueOf();
        const userInfo = await UserQueries.getUserInfoById(userId);
        const favoriteProductIds = userInfo?.info.productSettings?.map((product) => product.toString()) || [];
        const updatedProduct = new ProductResponseModel(product, req);
        if (favoriteProductIds.includes(product?._id?.toString())) {
            updatedProduct.isFavorite = true;
        }

        const cartAggregate = await cartQueries.getCartAggregateByUserId(user);
        const cartProduct = cartAggregate.find(cp => String(cp._id) === String(product._id));
        if (cartProduct && cartProduct.quantityInCart) {
            updatedProduct.quantityInCart = cartProduct.quantityInCart;
        }
        return updatedProduct;
    } catch (error) {
        prepareErrorLog(error, setFavoriteProductFlagById.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
};


const setFlagIsFavoriteForLists = async (userId, product, res, req) => {
    try {
        const user = userId.valueOf();
        const userInfo = await UserQueries.getUserInfoById(userId);
        const favoriteProductIds = userInfo?.info.productSettings?.map((product) => product.toString()) || [];
        let updatedProduct = product.productIds;
        // updatedProduct = updatedProduct.map(item=>{
        //     return new ProductResponseModel(item,req);
        // })
        if (favoriteProductIds.includes(updatedProduct?._id?.toString())) {
            updatedProduct.isFavorite = true;
        }

        const cartAggregate = await cartQueries.getCartAggregateByUserId(user);
        const cartProduct = cartAggregate.find(cp => String(cp._id) === String(updatedProduct._id));
        if (cartProduct && cartProduct.quantityInCart) {
            updatedProduct.quantityInCart = cartProduct.quantityInCart;
        }
        product.productIds = updatedProduct;
        return product;
    } catch (error) {
        prepareErrorLog(error, setFlagIsFavoriteForLists.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const checkIfListForUser = async (req, res, next) => {
    const {listId} = req.requestModel;
    const userId = req.userFromToken._id;

    const getListInfo = await listQueries.getListById(listId);

    if (getListInfo?.userId.toString() !== userId.toString()) {
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
    next();
}


const checkIfAllImagesPass = async (req, res, next) => {
    try {
        if (req.files && req.files.length > 0) {
            for (let i = 0; i < req.files.length; i++) {
                const file = req.files[i];
                if (!getFileStorePathForFile(file.mimetype.split("/")[1])) {
                    return res
                        .status(ErrorStatus.ONLY_IMAGES.code)
                        .json(prepareErrorResponse(ErrorStatus.ONLY_IMAGES, "This File is not an Image : " + file.originalname, extractLanguageFromRequest(req)));
                }
            }
        } else {
            return res
                .status(ErrorStatus.FILE_MISSING.code)
                .json(prepareErrorResponse(ErrorStatus.FILE_MISSING, null, extractLanguageFromRequest(req)));
        }

        next();
    } catch (error) {
        prepareErrorLog(error, checkIfAllImagesPass.name);

        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }

}

const checkIfAllImagesPassOnUpdate = async (req, res, next) => {
    try {
        if (req.files && req.files.length > 0) {
            for (let i = 0; i < req.files.length; i++) {
                const file = req.files[i];
                if (!getFileStorePathForFile(file.mimetype.split("/")[1])) {
                    return res
                        .status(ErrorStatus.ONLY_IMAGES.code)
                        .json(prepareErrorResponse(ErrorStatus.ONLY_IMAGES, "This File is not an Image : " + file.originalname, extractLanguageFromRequest(req)));
                }
            }
        }
        next()
    } catch (error) {
        prepareErrorLog(error, checkIfAllImagesPassOnUpdate.name);

        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const checkIfAllBannerImagesPass = async (req, res, next) => {
    try {
        if (req.bannerFiles && req.bannerFiles.length > 0) {
            for (let i = 0; i < req.bannerFiles.length; i++) {
                const file = req.bannerFiles[i];
                if (!getFileStorePathForFile(file.mimetype.split("/")[1])) {
                    return res
                        .status(ErrorStatus.ONLY_IMAGES.code)
                        .json(prepareErrorResponse(ErrorStatus.ONLY_IMAGES, "This Banner File is not an Image : " + file.originalname, extractLanguageFromRequest(req)));
                }
            }
        } else {
            return res
                .status(ErrorStatus.FILE_MISSING.code)
                .json(prepareErrorResponse(ErrorStatus.FILE_MISSING, null, extractLanguageFromRequest(req)));
        }

        next();
    } catch (error) {
        prepareErrorLog(error, checkIfAllBannerImagesPass.name);

        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }

}

const checkIfAllBannerImagesPassOnUpdate = async (req, res, next) => {
    try {
        if (req.bannerFiles && req.bannerFiles.length > 0) {
            for (let i = 0; i < req.bannerFiles.length; i++) {
                const file = req.bannerFiles[i];
                if (!getFileStorePathForFile(file.mimetype.split("/")[1])) {
                    return res
                        .status(ErrorStatus.ONLY_IMAGES.code)
                        .json(prepareErrorResponse(ErrorStatus.ONLY_IMAGES, "This Banner File is not an Image : " + file.originalname, extractLanguageFromRequest(req)));
                }
            }
        }
        next()
    } catch (error) {
        prepareErrorLog(error, checkIfAllBannerImagesPassOnUpdate.name);

        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const checkProductNames = async (productId, name, nameAr, req, res) => {
    try {
        const checkProductName = await productQueries.getProductByName(req?.requestModel?.name);
        const checkProductNameAr = await productQueries.getProductByNameAr(req?.requestModel?.nameAr);
        if (!isEmpty(checkProductName)) {
            if (checkProductName._id.valueOf() === productId.valueOf()) {
                delete req.requestModel.name;
            } else {
                return res
                    .status(ErrorStatus.PRODUCT_EXIST.code)
                    .json(prepareErrorResponse(ErrorStatus.PRODUCT_EXIST, null, extractLanguageFromRequest(req)));
            }
        }
        if (!isEmpty(checkProductNameAr)) {
            if (checkProductNameAr._id.valueOf() === productId.valueOf()) {
                delete req.requestModel.nameAr;
            } else {
                return res
                    .status(ErrorStatus.PRODUCT_EXIST_IN_ARABIC.code)
                    .json(prepareErrorResponse(ErrorStatus.PRODUCT_EXIST_IN_ARABIC, null, extractLanguageFromRequest(req)));
            }
        }
    } catch (error) {
        prepareErrorLog(error, checkProductNames.name);

        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}

const checkIfTotalPriceInCartLessThanWallet = async (req, res, next) => {
    const userId = req.userFromToken._id;
    const getUserInfo = await UserQueries.getUserInfoById(userId);
    const getTotalPriceFromCart = await CartQueries.findUserCart(userId);
    if (getTotalPriceFromCart.length <= 0) {
        return res
            .status(ErrorStatus.CART_IS_EMPTY.code)
            .json(prepareErrorResponse(ErrorStatus.CART_IS_EMPTY, null, extractLanguageFromRequest(req)));
    }
    if (getUserInfo?.info?.general?.walletBalance < getTotalPriceFromCart[0]?.totalPrice) {
        return res
            .status(ErrorStatus.YOU_DONT_HAVE_ENOUGH_BALANCE.code)
            .json(prepareErrorResponse(ErrorStatus.YOU_DONT_HAVE_ENOUGH_BALANCE, null, extractLanguageFromRequest(req)));
    }
    next();
}

const checkIfThereIsAnyProductOutOfStock = async (productIds) => {
    const outOfStockProductIds = [];
    let outOfStock = false;
    await Promise.all(productIds.map(async (item) => {
        const getStock = await StockQueries.getStockByProductId(item.productId);
        const getQuantity = getStock?.quantity ?? 0;
        if (getQuantity <= 0) {
            outOfStockProductIds.push(item.productId);
            outOfStock = true
        }
    }));
    return {
        outOfStock,
        outOfStockProductIds
    };
}

const returnAllOutOfStockProduct = async (productIds) => {
    const outOfStockProducts = [];
    await Promise.all(productIds.map(async (item) => {
        const product = await productQueries.getProductById(item, true);
        outOfStockProducts.push(product);
    }));
    return outOfStockProducts;
};


const updateStockToAddQuantitySold = async (productIds) => {
    await Promise.all(productIds.map(async (item) => {
        await StockQueries.updateStockById(item.productId, item.quantity);
    }));
}

// Helper function to find the category ID of a product
const findCategoryId = (categories, product) => {
    for (const category of categories) {
        const subCategory = category.subCategories.find(subCat =>
            subCat.productIds.some(productId => productId._id.toString() === product.productId._id.toString())
        );
        if (subCategory) {
            return category._id;
        }
    }
    return null;
};

const calculateTotalAmount = async (orders) => {
    const ordersWithTotalAmount = orders.docs.map((order) => {

        const productsWithTotalAmount = order?.products?.map((product) => {
            const {quantity} = product;
            const foundProduct = product.productId;
            if (foundProduct) {
                let totalPrice = 0;
                if (foundProduct.discountValue) {
                    const discountValue = parseFloat(foundProduct.discountValue) || 0;
                    const discountedPrice = calculateFinalPriceAfterDiscount(foundProduct.price, discountValue);
                    
                    totalPrice = (discountedPrice * quantity).toFixed(2);
                } else {
                    totalPrice = (foundProduct.price * quantity).toFixed(2);
                }

                return {
                    ...product.toObject(),
                    totalPrice,
                };
            }

        });

        const orderTotalAmount = productsWithTotalAmount.reduce(
            (sum, product) => sum + parseFloat(product?.totalPrice),
            0
        );

        return {
            _id: order._id,
            userId: order.userId,
            products: productsWithTotalAmount,
            addressId: order.addressId,
            scheduledDate: order.scheduledDate,
            keyOrder: order.keyOrder,
            scheduledTime: order.scheduledTime,
            deliveryInstruction: order.deliveryInstruction,
            packingInstruction: order.packingInstruction,
            totalAmount: orderTotalAmount.toFixed(2),
            status: order.status,
            createdAt: order.createdAt,
            updatedAt: order.updatedAt,
        };
    });

    return {
        ...orders,
        docs: ordersWithTotalAmount,
    };

}


const calculateTotalAmountInOrderById = async (order) => {

    const productsWithTotalAmount = order?.products?.map((product) => {
        const {quantity} = product;
        const foundProduct = product.productId;
        if (foundProduct) {
            let totalPrice = 0;
            if (foundProduct.discountValue) {
                const discountValue = parseFloat(foundProduct.discountValue) || 0;
                const discountedPrice = calculateFinalPriceAfterDiscount(foundProduct.price, discountValue);
            
                totalPrice = (discountedPrice * quantity).toFixed(2);
            } else {
                totalPrice = (foundProduct.price * quantity).toFixed(2);
            }

            return {
                ...product.toObject(),
                totalPrice,

            };
        }
    });

    const orderTotalAmount = productsWithTotalAmount.reduce(
        (sum, product) => sum + parseFloat(product?.totalPrice || 0),
        0
    );

    return {
        ...order,
        status: order.status,
        deliveryInstruction: order.deliveryInstruction,
        products: productsWithTotalAmount,
        totalAmount: orderTotalAmount.toFixed(2),

    };

}
const generateKeyOrder = async () => {
    const latestOrder = await OrderModel.findOne({}, {keyOrder: 1}, {sort: {createdAt: -1}}).exec();

    let latestNumber = 0;
    if (latestOrder && latestOrder.keyOrder) {
        const latestKeyOrder = latestOrder.keyOrder;
        latestNumber = parseInt(latestKeyOrder.split('-')[1]);
    }

    const newNumber = latestNumber + 1;
    const formattedNumber = newNumber.toString().padStart(4, '0');
    return `INV-${formattedNumber}`;
}


const createCustomerInStripe = async (customerName, customerEmail, customerPhoneNumber) => {
    return await stripe.customers.create({
        email: `${customerEmail}`,
        name: `${customerName}`,
        phone: `${customerPhoneNumber}`
    })
}


const getTotalAmountForStripe = async (req, res) => {
    const userId = req.userFromToken._id;
    let totalPrice = 0;
    const cartInfo = await cartQueries.getCartInfo(userId);
    if (cartInfo?.productIds?.length < 0) {
        return res
            .status(ErrorStatus.CART_IS_EMPTY.code)
            .json(prepareErrorResponse(ErrorStatus.CART_IS_EMPTY, null, extractLanguageFromRequest(req)));

    } else {
        const getTotalAmount = cartInfo?.productIds?.map(async (item) => {
            const product = item.productId;
            const price = product.price;
            let finalPriceAfterDiscount = price;

            if (product.discountValue) {
                const discountValue = parseFloat(product.discountValue) || 0;
                finalPriceAfterDiscount = calculateFinalPriceAfterDiscount(product.price, discountValue);            
            }

            totalPrice += finalPriceAfterDiscount * item.quantity;
            // totalOriginalPrice += product.price * item.quantity;

        });
        return {
            amount: totalPrice
        }
    }
}

const getCartInfoForStripe = async (req, res, userId) => {
    let totalPrice = 0;
    let totalOriginalPrice = 0;
    const cartInfo = await cartQueries.getCartInfo(userId);
    if (cartInfo?.productIds?.length < 0) {
        return res
            .status(ErrorStatus.CART_IS_EMPTY.code)
            .json(prepareErrorResponse(ErrorStatus.CART_IS_EMPTY, null, extractLanguageFromRequest(req)));
    } else {
        const productsInCartPromises = cartInfo?.productIds?.map(async (item) => {
            if (item.productId !== null) {
                const product = item?.productId;
                const price = product?.price;
                let finalPriceAfterDiscount = price;

                if (product?.discountValue) {
                    const discountValue = parseInt(product.discountValue) || 0;
                    finalPriceAfterDiscount = calculateFinalPriceAfterDiscount(product.price, discountValue);
                    
                }

                totalPrice += finalPriceAfterDiscount * item?.quantity;
                totalOriginalPrice += product?.price * item?.quantity;
                const userInfo = await UserQueries.getUserInfoById(userId);
                const favoriteProductIds = userInfo?.info?.productSettings?.map((product) => product?.toString()) || [];
                if (favoriteProductIds.includes(item?.productId?._id.toString())) {
                    product.isFavorite = true;
                }
                return {
                    _id: product?._id,
                    name: product?.name,
                    imageUrls: product?.imageUrls,
                    tags: product?.tags,
                    description: product?.description,
                    country: product?.country,
                    price: price,
                    isFavorite: product?.isFavorite,
                    priceModel: product?.priceModel,
                    isDiscount: product?.isDiscount,
                    discountValue: product?.discountValue,
                    brand: product?.brandId,
                    nutrition: product?.nutrition,
                    generalDescription: product?.generalDescription,
                    ingredient: product?.ingredient,
                    storage: product?.storage,
                    finalPriceAfterDiscount: finalPriceAfterDiscount,
                    quantityInCart: item?.quantity,
                };
            }
        });
        const productsInCart = await Promise.all(productsInCartPromises);
        return {
            productsInCart: productsInCart,
            totalPrice: totalPrice
        }
    }
}

const stripeValidationBeforeCheckout = async (userId, req, res) => {
    const getUserCart = await CartQueries.getUserCartWithoutPopulate(userId);
    if (!req.requestModel) {
        req.requestModel = {};
    }
    if (!isEmpty(getUserCart)) {
        // Check if there is any product out of stock
        const getAllOutOfStockProduct = await checkIfThereIsAnyProductOutOfStock(
            getUserCart?.productIds
        );

        if (getAllOutOfStockProduct.outOfStock === false) {
            const getAllProductFromCart = getUserCart.productIds;
            await Promise.all(
                getAllProductFromCart.map(async (item) => {
                    const getStock = await StockQueries.getStockByProductId(
                        item.productId
                    );
                    const getQuantity = getStock.quantity ?? 0;
                    if (item?.quantity > getStock.quantity ?? 0) {
                        const productInfo = await ProductQueries.getProductById(item.productId, true);
                        return res
                            .status(
                                ErrorStatus.ITEM_QUANTITY_IS_LESS_THAN_QUANTITY_SELECTED
                                    .code
                            )
                            .json(
                                prepareErrorResponse(
                                    ErrorStatus.ITEM_QUANTITY_IS_LESS_THAN_QUANTITY_SELECTED,
                                    productInfo.name + ", we only have in our stock " + getStock.quantity,
                                    extractLanguageFromRequest(req)
                                )
                            );
                    }
                })
            );

            req.requestModel.products = getUserCart.productIds;
            req.requestModel.userId = userId;

        } else {
            const outOfStockProducts = await returnAllOutOfStockProduct(
                getAllOutOfStockProduct.outOfStockProductIds
            );
            const order = outOfStockProducts.map(item => {
                return new ProductResponseModel(item, req);
            })
            return res
                .status(ErrorStatus.SERVER_DOWN.code)
                .json(
                    prepareSuccessResponse(
                        ErrorStatus.ITEM_OUT_OF_STOCK,
                        "Item(s) out of Stock",
                        order
                    )
                );
        }
    } else {
        return res
            .status(ErrorStatus.CART_NOT_FOUND.code)
            .json(
                prepareErrorResponse(
                    ErrorStatus.CART_NOT_FOUND,
                    null,
                    extractLanguageFromRequest(req)
                )
            );
    }
};

const uploadZipFileValidatorHelperFunction = async (req, res, next) => {
    try {
        if (!req.requestModel) {
            req.requestModel = {}
        }
        if (!req.file) {
            return res
                .status(ErrorStatus.NO_ZIP_FILE.code)
                .json(prepareErrorResponse(ErrorStatus.NO_ZIP_FILE, null, extractLanguageFromRequest(req)));
        }

        // Check if the uploaded file is a zip file
        if (req.file.mimetype !== 'application/zip') {
            return res
                .status(ErrorStatus.UPLOAD_ZIP_FILE.code)
                .json(prepareErrorResponse(ErrorStatus.UPLOAD_ZIP_FILE, null, extractLanguageFromRequest(req)));
        }
        const zipFileBuffer = req.file.buffer; // Contains the zip file data as a Buffer
        const rejectedImageNames = [];

        // Process the zip file
        unzipper.Open.buffer(zipFileBuffer)
            .then((d) => {
                // Extract and process image files only
                d.files.map((file) => {
                    console.log(file.path);
                    const pathParts = file.path.split('/');
                    if (!isImageFileName(pathParts[2])) {
                        rejectedImageNames.push(pathParts[pathParts.length - 1]);
                    }
                });
                if (rejectedImageNames.length > 0) {
                    req.requestModel.rejectedImages = rejectedImageNames;
                } else {
                    req.requestModel.rejectedImages = [];
                }
            })
            .catch((err) => {
                return res
                    .status(ErrorStatus.SERVER_DOWN.code)
                    .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, "Error processing the zip file.", extractLanguageFromRequest(req)));
            }).finally(() => {
            // Call next() here, inside the .finally() block to ensure it's executed after processing is complete.
            next()
        });

    } catch (error) {
        prepareErrorLog(error, uploadZipFileValidatorHelperFunction.name);
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
}
module.exports = {
    uploadZipFileValidatorHelperFunction,
    hasSpecialChars,
    getIdsNotInArray,
    handleSpecialCharsInFileName,
    getFileStorePathForFile,
    isActive,
    checkIfExistInAnArray,
    setFavoriteProductFlag,
    setFavoriteProductFlagById,
    setFavoriteProductFlagNotPaginated,
    checkIfListForUser,
    checkIfAllImagesPass,
    checkIfAllBannerImagesPass,
    checkIfAllBannerImagesPassOnUpdate,
    checkProductNames,
    checkIfTotalPriceInCartLessThanWallet,
    checkIfThereIsAnyProductOutOfStock,
    returnAllOutOfStockProduct,
    updateStockToAddQuantitySold,
    findCategoryId,
    calculateTotalAmount,
    generateKeyOrder,
    setFlagIsFavoriteForLists,
    checkIfAllImagesPassOnUpdate,
    calculateTotalAmountInOrderById,
    createCustomerInStripe,
    getTotalAmountForStripe,
    getCartInfoForStripe,
    stripeValidationBeforeCheckout
}
