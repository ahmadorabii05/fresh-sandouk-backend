const AWS = require("aws-sdk");
const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {prepareErrorLog} = require("./loggingKit");
const {extractLanguageFromRequest} = require("../dataHandlerLayer/common/LanguageHeaderHandler");
const {hasSpecialChars} = require("./helperKit");
require('dotenv').config();

const s3Client = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: "eu-west-2",
});

const getBucketPath = () => {
    return `kibson-category-bucket`;
};

const uploadFileOptional = (req, res, next) => {
    if (req.file) {
        uploadFile(req, res, next);
    } else {
        next();
    }
}

const uploadFile = (req, res, next) => {
    if (req.file) {
        const fileName = req.file.originalname;
        const extensionIndex = fileName.lastIndexOf('.');
        const fileNameWithoutExtension = fileName.substring(0, extensionIndex);
        if (!hasSpecialChars(fileNameWithoutExtension)) {
            if (getFileStorePathForFile(req.file.mimetype.split("/")[1])) {
                const params = {
                    Bucket: getBucketPath(),
                    Key: req.body.name + '$$$' + req.file.originalname.toLowerCase(),
                    Body: req.file.buffer,
                };
                s3Client.upload(params, (err, data) => {

                    if (err) {
                        prepareErrorLog(err);
                        return res
                            .status(ErrorStatus.FAILED_TO_UPLOAD.code)
                            .json(prepareErrorResponse(ErrorStatus.FAILED_TO_UPLOAD, null, extractLanguageFromRequest(req)));
                    }
                    req.fileLocation = data.Location;
                    next();
                });
            } else {
                return res
                    .status(ErrorStatus.ONLY_IMAGES.code)
                    .json(prepareErrorResponse(ErrorStatus.ONLY_IMAGES, null, extractLanguageFromRequest(req)));
            }
        }else{
            return res
                .status(ErrorStatus.FILE_HAS_SPECIAL_CHARACTERS.code)
                .json(prepareErrorResponse(ErrorStatus.FILE_HAS_SPECIAL_CHARACTERS, null, extractLanguageFromRequest(req)));
        }

    } else {
        return res
            .status(ErrorStatus.FILE_MISSING.code)
            .json(prepareErrorResponse(ErrorStatus.FILE_MISSING, null, extractLanguageFromRequest(req)));
    }
};

const getFileStorePathForFile = (fileName) => {
    return ['png', 'jpg', 'jpeg', 'jfif'].includes(fileName.toLowerCase());
}
module.exports = {
    uploadFile,
    uploadFileOptional
};
