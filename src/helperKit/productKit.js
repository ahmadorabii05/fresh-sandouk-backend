function calculateFinalPriceAfterDiscount(price, discountValue) {
    /**
     * Old logic
     * const discountPercentage = parseInt(product.discountValue) || 0;
     * const discountAmount = (product.price * discountPercentage) / 100;
     * const finalPrice = product.price - discountAmount;
     */
    return price - discountValue;
}


module.exports = {
    calculateFinalPriceAfterDiscount
}