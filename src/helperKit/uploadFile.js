const AWS = require("aws-sdk");
const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {prepareErrorLog} = require("./loggingKit");
const {extractLanguageFromRequest} = require("../dataHandlerLayer/common/LanguageHeaderHandler");
require('dotenv').config();
const path = require('path');

const s3Client = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: "eu-west-2",
});

const getBucketPath = (path) => {
    return `kibson${path}`;
};

const uploadFileOptional = async (res, file, id) => {
    if (file) {
        uploadFile(res, file, id);
    }
}

const uploadFile = async (res, file, id,bucketPath) => {
    return new Promise((resolve, reject) => {
        const uploadedFilePath = file.originalname;
        const fileExtension = path.extname(uploadedFilePath);

        // Set the correct Content-Type based on the file format
        let contentType;
        if (fileExtension.toLowerCase() === '.png') {
            contentType = 'image/png';
        } else if (fileExtension.toLowerCase() === '.jpeg' || fileExtension.toLowerCase() === '.jpg') {
            contentType = 'image/jpeg';
        } else {
            // You may need to handle other image formats as needed
            contentType = 'application/octet-stream';
        }
        const params = {
            Bucket: getBucketPath(bucketPath),
            Key: `${Date.now()}/${id}${fileExtension}`,
            Body: file.buffer,
            ContentType: contentType, // Set the Content-Type here
        }
        s3Client.upload(params, (err, data) => {
            if (err) {
                prepareErrorLog(err);
                return res
                    .status(ErrorStatus.FAILED_TO_UPLOAD.code)
                    .json(prepareErrorResponse(ErrorStatus.FAILED_TO_UPLOAD, null, extractLanguageFromRequest(req)));
            }
            resolve(data.Location);
        });

    })
};

const uploadFileViaZipFile = async (req,res, file, id,bucketPath) => {
    return new Promise((resolve, reject) => {
        const uploadedFilePath = file.name;
        const fileExtension = path.extname(uploadedFilePath);
        // Set the correct Content-Type based on the file format
        let contentType;
        if (fileExtension.toLowerCase() === '.png') {
            contentType = 'image/png';
        } else if (fileExtension.toLowerCase() === '.jpeg' || fileExtension.toLowerCase() === '.jpg') {
            contentType = 'image/jpeg';
        } else {
            // You may need to handle other image formats as needed
            contentType = 'application/octet-stream';
        }
        const params = {
            Bucket: bucketPath,
            Key: `${Date.now()}/${id}${fileExtension}`,
            Body: file.buffer,
            ContentType: contentType, // Set the Content-Type here
        }
        s3Client.upload(params, (err, data) => {
            if (err) {
                prepareErrorLog(err);
                return res
                    .status(ErrorStatus.FAILED_TO_UPLOAD.code)
                    .json(prepareErrorResponse(ErrorStatus.FAILED_TO_UPLOAD, null, extractLanguageFromRequest(req)));
            }
            resolve(data.Location);
        });

    })
};

module.exports = {
    uploadFile,
    uploadFileOptional,
    uploadFileViaZipFile,
};
