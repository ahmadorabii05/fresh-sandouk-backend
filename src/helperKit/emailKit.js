const {
    changePasswordTemplate,
} = require("../constants/emailTemplates/changePasswordTemplates");
const {
    forgotPasswordTemplate,
} = require("../constants/emailTemplates/forgetPasswordTemplates");
const {
    userCreatedTemplate,
} = require("../constants/emailTemplates/userCreatedTemplates");
const { sendEmailService } = require("./emailServiceKit");

const EmailTypes = {
    ForgotPassword: 0,
    UserCreated: 1,
    ChangePassword: 2,
};

const sendEmail = (user, type) => {
    switch (type) {
        case EmailTypes.ForgotPassword:
            sendEmailService({
                to: user.info?.general?.email ?? "",
                subject: "Forgot Password",
                html: forgotPasswordTemplate(
                    user.info?.general?.fullName ?? "",
                    user.info?.general?.resetToken
                ),
            });
            break;
        case EmailTypes.UserCreated:
            sendEmailService({
                to: user.info?.general?.email ?? "",
                subject: "Account Verification",
                html: userCreatedTemplate(
                    user.info?.general?.fullName ?? "",
                    user.info?.general?.verificationToken
                ),
            });
            break;
        case EmailTypes.ChangePassword:
            sendEmailService({
                to: user.info?.general?.email ?? "",
                subject: "Change Password",
                html: changePasswordTemplate(user.info?.general?.fullName ?? ""),
            });
            break;
        default:
            break;
    }
};

module.exports = {
    EmailTypes,
    sendEmail,
};
