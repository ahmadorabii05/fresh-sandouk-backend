const en = {
    // General
    BAD_REQUEST: "Bad Request",
    NOT_AUTHORIZED: "Not Authorized",
    NOT_FOUND: "Not Found",
    SERVER_DOWN: "Server Down",
    OUT_DATED: "Out Dated",
    FAILED_TO_UPLOAD: "Failed To Upload",
    FILE_MISSING: "File Missing",
    INVALID_EMAIL: "Invalid Email",
    INVALID_PASSWORD: "Password must be at least 8 characters",
    IS_REQUIRED: "is required",
    INVALID_PERMISSION: "Invalid Permission",
    INVALID_GENDER: "Invalid Gender",
    INVALID_TYPE: "Invalid Type",
    INVALID_BANNER_LOCATION: "Banner Location Invalid",

    // Role
    ROLE_EXIST: "Role already exists",
    ROLE_DOESNT_EXIST: "Role does not exist",
    YOUR_ARE_NOT_AN_ADMIN: "Sorry, you are not an admin",

    //category
    CATEGORY_EXIST: "Category already exists",
    CATEGORY_EXIST_IN_ARABIC: "Category In Arabic already exists",

    CATEGORY_DOESNT_EXIST: "Category does not exist",
    CATEGORY_HAS_SUBCATEGORY: "This Category has subCategories you have to delete them first",
    CATEGORY_NAME_SPECIAL_CHARACTER: "Category name must not contains special character",

    //subCategory
    SUB_CATEGORY_EXIST: 'SubCategory already exists',
    SUB_CATEGORY_EXIST_IN_ARABIC: 'SubCategory In Arabic already exists',
    SUB_CATEGORY_DOESNT_EXIST: 'SubCategory does not exist',
    SUB_CATEGORY_HAS_PRODUCTS: "This SubCategory has products you have to delete them first",
    SUB_CATEGORY_NAME_SPECIAL_CHARACTER: "SubCategory name must not contains special character",

    // User
    DOES_NOT_EXIST: "User does not exist",
    WRONG_CURRENT_PASSWORD: "Current Password Invalid",
    USER_EXIST: "User already exists",
    USER_EMAIL_EXIST: "Email already exists",
    WRONG_PASSWORD: "Wrong Password",
    REQUEST_EXPIRED: "Request Expired",
    NAME_EXIST: "name is already exists",
    NAME_MUST_BE_AT_LEAST_6_CHARACTER: "Name Must be at least 6 characters",
    PASSWORD_MUST_BE_MORE_THAN_8_CHARACTERS: "Password Must be at least 8 characters",
    USER_NOT_VERIFIED: "you must verify your email",
    MISSING_DATA: "Email/LoginId from socialLogin are missing, please provide me at least one of them",
    FILE_HAS_SPECIAL_CHARACTERS: "FileName must not contains special character",
    NON_ACTIVE_USER: "Sorry, you are not Active anymore",
    //upload file
    ONLY_IMAGES: "You can only upload images",

    //products
    PRODUCT_EXIST: "Product of the same name exists",
    PRODUCT_DOESNT_EXIST: "Product doesn't exist",
    PRODUCT_NAME_SPECIAL_CHARACTER: "Product name has special characters",
    PRODUCT_EXIST_IN_ARABIC: "Product Arabic name is already exist",
    SKU_ALREADY_EXIST: "Stock Keeping Unit is already exist",

    //nutrition
    NUTRITION_DOESNT_EXIST: "Nutrition doesn't exist",
    //otp phoneNumber
    OTP_TOKEN: "OTP code number is invalid please generate a new one",

    //tags
    TAGS_NAME_ALREADY_EXIST: "Tag already exists",
    TAG_DOESNT_EXIST: "Tag doesn't exist",
    TAG_IN_ARABIC_ALREADY_EXIST: "Tag name in Arabic already exist",

    //cart
    PRODUCT_DOESNT_EXIST_IN_CART: "Product doesn't exist in your cart",
    ADD_PRODUCT_TO_CART_FIRST: "Add this product to your cart first",
    CART_NOT_FOUND: "Cart doesn't exist",
    CART_IS_EMPTY: "Cart is empty",

    COUNTRY_NAME_DOES_NOT_EXIST: "Country Name does not exist",
    //list
    LIST_ALREADY_EXIST: "List already exist",
    LIST_DOES_NOT_EXIST: "List does not exist",

    //priceModel
    PRICE_MODEL_DOES_NOT_EXIST: "PriceModel does not exist",
    PRICE_MODEL_ALREADY_EXIST: "PriceModel already exist",
    PRICE_MODEL_IN_ARABIC_ALREADY_EXIST: "PriceModel in Arabic already exist",
    //brand
    BRAND_DOES_NOT_EXIST: "Brand does not exist",
    BRAND_ALREADY_EXIST: "Brand already exist",
    BRAND_ALREADY_EXIST_IN_ARABIC: "Brand Name in Arabic is already exist",

    INVALID_ADDRESS: "Invalid Address title",

    //stock
    PRODUCT_IN_STOCK_ALREADY_EXIST: "Product in stock already exist",
    PRODUCT_IN_STOCK_DOES_NOT_EXIST: "Product does not exist in the stock",
    ITEM_OUT_OF_STOCK: "Item(s) out of Stock",
    //order
    YOU_DONT_HAVE_ENOUGH_BALANCE: "There is not enough balance in your account",
    //address
    ADDRESS_DOES_NOT_EXIST: "You have to create Address first",
    //Order
    ORDER_DOES_NOT_EXIST: "Order doesn't exist",
    //stripe
    STRIPE_ERROR_CREATE_CUSTOMER: "Something went wrong in create customer with payment gateway",

    //upload zip file
    UPLOAD_ZIP_FILE: "Uploaded file must be a zip file.",
    NO_ZIP_FILE: "No zip file uploaded."
};

const ar = {
    // General
    BAD_REQUEST: "طلب غير جيد",
    NOT_AUTHORIZED: "غير مخول",
    NOT_FOUND: "لم يتم العثور على",
    SERVER_DOWN: "تعطل الخادم",
    OUT_DATED: "خارج التاريخ",
    FAILED_TO_UPLOAD: "فشل التحميل",
    FILE_MISSING: "ملف المفقودين",
    INVALID_EMAIL: "بريد إلكتروني خاطئ",
    INVALID_PASSWORD: "يجب أن تكون كلمة المرور ٨ أحرف على الأقل",
    IS_REQUIRED: "مطلوب",
    INVALID_PERMISSION: "إذن غير صالح",
    INVALID_GENDER: "الجنس غير صالح",
    INVALID_COUNTRY_CODE: "رمز البلد غير صالح",
    INVALID_TYPE: "النوع غير موجود",
    USER_NOT_VERIFIED: "يجب عليك التحقق من بريدك الإلكتروني",
    FILE_HAS_SPECIAL_CHARACTERS: "يجب ألا يحتوي اسم الملف على حرف خاص",
    INVALID_BANNER_LOCATION: "Banner Location Invalid",
    // Role
    ROLE_EXIST: "الدور موجود",
    ROLE_DOESNT_EXIST: "الدور غير موجود",
    YOUR_ARE_NOT_AN_ADMIN: "آسف ، أنت لست مسؤول",

    //category
    CATEGORY_EXIST: "الفئة موجودة بالفعل",
    CATEGORY_EXIST_IN_ARABIC: "الفئة بالعربية موجودة بالفعل",
    CATEGORY_DOESNT_EXIST: "الفئة غير موجودة",
    CATEGORY_HAS_SUBCATEGORY: "تحتوي هذه الفئة على فئات فرعية عليك حذفها أولاً",
    CATEGORY_NAME_SPECIAL_CHARACTER: "يجب ألا يحتوي اسم الفئة على حرف خاص",

    //subCategory
    SUB_CATEGORY_EXIST: 'الفئة الفرعية موجودة بالفعل',
    SUB_CATEGORY_EXIST_IN_ARABIC: 'الفئة الفرعية بالعربية موجودة بالفعل',
    SUB_CATEGORY_DOESNT_EXIST: 'الفئة الفرعية غير موجودة',
    SUB_CATEGORY_HAS_PRODUCTS: "تحتوي هذه الفئة الفرعية على منتجات يجب عليك حذفها أولاً",
    SUB_CATEGORY_NAME_SPECIAL_CHARACTER: "يجب ألا يحتوي اسم الفئة الفرعية على حرف خاص",
    // User
    DOES_NOT_EXIST: "المستخدم غير موجود",
    WRONG_CURRENT_PASSWORD: "كلمة المرور الحالية غير صحيحة",
    USER_EXIST: "المستخدم موجود",
    USER_EMAIL_EXIST: "البريد الالكتروني موجود",
    NAME_EXIST: "الاسم موجود",
    WRONG_PASSWORD: "كلمة مرور خاطئة",
    REQUEST_EXPIRED: "طلب منتهي الصلاحية",
    PASSWORD_MUST_BE_MORE_THAN_8_CHARACTERS: "يجب أن تكون كلمة المرور أكثر من 8 أحرف",
    NAME_MUST_BE_AT_LEAST_6_CHARACTER: "يجب أن يكون الاسم على الأقل 6 أحرف",
    MISSING_DATA: "البريد الإلكتروني / معرف تسجيل الدخول من SocialLogin مفقود ، يرجى تقديم واحد منهم على الأقل",
    ONLY_IMAGES: "يمكنك فقط تحميل الصور",
    NON_ACTIVE_USER: "عذرا ، أنت غير نشط بعد الآن",

    //products
    PRODUCT_EXIST: "يوجد منتج بنفس الاسم",
    PRODUCT_DOESNT_EXIST: "المنتج غير موجود",
    PRODUCT_NAME_SPECIAL_CHARACTER: "اسم المنتج له رموز خاصة",
    PRODUCT_EXIST_IN_ARABIC: "الاسم العربي للمنتج موجود بالفعل",
    SKU_ALREADY_EXIST: "وحدة حفظ المخزون موجودة بالفعل",

    //nutrition
    NUTRITION_DOESNT_EXIST: "التغذية غير موجودة",
    //otp phoneNumber
    OTP_TOKEN: "رقم رمز OTP غير صالح ، يرجى إنشاء رقم جديد",
    //tags
    TAGS_NAME_ALREADY_EXIST: "العلامة موجودة بالفعل",
    TAG_DOESNT_EXIST: "العلامة غير موجودة",
    TAG_IN_ARABIC_ALREADY_EXIST: "اسم العلامة باللغة العربية موجود بالفعل",


    //cart
    PRODUCT_DOESNT_EXIST_IN_CART: "المنتج غير موجود في سلة التسوق الخاصة بك",
    ADD_PRODUCT_TO_CART_FIRST: "أضف هذا المنتج إلى عربة التسوق الخاصة بك أولاً",
    CART_NOT_FOUND: "عربة التسوق غير موجودة",
    CART_IS_EMPTY: "عربة التسوق فارغة",

//country
    COUNTRY_NAME_DOES_NOT_EXIST: "اسم المدينة ليس موجود",

    //list
    LIST_ALREADY_EXIST: "القائمة موجودة بالفعل",
    LIST_DOES_NOT_EXIST: "القائمة ليست موجودة",

    //priceModel
    PRICE_MODEL_DOES_NOT_EXIST: "نموذج السعر غير موجود",
    PRICE_MODEL_ALREADY_EXIST: "نموذج السعر موجود بالفعل",
    PRICE_MODEL_IN_ARABIC_ALREADY_EXIST: "نموذج السعر باللغة العربية موجود بالفعل",

    //brand
    BRAND_DOES_NOT_EXIST: "العلامة التجارية غير موجودة",
    BRAND_ALREADY_EXIST: "العلامة التجارية موجود بالفعل",
    BRAND_ALREADY_EXIST_IN_ARABIC: "اسم العلامة التجارية باللغة العربية موجود بالفعل",

    //stock
    PRODUCT_IN_STOCK_ALREADY_EXIST: "المنتج في المخزون موجود بالفعل",
    PRODUCT_IN_STOCK_DOES_NOT_EXIST: "المنتج غير موجود في المخزون",
    ITEM_OUT_OF_STOCK: "عنصر (عناصر) انتهى من المخزون",

    //order
    YOU_DONT_HAVE_ENOUGH_BALANCE: "لا يوجد رصيد كافي في حسابك",
    //address
    ADDRESS_DOES_NOT_EXIST: "يجب عليك إنشاء العنوان أولاً",
    //Order
    ORDER_DOES_NOT_EXIST: "الطلب غير موجود",
    //stripe
    STRIPE_ERROR_CREATE_CUSTOMER: "حدث خطأ ما أثناء إنشاء عميل باستخدام بوابة الدفع",
    //upload zip file
    UPLOAD_ZIP_FILE: "يجب أن يكون الملف الذي تم تحميله ملفًا مضغوطًا.",
    NO_ZIP_FILE: "لم يتم تحميل أي ملف مضغوط."
};

const isArabic = (appLocale) => {
    return appLocale === "ar";
};

const getLocalizedStringForKey = (key, locale) => {
    const appLocale = locale || "ar";
    switch (key) {
        case "BAD_REQUEST":
            return isArabic(appLocale) ? ar.BAD_REQUEST : en.BAD_REQUEST;
        case "NOT_AUTHORIZED":
            return isArabic(appLocale) ? ar.NOT_AUTHORIZED : en.NOT_AUTHORIZED;
        case "NOT_FOUND":
            return isArabic(appLocale) ? ar.NOT_FOUND : en.NOT_FOUND;
        case "OUT_DATED":
            return isArabic(appLocale) ? ar.OUT_DATED : en.OUT_DATED;
        case "FAILED_TO_UPLOAD":
            return isArabic(appLocale) ? ar.FAILED_TO_UPLOAD : en.FAILED_TO_UPLOAD;
        case "FILE_MISSING":
            return isArabic(appLocale) ? ar.FILE_MISSING : en.FILE_MISSING;
        case "ROLE_EXIST":
            return isArabic(appLocale) ? ar.ROLE_EXIST : en.ROLE_EXIST;
        case "ROLE_DOESNT_EXIST":
            return isArabic(appLocale) ? ar.ROLE_DOESNT_EXIST : en.ROLE_DOESNT_EXIST;
        case "INVALID_TYPE":
            return isArabic(appLocale) ? ar.INVALID_TYPE : en.INVALID_TYPE;
        //files
        case "FILE_HAS_SPECIAL_CHARACTERS":
            return isArabic(appLocale) ? ar.FILE_HAS_SPECIAL_CHARACTERS : en.FILE_HAS_SPECIAL_CHARACTERS
        // Users
        case "DOES_NOT_EXIST":
            return isArabic(appLocale) ? ar.DOES_NOT_EXIST : en.DOES_NOT_EXIST;
        case "USER_EXIST":
            return isArabic(appLocale) ? ar.USER_EXIST : en.USER_EXIST;
        case "USER_EMAIL_EXIST":
            return isArabic(appLocale) ? ar.USER_EMAIL_EXIST : en.USER_EMAIL_EXIST;
        case "WRONG_PASSWORD":
            return isArabic(appLocale) ? ar.WRONG_PASSWORD : en.WRONG_PASSWORD;
        case "NAME_EXIST":
            return isArabic(appLocale) ? ar.NAME_EXIST : en.NAME_EXIST;
        case "MISSING_DATA" :
            return isArabic(appLocale) ? ar.MISSING_DATA : en.MISSING_DATA
        case "WRONG_CURRENT_PASSWORD":
            return isArabic(appLocale) ? ar.WRONG_CURRENT_PASSWORD : en.WRONG_CURRENT_PASSWORD;
        case "REQUEST_EXPIRED":
            return isArabic(appLocale) ? ar.REQUEST_EXPIRED : en.REQUEST_EXPIRED;
        case "INVALID_EMAIL":
            return isArabic(appLocale) ? ar.INVALID_EMAIL : en.INVALID_EMAIL;
        case "INVALID_PASSWORD":
            return isArabic(appLocale) ? ar.INVALID_PASSWORD : en.INVALID_PASSWORD;
        case "IS_REQUIRED":
            return isArabic(appLocale) ? ar.IS_REQUIRED : en.IS_REQUIRED;
        case "INVALID_PERMISSION":
            return isArabic(appLocale)
                ? ar.INVALID_PERMISSION
                : en.INVALID_PERMISSION;
        case "NAME_MUST_BE_AT_LEAST_6_CHARACTER" :
            return isArabic(appLocale) ? ar.NAME_MUST_BE_AT_LEAST_6_CHARACTER : en.NAME_MUST_BE_AT_LEAST_6_CHARACTER
        case "USER_NOT_VERIFIED":
            return isArabic(appLocale) ? ar.USER_NOT_VERIFIED : en.USER_NOT_VERIFIED
        case "INVALID_COUNTRY_CODE":
            return isArabic(appLocale)
                ? ar.INVALID_COUNTRY_CODE
                : en.INVALID_COUNTRY_CODE;
        case "PASSWORD_MUST_BE_MORE_THAN_8_CHARACTERS":
            return isArabic(appLocale) ? ar.PASSWORD_MUST_BE_MORE_THAN_8_CHARACTERS : en.PASSWORD_MUST_BE_MORE_THAN_8_CHARACTERS
        case "NON_ACTIVE_USER":
            return isArabic(appLocale) ? ar.NON_ACTIVE_USER : en.NON_ACTIVE_USER
        case "CATEGORY_EXIST":
            return isArabic(appLocale) ? ar.CATEGORY_EXIST : en.CATEGORY_EXIST
        case "CATEGORY_EXIST_IN_ARABIC":
            return isArabic(appLocale) ? ar.CATEGORY_EXIST_IN_ARABIC : en.CATEGORY_EXIST_IN_ARABIC
        case "CATEGORY_DOESNT_EXIST" :
            return isArabic(appLocale) ? ar.CATEGORY_DOESNT_EXIST : en.CATEGORY_DOESNT_EXIST
        case "CATEGORY_HAS_SUBCATEGORY":
            return isArabic(appLocale) ? ar.CATEGORY_HAS_SUBCATEGORY : en.CATEGORY_HAS_SUBCATEGORY
        case "CATEGORY_NAME_SPECIAL_CHARACTER":
            return isArabic(appLocale) ? ar.CATEGORY_NAME_SPECIAL_CHARACTER : en.CATEGORY_NAME_SPECIAL_CHARACTER
        //subCategory
        case "SUB_CATEGORY_EXIST":
            return isArabic(appLocale) ? ar.SUB_CATEGORY_EXIST : en.SUB_CATEGORY_EXIST
        case "SUB_CATEGORY_EXIST_IN_ARABIC":
            return isArabic(appLocale) ? ar.SUB_CATEGORY_EXIST_IN_ARABIC : en.SUB_CATEGORY_EXIST_IN_ARABIC
        case "SUB_CATEGORY_DOESNT_EXIST":
            return isArabic(appLocale) ? ar.SUB_CATEGORY_DOESNT_EXIST : en.SUB_CATEGORY_DOESNT_EXIST
        case "SUB_CATEGORY_HAS_PRODUCTS":
            return isArabic(appLocale) ? ar.SUB_CATEGORY_HAS_PRODUCTS : en.SUB_CATEGORY_HAS_PRODUCTS
        case "SUB_CATEGORY_NAME_SPECIAL_CHARACTER":
            return isArabic(appLocale) ? ar.SUB_CATEGORY_NAME_SPECIAL_CHARACTER : en.SUB_CATEGORY_NAME_SPECIAL_CHARACTER
        case "SERVER_DOWN":
            return isArabic(appLocale) ? ar.SERVER_DOWN : en.SERVER_DOWN
        case "ONLY_IMAGES":
            return isArabic(appLocale) ? ar.ONLY_IMAGES : en.ONLY_IMAGES
        //roles
        case "YOUR_ARE_NOT_AN_ADMIN":
            return isArabic(appLocale) ? ar.YOUR_ARE_NOT_AN_ADMIN : en.YOUR_ARE_NOT_AN_ADMIN
        case "PRODUCT_EXIST" :
            return isArabic(appLocale) ? ar.PRODUCT_EXIST : en.PRODUCT_EXIST
        case "PRODUCT_EXIST_IN_ARABIC" :
            return isArabic(appLocale) ? ar.PRODUCT_EXIST_IN_ARABIC : en.PRODUCT_EXIST_IN_ARABIC
        case  "PRODUCT_DOESNT_EXIST":
            return isArabic(appLocale) ? ar.PRODUCT_DOESNT_EXIST : en.PRODUCT_DOESNT_EXIST
        case  "PRODUCT_NAME_SPECIAL_CHARACTER" :
            return isArabic(appLocale) ? ar.PRODUCT_NAME_SPECIAL_CHARACTER : en.PRODUCT_NAME_SPECIAL_CHARACTER
        //nutrition
        case "NUTRITION_DOESNT_EXIST":
            return isArabic(appLocale) ? ar.NUTRITION_DOESNT_EXIST : en.NUTRITION_DOESNT_EXIST
        //otp
        case "OTP_TOKEN":
            return isArabic(appLocale) ? ar.OTP_TOKEN : en.OTP_TOKEN
        case "TAG_DOESNT_EXIST":
            return isArabic(appLocale) ? ar.TAG_DOESNT_EXIST : en.TAG_DOESNT_EXIST
        case "TAG_IN_ARABIC_ALREADY_EXIST":
            return isArabic(appLocale) ? ar.TAG_IN_ARABIC_ALREADY_EXIST : en.TAG_IN_ARABIC_ALREADY_EXIST
        case "TAGS_NAME_ALREADY_EXIST":
            return isArabic(appLocale) ? ar.TAGS_NAME_ALREADY_EXIST : en.TAGS_NAME_ALREADY_EXIST
        //cart
        case "PRODUCT_DOESNT_EXIST_IN_CART" :
            return isArabic(appLocale) ? ar.PRODUCT_DOESNT_EXIST_IN_CART : en.PRODUCT_DOESNT_EXIST_IN_CART
        case "ADD_PRODUCT_TO_CART_FIRST" :
            return isArabic(appLocale) ? ar.ADD_PRODUCT_TO_CART_FIRST : en.ADD_PRODUCT_TO_CART_FIRST
        case "CART_NOT_FOUND" :
            return isArabic(appLocale) ? ar.CART_NOT_FOUND : en.CART_NOT_FOUND
        case "CART_IS_EMPTY" :
            return isArabic(appLocale) ? ar.CART_IS_EMPTY : en.CART_IS_EMPTY
        case "COUNTRY_NAME_DOES_NOT_EXIST" :
            return isArabic(appLocale) ? ar.COUNTRY_NAME_DOES_NOT_EXIST : en.COUNTRY_NAME_DOES_NOT_EXIST
        //list
        case "LIST_ALREADY_EXIST" :
            return isArabic(appLocale) ? ar.LIST_ALREADY_EXIST : en.LIST_ALREADY_EXIST
        case "LIST_DOES_NOT_EXIST":
            return isArabic((appLocale)) ? ar.LIST_DOES_NOT_EXIST : en.LIST_DOES_NOT_EXIST
        case "PRICE_MODEL_ALREADY_EXIST":
            return isArabic((appLocale)) ? ar.PRICE_MODEL_ALREADY_EXIST : en.PRICE_MODEL_ALREADY_EXIST
        case "PRICE_MODEL_DOES_NOT_EXIST":
            return isArabic((appLocale)) ? ar.PRICE_MODEL_DOES_NOT_EXIST : en.PRICE_MODEL_DOES_NOT_EXIST
        case "PRICE_MODEL_IN_ARABIC_ALREADY_EXIST":
            return isArabic((appLocale)) ? ar.PRICE_MODEL_IN_ARABIC_ALREADY_EXIST : en.PRICE_MODEL_IN_ARABIC_ALREADY_EXIST
        case "BRAND_ALREADY_EXIST":
            return isArabic((appLocale)) ? ar.BRAND_ALREADY_EXIST : en.BRAND_ALREADY_EXIST
        case "BRAND_DOES_NOT_EXIST":
            return isArabic((appLocale)) ? ar.BRAND_DOES_NOT_EXIST : en.BRAND_DOES_NOT_EXIST
        case "BRAND_ALREADY_EXIST_IN_ARABIC":
            return isArabic((appLocale)) ? ar.BRAND_ALREADY_EXIST_IN_ARABIC : en.BRAND_ALREADY_EXIST_IN_ARABIC
        case "INVALID_ADDRESS":
            return isArabic(appLocale) ? ar.INVALID_ADDRESS : en.INVALID_ADDRESS
        //stock
        case "PRODUCT_IN_STOCK_DOES_NOT_EXIST":
            return isArabic((appLocale)) ? ar.PRODUCT_IN_STOCK_DOES_NOT_EXIST : en.PRODUCT_IN_STOCK_DOES_NOT_EXIST
        case "PRODUCT_IN_STOCK_ALREADY_EXIST":
            return isArabic(appLocale) ? ar.PRODUCT_IN_STOCK_ALREADY_EXIST : en.PRODUCT_IN_STOCK_ALREADY_EXIST
        case "YOU_DONT_HAVE_ENOUGH_BALANCE":
            return isArabic(appLocale) ? ar.YOU_DONT_HAVE_ENOUGH_BALANCE : en.YOU_DONT_HAVE_ENOUGH_BALANCE
        case "ITEM_OUT_OF_STOCK":
            return isArabic(appLocale) ? ar.ITEM_OUT_OF_STOCK : en.ITEM_OUT_OF_STOCK
        //address
        case "ADDRESS_DOES_NOT_EXIST":
            return isArabic(appLocale) ? ar.ADDRESS_DOES_NOT_EXIST : en.ADDRESS_DOES_NOT_EXIST
        case "SKU_ALREADY_EXIST":
            return isArabic(appLocale) ? ar.SKU_ALREADY_EXIST : en.SKU_ALREADY_EXIST
        //Order
        case "ORDER_DOES_NOT_EXIST":
            return isArabic(appLocale) ? ar.ORDER_DOES_NOT_EXIST : en.ORDER_DOES_NOT_EXIST
        case "INVALID_BANNER_LOCATION":
            return isArabic(appLocale) ? ar.INVALID_BANNER_LOCATION : en.INVALID_BANNER_LOCATION
        //stripe
        case "STRIPE_ERROR_CREATE_CUSTOMER":
            return isArabic(appLocale) ? ar.STRIPE_ERROR_CREATE_CUSTOMER : en.STRIPE_ERROR_CREATE_CUSTOMER
        //upload zip file
        case "UPLOAD_ZIP_FILE":
            return isArabic(appLocale) ? ar.UPLOAD_ZIP_FILE : en.UPLOAD_ZIP_FILE
        case "NO_ZIP_FILE":
            return isArabic(appLocale) ? ar.NO_ZIP_FILE : en.NO_ZIP_FILE
        default:
            return "";
    }
};

module.exports = {
    getLocalizedStringForKey,
};
