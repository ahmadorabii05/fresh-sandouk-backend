const checkIfPriceValidForStripe = (value) => {
    const finalPrice = value * 100;
    // return finalPrice >= 200 // 2 aed is the minimum order
    return finalPrice >= 15000 // 150 aed is the minimum order
}

module.exports = {
    checkIfPriceValidForStripe
}