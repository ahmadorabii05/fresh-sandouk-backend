const AWS = require("aws-sdk");
require('dotenv').config();
AWS.config.httpOptions.timeout = 0;


const s3Client = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: "eu-west-2",

});



const deleteFileInAWS =async (bucketName, fileName) => {
    let params = {
        Bucket: bucketName,
        Key: fileName
    };
    await s3Client.deleteObject(params).promise();
}

module.exports = {
    deleteFileInAWS
}