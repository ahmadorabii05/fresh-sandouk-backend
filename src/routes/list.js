const express = require("express");
const router = express.Router();
const {partial} = require("lodash");
const {tokenSecurity} = require("../securityLayer/tokenSecurity");
const {createListValidator, editListNameValidator, addProductToListValidator, deleteListValidator} = require("../validationLayer/validator/listValidator");
const {listController} = require("../controllers/list");
const {createListDataHandler, editListNameDataHandler, getListDataHandler, addProductToListDataHandler,
    deleteListDataHandler, getListByIdDataHandler
} = require("../dataHandlerLayer/listDataHandler");
const {createListPresenter, editListNamePresenter, getListPresenter, addProductToListPresenter, deleteListPresenter,
    getListIdPresenter
} = require("../presentationLayer/listPresenter");
const {checkIfListForUser} = require("../helperKit/helperKit");

router.post(
    "/",
    tokenSecurity,
    createListValidator,
    createListDataHandler,
    createListPresenter,
    partial(listController)
)
router.post(
    "/add-product-to-list",
    tokenSecurity,
    addProductToListValidator,
    addProductToListDataHandler,
    addProductToListPresenter,
    partial(listController)
)
router.patch(
    '/edit-list',
    tokenSecurity,
    editListNameValidator,
    editListNameDataHandler,
    editListNamePresenter,
    partial(listController)
)

router.get(
    "/get-lists",
    tokenSecurity,
    getListDataHandler,
    getListPresenter,
    partial(listController)
)
router.get(
    "/:id",
    tokenSecurity,
    getListByIdDataHandler,
    getListIdPresenter,
    partial(listController)
)

router.patch(
    "/",
    tokenSecurity,
    deleteListValidator,
    checkIfListForUser,
    deleteListDataHandler,
    deleteListPresenter,
    partial(listController)
)


module.exports = router;
