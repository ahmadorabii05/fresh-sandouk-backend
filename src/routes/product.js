const express = require("express");
const router = express.Router();
const { partial } = require("lodash");
const { tokenSecurity, tokenSecurityMightExist } = require("../securityLayer/tokenSecurity");
const { isAdmin } = require("../services/userService");
const { isActive, checkIfAllImagesPass, checkIfAllImagesPassOnUpdate } = require("../helperKit/helperKit");
const { productController } = require("../controllers/product");
const {
    createProductValidator, deleteNutritionInProductValidator, deleteProductByIdsValidator,
    updateProductByIdValidator, createBulkProductValidator
} = require("../validationLayer/validator/productValidator");
const {
    createProductDataHandler, deleteNutritionInProductDataHandler, deleteProductByIdsDataHandler,
    updateProductByIdDataHandler, getProductByIdDataHandler, getProductsPaginatedDataHandler,
    getMyFavoriteProductsDataHandler, deleteProductByIdDataHandler, getProductsBySearchPaginatedDataHandler,
    createBulkProductDataHandler,
    bulkUpdateProductDataHandler,
    forceDeleteProductByIdDataHandler
} = require("../dataHandlerLayer/productDataHandler");
const multer = require("multer");
const {
    createProductPresenter, deleteNutritionInProductPresenter, deleteProductByIdsPresenter,
    updateProductByIdPresenter, getProductByIdPresenter, getAllProductsPaginatedPresenter,
    getMyFavoriteProductsPresenter, deleteProductByIdPresenter, getProductsBySearchPaginatedPresenter,
    createBulkProductPresenter,
    bulkUpdateProductPresenter
} = require("../presentationLayer/productPresenter");
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });


router.post(
    "/",
    tokenSecurity,
    isAdmin,
    isActive,
    upload.array("files", 5), // Change "file" to "files" and specify the maximum number of files allowed (e.g., 2)
    createProductValidator,
    checkIfAllImagesPass,
    createProductDataHandler,
    createProductPresenter,
    partial(productController)
);


router.post(
    "/create-bulk-product",
    tokenSecurity,
    isAdmin,
    isActive,
    // upload.array("files", 5), // Change "file" to "files" and specify the maximum number of files allowed (e.g., 2)
    createBulkProductValidator,
    // checkIfAllImagesPass,
    createBulkProductDataHandler,
    createBulkProductPresenter,
    partial(productController)
);

//delete Nutrition
router.patch(
    "/delete-nutrition",
    tokenSecurity,
    isAdmin,
    isActive,
    deleteNutritionInProductValidator,
    deleteNutritionInProductDataHandler,
    deleteNutritionInProductPresenter,
    partial(productController)
)

//delete Product
router.patch(
    "/delete-products",
    tokenSecurity,
    isAdmin,
    isActive,
    deleteProductByIdsValidator,
    deleteProductByIdsDataHandler,
    deleteProductByIdsPresenter,
    partial(productController)
)

//delete Product
router.delete(
    "/:id",
    tokenSecurity,
    isAdmin,
    isActive,
    deleteProductByIdDataHandler,
    deleteProductByIdPresenter,
    partial(productController)
)

router.delete(
    "/force-delete/:id",
    tokenSecurity,
    isAdmin,
    isActive,
    forceDeleteProductByIdDataHandler,
    deleteProductByIdPresenter,
    partial(productController)
)

router.patch(
    "/update-product/:id",
    tokenSecurity,
    isAdmin,
    isActive,
    upload.array("files"),
    updateProductByIdValidator,
    checkIfAllImagesPassOnUpdate,
    updateProductByIdDataHandler,
    updateProductByIdPresenter,
    partial(productController)
)


router.post(
    "/update-bulk-product",
    tokenSecurity,
    isAdmin,
    isActive,
    bulkUpdateProductDataHandler,
    bulkUpdateProductPresenter,
    partial(productController)
);

router.get(
    "/",
    tokenSecurityMightExist,
    getProductsPaginatedDataHandler,
    getAllProductsPaginatedPresenter,
    partial(productController)
)

router.get(
    "/my-favorite-products",
    tokenSecurity,
    getMyFavoriteProductsDataHandler,
    getMyFavoriteProductsPresenter,
    partial(productController)
)

router.get(
    "/get-by-search",
    tokenSecurityMightExist,
    getProductsBySearchPaginatedDataHandler,
    getProductsBySearchPaginatedPresenter,
    partial(productController)

)
router.get(
    "/:id",
    tokenSecurityMightExist,
    getProductByIdDataHandler,
    getProductByIdPresenter,
    partial(productController)
)
// const productQueries = require("../dataBaseLayer/queries/product/productQueries");

// router.patch(
//     "/update-is-deleted",
//     // tokenSecurity,
//     async (req, res, next) => {
//         return await productQueries.updateAllToIsDeletedFalse();
//     },
// )



module.exports = router;
