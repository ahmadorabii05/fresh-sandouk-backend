const express = require("express");
const router = express.Router();
const {partial} = require("lodash");
const {tokenSecurity, tokenSecurityMightExist} = require("../securityLayer/tokenSecurity");
const {isAdmin} = require("../services/userService");
const {brandController} = require("../controllers/brand");
const {getAllBrandsDataHandler, createBrandDataHandler, getBrandByIdDataHandler, updateBrandByIdDataHandler,
    deleteBrandDataHandler, deleteBrandByIdDataHandler, createBulkBrandDataHandler, getBrandBySearchPaginatedDataHandler
} = require("../dataHandlerLayer/brandDataHandler");

const {getAllBrandsPresenter, createBrandPresenter, getBrandByIdPresenter, updateBrandByIdPresenter,
    deleteBrandPresenter, getBrandBySearchPaginatedPresenter, createBulkBrandPresenter

} = require("../presentationLayer/brandPresenter");
const {createBrandValidator, updateBrandByIdValidator, deleteBrandValidator, createBulkBrandValidator} = require("../validationLayer/validator/brandValidation");
const {isActive} = require("../helperKit/helperKit");
const multer = require("multer");
const storage = multer.memoryStorage();
const upload = multer({storage: storage});
router.get(
    "/",
    tokenSecurity,
    isAdmin,
    getAllBrandsDataHandler,
    getAllBrandsPresenter,
    partial(brandController)
)

router.post(
    "/",
    tokenSecurity,
    isAdmin,
    isActive,
    upload.single("file"),
    createBrandValidator,
    createBrandDataHandler,
    createBrandPresenter,
    partial(brandController)
)

router.post(
    "/create-bulk-brand",
    tokenSecurity,
    isAdmin,
    isActive,
    // upload.single("file"),
    createBulkBrandValidator,
    createBulkBrandDataHandler,
    createBulkBrandPresenter,
    partial(brandController)
)

router.get(
    "/get-by-search",
    tokenSecurityMightExist,
    getBrandBySearchPaginatedDataHandler,
    getBrandBySearchPaginatedPresenter,
    partial(brandController)

)

router.get(
    "/:id",
    tokenSecurity,
    isAdmin,
    getBrandByIdDataHandler,
    getBrandByIdPresenter,
    partial(brandController)
)

router.patch(
    "/update-brand/:id",
    tokenSecurity,
    isAdmin,
    upload.single("file"),
    updateBrandByIdValidator,
    updateBrandByIdDataHandler,
    updateBrandByIdPresenter,
    partial(brandController),
)

router.patch(
    "/",
    tokenSecurity,
    isAdmin,
    deleteBrandValidator,
    deleteBrandDataHandler,
    deleteBrandPresenter,
    partial(brandController),

)

router.delete(
    "/:id",
    tokenSecurity,
    isAdmin,
    isActive,
    deleteBrandByIdDataHandler,
    deleteBrandPresenter,
    partial(brandController)
)

module.exports = router;
