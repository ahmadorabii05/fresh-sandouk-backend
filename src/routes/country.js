const express = require("express");
const router = express.Router();
const {partial} = require("lodash");
const {tokenSecurity} = require("../securityLayer/tokenSecurity");
const {isAdmin} = require("../services/userService");
const {uploadCountriesDataHandler, getAllCountriesDataHandler} = require("../dataHandlerLayer/countriesDataHandler");
const {uploadCountryPresenter, getAllCountriesPresenter} = require("../presentationLayer/countryPresenter");
const {countryController} = require("../controllers/country");


router.post(
    '/',
    tokenSecurity,
    isAdmin,
    uploadCountriesDataHandler,
    uploadCountryPresenter,
    partial(countryController)
)

router.get(
    "/",
    getAllCountriesDataHandler,
    getAllCountriesPresenter,
    partial(countryController)
)

module.exports = router;
