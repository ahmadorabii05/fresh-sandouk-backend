const express = require("express");
const router = express.Router();
const {partial} = require("lodash");
const { authController } = require("../controllers/auth");
const {
    loginDataHandler,
    forgotPasswordDataHandler,
    changePasswordDataHandler,
    socialLoginDataHandler,
    signUpDataHandler,
    accountVerificationDataHandler,
    verifyCodeDataHandler,
} = require("../dataHandlerLayer/authDataHandler");
const {
    loginPresenter,
    logoutPresenter,
    forgotPasswordPresenter,
    changePasswordPresenter,
    socialLoginPresenter,
    signUpPresenter,
    accountVerificationPresenter, verifyCodePresenter
} = require("../presentationLayer/authPresenter");
const {
    loginSecurity,
    logoutSecurity,
    socialLoginSecurity,
    signUpSecurity,
} = require("../securityLayer/authSecurity");
const { roleSecurity } = require("../securityLayer/roleSecurity");
const { tokenSecurity } = require("../securityLayer/tokenSecurity");
const {
    loginValidator,
    forgotPasswordValidator,
    changePasswordValidator,
    socialLoginValidator,
    signUpValidator,
    accountVerificationValidator,verifyCodeValidator
} = require("../validationLayer/validator/authValidator");
const {isVerified} = require("../services/userService");
const {resetPasswordValidator} = require("../validationLayer/validator/resetPasswordValidator");
const {resetPasswordDataHandler} = require("../dataHandlerLayer/resetPasswordDataHandler");


router.post(
    "/login-admin",
    loginValidator,
    // isVerified,
    loginDataHandler,
    roleSecurity,
    loginSecurity,
    loginPresenter,
    partial(authController)
);
router.post(
    "/login",
    loginValidator,
    // isVerified,
    loginDataHandler,
    loginSecurity,
    loginPresenter,
    partial(authController)
);



router.post(
    "/sign-up",
    signUpValidator,
    signUpDataHandler,
    signUpSecurity,
    signUpPresenter,
    partial(authController)
);

router.post(
    "/social-login",
    socialLoginValidator,
    socialLoginDataHandler,
    socialLoginSecurity,
    socialLoginPresenter,
    partial(authController)
);

router.post(
    "/forgot-password",
    forgotPasswordValidator,
    forgotPasswordDataHandler,
    forgotPasswordPresenter,
    partial(authController)
);

router.post(
    "/logout",
    tokenSecurity,
    logoutSecurity,
    logoutPresenter,
    partial(authController)
);
//new
router.post(
    "/verify-code",
    verifyCodeValidator,
    verifyCodeDataHandler,
    verifyCodePresenter,
    partial(authController)
)
router.post(
    "/change-password",
    tokenSecurity,
    changePasswordValidator,
    changePasswordDataHandler,
    changePasswordPresenter,
    partial(authController)
);

router.post(
    "/reset-password",
    resetPasswordValidator,
    resetPasswordDataHandler,
    changePasswordPresenter, //same as change password presenter
    partial(authController)
)

router.post(
    "/verify",
    accountVerificationValidator,
    accountVerificationDataHandler,
    accountVerificationPresenter,
    partial(authController)
);


module.exports = router;
