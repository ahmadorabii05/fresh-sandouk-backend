const express = require("express");
const router = express.Router();

const { partial } = require("lodash");
const { tokenSecurity, tokenSecurityMightExist } = require("../securityLayer/tokenSecurity");
const { isAdmin } = require("../services/userService");
const { subCategoryController } = require("../controllers/subCategory");
const { createSubCategoryValidator, deleteSubCategoryByIdValidator, updateSubCategoryByIdValidator,
    createBulkSubCategoryValidator
} = require("../validationLayer/validator/subCategoryValidator");
const { createSubCategoryDataHandler, getAllSubCategoryPaginatedDataHandler, deleteSubCategoryByIdDataHandler,
    getSubCategoryByIdDataHandler, updateSubCategoryByIdDataHandler, deleteSingleSubCategoryByIdDataHandler,
    getSubCategoryBySearchPaginatedDataHandler, createBulkSubCategoryDataHandler
} = require("../dataHandlerLayer/subCategoryDataHandler");
const { CreateSubCategoryPresenter, getAllSubCategoryPaginatedPresenter, deleteSubCategoryByIdsPresenter,
    getSubCategoryByIdPresenter, updateSubCategoryByIdPresenter, deleteSingleSubCategoryByIdsPresenter,
    getSubCategoryBySearchPaginatedPresenter, CreateBulkSubCategoryPresenter
} = require("../presentationLayer/subCategoryPresenter");
const multer = require("multer");
const { isActive } = require("../helperKit/helperKit");
const { categoryController } = require("../controllers/category");
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });


router.post(
    "/",
    tokenSecurity,
    isAdmin,
    isActive,
    upload.single("file"),
    createSubCategoryValidator,
    createSubCategoryDataHandler,
    CreateSubCategoryPresenter,
    partial(subCategoryController)
);

router.post(
    "/bulk-create-subcategory",
    tokenSecurity,
    isAdmin,
    isActive,
    // upload.single("file"),
    createBulkSubCategoryValidator,
    createBulkSubCategoryDataHandler,
    CreateBulkSubCategoryPresenter,
    partial(subCategoryController)
);

router.get(
    "/",
    tokenSecurityMightExist,
    getAllSubCategoryPaginatedDataHandler,
    getAllSubCategoryPaginatedPresenter,
    partial(subCategoryController)
)

router.get(
    "/get-by-search",
    tokenSecurityMightExist,
    getSubCategoryBySearchPaginatedDataHandler,
    getSubCategoryBySearchPaginatedPresenter,
    partial(subCategoryController)

)

router.get(
    "/:id",
    tokenSecurityMightExist,
    getSubCategoryByIdDataHandler,
    getSubCategoryByIdPresenter,
    partial(subCategoryController)
)

router.patch(
    "/delete-sub-category",
    tokenSecurity,
    isAdmin,
    isActive,
    deleteSubCategoryByIdValidator,
    deleteSubCategoryByIdDataHandler,
    deleteSubCategoryByIdsPresenter,
    partial(subCategoryController)
)

router.delete(
    "/:id",
    tokenSecurity,
    isAdmin,
    isActive,
    deleteSingleSubCategoryByIdDataHandler,
    deleteSingleSubCategoryByIdsPresenter,
    partial(subCategoryController)
)

router.patch(
    "/:id",
    tokenSecurity,
    isAdmin,
    isActive,
    upload.single("file"),
    updateSubCategoryByIdValidator,
    updateSubCategoryByIdDataHandler,
    updateSubCategoryByIdPresenter,
    partial(categoryController)
)

module.exports = router;
