const authRoute = require("./auth");
const roleRoute = require("./role");
const userRoute = require("./user");
const categoryRoute = require("./category");
const subCategoryRoute = require("./subcategory");
const productRoute = require("./product");
const tagsRoute = require("./tags");
const favoriteRoute = require("./favorite");
const cartRoute = require("./cart");
const listRoute = require("./list");
const countryRoute = require("./country");
const priceRoute = require("./price");
const brandRoute = require("./brand");
const bannerRoute = require("./banner");
const addressRoute = require("./address");
const stockRoute = require("./stock")
const orderRoute = require("./order")
const inventoryRoute = require("./inventory")
const salesRoute = require("./sale")
const statisticsRoute = require("./statistics")
const stripeRoute = require("./stripe");
const uploadZipFileRoute = require("./uploadZip")
const slotsRoute = require("./slot");

const router = (app) => {
    app.use("/auth", authRoute);
    app.use("/roles", roleRoute);
    app.use("/users", userRoute);
    app.use("/category", categoryRoute);
    app.use("/subCategory", subCategoryRoute);
    app.use("/products", productRoute);
    app.use("/tags", tagsRoute);
    app.use("/favorite", favoriteRoute);
    app.use("/cart", cartRoute);
    app.use("/list", listRoute);
    app.use("/country", countryRoute);
    app.use("/price", priceRoute);
    app.use("/brand", brandRoute);
    app.use("/banner", bannerRoute);
    app.use("/address", addressRoute);
    app.use("/stock", stockRoute);
    app.use("/order", orderRoute);
    app.use("/inventory", inventoryRoute);
    app.use("/sale", salesRoute);
    app.use("/statistics", statisticsRoute);
    app.use("/stripe", stripeRoute);
    app.use("/upload-zip-file", uploadZipFileRoute)
    // app.use("/slots", slotsRoute);


};

module.exports = router;
