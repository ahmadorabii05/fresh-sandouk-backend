const express = require("express");
const router = express.Router();
const {partial} = require("lodash");
const {tokenSecurity} = require("../securityLayer/tokenSecurity");
const {isAdmin} = require("../services/userService");
const {getInventoryReportDataHandler} = require("../presentationLayer/inventoryDataHandler");
const {getInventoryReportPresenter} = require("../dataHandlerLayer/inventoryPresenter");
const {inventoryController} = require("../controllers/inventory");


router.get(
    "/inventory-report",
    tokenSecurity,
    isAdmin,
    getInventoryReportDataHandler,
    getInventoryReportPresenter,
    partial(inventoryController)
)

module.exports = router