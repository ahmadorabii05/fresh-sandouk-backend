const express = require("express");
const router = express.Router();
const {partial} = require("lodash");
const {tokenSecurity} = require("../securityLayer/tokenSecurity");
const {isAdmin} = require("../services/userService");
const {getAllSalesDataHandler} = require("../presentationLayer/salesDataHandler");
const {getAllSalesPresenter} = require("../presentationLayer/salesPresenter");
const {salesController} = require("../controllers/sales");


router.get(
    "/sales-report",
    tokenSecurity,
    isAdmin,
    getAllSalesDataHandler,
    getAllSalesPresenter,
    partial(salesController)
)

module.exports = router