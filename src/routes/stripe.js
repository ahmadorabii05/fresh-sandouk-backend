const express = require("express");
const router = express.Router();
const {partial, isEmpty} = require("lodash");
const {tokenSecurity} = require("../securityLayer/tokenSecurity");
const {createCheckoutSheetDataHandler, createRefundDataHandler} = require("../dataHandlerLayer/stripeDataHandler");
const {createCheckoutSheetPresenter, createRefundPresenter} = require("../presentationLayer/stripePresenter");
const {stripeController} = require("../controllers/stripe");
require("dotenv").config();
let endpointSecret;
endpointSecret = `${process.env.SECRET_ENDPOINT}`;
const Stripe = require('stripe');
const {
    getCartInfoForStripe,
    stripeValidationBeforeCheckout,
    generateKeyOrder,
    updateStockToAddQuantitySold
} = require("../helperKit/helperKit");
const {ProductResponseModel} = require("../presentationLayer/models/responseModel/products/ProductResponseModel");
const {ErrorStatus} = require("../constants/error/errorStatus");
const {prepareErrorResponse} = require("../errorHandlerLayer/errorHandler");
const {extractLanguageFromRequest} = require("../dataHandlerLayer/common/LanguageHeaderHandler");
const stripe = Stripe(process.env.STRIPE_KEY);
const OrderQueries = require("../dataBaseLayer/queries/order/OrderQueries")
const CartQueries = require("../dataBaseLayer/queries/cart/cartQueries")
const {OrderResponseModel} = require("../presentationLayer/models/responseModel/order/OrderResponseModel");
const {createOrderValidator} = require("../validationLayer/validator/orderValidator");
const AddressQueries = require("../dataBaseLayer/queries/address/addressQueries");
const zlib = require("zlib");
const {checkIfPriceValidForStripe} = require("../helperKit/stripeKit");
const {createRefundValidator} = require("../validationLayer/validator/stripeValidator");
const {updateStockDataHandler} = require("../dataHandlerLayer/stockDataHandler");
router.post(
    "/",
    tokenSecurity,
    createOrderValidator,
    createCheckoutSheetDataHandler,
    createCheckoutSheetPresenter,
    partial(stripeController)
)

router.post('/create-checkout-url', tokenSecurity, createOrderValidator, async (req, res) => {
    try {
        const userId = req.userFromToken._id;
        await stripeValidationBeforeCheckout(userId, req, res);
        const productInfo = await getCartInfoForStripe(req, res, userId);
        let addressId;

        const test = productInfo.productsInCart.map(item => {
            if (item?.name !== "undefined" || item?.name !== undefined || !isEmpty(item.name)) {
                return new ProductResponseModel(item, req);
            }
        });
	if (!checkIfPriceValidForStripe(productInfo.totalPrice)) {
            return res
                .status(ErrorStatus.BAD_REQUEST.code)
                .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, 'Your order amount must be atleast 150 AED', extractLanguageFromRequest(req)));
        } else {
            const getAddress = await AddressQueries.getAddressByUser(userId);

            if (!isEmpty(getAddress)) {

                const getIfThereIsDefaultAddress = await AddressQueries.getIfIsDefaultExist(
                    userId
                );
                if (!isEmpty(getIfThereIsDefaultAddress)) {
                    // Ensure that req.requestModel is defined before setting properties on it
                    addressId = getIfThereIsDefaultAddress._id;
                } else {
                    return res
                        .status(ErrorStatus.YOU_SHOULD_CREATE_ADDRESS_FIRST.code)
                        .json(
                            prepareErrorResponse(
                                ErrorStatus.YOU_SHOULD_CREATE_ADDRESS_FIRST,
                                null,
                                extractLanguageFromRequest(req)
                            )
                        );
                }
            } else {
                return res
                    .status(ErrorStatus.ADDRESS_DOES_NOT_EXIST.code)
                    .json(
                        prepareErrorResponse(
                            ErrorStatus.ADDRESS_DOES_NOT_EXIST,
                            null,
                            extractLanguageFromRequest(req)
                        )
                    );
            }
            const line_items = test.map(item => {
                const imageUrl = item.imageUrls?.length > 0 ? item.imageUrls[0].imageUrl : [];
                return {
                    price_data: {
                        currency: 'aed',
                        product_data: {
                            name: item.name,
                            images: [imageUrl],
                            description: item.description,
                            metadata: {
                                id: item?.id.toString(),

                            }
                        },
                        unit_amount: parseInt(item?.finalPriceAfterDiscount * 100),
                    },
                    quantity: parseInt(item?.quantityInCart),
                };
            });
            if (line_items.length <= 0) {
                return res
                    .status(ErrorStatus.CART_IS_EMPTY.code)
                    .json(prepareErrorResponse(ErrorStatus.CART_IS_EMPTY, null, extractLanguageFromRequest(req)));

            }
            // Compress the JSON data
            const jsonString = JSON.stringify(req.requestModel.products);
            const compressedData = zlib.deflateSync(jsonString).toString('base64');
            const session = await stripe.checkout.sessions.create({
                line_items: line_items,
                payment_intent_data: {
                    metadata: {
                        userId: userId.toString(), // here you can set the metadata
                        // requestData:req.requestModel
                        products: compressedData,
                        scheduledDate: req?.body?.scheduledDate,
                        scheduledTime: req?.body?.scheduledTime,
                        deliveryInstruction: req.body?.deliveryInstruction || "",
                        packingInstruction: req.body?.packingInstruction || "",
                        addressId: addressId.toString()
                    },
                },
                mode: 'payment',
                success_url: `${process.env.FRONTEND_BASEURL}/myshopcart?redirect_status=success`,
                cancel_url: `${process.env.FRONTEND_BASEURL}/myshopcart?redirect_status=cancel`,
            });
            return res.status(200).json({url: session.url});
        }

    } catch (error) {
        return res
            .status(ErrorStatus.SERVER_DOWN.code)
            .json(prepareErrorResponse(ErrorStatus.SERVER_DOWN, null, extractLanguageFromRequest(req)));
    }
});

router.post('/webhook', express.raw({type: 'application/json'}), async (request, response) => {
    let event;
    try {
        // Use your actual webhook endpoint secret here

        const header = stripe.webhooks.generateTestHeaderString({
            payload: JSON.stringify(request.body),
            secret: endpointSecret,
        });

        event = stripe.webhooks.constructEvent(JSON.stringify(request.body), header, endpointSecret);
    } catch (err) {
        response.status(400).send(`Webhook Error: ${err.message}`);
        return;
    }
    // Handle the event
    switch (event.type) {
        case 'payment_intent.succeeded':
            const paymentIntentSucceeded = event.data.object;
            const getUserCart = await CartQueries.getUserCartWithoutPopulate(paymentIntentSucceeded.metadata.userId);
            // const getTotalPriceFromCart = await CartQueries.findUserCart(userId);
            const keyOrder = await generateKeyOrder();
            const Order = {
                createIdFromStripe: event.data.object.id,
                addressId: paymentIntentSucceeded.metadata.addressId,
                userId: paymentIntentSucceeded.metadata.userId,
                products: JSON.parse(zlib.inflateSync(Buffer.from(paymentIntentSucceeded.metadata.products, 'base64')).toString()),
                scheduledDate: paymentIntentSucceeded.metadata.scheduledDate,
                scheduledTime: paymentIntentSucceeded.metadata.scheduledTime,
                deliveryInstruction: paymentIntentSucceeded.metadata.deliveryInstruction,
                packingInstruction: paymentIntentSucceeded.metadata.packingInstruction,
                keyOrder: keyOrder,
            }

            const createOrder = await OrderQueries.createOrder(Order);
            // Remove from stock and add to quantity sold
            await updateStockToAddQuantitySold(getUserCart.productIds);
            await CartQueries.removeAllProductAfterOrder(paymentIntentSucceeded.metadata.userId);
            // await UserQueries.deductPriceFromWallet(userId, getTotalPriceFromCart[0].totalPrice)
            const newOrder = await OrderQueries.getOrderById(createOrder._id);
            const responseOrder = new OrderResponseModel(newOrder, request);
            return response.status(200).json({
                responseOrder
            })

        case "payment_intent.created":
            const paymentIntentCreate = event.data.object;
            break;
        case "payment_intent.payment_failed":
            // Handle the payment failure here
            const paymentFailedData = event.data.object;

            // Check if there is an error message in paymentFailedData
            if (paymentFailedData.error) {
                // Return the specific error message
                return response.status(400).json({
                    error: paymentFailedData.error.message,
                });
            } else {
                // If there is no specific error message, you can provide a generic message
                return response.status(400).json({
                    error: "Payment failed",
                });
            }
        // ... handle other event types
        case "charge.refund.updated":
            await updateStockDataHandler(event.data.object.payment_intent,event.data.object.amount);
            break;
        default:
            console.log(`Unhandled event type ${event.type}`);
    }

    // Return a 200 response to acknowledge receipt of the event
    response.send();
});

router.post(
    "/refund",
    tokenSecurity,
    createRefundValidator,
    createRefundDataHandler,
    createRefundPresenter,
    partial(stripeController)
)
module.exports = router;
