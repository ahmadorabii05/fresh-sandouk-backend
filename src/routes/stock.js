const express = require("express");
const router = express.Router();
const {partial} = require("lodash");
const {tokenSecurity} = require("../securityLayer/tokenSecurity");
const {isAdmin} = require("../services/userService");
const {
} = require("../dataHandlerLayer/brandDataHandler");
const {
} = require("../presentationLayer/brandPresenter");
const {stockController} = require("../controllers/stock");
const {createProductInStockValidator, updateProductInStockValidator} = require("../validationLayer/validator/stockValidator");
const {createProductInStockPresenter, getProductInStockByIdPresenter,
} = require("../presentationLayer/StockPresenter");
const {createProductInStockDataHandler, getProductInStockByIdDataHandler, updateProductInStockDataHandler,
    updateStockDataHandler,
} = require("../dataHandlerLayer/stockDataHandler");
const {isActive} = require("../helperKit/helperKit");
const {

} = require("../validationLayer/validator/brandValidation");



router.post(
    "/",
    tokenSecurity,
    isAdmin,
    createProductInStockValidator,
    createProductInStockDataHandler,
    createProductInStockPresenter,
    partial(stockController)
)

router.patch(
    "/update-stock",
    tokenSecurity,
    updateStockDataHandler,
    // updateStockPresenter,
    partial(stockController)
)

router.patch(
    "/:id",
    tokenSecurity,
    isAdmin,
    isActive,
    updateProductInStockValidator,
    updateProductInStockDataHandler,
    createProductInStockPresenter,
    partial(stockController)
)

router.get(
    "/:id",
    tokenSecurity,
    isAdmin,
    getProductInStockByIdDataHandler,
    getProductInStockByIdPresenter,
    partial(stockController)
)





module.exports = router;
