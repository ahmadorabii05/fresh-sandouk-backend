const express = require("express");
const router = express.Router();
const {partial} = require("lodash");
const {tokenSecurity} = require("../securityLayer/tokenSecurity");
const {getAllAddressDataHandler, createAddressDataHandler, getAddressByIdDataHandler, updateAddressByIdDataHandler,
    deleteAddressDataHandler
} = require("../dataHandlerLayer/addressDataHandler");
const {getAllAddressPresenter, createAddressPresenter, getAddressByIdPresenter, updateAddressByIdPresenter,
    deleteAddressPresenter
} = require("../presentationLayer/addressPresenter");
const {addressController}= require("../controllers/address")
const {createAddressValidator, updateAddressByIdValidator, deleteAddressValidator} = require("../validationLayer/validator/addressValidator");

router.get(
    "/",
    tokenSecurity,
    getAllAddressDataHandler,
    getAllAddressPresenter,
    partial(addressController)
)

router.post(
    "/",
    tokenSecurity,
    createAddressValidator,
    createAddressDataHandler,
    createAddressPresenter,
    partial(addressController)
)

router.get(
    "/:id",
    tokenSecurity,
    getAddressByIdDataHandler,
    getAddressByIdPresenter,
    partial(addressController)
)


router.patch(
    "/update-address/:id",
    tokenSecurity,
    updateAddressByIdValidator,
    updateAddressByIdDataHandler,
    updateAddressByIdPresenter,
    partial(addressController),
)

router.patch(
    "/",
    tokenSecurity,
    deleteAddressValidator,
    deleteAddressDataHandler,
    deleteAddressPresenter,
    partial(addressController),
)






module.exports = router;