const express = require("express");
const router = express.Router();
const { partial } = require("lodash");
const { tokenSecurity, tokenSecurityMightExist } = require("../securityLayer/tokenSecurity");
const { stockController } = require("../controllers/stock");
const { createOrderValidator } = require("../validationLayer/validator/orderValidator");
const { createOrderDataHandler, getOrderByIdDataHandler, getOrdersBySearchPaginatedDataHandler, editOrderQuantityFieldDataHandler, getTimeSlotsDataHandler, editOrderStatusDataHandler } = require("../dataHandlerLayer/orderDataHandler");
const { createOrderPresenter, getOrdersBySearchPaginatedPresenter } = require("../presentationLayer/orderPresenter");
const { checkIfTotalPriceInCartLessThanWallet, isActive } = require("../helperKit/helperKit");
const { invoiceController } = require("../controllers/invoice");
const { getAllInvoiceReportDataHandler } = require("../dataHandlerLayer/invoiceDataHandler");
const { getAllInvoiceReportPresenter, getOrderByIdPresenter, editOrderQuantityPresenter, getTimeSlotPresenter, editOrderStatusPresenter } = require("../presentationLayer/invoicePresenter");
const {getUserByIdWithOrderDataHandler} = require("../dataHandlerLayer/userDataHandler");
const {getUserByIdWithOrderPresenter} = require("../presentationLayer/userPresenter");
const {userController} = require("../controllers/user");
// const {isAdmin} = require("../services/userService");
// const {getAllOrdersPerUserDataHandler} = require("../dataHandlerLayer/getAllOrdersPerUserDataHandler");


router.get(
    "/time-slots",
    getTimeSlotsDataHandler,
    getTimeSlotPresenter,
    partial(invoiceController)
)

router.post(
    "/",
    tokenSecurity,
    createOrderValidator,
    checkIfTotalPriceInCartLessThanWallet,
    createOrderDataHandler,
    createOrderPresenter,
    partial(stockController)
)


router.get(
    "/invoice-report",
    tokenSecurity,
    getAllInvoiceReportDataHandler,
    getAllInvoiceReportPresenter,
    partial(invoiceController)
)

router.get(
    "/get-by-search",
    tokenSecurityMightExist,
    getOrdersBySearchPaginatedDataHandler,
    getOrdersBySearchPaginatedPresenter,
    partial(invoiceController)

)
// router.get(
//     "/get-all-orders",
//     tokenSecurity,
//     getAllOrdersPerUserDataHandler,
//     // getAllOrdersPerUserPresenter,
//     // partial(invoiceController)
// )
router.get(
    "/:id",
    tokenSecurity,
    // isAdmin,
    isActive,
    getOrderByIdDataHandler,
    getOrderByIdPresenter,
    partial(invoiceController)
)

router.put(
    "/",
    tokenSecurity,
    editOrderQuantityFieldDataHandler,
    editOrderQuantityPresenter,
    partial(invoiceController)

)

router.put(
    "/update-status",
    tokenSecurity,
    editOrderStatusDataHandler,
    editOrderStatusPresenter,
    partial(invoiceController)
)

module.exports = router
