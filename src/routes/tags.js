const express = require("express");
const router = express.Router();
const { partial } = require("lodash");
const { tokenSecurity, tokenSecurityMightExist } = require("../securityLayer/tokenSecurity");
const {isAdmin} = require("../services/userService");
const {createTagsValidator, updateTagsValidator, deleteTagsValidator, createBulkTagsValidator} = require("../validationLayer/validator/tagsValidator");
const {createTagsDataHandler, getTagsDataHandler, getAllTagsDataHandler, updateTagsDataHandler,
    deleteTagsByIdDataHandler, deleteSingleTagDataHandler, createBulkTagsDataHandler,
    getTagsBySearchPaginatedDataHandler
} = require("../dataHandlerLayer/tagsDataHandler");
const {isActive} = require("../helperKit/helperKit");
const multer = require("multer");

const {createTagsPresenter, getTagsPresenter, getAllTagsPresenter, updateTagsPresenter, deleteTagsByIdPresenter, getTagsBySearchPaginatedPresenter,
    createBulkTagsPresenter
} = require("../presentationLayer/tagsPresenter");
const {tagsController} = require("../controllers/tags");
const storage = multer.memoryStorage();
const upload = multer({storage: storage});

router.post(
    "/",
    tokenSecurity,
    isAdmin,
    isActive,
    upload.single("file"),
    createTagsValidator,
    createTagsDataHandler,
    createTagsPresenter,
    partial(tagsController)
);

router.post(
    "/bulk-create-tags",
    tokenSecurity,
    isAdmin,
    isActive,
    // upload.single("file"),
    createBulkTagsValidator,
    createBulkTagsDataHandler,
    createBulkTagsPresenter,
    partial(tagsController)
);
router.get(
    "/get-by-search",
    tokenSecurityMightExist,
    getTagsBySearchPaginatedDataHandler,
    getTagsBySearchPaginatedPresenter,
    partial(tagsController)

)

router.get(
    "/:id",
    // tokenSecurity,
    getTagsDataHandler,
    getTagsPresenter,
    partial(tagsController)
);
router.get(
    "/",
    // tokenSecurity,
    getAllTagsDataHandler,
    getAllTagsPresenter,
    partial(tagsController)
);

router.patch(
    "/delete-tags",
    tokenSecurity,
    isAdmin,
    isActive,
    deleteTagsValidator,
    deleteTagsByIdDataHandler,
    deleteTagsByIdPresenter,
    partial(tagsController)
);

router.delete(
    "/:id",
    tokenSecurity,
    isAdmin,
    isActive,
    deleteSingleTagDataHandler,
    deleteTagsByIdPresenter,
    partial(tagsController)
)

router.patch(
    "/:id",
    tokenSecurity,
    isAdmin,
    isActive,
    upload.single("file"),
    updateTagsValidator,
    updateTagsDataHandler,
    updateTagsPresenter,
    partial(tagsController)
);

module.exports = router;
