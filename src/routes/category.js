const express = require("express");
const router = express.Router();
const { partial } = require("lodash");
const {
    categoryController
} = require("../controllers/category");
const { tokenSecurity, tokenSecurityMightExist } = require("../securityLayer/tokenSecurity");
const { isAdmin } = require("../services/userService");
const { createCategoryValidator, deleteCategoryByIdsValidator, updateCategoryByIdValidator, createBulkCategoryValidator } = require("../validationLayer/validator/categoryValidator");
const { createCategoryPresenter, getAllCategoryPresenter, getCategoryByIdPresenter, deleteCategoryByIdsPresenter,
    updateCategoryByIdPresenter, deleteCategoryByIdPresenter, getCategoryBySearchPaginatedPresenter,
    createBulkCategoryPresenter
} = require("../presentationLayer/categoryPresenter");
const multer = require("multer");
const { isActive, checkIfAllBannerImagesPass } = require("../helperKit/helperKit");
const { createCategoryDataHandler, createBulkCategoryDataHandler, getAllCategoryDataHandler,
    getCategoryBySearchPaginatedDataHandler, getCategoryByIdDataHandler, deleteCategoryByIdsDataHandler,
    updateCategoryByIdDataHandler, deleteCategoryByIdDataHandler
} = require("../dataHandlerLayer/categoryDataHandler");
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

router.post(
    "/",
    tokenSecurity,
    isAdmin,
    isActive,
    upload.fields([
        { name: 'file', maxCount: 1 },
        { name: 'bannerFiles', maxCount: 5 }
    ]),
    createCategoryValidator,
    createCategoryDataHandler,
    createCategoryPresenter,
    partial(categoryController)
);


router.post(
    "/bulk-create-category",
    tokenSecurity,
    isAdmin,
    isActive,
    // upload.single("file"),
    createBulkCategoryValidator,
    createBulkCategoryDataHandler,
    createBulkCategoryPresenter,
    partial(categoryController)
);

router.get(
    "/",
    tokenSecurityMightExist,
    getAllCategoryDataHandler,
    getAllCategoryPresenter,
    partial(
        categoryController
    )
)

router.get(
    "/get-by-search",
    tokenSecurityMightExist,
    getCategoryBySearchPaginatedDataHandler,
    getCategoryBySearchPaginatedPresenter,
    partial(categoryController)

)

router.get(
    "/:id",
    tokenSecurityMightExist,
    getCategoryByIdDataHandler,
    getCategoryByIdPresenter,
    partial(categoryController)
)

router.patch(
    "/delete-category",
    tokenSecurity,
    isAdmin,
    isActive,
    deleteCategoryByIdsValidator,
    deleteCategoryByIdsDataHandler,
    deleteCategoryByIdsPresenter,
    partial(categoryController)

)
router.patch(
    "/:id",
    tokenSecurity,
    isAdmin,
    isActive,
    upload.fields([
        { name: 'file', maxCount: 1 },
        { name: 'bannerFiles', maxCount: 5 }
    ]),
    updateCategoryByIdValidator,
    updateCategoryByIdDataHandler,
    updateCategoryByIdPresenter,
    partial(categoryController)
)

router.delete(
    "/:id",
    tokenSecurity,
    isAdmin,
    isActive,
    deleteCategoryByIdDataHandler,
    deleteCategoryByIdPresenter,
    partial(categoryController)
)


module.exports = router;
