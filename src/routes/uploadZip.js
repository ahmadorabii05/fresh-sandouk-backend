const express = require("express");
const router = express.Router();
const { partial } = require("lodash");
const {tokenSecurity} = require("../securityLayer/tokenSecurity");
const {isAdmin} = require("../services/userService");
const {isActive, uploadZipFileValidatorHelperFunction} = require("../helperKit/helperKit");
const {uploadZipFileController} = require("../controllers/uploadZipFile");
const multer = require("multer");
const {uploadZipFileDataHandler} = require("../dataHandlerLayer/uploadZipFileDataHandler");
const {uploadZipFilePresenter} = require("../presentationLayer/uploadZipFilePresenter");

const storage = multer.memoryStorage();
const upload = multer({storage: storage});
router.post(
    "/",
    tokenSecurity,
    isAdmin,
    isActive,
    upload.single('zipFile'),
    uploadZipFileValidatorHelperFunction,
    uploadZipFileDataHandler,
    uploadZipFilePresenter,
    partial(uploadZipFileController)

)

module.exports = router;