
const { DateTime } = require('luxon');


//city = {Dubai, Abu Dhabi} (String)
//timestamp = milliseconds  (Double)
function getTimeSlots(city, timestamp) {
    const currentDateTime = DateTime.fromMillis(timestamp);
    let cutOffDateTime;

    let slotsDubai = [
        { slot_no: 1, timing: '7 am to 11 am', cut_off: '18:00', delivery_day: '' },
        { slot_no: 2, timing: '11 am to 3 pm', cut_off: '07:00', delivery_day: '' },
        { slot_no: 3, timing: '4 pm to 9 pm', cut_off: '13:00', delivery_day: '' },
    ];

    let slotsAbuDhabi = [
        { slot_no: 1, timing: '12 pm to 5 pm', cut_off: '22:00', delivery_day: '' },
    ];

    const validDeliveryDaysDubai = [1, 2, 3, 4, 5, 6, 7];
    const validDeliveryDaysAbuDubai = [1, 2, 4];

    const timeFormat = "HH:mm";
    let deliveryDay;

    switch (city) {
        case 'Dubai':
            cutOffDateTime = currentDateTime.set({ hour: 18, minute: 0, second: 0, millisecond: 0 });
            if (currentDateTime < cutOffDateTime.set({ hour: 7 })) {
                deliveryDay = cutOffDateTime.toFormat('yyyy-MM-dd');
                slotsDubai = slotsDubai.slice(1);
                slotsDubai.forEach(slot => {
                    slot.delivery_day = deliveryDay;
                });
            } else if (currentDateTime < cutOffDateTime.set({ hour: 13 })) {
                deliveryDay = cutOffDateTime.toFormat('yyyy-MM-dd');
                slotsDubai = slotsDubai.slice(2);
                slotsDubai.forEach(slot => {
                    slot.delivery_day = deliveryDay;
                });
            } else if (currentDateTime < cutOffDateTime) {
                deliveryDay = cutOffDateTime.plus({ days: 1 }).toFormat('yyyy-MM-dd');
                slotsDubai = slotsDubai.slice(1);
                slotsDubai.forEach(slot => {
                    slot.delivery_day = deliveryDay;
                });
            } else {
                deliveryDay = cutOffDateTime.plus({ days: 1 }).toFormat('yyyy-MM-dd');
                slotsDubai = slotsDubai.slice(1);
                slotsDubai.forEach(slot => {
                    slot.delivery_day = deliveryDay;
                });
            }

            updateDeliveryDay(slotsDubai, validDeliveryDaysDubai);


            return slotsDubai;

        case 'Abu Dhabi':
            cutOffDateTime = currentDateTime.set({ hour: 22, minute: 0, second: 0, millisecond: 0 });
            if (currentDateTime < cutOffDateTime) {
                deliveryDay = cutOffDateTime.plus({ days: 1 }).toFormat('yyyy-MM-dd');
                slotsAbuDhabi.forEach(slot => {
                    slot.delivery_day = deliveryDay;
                });
            } else {
                deliveryDay = cutOffDateTime.plus({ days: 2 }).toFormat('yyyy-MM-dd');
                slotsAbuDhabi.forEach(slot => {
                    slot.delivery_day = deliveryDay;
                });
            }
            updateDeliveryDay(slotsAbuDhabi, validDeliveryDaysAbuDubai);

            return slotsAbuDhabi;

        default:
            return [];
    }
}


function isDeliveryDayValid(validDeliveryDays, weekday) {
    const isValid = validDeliveryDays.includes(weekday);
    return isValid;
}

function updateDeliveryDay(slots, validDeliveryDays) {
    slots.forEach(slot => {
        while (!isDeliveryDayValid(validDeliveryDays, DateTime.fromFormat(slot.delivery_day, 'yyyy-MM-dd').weekday)) {
            slot.delivery_day = DateTime.fromFormat(slot.delivery_day, 'yyyy-MM-dd').plus({ days: 1 }).toFormat('yyyy-MM-dd');
        }
    });
}




module.exports = {
    getTimeSlots,
};