const express = require("express");
const router = express.Router();
const {partial} = require("lodash");
const {tokenSecurity} = require("../securityLayer/tokenSecurity");
const {favoriteController} = require("../controllers/favorite");
const {createFavoriteValidator} = require("../validationLayer/validator/favoriteValidator");
const {createFavoriteDataHandler} = require("../dataHandlerLayer/favoriteDataHandler");
const {createFavoritePresenter} = require("../presentationLayer/favoritePresenter");


router.post(
    "/",
    tokenSecurity,
    createFavoriteValidator,
    createFavoriteDataHandler,
    createFavoritePresenter,
    partial(favoriteController)
)

module.exports = router;
