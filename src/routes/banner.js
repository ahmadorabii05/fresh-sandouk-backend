const express = require("express");
const router = express.Router();
const { partial } = require("lodash");
const { tokenSecurity } = require("../securityLayer/tokenSecurity");
const { isAdmin } = require("../services/userService");
const { bannerController } = require("../controllers/banner");
const { getAllBannersDataHandler, createBannerDataHandler, getBannerByIdDataHandler, updateBannerByIdDataHandler,
    deleteBannerDataHandler, deleteBannerByIdDataHandler
} = require("../dataHandlerLayer/bannerDataHandler");
const { getAllBannersPresenter, createBannerPresenter, getBannerByIdPresenter, updateBannerByIdPresenter,
    deleteBannerPresenter
} = require("../presentationLayer/bannerPresenter");
const { createBannerValidator, updateBannerByIdValidator, deleteBannerValidator } = require("../validationLayer/validator/bannerValidator");
const { isActive } = require("../helperKit/helperKit");
const multer = require("multer");
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });
router.get(
    "/",
    // tokenSecurity,
    // isAdmin,
    getAllBannersDataHandler,
    getAllBannersPresenter,
    partial(bannerController)
)

router.post(
    "/",
    tokenSecurity,
    isAdmin,
    isActive,
    upload.single("file"),
    createBannerValidator,
    createBannerDataHandler,
    createBannerPresenter,
    partial(bannerController)
)

router.get(
    "/:id",
    tokenSecurity,
    isAdmin,
    getBannerByIdDataHandler,
    getBannerByIdPresenter,
    partial(bannerController)
)

router.patch(
    "/update-banner/:id",
    tokenSecurity,
    isAdmin,
    upload.single("file"),
    updateBannerByIdValidator,
    updateBannerByIdDataHandler,
    updateBannerByIdPresenter,
    partial(bannerController),
)

router.patch(
    "/",
    tokenSecurity,
    isAdmin,
    deleteBannerValidator,
    deleteBannerDataHandler,
    deleteBannerPresenter,
    partial(bannerController),

)

router.delete(
    "/:id",
    tokenSecurity,
    isAdmin,
    isActive,
    deleteBannerByIdDataHandler,
    deleteBannerPresenter,
    partial(bannerController)
)
module.exports = router;
