const express = require("express");
const router = express.Router();
const { partial } = require("lodash");
const {
    updateUserByIdValidator,
    changePasswordValidator,
    createUserValidator,
    updateUserByIdFromMobileValidator, updateUserProfileFromMobileValidator,
} = require("../validationLayer/validator/userValidator");
const {
    userController
} = require("../controllers/user");
const {
    getUserByIdDataHandler,
    getAllUsersDataHandler,
    updateUserByIdDataHandler,
    changePasswordDataHandler,
    createUserDataHandler,
    deleteUserByIdDataHandler,
    uploadUserImageDataHandler, updateUserByIdFromAdminsDataHandler, getUserInfoByTokenDataHandler,
    updateUserProfileDataHandler, getUserByIdWithOrderDataHandler,
    getUsersBySearchPaginatedDataHandler
} = require("../dataHandlerLayer/userDataHandler");
const {
    getUserByIdPresenter,
    getAllUsersPresenter,
    updateUserByIdPresenter,
    changePasswordPresenter,
    createUserByIdPresenter,
    deleteUserByIdPresenter,
    uploadUserImagePresenter,
    getUserInfoByTokenPresenter,
    getUserByIdWithOrderPresenter,
    getUsersBySearchPaginatedPresenter
} = require("../presentationLayer/userPresenter");
const { tokenSecurity, tokenSecurityMightExist } = require("../securityLayer/tokenSecurity");
const multer = require("multer");
const { isAdmin } = require("../services/userService");
const { isActive } = require("../helperKit/helperKit");
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });
// const {uploadFile, uploadFileOptional} = require("../helperKit/imageUploadKit");


router.post(
    "/",
    tokenSecurity,
    isAdmin,
    isActive,
    createUserValidator,
    createUserDataHandler,
    createUserByIdPresenter,
    partial(userController)
);

router.post(
    "/upload-image",
    tokenSecurity,
    upload.single("file"),
    // uploadFile,
    uploadUserImageDataHandler,
    uploadUserImagePresenter,
    partial(userController)
);

router.get(
    "/",
    tokenSecurity,
    isAdmin,
    isActive,
    getAllUsersDataHandler,
    getAllUsersPresenter,
    partial(userController)
);


router.get(
    "/get-by-search",
    tokenSecurityMightExist,
    getUsersBySearchPaginatedDataHandler,
    getUsersBySearchPaginatedPresenter,
    partial(userController)

)

router.get(
    "/get-current-user-info",
    tokenSecurity,
    getUserInfoByTokenDataHandler,
    getUserInfoByTokenPresenter,
    partial(userController)
)
router.get(
    "/get-user-with-order/:id",
    tokenSecurity,
    // isAdmin,
    isActive,
    getUserByIdWithOrderDataHandler,
    getUserByIdWithOrderPresenter,
    partial(userController)
)
router.get(
    "/:id",
    tokenSecurity,
    isAdmin,
    getUserByIdDataHandler,
    getUserByIdPresenter,
    partial(userController)
);


router.patch(
    "/change-password",
    tokenSecurity,
    changePasswordValidator,
    changePasswordDataHandler,
    changePasswordPresenter,
    partial(userController)
);
//user can update himself
router.patch(
    "/mobile",
    tokenSecurity,
    updateUserByIdFromMobileValidator,
    updateUserByIdDataHandler,
    updateUserByIdPresenter,
    partial(userController)
);

router.patch(
    "/mobile-update-user-profile",
    tokenSecurity,
    updateUserProfileFromMobileValidator,
    updateUserProfileDataHandler,
    updateUserByIdPresenter,
    partial(userController)
)
//admins can update users
router.patch(
    "/admin-update-user/:id",
    tokenSecurity,
    isAdmin,
    isActive,
    // updateUserByIdFromMobileValidator,
    updateUserByIdFromAdminsDataHandler,
    updateUserByIdPresenter,
    partial(userController)
)

router.patch(
    "/update-user-info",
    tokenSecurity,
    upload.single("file"),
    updateUserByIdValidator,
    updateUserByIdDataHandler,
    updateUserByIdPresenter,
    partial(userController)
);


//delete user set inactive
router.delete(
    "/:id",
    tokenSecurity,
    isAdmin,
    isActive,
    deleteUserByIdDataHandler,
    deleteUserByIdPresenter,
    partial(userController)
);


module.exports = router;
