const express = require("express");
const router = express.Router();
const {partial} = require("lodash");
const {tokenSecurity} = require("../securityLayer/tokenSecurity");
const {cartController} = require("../controllers/cart");
const {addToCartValidator, decreaseToCartValidator, deleteItemFromCartValidator} = require("../validationLayer/validator/cartValidator");
const {addToCartDataHandler, decreaseQuantityDataHandler, deleteItemFromCartDataHandler, getCartInfoDataHandler} = require("../dataHandlerLayer/cartDataHandler");
const {addToCartPresenter, decreaseQuantityPresenter, deleteItemFromCartPresenter, getCartInfoDataPresenter} = require("../presentationLayer/cartPresenter");

router.post(
    "/",
    tokenSecurity,
    addToCartValidator,
    addToCartDataHandler,
    addToCartPresenter,
    partial(cartController)
    )

router.get(
    "/",
    tokenSecurity,
    getCartInfoDataHandler,
    getCartInfoDataPresenter,
    partial(cartController)
)

router.patch(
    "/",
    tokenSecurity,
    decreaseToCartValidator,
    decreaseQuantityDataHandler,
    decreaseQuantityPresenter,
    partial(cartController)
)
router.patch(
    "/delete-item-from-cart",
    tokenSecurity,
    deleteItemFromCartValidator,
    deleteItemFromCartDataHandler,
    deleteItemFromCartPresenter,
    partial(cartController)
)


module.exports = router;
