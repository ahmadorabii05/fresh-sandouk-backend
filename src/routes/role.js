const express = require("express");
const router = express.Router();
const { partial } = require("lodash");
const {
    roleController
} = require("../controllers/role");
const {
    createRoleDataHandler,
    getRoleDataHandler,
    updateRoleDataHandler,
    getAllRolesDataHandler,
    deleteRoleByIdDataHandler,
} = require("../dataHandlerLayer/roleDataHandler");
const {
    createRolePresenter,
    getRolePresenter,
    updateRolePresenter,
    getAllRolesPresenter,
    deleteRolePresenter,
} = require("../presentationLayer/rolePresenter");
const {
    createRoleValidator,
    updateRoleValidator,
} = require("../validationLayer/validator/roleValidator");
const { tokenSecurity } = require("../securityLayer/tokenSecurity");
const {isAdmin} = require("../services/userService");

router.post(
    "/",
    // tokenSecurity,
    // isAdmin,
    createRoleValidator,
    createRoleDataHandler,
    createRolePresenter,
    partial(roleController)
);
router.get(
    "/:id",
    tokenSecurity,
    isAdmin,
    getRoleDataHandler,
    getRolePresenter,
    partial(roleController)
);
router.get(
    "/",
    tokenSecurity,
    isAdmin,
    getAllRolesDataHandler,
    getAllRolesPresenter,
    partial(roleController)
);

router.patch(
    "/:id",
    tokenSecurity,
    isAdmin,
    updateRoleValidator,
    updateRoleDataHandler,
    updateRolePresenter,
    partial(roleController)
);

router.delete(
    "/:id",
    tokenSecurity,
    isAdmin,
    deleteRoleByIdDataHandler,
    deleteRolePresenter,
    partial(roleController)
);

module.exports = router;
