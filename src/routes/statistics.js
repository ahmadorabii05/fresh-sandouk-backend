const express = require("express");
const router = express.Router();
const { partial } = require("lodash");
const { tokenSecurity } = require("../securityLayer/tokenSecurity");
const { isAdmin } = require("../services/userService");
const { isActive } = require("../helperKit/helperKit");
const { getStatisticsValidator } = require("../validationLayer/validator/statisticsValidator");
const { getStatisticsDataHandler } = require("../dataHandlerLayer/statisticsDataHandler");
const { getStatisticsPresenter } = require("../presentationLayer/statisticsPresenter");
const { statisticsController } = require("../controllers/statistics");

router.post(
    "/",
    tokenSecurity,
    isAdmin,
    isActive,
    getStatisticsValidator,
    getStatisticsDataHandler,
    getStatisticsPresenter,
    partial(statisticsController)
)

module.exports = router