const express = require("express");
const router = express.Router();
const {partial} = require("lodash");
const {tokenSecurity, tokenSecurityMightExist} = require("../securityLayer/tokenSecurity");
const {isAdmin} = require("../services/userService");
const {priceController} = require("../controllers/price");


const {
    getAllPricesPresenter, createPricePresenter, getPriceByIdPresenter, updatePriceByIdPresenter,
    deletePricePresenter, deletePriceByIDPresenter, getPriceBySearchPaginatedPresenter, createBulkPricePresenter
} = require("../presentationLayer/pricePresenter");
const {
    createPriceValidator,
    updatePriceByIdValidator,
    deletePriceValidator,
    createBulkPriceValidator
} = require("../validationLayer/validator/priceValidator");
const {isActive} = require("../helperKit/helperKit");
const {
    getAllPricesDataHandler, createPriceDataHandler, createBulkPriceDataHandler,
    getPriceBySearchPaginatedDataHandler, getPriceByIdDataHandler, updatePriceByIdDataHandler, deletePriceDataHandler,
    deletePriceByIdDataHandler
} = require("../dataHandlerLayer/priceModelDataHandler");

router.get(
    "/",
    tokenSecurity,
    isAdmin,
    getAllPricesDataHandler,
    getAllPricesPresenter,
    partial(priceController)
)

router.post(
    "/",
    tokenSecurity,
    isAdmin,
    isActive,
    createPriceValidator,
    createPriceDataHandler,
    createPricePresenter,
    partial(priceController)
)


router.post(
    "/create-bulk-priceModel",
    tokenSecurity,
    isAdmin,
    isActive,
    createBulkPriceValidator,
    createBulkPriceDataHandler,
    createBulkPricePresenter,
    partial(priceController)
)

router.get(
    "/get-by-search",
    tokenSecurityMightExist,
    getPriceBySearchPaginatedDataHandler,
    getPriceBySearchPaginatedPresenter,
    partial(priceController)
)

router.get(
    "/:id",
    tokenSecurity,
    isAdmin,
    getPriceByIdDataHandler,
    getPriceByIdPresenter,
    partial(priceController)
)

router.patch(
    "/update-price/:id",
    tokenSecurity,
    isAdmin,
    updatePriceByIdValidator,
    updatePriceByIdDataHandler,
    updatePriceByIdPresenter,
    partial(priceController),
)

router.patch(
    "/",
    tokenSecurity,
    isAdmin,
    deletePriceValidator,
    deletePriceDataHandler,
    deletePricePresenter,
    partial(priceController),
)
router.delete(
    "/:id",
    tokenSecurity,
    isAdmin,
    isActive,
    deletePriceByIdDataHandler,
    deletePriceByIDPresenter,
    partial(priceController),
)

module.exports = router;
