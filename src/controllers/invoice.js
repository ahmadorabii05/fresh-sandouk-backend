/**
 * invoice
 * @returns {SuccessResponseModel<RoleResponseModel>}
 */
const invoiceController = (req, res) => {
    return res.status(req.statusCode).json(req.presenterModel);
};

module.exports = {
    invoiceController,
};