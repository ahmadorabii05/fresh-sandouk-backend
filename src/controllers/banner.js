/**
 * Authentication
 * @returns {SuccessResponseModel<T>}
 */
const bannerController = (req, res) => {
    return res.status(req.statusCode).json(req.presenterModel);
};

module.exports = {
    bannerController
};
