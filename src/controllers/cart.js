/**
 * Authentication
 * @returns {SuccessResponseModel<T>}
 */
const cartController = (req, res) => {
    return res.status(req.statusCode).json(req.presenterModel);
};

module.exports = {
    cartController
};
