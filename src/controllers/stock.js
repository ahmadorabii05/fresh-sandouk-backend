/**
 * Stock
 * @returns {SuccessResponseModel<RoleResponseModel>}
 */
const stockController = (req, res) => {
    return res.status(req.statusCode).json(req.presenterModel);
};

module.exports = {
    stockController,
};