/**
 * Role
 * @returns {SuccessResponseModel<SubCategoryResponseModel>}
 */
const subCategoryController = (req, res) => {
    return res.status(req.statusCode).json(req.presenterModel);
};

module.exports = {
    subCategoryController,
};
