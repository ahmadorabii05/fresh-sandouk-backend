


/**
 * ZipFile
 * @returns {SuccessResponseModel<TagsResponseModel>}
 */
const uploadZipFileController = (req, res) => {
    return res.status(req.statusCode).json(req.presenterModel);
};

module.exports = {
    uploadZipFileController,
};
