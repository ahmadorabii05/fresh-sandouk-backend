/**
 * Authentication
 * @returns {SuccessResponseModel<T>}
 */
const addressController = (req, res) => {
    return res.status(req.statusCode).json(req.presenterModel);
};

module.exports = {
    addressController
};
