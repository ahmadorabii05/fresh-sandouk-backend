/**
 * User
 * @returns {SuccessResponseModel<UserResponseModel>}
 */
const userController = (req, res) => {
    return res.status(req.statusCode).json(req.presenterModel);
};

module.exports = {
    userController
};