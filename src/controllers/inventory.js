/**
 * Inventory
 * @returns {SuccessResponseModel<RoleResponseModel>}
 */
const inventoryController = (req, res) => {
    return res.status(req.statusCode).json(req.presenterModel);
};

module.exports = {
    inventoryController,
};