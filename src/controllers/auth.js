/**
 * Authentication
 * @returns {SuccessResponseModel<T>}
 */
const authController = (req, res) => {
    return res.status(req.statusCode).json(req.presenterModel);
};

module.exports = {
    authController
};
