/**
 * Role
 * @returns {SuccessResponseModel<RoleResponseModel>}
 */
const roleController = (req, res) => {
    return res.status(req.statusCode).json(req.presenterModel);
};

module.exports = {
    roleController,
};
