


const categoryController = (req, res) => {
    return res.status(req.statusCode).json(req.presenterModel);
};

module.exports = {
    categoryController,
};