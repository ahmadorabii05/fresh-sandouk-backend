


const countryController = (req, res) => {
    return res.status(req.statusCode).json(req.presenterModel);
};

module.exports = {
    countryController,
};