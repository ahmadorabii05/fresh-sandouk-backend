/**
 * Role
 * @returns {SuccessResponseModel<TagsResponseModel>}
 */
const tagsController = (req, res) => {
    return res.status(req.statusCode).json(req.presenterModel);
};

module.exports = {
    tagsController,
};
