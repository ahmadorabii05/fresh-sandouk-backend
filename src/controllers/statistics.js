/**
 * statistics
 * @returns {SuccessResponseModel<statisticsResponseModel>}
 */
const statisticsController = (req, res) => {
    return res.status(req.statusCode).json(req.presenterModel);
};

module.exports = {
    statisticsController,
};